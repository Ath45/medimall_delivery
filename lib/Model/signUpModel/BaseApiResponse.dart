import 'dart:convert';

BaseApiResponse welcomeFromJson(String str) => BaseApiResponse.fromJson(json.decode(str));

String welcomeToJson(BaseApiResponse data) => json.encode(data.toJson());

class BaseApiResponse {
  BaseApiResponse({
    required this.error,
    required this.message,
    required this.data,
  });

  bool error;
  String message;
  Data data;

  factory BaseApiResponse.fromJson(Map<String, dynamic> json) => BaseApiResponse(
    error: json["error"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "error": error,
    "message": message,
    "data": data.toJson(),
  };
}

class Data {
  Data();

  factory Data.fromJson(Map<String, dynamic> json) => Data(
  );

  Map<String, dynamic> toJson() => {
  };
}
