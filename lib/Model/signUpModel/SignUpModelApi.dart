import 'dart:convert';

SignUpModelApi welcomeFromJson(String str) => SignUpModelApi.fromJson(json.decode(str));

String welcomeToJson(SignUpModelApi data) => json.encode(data.toJson());

class SignUpModelApi {
  SignUpModelApi({
    required this.error,
    required this.message,
    required this.data,
  });

  bool error;
  String message;
  Data data;

  factory SignUpModelApi.fromJson(Map<String, dynamic> json) => SignUpModelApi(
    error: json["error"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "error": error,
    "message": message,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    required this.userDetails,
    required this.token,
  });

  UserDetails userDetails;
  String token;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    userDetails: UserDetails.fromJson(json["userDetails"]),
    token: json["token"],
  );

  Map<String, dynamic> toJson() => {
    "userDetails": userDetails.toJson(),
    "token": token,
  };
}

class UserDetails {
  UserDetails({
    required this.store,
    required this.pincode,
    required this.isActive,
    required this.isApproved,
    required this.id,
    required this.fullName,
    required this.mobile,
    required this.email,
    required this.password,
    required this.drivingLicenseNumber,
    required this.address,
    required this.city,
    required this.licence,
    required this.aadhar,
    required this.rcBook,
    required this.deliveryBoyId,
    required this.createdAt,
    required this.updatedAt,
    required this.v,
  });

  List<dynamic> store;
  List<dynamic> pincode;
  bool isActive;
  String isApproved;
  String id;
  String fullName;
  String mobile;
  String email;
  String password;
  String drivingLicenseNumber;
  String address;
  String city;
  String licence;
  String aadhar;
  String rcBook;
  String deliveryBoyId;
  DateTime createdAt;
  DateTime updatedAt;
  int v;

  factory UserDetails.fromJson(Map<String, dynamic> json) => UserDetails(
    store: List<dynamic>.from(json["store"].map((x) => x)),
    pincode: List<dynamic>.from(json["pincode"].map((x) => x)),
    isActive: json["isActive"],
    isApproved: json["isApproved"],
    id: json["_id"],
    fullName: json["fullName"],
    mobile: json["mobile"],
    email: json["email"],
    password: json["password"],
    drivingLicenseNumber: json["drivingLicenseNumber"],
    address: json["address"],
    city: json["city"],
    licence: json["licence"],
    aadhar: json["aadhar"],
    rcBook: json["rcBook"],
    deliveryBoyId: json["deliveryBoyId"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
    v: json["__v"],
  );

  Map<String, dynamic> toJson() => {
    "store": List<dynamic>.from(store.map((x) => x)),
    "pincode": List<dynamic>.from(pincode.map((x) => x)),
    "isActive": isActive,
    "isApproved": isApproved,
    "_id": id,
    "fullName": fullName,
    "mobile": mobile,
    "email": email,
    "password": password,
    "drivingLicenseNumber": drivingLicenseNumber,
    "address": address,
    "city": city,
    "licence": licence,
    "aadhar": aadhar,
    "rcBook": rcBook,
    "deliveryBoyId": deliveryBoyId,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
    "__v": v,
  };
}
