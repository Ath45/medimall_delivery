import 'dart:io';

class SignUpRequestModel
{
  String fullName;
  String mobileNumber;
  String emailId;
  String password;
  String passwordRepeat;
  String licenceNumber;
  String address;
  String aadhar;
  String city;
  File licenseImage;
  File adhaarImage;
  File rcBookImage;
  SignUpRequestModel(
  {
   required this.fullName,
   required this.mobileNumber,
   required this.emailId,
   required this.password,
   required this.passwordRepeat,
   required this.address,
   required this.aadhar,
   required this.city,
   required this.licenceNumber,
   required this.licenseImage,
   required this.adhaarImage,
   required this.rcBookImage
}
      );
}

class UploadListModel
{
  String text;
  bool flag;
  String controller;
  UploadListModel({ required this.controller,required this.text,required this.flag});
}