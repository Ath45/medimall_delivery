import 'dart:convert';

ProfileModal welcomeFromJson(String str) => ProfileModal.fromJson(json.decode(str));

String welcomeToJson(ProfileModal data) => json.encode(data.toJson());

class ProfileModal {
  ProfileModal({
    required this.status,
    required this.data,
  });

  bool status;
  Data data;

  factory ProfileModal.fromJson(Map<String, dynamic> json) => ProfileModal(
    status: json["status"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    required this.fullName,
    required this.deliveryBoyId,
    required this.credit,
    required this.profilePic,
    required this.status,
    required this.mobile,
    required this.email,
    required this.id,
  });

  String fullName;
  String deliveryBoyId;
  String credit;
  String profilePic;
  String status;
  String mobile;
  String email;
  String id;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    fullName: json["fullName"],
    deliveryBoyId: json["deliveryBoyId"],
    credit: json["credit"].toString(),
    profilePic: json["profilePic"],
    status: json["status"],
    mobile: json["mobile"],
    email: json["email"],
    id: json["_id"],
  );

  Map<String, dynamic> toJson() => {
    "fullName": fullName,
    "deliveryBoyId": deliveryBoyId,
    "credit": credit,
    "profilePic": profilePic,
    "status": status,
    "mobile": mobile,
    "email": email,
    "_id": id,
  };
}
