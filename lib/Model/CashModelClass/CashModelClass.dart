//CashModelClass
import 'dart:convert';

import 'package:get/get_rx/get_rx.dart';

CashModelClass welcomeFromJson(String str) => CashModelClass.fromJson(json.decode(str));

String welcomeToJson(CashModelClass data) => json.encode(data.toJson());

class CashModelClass {
  CashModelClass({
    required this.error,
    required this.message,
    required this.data,
  });

  bool error;
  String message;
  Data data;

  factory CashModelClass.fromJson(Map<String, dynamic> json) => CashModelClass(
    error: json["error"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "error": error,
    "message": message,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    required this.bankDetails,
    required this.totalDueAmt,
  });

  RxList<BankDetail> bankDetails;
  String totalDueAmt;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    bankDetails: RxList<BankDetail>.from(json["BankDetails"].map((x) => BankDetail.fromJson(x))),
    totalDueAmt: json["totalDueAmount"].toString(),
  );

  Map<String, dynamic> toJson() => {
    "BankDetails": List<dynamic>.from(bankDetails.map((x) => x.toJson())),
    "totalDueAmount": totalDueAmt,
  };
}

class BankDetail {
  BankDetail({
  required this.id,
    required this.bankName,
    required this.accountHolderName,
    required this.accountNumber,
    required this.ifscCode,
    required this.upid,
    required this.v,
  });

  String id;
  String bankName;
  String accountHolderName;
  String accountNumber;
  String ifscCode;
  String upid;
  String v;

  factory BankDetail.fromJson(Map<String, dynamic> json) => BankDetail(
    id: json["_id"],
    bankName: json["bankName"],
    accountHolderName: json["accountHolderName"],
    accountNumber: json["accountNumber"],
    ifscCode: json["ifscCode"],
    upid: json["upid"],
    v: json["__v"].toString(),
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "bankName": bankName,
    "accountHolderName": accountHolderName,
    "accountNumber": accountNumber,
    "ifscCode": ifscCode,
    "upid": upid,
    "__v": v,
  };
}
