import 'dart:convert';

UserInfoModelClass welcomeFromJson(String str) => UserInfoModelClass.fromJson(json.decode(str));

String welcomeToJson(UserInfoModelClass data) => json.encode(data.toJson());

class UserInfoModelClass {
  UserInfoModelClass({
    required this.status,
    required this.data,
  });

  bool status;
  List<Datum> data;

  factory UserInfoModelClass.fromJson(Map<String, dynamic> json) => UserInfoModelClass(
    status: json["status"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    required this.rcBook,
    required this.userInfo,
    required this.license,
    required this.adhaar,
  });

  RcBook rcBook;
  UserInfo userInfo;
  License license;
  Adhaar adhaar;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    rcBook: RcBook.fromJson(json["rcBook"]),
    userInfo: UserInfo.fromJson(json["userInfo"]),
    license: License.fromJson(json["license"]),
    adhaar: Adhaar.fromJson(json["adhaar"]),
  );

  Map<String, dynamic> toJson() => {
    "rcBook": rcBook.toJson(),
    "userInfo": userInfo.toJson(),
    "license": license.toJson(),
    "adhaar": adhaar.toJson(),
  };
}

class Adhaar {
  Adhaar({
    required this.aadharCardNumber,
    required this.aadhar,
  });

  String aadharCardNumber;
  String aadhar;

  factory Adhaar.fromJson(Map<String, dynamic> json) => Adhaar(
    aadharCardNumber: json["aadharCardNumber"],
    aadhar: json["aadhar"].toString(),
  );

  Map<String, dynamic> toJson() => {
    "aadharCardNumber": aadharCardNumber,
    "aadhar": aadhar,
  };
}

class License {
  License({
    required this.drivingLicenseNumber,
    required this.licence,
  });

  String drivingLicenseNumber;
  String licence;

  factory License.fromJson(Map<String, dynamic> json) => License(
    drivingLicenseNumber: json["drivingLicenseNumber"],
    licence: json["licence"].toString(),
  );

  Map<String, dynamic> toJson() => {
    "drivingLicenseNumber": drivingLicenseNumber,
    "licence": licence,
  };
}

class RcBook {
  RcBook({
    required this.rcBook,
  });

  String rcBook;

  factory RcBook.fromJson(Map<String, dynamic> json) => RcBook(
    rcBook: json["rcBook"],
  );

  Map<String, dynamic> toJson() => {
    "rcBook": rcBook,
  };
}

class UserInfo {
  UserInfo({
    required this.fullName,
    required this.email,
    required this.mobile,
    required this.address,
    required this.city,
  });

  String fullName;
  String email;
  String mobile;
  String address;
  String city;

  factory UserInfo.fromJson(Map<String, dynamic> json) => UserInfo(
    fullName: json["fullName"],
    email: json["email"],
    mobile: json["mobile"],
    address: json["address"],
    city: json["city"],
  );

  Map<String, dynamic> toJson() => {
    "fullName": fullName,
    "email": email,
    "mobile": mobile,
    "address": address,
    "city": city,
  };
}
