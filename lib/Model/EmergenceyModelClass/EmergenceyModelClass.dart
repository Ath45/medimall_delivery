// EmergenceyModelClass
import 'dart:convert';

EmergenceyModelClass welcomeFromJson(String str) => EmergenceyModelClass.fromJson(json.decode(str));

String welcomeToJson(EmergenceyModelClass data) => json.encode(data.toJson());

class EmergenceyModelClass {
  EmergenceyModelClass({
    required this.error,
    required this.message,
    required this.data,
  });

  bool error;
  String message;
  Data data;

  factory EmergenceyModelClass.fromJson(Map<String, dynamic> json) => EmergenceyModelClass(
    error: json["error"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "error": error,
    "message": message,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    required this.emergencyContact,
  });

  List<EmergencyContact> emergencyContact;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    emergencyContact: List<EmergencyContact>.from(json["EmergencyContact"].map((x) => EmergencyContact.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "EmergencyContact": List<dynamic>.from(emergencyContact.map((x) => x.toJson())),
  };
}

class EmergencyContact {
  EmergencyContact({
    required this.id,
    required this.name,
    required this.contactNumber,
    required this.v,
  });

  String id;
  String name;
  String contactNumber;
  int v;

  factory EmergencyContact.fromJson(Map<String, dynamic> json) => EmergencyContact(
    id: json["_id"],
    name: json["Name"],
    contactNumber: json["contactNumber"],
    v: json["__v"],
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "Name": name,
    "contactNumber": contactNumber,
    "__v": v,
  };
}
