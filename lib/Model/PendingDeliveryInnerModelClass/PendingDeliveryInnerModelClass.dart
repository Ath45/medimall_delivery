import 'dart:convert';

PendingDeliveryInnerModelClass welcomeFromJson(String str) => PendingDeliveryInnerModelClass.fromJson(json.decode(str));

String welcomeToJson(PendingDeliveryInnerModelClass data) => json.encode(data.toJson());

class PendingDeliveryInnerModelClass {
  PendingDeliveryInnerModelClass({
    required this.error,
    required this.message,
    required this.data,
  });

  bool error;
  String message;
  Data data;

  factory PendingDeliveryInnerModelClass.fromJson(Map<String, dynamic> json) => PendingDeliveryInnerModelClass(
    error: json["error"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "error": error,
    "message": message,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    required this.deliveryPendingOrder,
  });

  List<DeliveryPendingOrder> deliveryPendingOrder;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    deliveryPendingOrder: List<DeliveryPendingOrder>.from(json["deliveryPendingOrder"].map((x) => DeliveryPendingOrder.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "deliveryPendingOrder": List<dynamic>.from(deliveryPendingOrder.map((x) => x.toJson())),
  };
}

class DeliveryPendingOrder {
  DeliveryPendingOrder({
    required this.products,
    required this.status,
    required this.paymentType,
    required this.address,
    required this.totalAmountToBePaid,
    required this.storeAddress,
  });

  List<Product> products;
  String status;
  String paymentType;
  Address address;
  String totalAmountToBePaid;
  StoreAddress storeAddress;

  factory DeliveryPendingOrder.fromJson(Map<String, dynamic> json) => DeliveryPendingOrder(
    products: List<Product>.from(json["products"].map((x) => Product.fromJson(x))),
    status: json["status"],
    paymentType: json["paymentType"],
    address: Address.fromJson(json["address"]),
    totalAmountToBePaid: json["totalAmountToBePaid"].toString(),
    storeAddress: StoreAddress.fromJson(json["storeAddress"]),
  );

  Map<String, dynamic> toJson() => {
    "products": List<dynamic>.from(products.map((x) => x.toJson())),
    "status": status,
    "paymentType": paymentType,
    "address": address.toJson(),
    "totalAmountToBePaid": totalAmountToBePaid,
    "storeAddress": storeAddress.toJson(),
  };
}

class Address {
  Address({
    required this.id,
    required this.name,
    required this.mobile,
    required this.pincode,
    required this.house,
    required this.landmark,
    required this.street,
    required this.type,
    required this.state,
    required this.wholeAddress,
  });

  String id;
  String name;
  String mobile;
  String pincode;
  String house;
  String landmark;
  String street;
  String type;
  String state;
  String wholeAddress;

  factory Address.fromJson(Map<String, dynamic> json) => Address(
    id: json["_id"],
    name: json["name"],
    mobile: json["mobile"],
    pincode: json["pincode"],
    house: json["house"],
    landmark: json["landmark"],
    street: json["street"],
    type: json["type"],
    state: json["state"],
    wholeAddress: json["wholeAddress"],
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "name": name,
    "mobile": mobile,
    "pincode": pincode,
    "house": house,
    "landmark": landmark,
    "street": street,
    "type": type,
    "state": state,
    "wholeAddress": wholeAddress,
  };
}

class Product {
  Product({
    required this.cartId,
    required this.variantId,
    required this.productId,
    required this.quantity,
    required this.productName,
    required this.brandName,
    required this.type,
    required this.description,
    required this.isPrescriptionRequired,
    required this.image,
    required this.price,
    required this.offerType,
    required this.specialPrice,
    required this.uomValue,
    required this.discountAmount,
    required this.discountInPercentage,
    required this.outOfStock,
    required this.isThisProductAddedToWhishList,
    required this.returnStatus,
  });

  String cartId;
  String variantId;
  String productId;
  String quantity;
  String productName;
  String brandName;
  String type;
  String description;
  bool isPrescriptionRequired;
  String image;
  String price;
  String offerType;
  String specialPrice;
  String uomValue;
  String discountAmount;
  String discountInPercentage;
  bool outOfStock;
  bool isThisProductAddedToWhishList;
  String returnStatus;

  factory Product.fromJson(Map<String, dynamic> json) => Product(
    cartId: json["cartId"].toString(),
    variantId: json["variantId"].toString(),
    productId: json["product_id"],
    quantity: json["quantity"].toString(),
    productName: json["productName"].toString(),
    brandName: json["brandName"].toString(),
    type: json["type"].toString(),
    description: json["description"].toString(),
    isPrescriptionRequired: json["IsPrescriptionRequired"],
    image: json["image"],
    price: json["price"].toString(),
    offerType: json["offerType"].toString(),
    specialPrice: json["specialPrice"].toString(),
    uomValue: json["uomValue"].toString(),
    discountAmount: json["discountAmount"].toString(),
    discountInPercentage: json["discountInPercentage"].toString(),
    outOfStock: json["outOfStock"],
    isThisProductAddedToWhishList: json["isThisProductAddedToWhishList"],
    returnStatus: json["returnStatus"].toString(),
  );

  Map<String, dynamic> toJson() => {
    "cartId": cartId,
    "variantId": variantId,
    "product_id": productId,
    "quantity": quantity,
    "productName": productName,
    "brandName": brandName,
    "type": type,
    "description": description,
    "IsPrescriptionRequired": isPrescriptionRequired,
    "image": image,
    "price": price,
    "offerType": offerType,
    "specialPrice": specialPrice,
    "uomValue": uomValue,
    "discountAmount": discountAmount,
    "discountInPercentage": discountInPercentage,
    "outOfStock": outOfStock,
    "isThisProductAddedToWhishList": isThisProductAddedToWhishList,
    "returnStatus": returnStatus,
  };
}

class StoreAddress {
  StoreAddress({
    required this.name,
    required this.email,
    required this.phone,
    required this.address,
    required this.pin,
    required this.state,
    required this.country,
    required this.gst,
  });

  String name;
  String email;
  String phone;
  String address;
  String pin;
  String state;
  String country;
  String gst;

  factory StoreAddress.fromJson(Map<String, dynamic> json) => StoreAddress(
    name: json["name"].toString(),
    email: json["email"].toString(),
    phone: json["phone"].toString(),
    address: json["address"].toString(),
    pin: json["pin"].toString(),
    state: json["state"].toString(),
    country: json["country"].toString(),
    gst: json["gst"].toString(),
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "email": email,
    "phone": phone,
    "address": address,
    "pin": pin,
    "state": state,
    "country": country,
    "gst": gst,
  };
}