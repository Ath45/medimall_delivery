import 'dart:convert';

BaseApiResponse welcomeFromJson(String str) => BaseApiResponse.fromJson(json.decode(str));

String welcomeToJson(BaseApiResponse data) => json.encode(data.toJson());

class BaseApiResponse {
  BaseApiResponse({
    required this.error,
    required this.message,
  });

  bool error;
  String message;

  factory BaseApiResponse.fromJson(Map<String, dynamic> json) => BaseApiResponse(
    error: json["error"],
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "error": error,
    "message": message,
  };
}
