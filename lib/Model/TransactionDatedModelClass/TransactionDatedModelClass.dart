//TransactionDatedModelClass
import 'dart:convert';

TransactionDatedModelClass welcomeFromJson(String str) => TransactionDatedModelClass.fromJson(json.decode(str));

String welcomeToJson(TransactionDatedModelClass data) => json.encode(data.toJson());

class TransactionDatedModelClass {
  TransactionDatedModelClass({
    required this.error,
    required this.message,
    required this.data,
  });

  bool error;
  String message;
  Data data;

  factory TransactionDatedModelClass.fromJson(Map<String, dynamic> json) => TransactionDatedModelClass(
    error: json["error"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "error": error,
    "message": message,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    required this.transactions,
  });

  List<Transaction> transactions;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    transactions: List<Transaction>.from(json["DatedTransactions"].map((x) => Transaction.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "DatedTransactions": List<dynamic>.from(transactions.map((x) => x.toJson())),
  };
}

class Transaction {
  Transaction({
    required this.status,
    required this.paidToAdmin,
    required this.orderObjectId,
    required this.orderId,
    required this.paymentType,
    required this.deliveredDate,
    required this.totalAmountToBePaid,
  });

  String status;
  String paidToAdmin;
  String orderObjectId;
  String orderId;
  String paymentType;
  String deliveredDate;
  String totalAmountToBePaid;

  factory Transaction.fromJson(Map<String, dynamic> json) => Transaction(
    status: json["status"],
    paidToAdmin: json["paidToAdmin"],
    orderObjectId: json["orderObjectId"],
    orderId: json["orderId"],
    paymentType: json["paymentType"],
    deliveredDate: json["deliveredDate"],
    totalAmountToBePaid: json["totalAmountToBePaid"].toString(),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "paidToAdmin": paidToAdmin,
    "orderObjectId": orderObjectId,
    "orderId": orderId,
    "paymentType": paymentType,
    "deliveredDate": deliveredDate,
    "totalAmountToBePaid": totalAmountToBePaid,
  };
}
