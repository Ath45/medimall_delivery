import 'dart:convert';

import 'package:get/get.dart';

PendingOrderLimit welcomeFromJson(String str) => PendingOrderLimit.fromJson(json.decode(str));

String welcomeToJson(PendingOrderLimit data) => json.encode(data.toJson());

class PendingOrderLimit {
  PendingOrderLimit({
    required this.error,
    required this.message,
    required this.data,
  });

  bool error;
  String message;
  Data data;

  factory PendingOrderLimit.fromJson(Map<String, dynamic> json) => PendingOrderLimit(
    error: json["error"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "error": error,
    "message": message,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    required this.deliveryPendingOrders,
  });

  RxList<DeliveryPendingOrder> deliveryPendingOrders;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    deliveryPendingOrders: RxList<DeliveryPendingOrder>.from(json["deliveryPendingOrders"].map((x) => DeliveryPendingOrder.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "deliveryPendingOrders": RxList<dynamic>.from(deliveryPendingOrders.map((x) => x.toJson())),
  };
}

class DeliveryPendingOrder {
  DeliveryPendingOrder({
    required this.orderId,
    required this.orderobjectId,
    required this.paymentType,
    required this.userName,
    required this.totalAmountToBePaid,
    required this.status,
  });

  String orderId;
  String orderobjectId;
  String paymentType;
  String userName;
  String totalAmountToBePaid;
  String status;
  factory DeliveryPendingOrder.fromJson(Map<String, dynamic> json) => DeliveryPendingOrder(
    orderId: json["orderId"].toString(),
    paymentType: json["paymentType"].toString(),
    orderobjectId: json["orderObjectId"].toString(),
    userName: json["userName"].toString(),
    totalAmountToBePaid: json["totalAmountToBePaid"].toString(),
    status: json["status"].toString(),
  );

  Map<String, dynamic> toJson() => {
    "orderId": orderId,
    "orderObjectId": orderobjectId,
    "paymentType": paymentType,
    "userName": userName,
    "totalAmountToBePaid": totalAmountToBePaid,
    "status": status,
  };
}
