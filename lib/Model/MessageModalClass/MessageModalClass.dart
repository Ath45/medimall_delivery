//MessageModalClass
import 'dart:convert';

MessageModalClass welcomeFromJson(String str) => MessageModalClass.fromJson(json.decode(str));

String welcomeToJson(MessageModalClass data) => json.encode(data.toJson());

class MessageModalClass {
  MessageModalClass({
    required this.error,
    required this.message,
    required this.data,
  });

  bool error;
  String message;
  Data data;

  factory MessageModalClass.fromJson(Map<String, dynamic> json) => MessageModalClass(
    error: json["error"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "error": error,
    "message": message,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    required this.deliveryBoyMessage,
    required this.messageCount,
  });

  List<DeliveryBoyMessage> deliveryBoyMessage;
  String messageCount;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    deliveryBoyMessage: List<DeliveryBoyMessage>.from(json["deliveryBoyMessage"].map((x) => DeliveryBoyMessage.fromJson(x))),
    messageCount: json["messageCount"].toString(),
  );

  Map<String, dynamic> toJson() => {
    "deliveryBoyMessage": List<dynamic>.from(deliveryBoyMessage.map((x) => x.toJson())),
    "messageCount": messageCount,
  };
}

class DeliveryBoyMessage {
  DeliveryBoyMessage({
    required this.id,
    required this.isRead,
    required this.issueRelated,
    required this.issue,
    required this.createdAt,
    required this.reply,
  });

  String id;
  bool isRead;
  String issueRelated;
  String issue;
  String createdAt;
  String reply;

  factory DeliveryBoyMessage.fromJson(Map<String, dynamic> json) => DeliveryBoyMessage(
    id: json["_id"],
    isRead: json["isRead"],
    issueRelated: json["IssueRelated"].toString(),
    issue: json["Issue"].toString(),
    createdAt: json["createdAt"].toString(),
    reply: json["reply"].toString(),
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "isRead": isRead,
    "IssueRelated": issueRelated,
    "Issue": issue,
    "createdAt": createdAt,
    "reply": reply,
  };
}
