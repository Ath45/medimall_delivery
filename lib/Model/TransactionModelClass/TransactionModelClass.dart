//TransactionModelClass
import 'dart:convert';

TransactionModelClass welcomeFromJson(String str) => TransactionModelClass.fromJson(json.decode(str));

String welcomeToJson(TransactionModelClass data) => json.encode(data.toJson());

class TransactionModelClass {
  TransactionModelClass({
    required this.error,
    required this.message,
    required this.data,
  });

  bool error;
  String message;
  Data data;

  factory TransactionModelClass.fromJson(Map<String, dynamic> json) => TransactionModelClass(
    error: json["error"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "error": error,
    "message": message,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    required this.transactions,
  });

  List<Transaction> transactions;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    transactions: List<Transaction>.from(json["transactions"].map((x) => Transaction.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "transactions": List<dynamic>.from(transactions.map((x) => x.toJson())),
  };
}

class Transaction {
  Transaction({
    required this.status,
    required this.paidToAdmin,
    required this.orderObjectId,
    required this.orderId,
    required this.paymentType,
    required this.deliveredDate,
    required this.totalAmountToBePaid,
  });

  String status;
  String paidToAdmin;
  String orderObjectId;
  String orderId;
  String paymentType;
  String deliveredDate;
  String totalAmountToBePaid;

  factory Transaction.fromJson(Map<String, dynamic> json) => Transaction(
    status: json["status"],
    paidToAdmin: json["paidToAdmin"],
    orderObjectId: json["orderObjectId"],
    orderId: json["orderId"],
    paymentType: json["paymentType"],
    deliveredDate: json["deliveredDate"].toString(),
    totalAmountToBePaid: json["totalAmountToBePaid"].toString(),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "paidToAdmin": paidToAdmin,
    "orderObjectId": orderObjectId,
    "orderId": orderId,
    "paymentType": paymentType,
    "deliveredDate": deliveredDate,
    "totalAmountToBePaid": totalAmountToBePaid,
  };
}
