//PickUpDetailsModel
import 'dart:convert';

PickUpDetailsModel welcomeFromJson(String str) =>
    PickUpDetailsModel.fromJson(json.decode(str));

String welcomeToJson(PickUpDetailsModel data) => json.encode(data.toJson());

class PickUpDetailsModel {
  PickUpDetailsModel({
    required this.error,
    required this.message,
    required this.data,
  });

  bool error;
  String message;
  Data data;

  factory PickUpDetailsModel.fromJson(Map<String, dynamic> json) =>
      PickUpDetailsModel(
        error: json["error"],
        message: json["message"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "error": error,
        "message": message,
        "data": data.toJson(),
      };
}

class Data {
  Data({
    required this.singleReturnOrders,
  });

  List<SingleReturnOrder> singleReturnOrders;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        singleReturnOrders: List<SingleReturnOrder>.from(
            json["singleReturnOrders"]
                .map((x) => SingleReturnOrder.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "singleReturnOrders":
            List<dynamic>.from(singleReturnOrders.map((x) => x.toJson())),
      };
}

class SingleReturnOrder {
  SingleReturnOrder({
    required this.products,
    required this.returnId,
    required this.status,
    required this.orderObjectId,
    required this.orderId,
    required this.address,
    required this.storeAddress,
    required this.Comments,
    required this.returnOrderReason,
    required this.product_img,
    required this.signature,
  });

  List<Product> products;
  String status;
  String orderObjectId;
  String orderId;
  String Comments;
  String product_img;
  String returnOrderReason;
  String signature;
  Address address;
  String returnId;
  StoreAddress storeAddress;

  factory SingleReturnOrder.fromJson(Map<String, dynamic> json) =>
      SingleReturnOrder(
        products: List<Product>.from(
            json["products"].map((x) => Product.fromJson(x))),
        status: json["status"].toString(),
        orderObjectId: json["orderObjectId"].toString(),
        orderId: json["orderId"].toString(),
        Comments: json["Comments"].toString(),
        returnId: json["returnId"].toString(),
        returnOrderReason: json["returnOrderReason"].toString(),
        product_img: json["product_img"].toString(),
        signature: json["signature"].toString(),
        address: Address.fromJson(json["address"]),
        storeAddress: StoreAddress.fromJson(json["storeAddress"]),
      );

  Map<String, dynamic> toJson() => {
        "products": List<dynamic>.from(products.map((x) => x.toJson())),
        "status": status,
        "orderObjectId": orderObjectId,
        "orderId": orderId,
        "address": address.toJson(),
        "storeAddress": storeAddress.toJson(),
        "returnId": returnId,
      };
}

class StoreAddress {
  StoreAddress({
    required this.name,
    required this.email,
    required this.phone,
    required this.address,
    required this.pin,
    required this.state,
    required this.country,
    required this.gst,
  });

  String name;
  String email;
  String phone;
  String address;
  String pin;
  String state;
  String country;
  String gst;

  factory StoreAddress.fromJson(Map<String, dynamic> json) => StoreAddress(
        name: json["name"].toString(),
        email: json["email"].toString(),
        phone: json["phone"].toString(),
        address: json["address"].toString(),
        pin: json["pin"].toString(),
        state: json["state"].toString(),
        country: json["country"].toString(),
        gst: json["gst"].toString(),
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "email": email,
        "phone": phone,
        "address": address,
        "pin": pin,
        "state": state,
        "country": country,
        "gst": gst,
      };
}

class Address {
  Address({
    required this.isDisabled,
    required this.id,
    required this.name,
    required this.mobile,
    required this.pincode,
    required this.house,
    required this.landmark,
    required this.street,
    required this.type,
    required this.state,
    required this.userId,
    required this.createdAt,
    required this.updatedAt,
    required this.v,
  });

  String isDisabled;
  String id;
  String name;
  String mobile;
  String pincode;
  String house;
  String landmark;
  String street;
  String type;
  String state;
  String userId;
  String createdAt;
  String updatedAt;
  String v;

  factory Address.fromJson(Map<String, dynamic> json) => Address(
        isDisabled: json["isDisabled"].toString(),
        id: json["_id"].toString(),
        name: json["name"].toString(),
        mobile: json["mobile"].toString(),
        pincode: json["pincode"].toString(),
        house: json["house"].toString(),
        landmark: json["landmark"].toString(),
        street: json["street"].toString(),
        type: json["type"].toString(),
        state: json["state"].toString(),
        userId: json["userId"].toString(),
        createdAt: json["createdAt"].toString(),
        updatedAt: json["updatedAt"].toString(),
        v: json["__v"].toString(),
      );

  Map<String, dynamic> toJson() => {
        "isDisabled": isDisabled,
        "_id": id,
        "name": name,
        "mobile": mobile,
        "pincode": pincode,
        "house": house,
        "landmark": landmark,
        "street": street,
        "type": type,
        "state": state,
        "userId": userId,
        "createdAt": createdAt,
        "updatedAt": updatedAt,
        "__v": v,
      };
}

class Product {
  Product({
    required this.cartId,
    required this.variantId,
    required this.productId,
    required this.quantity,
    required this.productName,
    required this.brandName,
    required this.type,
    required this.description,
    required this.isPrescriptionRequired,
    required this.image,
    required this.price,
    required this.offerType,
    required this.specialPrice,
    required this.uomValue,
    required this.discountAmount,
    required this.discountInPercentage,
    required this.outOfStock,
    required this.isThisProductAddedToWhishList,
    required this.returnStatus,
  });

  String cartId;
  String variantId;
  String productId;
  String quantity;
  String productName;
  String brandName;
  String type;
  String description;
  bool isPrescriptionRequired;
  String image;
  String price;
  String offerType;
  String specialPrice;
  String uomValue;
  String discountAmount;
  String discountInPercentage;
  bool outOfStock;
  bool isThisProductAddedToWhishList;
  String returnStatus;

  factory Product.fromJson(Map<String, dynamic> json) => Product(
        cartId: json["cartId"],
        variantId: json["variantId"],
        productId: json["product_id"],
        quantity: json["quantity"].toString(),
        productName: json["productName"].toString(),
        brandName: json["brandName"].toString(),
        type: json["type"].toString(),
        description: json["description"].toString(),
        isPrescriptionRequired: json["IsPrescriptionRequired"],
        image: json["image"].toString(),
        price: json["price"].toString(),
        offerType: json["offerType"].toString(),
        specialPrice: json["specialPrice"].toString(),
        uomValue: json["uomValue"],
        discountAmount: json["discountAmount"].toString(),
        discountInPercentage: json["discountInPercentage"].toString(),
        outOfStock: json["outOfStock"],
        isThisProductAddedToWhishList: json["isThisProductAddedToWhishList"],
        returnStatus: json["returnStatus"],
      );

  Map<String, dynamic> toJson() => {
        "cartId": cartId,
        "variantId": variantId,
        "product_id": productId,
        "quantity": quantity,
        "productName": productName,
        "brandName": brandName,
        "type": type,
        "description": description,
        "IsPrescriptionRequired": isPrescriptionRequired,
        "image": image,
        "price": price,
        "offerType": offerType,
        "specialPrice": specialPrice,
        "uomValue": uomValue,
        "discountAmount": discountAmount,
        "discountInPercentage": discountInPercentage,
        "outOfStock": outOfStock,
        "isThisProductAddedToWhishList": isThisProductAddedToWhishList,
        "returnStatus": returnStatus,
      };
}
