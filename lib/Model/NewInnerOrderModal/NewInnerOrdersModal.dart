import 'dart:convert';

NewInnerOrdersModal welcomeFromJson(String str) => NewInnerOrdersModal.fromJson(json.decode(str));

String welcomeToJson(NewInnerOrdersModal data) => json.encode(data.toJson());

class NewInnerOrdersModal {
  NewInnerOrdersModal({
    required this.error,
    required this.message,
    required this.data,
  });

  bool error;
  String message;
  Data data;

  factory NewInnerOrdersModal.fromJson(Map<String, dynamic> json) => NewInnerOrdersModal(
    error: json["error"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "error": error,
    "message": message,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    required this.pickupPendingOrders,
  });

  List<PickupPendingOrder> pickupPendingOrders;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    pickupPendingOrders: List<PickupPendingOrder>.from(json["pickupPendingOrders"].map((x) => PickupPendingOrder.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "pickupPendingOrders": List<dynamic>.from(pickupPendingOrders.map((x) => x.toJson())),
  };
}

class PickupPendingOrder {
  PickupPendingOrder({
    required this.products,
    required this.paymentType,
    required this.address,
    required this.totalAmountToBePaid,
    required this.pickAddress,
  });

  List<Product> products;
  String paymentType;
  Address address;
  String totalAmountToBePaid;
  PickAddress pickAddress;

  factory PickupPendingOrder.fromJson(Map<String, dynamic> json) => PickupPendingOrder(
    products: List<Product>.from(json["products"].map((x) => Product.fromJson(x))),
    paymentType: json["paymentType"],
    address: Address.fromJson(json["address"]),
    pickAddress: PickAddress.fromJson(json["storeAddress"]),
    totalAmountToBePaid: json["totalAmountToBePaid"].toString(),
  );

  Map<String, dynamic> toJson() => {
    "products": List<dynamic>.from(products.map((x) => x.toJson())),
    "paymentType": paymentType,
    "address": address.toJson(),
    "storeAddress": pickAddress.toJson(),
    "totalAmountToBePaid": totalAmountToBePaid,
  };
}

class Address {
  Address({
    required this.id,
    required this.name,
    required this.mobile,
    required this.pincode,
    required this.house,
    required this.street,
    required this.landmark,
    required this.type,
    required this.wholeAddress,
  });

  String id;
  String name;
  String mobile;
  String pincode;
  String house;
  String street;
  String landmark;
  String type;
  String wholeAddress;

  factory Address.fromJson(Map<String, dynamic> json) => Address(
      id: json["_id"].toString(),
      name: json["name"].toString(),
      mobile: json["mobile"].toString(),
      pincode: json["pincode"].toString(),
      house: json["house"].toString(),
      street: json["street"].toString(),
      landmark: json["landmark"].toString(),
      type: json["type"].toString(),
      wholeAddress: json["wholeAddress"].toString()
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "name": name,
    "mobile": mobile,
    "pincode": pincode,
    "house": house,
    "street": street,
    "landmark": landmark,
    "type": type,
    "wholeAddress": wholeAddress,
  };
}

class PickAddress {
  PickAddress({
    required this.name,
    required this.email,
    required this.phone,
    required this.address,
    required this.pin,
    required this.state,
    required this.country,
    required this.gst,
  });

  String name;
  String email;
  String phone;
  String address;
  String pin;
  String state;
  String country;
  String gst;

  factory PickAddress.fromJson(Map<String, dynamic> json) => PickAddress(
    name: json["name"].toString(),
    email: json["email"].toString(),
    phone: json["phone"].toString(),
    address: json["address"].toString(),
    pin: json["pin"].toString(),
    state: json["state"].toString(),
    country: json["country"].toString(),
    gst: json["gst"].toString(),
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "email": email,
    "phone": phone,
    "address": address,
    "pin": pin,
    "state": state,
    "country": country,
    "gst": gst,
  };
}

class Product {
  Product({
    required this.cartId,
    required this.variantId,
    required this.productId,
    required this.quantity,
    required this.productName,
    required this.brandName,
    required this.type,
    required this.description,
    required this.isPrescriptionRequired,
    required this.image,
    required this.price,
    required this.offerType,
    required this.specialPrice,
    required this.uomValue,
    required this.discountAmount,
    required this.discountInPercentage,
    required this.outOfStock,
    required this.isThisProductAddedToWhishList,
  });

  String cartId;
  String variantId;
  String productId;
  String quantity;
  String productName;
  String brandName;
  String type;
  String description;
  String isPrescriptionRequired;
  String image;
  String price;
  String offerType;
  String specialPrice;
  String uomValue;
  String discountAmount;
  String discountInPercentage;
  String outOfStock;
  String isThisProductAddedToWhishList;

  factory Product.fromJson(Map<String, dynamic> json) => Product(
    cartId: json["cartId"].toString(),
    variantId: json["variantId"].toString(),
    productId: json["product_id"].toString(),
    quantity: json["quantity"].toString(),
    productName: json["productName"].toString(),
    brandName: json["brandName"].toString(),
    type: json["type"].toString(),
    description: json["description"].toString(),
    isPrescriptionRequired: json["IsPrescriptionRequired"].toString(),
    image: json["image"].toString(),
    price: json["price"].toString(),
    offerType: json["offerType"].toString() == "null" ? "" : json["offerType"],
    specialPrice: json["specialPrice"].toString(),
    uomValue: json["uomValue"].toString(),
    discountAmount: json["discountAmount"].toString(),
    discountInPercentage: json["discountInPercentage"].toString(),
    outOfStock: json["outOfStock"].toString(),
    isThisProductAddedToWhishList: json["isThisProductAddedToWhishList"].toString(),
  );

  Map<String, dynamic> toJson() => {
    "cartId": cartId,
    "variantId": variantId,
    "product_id": productId,
    "quantity": quantity,
    "productName": productName,
    "brandName": brandName,
    "type": type,
    "description": description,
    "IsPrescriptionRequired": isPrescriptionRequired,
    "image": image,
    "price": price,
    "offerType": offerType == null ? null : offerType,
    "specialPrice": specialPrice,
    "uomValue": uomValue,
    "discountAmount": discountAmount,
    "discountInPercentage": discountInPercentage,
    "outOfStock": outOfStock,
    "isThisProductAddedToWhishList": isThisProductAddedToWhishList,
  };
}
