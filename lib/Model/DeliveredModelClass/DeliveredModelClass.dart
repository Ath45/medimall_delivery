import 'dart:convert';

import 'package:get/get.dart';

DeliveredModelClass welcomeFromJson(String str) => DeliveredModelClass.fromJson(json.decode(str));

String welcomeToJson(DeliveredModelClass data) => json.encode(data.toJson());

class DeliveredModelClass {
  DeliveredModelClass({
    required this.error,
    required this.message,
    required this.data,
  });

  bool error;
  String message;
  Data data;

  factory DeliveredModelClass.fromJson(Map<String, dynamic> json) => DeliveredModelClass(
    error: json["error"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "error": error,
    "message": message,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    required this.deliveredOrders,
    required this.totalAmount,
  });

  RxList<DeliveredOrder> deliveredOrders;
  String totalAmount;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    deliveredOrders: RxList<DeliveredOrder>.from(json["deliveredOrders"].map((x) => DeliveredOrder.fromJson(x))),
    totalAmount: json["totalAmount"].toString(),
  );

  Map<String, dynamic> toJson() => {
    "deliveredOrders": List<dynamic>.from(deliveredOrders.map((x) => x.toJson())),
    "totalAmount": totalAmount,
  };
}

class DeliveredOrder {
  DeliveredOrder({
    required this.orderId,
    required this.idOr,
    required this.paymentType,
    required this.userName,
    required this.totalAmountToBePaid,
    required this.deliveryCharge,
    required this.isThisCartEligibleForFreeDelivery,
  });

  String orderId;
  String idOr;
  String paymentType;
  String userName;
  String totalAmountToBePaid;
  String deliveryCharge;
  bool isThisCartEligibleForFreeDelivery;

  factory DeliveredOrder.fromJson(Map<String, dynamic> json) => DeliveredOrder(
    orderId: json["orderId"].toString(),
    idOr: json["orderObjectId"].toString(),
    paymentType: json["paymentType"].toString(),
    userName: json["userName"].toString(),
    totalAmountToBePaid: json["totalAmountToBePaid"].toString(),
    deliveryCharge: json["deliveryCharge"].toString(),
    isThisCartEligibleForFreeDelivery: json["isThisCartEligibleForFreeDelivery"],
  );

  Map<String, dynamic> toJson() => {
    "orderId": orderId,
    "orderObjectId": idOr,
    "paymentType": paymentType,
    "userName": userName,
    "totalAmountToBePaid": totalAmountToBePaid,
    "deliveryCharge": deliveryCharge,
    "isThisCartEligibleForFreeDelivery": isThisCartEligibleForFreeDelivery,
  };
}
