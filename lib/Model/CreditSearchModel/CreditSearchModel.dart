//CreditSearchModel
import 'dart:convert';

import 'package:get/get.dart';

CreditSearchModel welcomeFromJson(String str) => CreditSearchModel.fromJson(json.decode(str));

String welcomeToJson(CreditSearchModel data) => json.encode(data.toJson());

class CreditSearchModel {
  CreditSearchModel({
    required this.error,
    required this.message,
    required this.data,
  });

  bool error;
  String message;
  Data data;

  factory CreditSearchModel.fromJson(Map<String, dynamic> json) => CreditSearchModel(
    error: json["error"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "error": error,
    "message": message,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    required this.deliveryBoyCredits,
    required this.creditBalance,
  });

  RxList<DeliveryBoyCredit> deliveryBoyCredits;
  String creditBalance;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    deliveryBoyCredits: RxList<DeliveryBoyCredit>.from(json["SearchedCredits"].map((x) => DeliveryBoyCredit.fromJson(x))),
    creditBalance: json["creditBalance"].toString(),
  );

  Map<String, dynamic> toJson() => {
    "SearchedCredits": List<dynamic>.from(deliveryBoyCredits.map((x) => x.toJson())),
    "creditBalance": creditBalance,
  };
}

class DeliveryBoyCredit {
  DeliveryBoyCredit({
    required this.credit,
    required this.debit,
    required this.balance,
    required this.deliveryBoyId,
    required this.orderId,
    required this.createdAt,
  });

  String credit;
  String debit;
  String balance;
  String deliveryBoyId;
  String orderId;
  String createdAt;

  factory DeliveryBoyCredit.fromJson(Map<String, dynamic> json) => DeliveryBoyCredit(
    credit: json["credit"].toString(),
    debit: json["debit"].toString(),
    balance: json["balance"].toString(),
    deliveryBoyId: json["deliveryBoyId"],
    orderId: json["orderId"],
    createdAt: json["createdAt"],
  );

  Map<String, dynamic> toJson() => {
    "credit": credit,
    "debit": debit,
    "balance": balance,
    "deliveryBoyId": deliveryBoyId,
    "orderId": orderId,
    "createdAt": createdAt,
  };
}
