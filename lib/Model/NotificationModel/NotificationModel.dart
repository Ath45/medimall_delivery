//NotificationModel
import 'dart:convert';

NotificationModel welcomeFromJson(String str) => NotificationModel.fromJson(json.decode(str));

String welcomeToJson(NotificationModel data) => json.encode(data.toJson());

class NotificationModel {
  NotificationModel({
    required this.error,
    required this.message,
    required this.data,
  });

  bool error;
  String message;
  Data data;

  factory NotificationModel.fromJson(Map<String, dynamic> json) => NotificationModel(
    error: json["error"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "error": error,
    "message": message,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    required this.deliveryBoyNotification,
    required this.notificationCount,
  });

  List<DeliveryBoyNotification> deliveryBoyNotification;
  String notificationCount;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    deliveryBoyNotification: List<DeliveryBoyNotification>.from(json["deliveryBoyNotification"].map((x) => DeliveryBoyNotification.fromJson(x))),
    notificationCount: json["notificationCount"].toString(),
  );

  Map<String, dynamic> toJson() => {
    "deliveryBoyNotification": List<dynamic>.from(deliveryBoyNotification.map((x) => x.toJson())),
    "notificationCount": notificationCount,
  };
}

class DeliveryBoyNotification {
  DeliveryBoyNotification({
    required this.id,
    required this.isRead,
    required this.orderObjId,
    required this.orderId,
    required this.paymentType,
    required this.type,
    required this.userName,
    required this.totalAmountToBePaid,
  });

  String id;
  String isRead;
  String orderObjId;
  String orderId;
  String paymentType;
  String type;
  String userName;
  String totalAmountToBePaid;

  factory DeliveryBoyNotification.fromJson(Map<String, dynamic> json) => DeliveryBoyNotification(
    id: json["_id"].toString(),
    isRead: json["isRead"].toString(),
    orderObjId: json["orderObjectId"],
    orderId: json["orderId"].toString(),
    paymentType: json["paymentType"].toString(),
    type: json["type"].toString(),
    userName: json["userName"].toString(),
    totalAmountToBePaid: json["totalAmountToBePaid"].toString(),
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "isRead": isRead == null ? null : isRead,
    "orderObjectId": orderObjId,
    "orderId": orderId,
    "paymentType": paymentType,
    "type": type,
    "userName": userName,
    "totalAmountToBePaid": totalAmountToBePaid,
  };
}
