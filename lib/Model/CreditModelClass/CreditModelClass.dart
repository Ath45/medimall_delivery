//CreditModelClass
import 'dart:convert';

import 'package:get/get.dart';

CreditModelClass welcomeFromJson(String str) => CreditModelClass.fromJson(json.decode(str));

String welcomeToJson(CreditModelClass data) => json.encode(data.toJson());

class CreditModelClass {
  CreditModelClass({
    required this.error,
    required this.message,
    required this.data,
  });

  bool error;
  String message;
  Data data;

  factory CreditModelClass.fromJson(Map<String, dynamic> json) => CreditModelClass(
    error: json["error"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "error": error,
    "message": message,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    required this.deliveryBoyCredits,
    required this.creditBalance,
  });

  RxList<DeliveryBoyCredit> deliveryBoyCredits;
  int creditBalance;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    deliveryBoyCredits: RxList<DeliveryBoyCredit>.from(json["DeliveryBoyCredits"].map((x) => DeliveryBoyCredit.fromJson(x))),
    creditBalance: json["creditBalance"],
  );

  Map<String, dynamic> toJson() => {
    "DeliveryBoyCredits": List<dynamic>.from(deliveryBoyCredits.map((x) => x.toJson())),
    "creditBalance": creditBalance,
  };
}

class DeliveryBoyCredit {
  DeliveryBoyCredit({
    required this.credit,
    required this.debit,
    required this.balance,
    required this.deliveryBoyId,
    required this.orderId,
    required this.type,
    required this.createdAt,
  });

  String credit;
  String debit;
  String balance;
  String deliveryBoyId;
  String orderId;
  String type;
  String createdAt;

  factory DeliveryBoyCredit.fromJson(Map<String, dynamic> json) => DeliveryBoyCredit(
    credit: json["credit"].toString(),
    debit: json["debit"].toString(),
    balance: json["balance"].toString(),
    deliveryBoyId: json["deliveryBoyId"].toString(),
    orderId: json["orderId"].toString(),
    type: json["type"].toString(),
    createdAt: json["createdAt"],
  );

  Map<String, dynamic> toJson() => {
    "credit": credit,
    "debit": debit,
    "balance": balance,
    "deliveryBoyId": deliveryBoyId,
    "orderId": orderId,
    "createdAt": createdAt,
  };
}
