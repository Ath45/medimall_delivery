import 'dart:convert';

import 'package:get/get.dart';

PickUpModelClass welcomeFromJson(String str) =>
    PickUpModelClass.fromJson(json.decode(str));

String welcomeToJson(PickUpModelClass data) => json.encode(data.toJson());

class PickUpModelClass {
  PickUpModelClass({
    required this.error,
    required this.message,
    required this.data,
  });

  bool error;
  String message;
  Data data;

  factory PickUpModelClass.fromJson(Map<String, dynamic> json) =>
      PickUpModelClass(
        error: json["error"],
        message: json["message"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "error": error,
        "message": message,
        "data": data.toJson(),
      };
}

class Data {
  Data({
    required this.returnOrders,
  });

  RxList<ReturnOrder> returnOrders;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        returnOrders: RxList<ReturnOrder>.from(
            json["returnOrders"].map((x) => ReturnOrder.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "returnOrders": List<dynamic>.from(returnOrders.map((x) => x.toJson())),
      };
}

class ReturnOrder {
  ReturnOrder({
    required this.products,
    required this.orderObjectId,
    required this.orderId,
    required this.userName,
    required this.status,
    required this.productCount,
    required this.returnId,
  });

  List<Product> products;
  String orderObjectId;
  String returnId;
  String orderId;
  String userName;
  String productCount;
  String status;

  factory ReturnOrder.fromJson(Map<String, dynamic> json) => ReturnOrder(
        products: List<Product>.from(
            json["products"].map((x) => Product.fromJson(x))),
        orderObjectId: json["orderObjectId"].toString(),
        orderId: json["orderId"].toString(),
        returnId: json["returnId"].toString(),
        userName: json["userName"].toString(),
        status: json["status"].toString(),
        productCount: json["productCount"].toString(),
      );

  Map<String, dynamic> toJson() => {
        "products": List<dynamic>.from(products.map((x) => x.toJson())),
        "orderObjectId": orderObjectId,
        "orderId": orderId,
        "userName": userName,
        "returnId": returnId,
        "status": status,
        "productCount": productCount,
      };
}

class Product {
  Product({
    required this.cartId,
    required this.variantId,
    required this.productId,
    required this.quantity,
    required this.productName,
    required this.brandName,
    required this.type,
    required this.description,
    required this.isPrescriptionRequired,
    required this.image,
    required this.price,
    required this.offerType,
    required this.specialPrice,
    required this.uomValue,
    required this.discountAmount,
    required this.discountInPercentage,
    required this.outOfStock,
    required this.isThisProductAddedToWhishList,
    required this.returnStatus,
  });

  String cartId;
  String variantId;
  String productId;
  String quantity;
  String productName;

  String brandName;
  String type;
  String description;
  bool isPrescriptionRequired;
  String image;
  String price;
  String offerType;
  String specialPrice;
  String uomValue;
  String discountAmount;
  String discountInPercentage;
  bool outOfStock;
  bool isThisProductAddedToWhishList;
  String returnStatus;

  factory Product.fromJson(Map<String, dynamic> json) => Product(
        cartId: json["cartId"],
        variantId: json["variantId"],
        productId: json["product_id"],
        quantity: json["quantity"].toString(),
        productName: json["productName"].toString(),
        brandName: json["brandName"].toString(),
        type: json["type"].toString(),
        description: json["description"].toString(),
        isPrescriptionRequired: json["IsPrescriptionRequired"],
        image: json["image"],
        price: json["price"].toString(),
        offerType: json["offerType"].toString(),
        specialPrice: json["specialPrice"].toString(),
        uomValue: json["uomValue"],
        discountAmount: json["discountAmount"].toString(),
        discountInPercentage: json["discountInPercentage"].toString(),
        outOfStock: json["outOfStock"],
        isThisProductAddedToWhishList: json["isThisProductAddedToWhishList"],
        returnStatus: json["returnStatus"],
      );

  Map<String, dynamic> toJson() => {
        "cartId": cartId,
        "variantId": variantId,
        "product_id": productId,
        "quantity": quantity,
        "productName": productName,
        "brandName": brandName,
        "type": type,
        "description": description,
        "IsPrescriptionRequired": isPrescriptionRequired,
        "image": image,
        "price": price,
        "offerType": offerType,
        "specialPrice": specialPrice,
        "uomValue": uomValue,
        "discountAmount": discountAmount,
        "discountInPercentage": discountInPercentage,
        "outOfStock": outOfStock,
        "isThisProductAddedToWhishList": isThisProductAddedToWhishList,
        "returnStatus": returnStatus,
      };
}
