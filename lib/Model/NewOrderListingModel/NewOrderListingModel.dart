// import 'dart:convert';
//
// import 'package:get/get.dart';
//
// NewOrderListingModel welcomeFromJson(String str) => NewOrderListingModel.fromJson(json.decode(str));
//
// String welcomeToJson(NewOrderListingModel data) => json.encode(data.toJson());
//
// class NewOrderListingModel {
//   NewOrderListingModel({
//     required this.error,
//     required this.message,
//     required this.data,
//   });
//
//   bool error;
//   String message;
//   Data data;
//
//   factory NewOrderListingModel.fromJson(Map<String, dynamic> json) => NewOrderListingModel(
//     error: json["error"],
//     message: json["message"],
//     data: Data.fromJson(json["data"]),
//   );
//
//   Map<String, dynamic> toJson() => {
//     "error": error,
//     "message": message,
//     "data": data.toJson(),
//   };
// }
//
// class Data {
//   Data({
//     required this.pickupPendingOrders,
//   });
//
//   RxList<PickupPendingOrder> pickupPendingOrders;
//
//   factory Data.fromJson(Map<String, dynamic> json) => Data(
//     pickupPendingOrders: RxList<PickupPendingOrder>.from(json["pickupPendingOrders"].map((x) => PickupPendingOrder.fromJson(x))),
//   );
//
//   Map<String, dynamic> toJson() => {
//     "pickupPendingOrders": RxList<dynamic>.from(pickupPendingOrders.map((x) => x.toJson())),
//   };
// }
//
// class PickupPendingOrder {
//   PickupPendingOrder({
//     required this.orderId,
//     required this.paymentType,
//     required this.userName,
//     required this.totalAmountToBePaid,
//     required this.orderObjectId,
//   });
//
//   String orderId;
//   String orderObjectId;
//   String paymentType;
//   String userName;
//   int totalAmountToBePaid;
//
//   factory PickupPendingOrder.fromJson(Map<String, dynamic> json) => PickupPendingOrder(
//     orderId: json["orderId"],
//     orderObjectId: json["orderObjectId"],
//     paymentType: json["paymentType"],
//     userName: json["userName"],
//     totalAmountToBePaid: json["totalAmountToBePaid"],
//   );
//
//   Map<String, dynamic> toJson() => {
//     "orderId": orderId,
//     "paymentType": paymentType,
//     "userName": userName,
//     "totalAmountToBePaid": totalAmountToBePaid,
//     "orderObjectId": orderObjectId,
//   };
// }
import 'dart:convert';

NewOrderListingModel welcomeFromJson(String str) => NewOrderListingModel.fromJson(json.decode(str));

String welcomeToJson(NewOrderListingModel data) => json.encode(data.toJson());

class NewOrderListingModel {
  NewOrderListingModel({
    required this.error,
    required this.message,
    required this.data,
  });

  bool error;
  String message;
  Data data;

  factory NewOrderListingModel.fromJson(Map<String, dynamic> json) => NewOrderListingModel(
    error: json["error"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "error": error,
    "message": message,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    required this.pickupPendingOrders,
  });

  List<PickupPendingOrder> pickupPendingOrders;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    pickupPendingOrders: List<PickupPendingOrder>.from(json["pickupPendingOrders"].map((x) => PickupPendingOrder.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "pickupPendingOrders": List<dynamic>.from(pickupPendingOrders.map((x) => x.toJson())),
  };
}

class PickupPendingOrder {
  PickupPendingOrder({
    required this.status,
    required this.orderobjectId,
    required this.orderId,
    required this.paymentType,
    required this.userName,
    required this.totalAmountToBePaid,
  });

  String status;
  String orderobjectId;
  String orderId;
  String paymentType;
  String userName;
  String totalAmountToBePaid;

  factory PickupPendingOrder.fromJson(Map<String, dynamic> json) => PickupPendingOrder(
    status: json["status"].toString(),
    orderobjectId: json["orderObjectId"].toString(),
    orderId: json["orderId"].toString(),
    paymentType: json["paymentType"].toString(),
    userName: json["userName"].toString(),
    totalAmountToBePaid: json["totalAmountToBePaid"].toString(),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "orderObjectId": orderobjectId,
    "orderId": orderId,
    "paymentType": paymentType,
    "userName": userName,
    "totalAmountToBePaid": totalAmountToBePaid,
  };
}
