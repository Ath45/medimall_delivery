import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/AppPages/appPages.dart';
import 'package:medimallDelivery/Routes/routes.dart';
import 'package:shared_preferences/shared_preferences.dart';
SharedPreferences prefs = SharedPreferences.getInstance() as SharedPreferences;

main() async {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
  ));
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'MedFolio', //title of app
      initialRoute: alpha,
      getPages:AppPages.pages ,//navigating to home page
    );
  }
}
