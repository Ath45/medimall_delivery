import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:medimallDelivery/Routes/routes.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/util/constantsDC.dart';
import 'package:medimallDelivery/util/fonts.dart';


import 'package:get/get.dart';
class SignupPage extends StatefulWidget {
  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  @override
  Widget build(BuildContext context) {

    return
       Scaffold(
         body:  Container(
           decoration: BoxDecoration(
             gradient: LinearGradient(
               colors: [
                 Color.fromRGBO(0, 170, 255, 1),
                 Color.fromRGBO(0, 91, 236, 1),
               ],
               begin: Alignment.topCenter

             )
           ),
           child: Column(
             children: [
               SizedBox(height: height*.16,),
               Image.asset(
                 "assets/images/boyonBike.png",
                 width: MediaQuery.of(context).size.width,
                 fit: BoxFit.fitWidth,
               ),
               SizedBox(height: height*.05,),
               Text("Be the Sunshine & Let Yourself Twinkle",textAlign: TextAlign.center,style:TextStyle(fontFamily: PoppinsRegular,fontSize: height*.016,color: Colors.white,) ,),
               SizedBox(height: height*.02,),
               Container(padding: EdgeInsets.symmetric(horizontal: width*.2),
                   child: Text("Serving the Community and Earning Benefits Along the Way",textAlign: TextAlign.center,style:TextStyle(fontFamily: PoppinsLight,fontSize: height*.012,color: Colors.white70),)),
               SizedBox(height: height*.044,),
               GestureDetector(
                 onTap: (){
                   Get.offAllNamed(loginRoute);

                 },
                 child: Container(
                     height: 45,
                     width: 165,
                     decoration: BoxDecoration(
                       color: HexColor("#FFFFFF"),
                       borderRadius: BorderRadius.circular(8.0),
                       boxShadow: [
                         BoxShadow(
                           color: HexColor("#00000029"),
                           offset: Offset(0,3),
                           blurRadius: 6.0,
                         )
                       ]
                     ),
                     child: Center(child: Text("Sign in",style: TextStyle(color: HexColor("#0063EE"),fontSize: 17,fontFamily: "Segoe UI"),))),
               ),
             ],
           ),
         ),
       );
  }
}
