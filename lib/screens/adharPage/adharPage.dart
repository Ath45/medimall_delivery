//adharPage
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/AdharPageControlller/AdharPageControlller.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/util/constants.dart';
import 'package:medimallDelivery/widgets/myAppbar.dart';

class AdharPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var height = SizeConfig.screenHeight * 1.225;
    var width = SizeConfig.screenWidth * 1.225;

    /// Local variables <<---------------------------------------------------->>
    var fontsize12 = height * .012;
    var colorblack = Color.fromRGBO(0, 0, 0, 1);
    var colorblacklighter = Color.fromRGBO(0, 0, 0, .5);
    var style = TextStyle(
        fontSize: fontsize12, fontFamily: "SegoeSemi,", color: colorblack);
    var styleReg = TextStyle(
        fontSize: fontsize12,
        fontFamily: "SegoeReg,",
        color: colorblacklighter);
    var contentP = EdgeInsets.only(
        top: height * .015, bottom: height * .0, left: width * .008);
    AdharPageControlller.to.getData();
    return Scaffold(
      backgroundColor: HexColor('#FFFFFF'),
      appBar: myAppbar(context, 'Aadhar'),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(right: 30, left: 25, top: 20),
              child:Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [

                  GetX<AdharPageControlller>(
                      init: AdharPageControlller(),
                      builder: (controller) {
                        return Container(
                          width: height*0.3,
                          height: height*0.3,
                          child: CachedNetworkImage(
                            imageUrl:controller.url.value,
                            placeholder:(context, url) => CircularProgressIndicator(),
                            errorWidget: (context, url, error) => Icon(Icons.error),
                            fit: BoxFit.fill,
                          ),
                        );
                      }
                  ),
                  SizedBox(height: height*0.01,),

                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text(
                        "Aadhar Number",
                        style: styleReg,
                      ),
                      Container(
                        height: 50,
                        child: TextFormField(
                          style: styleReg,
                          enabled: false,
                          onChanged: (text) {},
                          controller: AdharPageControlller.to.aadharController,
                          decoration: InputDecoration(contentPadding: contentP),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                    ],
                  ),

                ],
              ),
            ),
          ),
          Center(child: GetX<AdharPageControlller>(builder: (controller) {
            return Visibility(
                visible: controller.loadIng.value,
                child: Container(
                    child: (CircularProgressIndicator(
                      valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue),
                    ))));
          }))
        ],

      ),
    );
  }
}