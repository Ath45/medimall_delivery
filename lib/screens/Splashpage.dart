import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shimmer/shimmer.dart';
import 'package:medimallDelivery/Controller/SplashController/splashController.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/util/constantsDC.dart';


class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final _splashController = Get.put(SplashController());

  @override
  Widget build(BuildContext context) {
    SplashController.to.context=context;

    print(_splashController.toString());
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomCenter,
              colors: [
                HexColor("#00AAFF"),
                HexColor("#005BEC"),
              ]),
        ),
        child: Builder(
          builder: (context) => Stack(
            children: [
              Container(
                height: double.maxFinite,
                width: double.maxFinite,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/images/splash.png"),
                    fit: BoxFit.cover,
                  ),
                ),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SlideTransition(
                              position: SplashController.to.animation,
                              child: Container(
                                  child: Image.asset(
                                "assets/images/ic_cloud.png",
                                height: 180,
                              ))),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          AnimatedOpacity(
                              opacity: SplashController.to.opacity.value,
                              duration: Duration(milliseconds: 200),
                              child: Container(
                                  child: Image.asset(
                                "assets/images/ic_lightning_left.png",
                                height: 100,
                              ))),
                          AnimatedOpacity(
                              opacity: SplashController.to.opacity.value,
                              duration: Duration(milliseconds: 200),
                              child: Container(
                                  child: Image.asset(
                                "assets/images/ic_lightning_right.png",
                                height: 100,
                              ))),
                        ],
                      ),
                      SlideTransition(
                          position: SplashController.to.animation2,
                          child: Container(margin: EdgeInsets.only(bottom: height*.04),
                              child: Image.asset(
                            "assets/images/ic_delveryboy.png",
                            height: 180,
                          ))),
                    ]

                    // Row(
                    //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //   children: [
                    //     Align(
                    //       alignment: Alignment.bottomRight,
                    //       child: Container(
                    //           child: Image.asset(
                    //         "assets/images/ic_lightning_left.png",
                    //         height: 100,
                    //       )),
                    //     ),
                    //     Align(
                    //       alignment: Alignment.bottomLeft,
                    //       child: Container(
                    //           child: Image.asset(
                    //         "assets/images/ic_lightning_right.png",
                    //         height: 100,
                    //       )),
                    //     ),
                    //   ],
                    // ),
                    // Container(
                    //   width: double.infinity,
                    //   child: AnimatedPositioned(
                    //     duration: Duration(microseconds: 2000),
                    //     left: _animatedPositionedTopValue,
                    //     onEnd: () {
                    //       if (!_animationAlreadyPaused) {
                    //         _asyncPauseAndContinueAnimation();
                    //       }
                    //     },
                    //     child: Container(
                    //         child: Image.asset(
                    //       "assets/images/ic_delveryboy.png",
                    //       height: 150,
                    //     )),
                    //   ),
                    // ),

                    ),
              ),
              Positioned(
                child: Align(
                  alignment: Alignment.center,
                  child: Padding(
                    // padding: EdgeInsets.only(top: SizeConfig.horizontal * 35),
                    padding: EdgeInsets.only(top: 150),
                    child: SizedBox(
                      width: width*.8,
                      height: 100.0,
                      child: Shimmer.fromColors(
                        baseColor: Colors.white,
                        highlightColor: Colors.grey,
                        child: Text(
                          'Faster Than Lightning',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 19.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
