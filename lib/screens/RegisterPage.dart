import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_getx_widget.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:image_picker/image_picker.dart';
import 'package:medimallDelivery/Controller/RegisterController/registerController.dart';

import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/util/constantsDC.dart';

import 'package:medimallDelivery/util/style.dart';
import 'package:medimallDelivery/util/validators.dart';
import 'package:medimallDelivery/widgets/MyTextBox.dart';
class RegisterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    RegisterController.to.context=context;
    return Scaffold(
        body: Container(
      height: height,
      width: width,
      decoration: BoxDecoration(
        // image: DecorationImage(
        //   image: AssetImage("assets/images/registerbg.png"),
        // ),
        gradient: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomCenter,
          colors: [
            HexColor("#00AAFF"),
            HexColor("#005BEC"),
          ],
        ),
      ),
      child: Form(
        key: RegisterController.to.formKeyFirst,
        child: ListView(
          children: [
            Stack(
              children: [
                Column(
                  children: [
                    Container(padding: EdgeInsets.symmetric(horizontal: width*.06),
                      child: Column(
                        children: [
                          SizedBox(height: height*.15,),
                          LoginTextBox(
                            controller: RegisterController.to.nameController,
                            header: "Full Name",
                            maxLength: 50,
                            placeHolder: "Full Name",
                            validator: () =>
                                isEmpty(RegisterController.to.nameController.text.trim()),
                            enablePadding: true,
                            textInputType: TextInputType.text,
                          ),
                          PhoneNumberTextBox(
                            controller: RegisterController.to.phoneController,
                            header: "Mobile Number",
                            placeHolder: "Mobile Number",
                            validator: (value) {
                              return RegisterController.to
                                  .validatePhone(value!);
                            },
                            textInputType: TextInputType.number,
                            enablePadding: true,
                          ),
                          EmailTextBox(
                              controller: RegisterController.to.mailController,
                              header: "Email ID",
                              placeHolder: "Email ID",
                              validator: () =>
                                  isEmpty(RegisterController.to.mailController.text.trim()),
                              enablePadding: true,
                              textInputType: TextInputType.text),
                          PasswordTextBox(
                              controller: RegisterController.to.passwordController,
                              header: "Password",
                              placeHolder: "Password",
                              validator: () =>
                                  isEmpty(RegisterController.to.passwordController.text.trim()),
                              enablePadding: true,
                              textInputType: TextInputType.text),
                          PasswordConfirmTextBox(
                              controller: RegisterController.to.passwordRepeatController,
                              header: "Confirm Password",
                              placeHolder: "Confirm Password",
                              validator: () => repeatPassword(
                                  RegisterController.to.passwordRepeatController.text.trim(),
                                  RegisterController.to.passwordController.text.trim()),
                              enablePadding: true,
                              textInputType: TextInputType.text),
                          LoginTextBox(
                              controller: RegisterController.to.licenseController,
                              header: "Driving License Number",
                              maxLength: 16,
                              placeHolder: "Driving License Number",
                              validator: () =>
                                  isEmpty(RegisterController.to.licenseController.text.trim()),
                              enablePadding: true,
                              textInputType: TextInputType.text),
                          AadharTextBox(
                              controller: RegisterController.to.aadarController,
                              header: "Aadhar",
                              placeHolder: "Aadhar",
                              validator: () =>
                                  isEmpty(RegisterController.to.aadarController.text.trim()),
                              enablePadding: true,
                              textInputType: TextInputType.number),
                          LoginTextBox(
                              controller: RegisterController.to.addressController,
                              header: "Address",
                              maxLength: 100,
                              placeHolder: "Address",
                              validator: () =>
                                  isEmpty(RegisterController.to.addressController.text.trim()),
                              enablePadding: true,
                              textInputType: TextInputType.text),
                          LoginTextBox(
                            maxLength: 50,
                              controller: RegisterController.to.cityController,
                              header: "City",
                              placeHolder: "City",
                              validator: () =>
                                  isEmpty(RegisterController.to.cityController.text.trim()),
                              enablePadding: true,
                              textInputType: TextInputType.text),
                          SizedBox(
                            height: height * .02,
                          ),


                        ],
                      ),
                    ),
                    Container(padding: EdgeInsets.symmetric(horizontal: width*.03),
                      child: Column(
                        children: [
                          UpLoadAssetList(),
                          SizedBox(
                            height: height * .03,
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: width * .03),
                            child: Text(
                              "By proceeding, I agree to Medimall Terms of Use and acknowledge that I have read the Privacy Policy.\n\n I also agree that Medimall or its representatives may contact me by email, phone, or SMS (including by automated means) at the email address or number I provide, including for marketing purposes.",
                              style: sRegF9CWhite,
                            ),
                          ),
                          SizedBox(
                            height: height * .03,
                          ),
                          GetX<RegisterController>(builder: (controller) {
                            return Center(
                              child: Container(
                                  child: Visibility(
                                      visible: controller.loading.value,
                                      child: Container(
                                          margin: EdgeInsets.only(bottom:20),
                                          child: (CircularProgressIndicator(
                                            valueColor:
                                            new AlwaysStoppedAnimation<Color>(Colors.white),
                                          ))
                                      )

                                  )
                              ),
                            );
                          }),
                          ContinueButton(),
                          SizedBox(
                            height: height * .02,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Container(height: height*.2,
                  child: Stack(
                    children: [
                      Container(padding: EdgeInsets.only(top: 20),
                        child: Row(mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image(image: AssetImage("assets/images/delilogo2.png"),height: height*.1,),
                          ],
                        ),
                      ),
                      Positioned(right: -60,bottom: 0,
                          child: Image(image: AssetImage("assets/images/deliveryboy.png"),height: height*.13,)),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    ));
  }
}



class ContinueButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (RegisterController.to.formKeyFirst.currentState!.validate()) {
          if(!RegisterController.to.loading.value)
            {
              RegisterController.to.fetchUserData();
             // RegisterController.to.loading.value = true;
            }

        }
          //  RegisterController.to.loading.value = true;
          // RegisterController.to.fetchUserData();
      },
      child: Column(
        children: [
          Container(
            height: height * .048,
            margin: EdgeInsets.symmetric(horizontal: width * .22),
            decoration: BoxDecoration(
                color: HexColor("#FFFFFF"),
                borderRadius: BorderRadius.circular(8.0),
                boxShadow: [
                  BoxShadow(
                    color: HexColor("#00000029"),
                    blurRadius: 6,
                  )
                ]),
            child: Center(
              child: Text(
                "continue",
                style: sSBoldF12,
              ),
            ),
          ),
          SizedBox(height: height*.02,),
          Row(mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(width: 55,),
              Container(padding: EdgeInsets.symmetric(horizontal: width*.06),
                child: Text(
                  "Already have an account ?",
                  style: sRegF12CWhite,
                ),
              ),
              GestureDetector(
                onTap: () {
                  // Get.toNamed(loginRoute);
                  Get.back();
                },
                child: Container(
                  height: 22,
                  width: 55,
                  decoration: BoxDecoration(
                    color: HexColor("#4D9EFF"),
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  child: Center(
                    child: Text(
                      "Sign in",
                      style: sRegF12CWhite,
                    ),
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}

class UpLoadAssetList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          GetBuilder<RegisterController>(
              init: RegisterController(),
              builder: (controller) {
                return Container(
                  child: GestureDetector(
                    onTap: () {
                      RegisterController.to
                          .getImageLincence(ImageSource.gallery);
                    },
                    child: Container(
                      height: height * .05,
                      padding: EdgeInsets.symmetric(horizontal: width*.052),
                      decoration: BoxDecoration(
                        color: HexColor("#4598F4"),
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                      child: Center(
                        child: controller.lincenceImageLoad
                            ? Image.file(File(controller.licenseImage!.path),
                                height: 40, width: 42, fit: BoxFit.fill)
                            : Row(
                              children: [
                                Image.asset(
                                    "assets/images/upLoadIcon.png",
                                    height: 18,
                                    width: 23,
                                  ),
                                SizedBox(width: width*.03,),
                                Text("License",style: sRegF12CWhite,)
                              ],
                            ),
                      ),
                    ),
                  ),
                );
              }),
          GetBuilder<RegisterController>(
              init: RegisterController(),
              builder: (controller) {
                return Container(
                  child: GestureDetector(
                      onTap: () {
                        //RegisterController.to.rcBookImage.value=await pickImage();
                        RegisterController.to
                            .getImageAdhar(ImageSource.gallery);
                      },
                      child: Container(
                        height: 50,
                        padding: EdgeInsets.symmetric(horizontal: width*.052),
                        decoration: BoxDecoration(
                          color: HexColor("#4598F4"),
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        child: Center(
                          child: controller.adharImageLoad
                              ? Image.file(File(controller.adhaarImage!.path),
                                  height: 40, width: 42, fit: BoxFit.fill)
                              :  Row(
                            children: [
                              Image.asset(
                                "assets/images/upLoadIcon.png",
                                height: 18,
                                width: 23,
                              ),
                              SizedBox(width: width*.03,),
                              Text("Aadhar",style: sRegF12CWhite,)
                            ],
                          ),
                        ),
                      )),
                );
              }),
          GetBuilder<RegisterController>(
              init: RegisterController(),
              builder: (controller) {
                return Container(
                    child: GestureDetector(
                        onTap: () {
                          //RegisterController.to.rcBookImage.value=await pickImage();

                          RegisterController.to.getImage(ImageSource.gallery);
                        },
                        child: Container(
                          height: 50,
                          padding: EdgeInsets.symmetric(horizontal: width*.052),
                          decoration: BoxDecoration(
                            color: HexColor("#4598F4"),
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                          child: Center(
                            child: controller.recBookImage
                                ? Image.file(File(controller.rcBookImage!.path),
                                    height: 40, width: 42, fit: BoxFit.fill)
                                :  Row(
                              children: [
                                Image.asset(
                                  "assets/images/upLoadIcon.png",
                                  height: 18,
                                  width: 23,
                                ),
                                SizedBox(width: width*.03,),
                                Text("Rc Book",style: sRegF12CWhite,)
                              ],
                            ),
                          ),
                        )));
              })
        ],
      ),
    );
  }
}
