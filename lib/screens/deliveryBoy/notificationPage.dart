import 'package:flutter/material.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/widgets/myAppbar.dart';
import 'package:medimallDelivery/widgets/ordersListItem.dart';

class NotificationsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: HexColor('#FFFFFF'),
      appBar: myAppbar(context, 'Notification'),
      body: Padding(
        padding: const EdgeInsets.only(top: 10),
        child: ListView(
          children: [
            newOrderContainer('John', '₹434', '64645475', 'Cash on Delivery'),
            newOrderContainer(
                'Amal Rahman', '₹299', '64645475', 'Cash on Delivery'),
            newOrderContainer('Sijo', '₹644', '64645475', 'Cash on Delivery'),
          ],
        ),
      ),
    );
  }

  GestureDetector newOrderContainer(
      String name, String price, String orderId, String delivery) {
    return GestureDetector(
      onTap: () {
        // Navigator.of(context).push(MaterialPageRoute(builder: (context){
        //   return NewOrderDetail();
        // }));
      },
      child: orderListItem(name: name,price: price,orderId: orderId,delivery: delivery, read: "true",),
    );
  }
}
