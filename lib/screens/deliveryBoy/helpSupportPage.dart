import 'package:flutter/material.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/widgets/myAppbar.dart';

class HelpSupport extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: HexColor('#FFFFFF'),
      appBar: myAppbar(context, 'Help & Support'),
      body: Padding(
        padding: const EdgeInsets.only(top: 20, right: 12, left: 12),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text('Issues',
                style: TextStyle(
                    fontSize: 14,
                    color: HexColor('#858585'),
                    fontWeight: FontWeight.bold)),
            SizedBox(height: 10),
            helpitem('Account Related Queries'),
            helpitem('Payment Related Queries'),
            helpitem('Offer Related Queries'),
            helpitem('Return Related Queries'),
            helpitem('Order Related Queries'),
          ],
        ),
      ),
    );
  }

  Padding helpitem(String issue) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 5),
      child: Container(
        height: 40,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5),
            boxShadow: [BoxShadow(color: Colors.grey.shade400, blurRadius: 4)]),
        child: Padding(
          padding: const EdgeInsets.only(left: 20, top: 11),
          child: Text(
            issue,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
          ),
        ),
      ),
    );
  }
}
