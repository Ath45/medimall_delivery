import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/PendingDeliveryController/pendingDeliveryController.dart';
import 'package:medimallDelivery/Routes/routes.dart';
import 'package:medimallDelivery/screens/deliveryBoy/pendingDelivery/pendingOrderDetail.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/util/constants.dart';
import 'package:medimallDelivery/widgets/PendingDeliveryList.dart';
import 'package:medimallDelivery/widgets/myAppbar.dart';

class PendingDelevery extends StatelessWidget {
  var styleRegGrey = TextStyle(
      color: Color.fromRGBO(98, 98, 100, 1),
      fontSize: 15,
      fontFamily: "SegoeReg");
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    PendingDeliveryController.to.getData();
    return Scaffold(
        backgroundColor: HexColor('#FAFAFA'),
        appBar: myAppbar(context, 'Pending Delivery'),
        body: Stack(
          children: [
            GetX<PendingDeliveryController>(
                init: PendingDeliveryController(),
                builder: (controller) {
                  return ListView.builder(
                    padding: EdgeInsets.symmetric(
                        vertical: SizeConfig.horizontal * 3,
                        horizontal: SizeConfig.horizontal * 2.5),
                    shrinkWrap: true,
                    itemCount: PendingDeliveryController.to.pendingOrder.length,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () {
                          // Navigator.of(context)
                          //     .push(MaterialPageRoute(builder: (context) {
                          //   return PendingOrderDetail();
                          // }));
                          Get.toNamed(pendingOrderDetail, arguments: [controller.pendingOrder[index].orderobjectId]);

                        },
                        child: pendingDeliveryList(
                          name: PendingDeliveryController.to.pendingOrder[index].userName,
                          price: PendingDeliveryController.to.pendingOrder[index].totalAmountToBePaid.toString(),
                          orderId: PendingDeliveryController.to.pendingOrder[index].orderId,
                          delivery: PendingDeliveryController.to.pendingOrder[index].paymentType,
                          status: PendingDeliveryController.to.pendingOrder[index].status,
                        ),
                      );
                    },
                  );
                }
            ),
            Center(child: GetX<PendingDeliveryController>(builder: (controller) {
              print("data" + controller.rxEmpty.value.toString());
              return new Center(
                  child: Visibility(
                      visible: controller.rxEmpty.value,
                      child: Container(
                          child: Text(
                            "No Data Available.....",
                            style: styleRegGrey,
                          ))));
            })),
            Center(child: GetX<PendingDeliveryController>(builder: (controller) {
              return Visibility(
                  visible: controller.loadIng.value,
                  child: Container(
                      child: (CircularProgressIndicator(
                        valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue),
                      ))));
            }))
          ],
        ));
  }
}
