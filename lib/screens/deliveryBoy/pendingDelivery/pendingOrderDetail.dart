import 'package:expansion_tile_card/expansion_tile_card.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/PendingOrderDetailsController/PendingOrderDetailsController.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/util/constants.dart';
import 'package:medimallDelivery/widgets/boldSubHeading.dart';
import 'package:medimallDelivery/widgets/myAppbar.dart';
import 'package:medimallDelivery/widgets/pendingCustomerDetails.dart';
import 'package:medimallDelivery/widgets/pendingDeliveryAddress.dart';
import 'package:medimallDelivery/widgets/pickedupAddress.dart';
import 'package:medimallDelivery/widgets/pickupPendingAddress.dart';
import 'package:medimallDelivery/widgets/subHeading.dart';
import 'package:medimallDelivery/widgets/widgetPendingList.dart';

class PendingOrderDetail extends StatelessWidget {
  final one = Get.arguments;
  final GlobalKey<ExpansionTileCardState> statusKey = new GlobalKey();
  String selectStatus = 'Picked Up';
  @override
  Widget build(BuildContext context) {
    PendingOrderDetailsController.to.pendGetData(one[0].toString());
    return Scaffold(
      backgroundColor: HexColor('#FAFAFA'),
      appBar: myAppbar(context, 'Pending Order'),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Padding(
              padding:  EdgeInsets.symmetric(horizontal: SizeConfig.horizontal*3,),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  WidgetPendingList(),
                  SizedBox(height: SizeConfig.horizontal*3,),
                  SubHeading(text:  'Customer Details',),
                  PendingCustomerDetail(),
                  SizedBox(height: SizeConfig.horizontal*3,),
                  SubHeading(text:  'Pickup Address',),
                  PickupPendingAddress(),
                  SizedBox(height: SizeConfig.horizontal*3,),
                  SubHeading(text:  'Delivery Address',),
                  PendingDeliveryAddress(),
                  SizedBox(height: SizeConfig.horizontal*3,),
                  GetX<PendingOrderDetailsController>(
                      init: PendingOrderDetailsController(),
                      builder: (controller) {
                      return SubHeadingBold(text: 'Amount to pay : ' +' ₹ ' +controller.amountToPaid.value.toString(),);
                    }
                  ),
                  SizedBox(height: SizeConfig.horizontal*3,),
                  Center(
                    child: Padding(
                      padding:  EdgeInsets.symmetric(vertical: SizeConfig.vertical*4),
                      child: MyButtons(text: 'Delivered'),
                    ),
                  )
                ],
              ),
            ),
          ),
          Center(child: GetX<PendingOrderDetailsController>(builder: (controller) {
            return Visibility(
                visible: controller.loadIng.value,
                child: Container(
                    child: (CircularProgressIndicator(
                      valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue),
                    ))));
          }))
        ],
      ),
    );
  }

  Padding cancelCommentContainer() {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,

          children: [
            Text(
              "Comments",
              style: TextStyle(fontSize: 14.0, color: Colors.black),

            ),
            SizedBox(height: 10),
            GestureDetector(
              onTap: (){
              },
              child: Container(
                height: 100,
                decoration: BoxDecoration(
                    border: Border.all(color: HexColor('#707070'), width: 0.5),
                    borderRadius: BorderRadius.circular(5)),
                child: Padding(
                  padding: const EdgeInsets.only(right: 9, left: 9),
                  child: TextFormField(
                      decoration: InputDecoration(
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none),

                      keyboardType: TextInputType.multiline,

                      maxLines: null,
                      initialValue:
                          'Lorem ipsum dolor sit amet consectetur adipiscing elit present anti'),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class MyButtons extends StatelessWidget {
  final String text;

  MyButtons({required this.text});

  @override
  Widget build(BuildContext context) {
    return  GestureDetector(
      onTap: () async {
        await showDialog(
            context: context,
            builder: (_) => AlertDialog(
          title: Text('Are you sure want to deliver?'),
          actions: [
            ElevatedButton(
                onPressed: () {
                  PendingOrderDetailsController.to.changeStatus();
                  Get.back();
                },
                child: Text('Yes')),
            TextButton(onPressed: () => Get.back(), child: Text('No'))
          ],
        ));
      },
      child: Container(
        height: SizeConfig.horizontal * 10,
        width: SizeConfig.horizontal * 40,
        decoration: BoxDecoration(
            color: HexColor('#4D9EFF'),
            boxShadow: [
              BoxShadow(
                  color: Colors.black.withOpacity(0.1),
                  offset: Offset(0, 3),
                  blurRadius: 10)
            ],
            borderRadius: BorderRadius.circular(5)),
        child: Center(
            child: Text(
              text,
              style: TextStyle(
                  fontSize: SizeConfig.horizontal * 3.7,
                  fontFamily: 'SegoeReg',
                  fontWeight: FontWeight.w500,
                  color: HexColor('#FFFFFF')),
            )),
      ),
    );
  }
}