import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/NotificationController/NotificationController.dart';
import 'package:medimallDelivery/Routes/routes.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/widgets/notificationBox.dart';
import 'package:medimallDelivery/widgets/ordersListItem.dart';
import '../../widgets/myAppbar.dart';

class NotificationPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    NotificationController.to.getData();
    return Scaffold(
      backgroundColor: HexColor('#FFFFFF'),
      appBar: myAppbar(context, 'Notification'),
      body: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child:GetX<NotificationController>(
                init: NotificationController(),
                builder: (controller) {
                  return ListView.builder(
                      shrinkWrap: true,
                      itemCount: controller.notList.length,
                      itemBuilder: (context, index) {
                        return Container(
                          child: GestureDetector(
                            onTap: () {
                              if(controller.notList[index].isRead=="true")
                                {
                                  Get.toNamed(newOrderDetails, arguments: [controller.notList[index].orderObjId]);
                                }else{
                                NotificationController.to.changeStatus(controller.notList[index].id,controller.notList[index].orderObjId);

                              }
                            },
                            child: NotificationBox(
                              orderStaus:controller.notList[index].type,
                              name: controller.notList[index].userName,
                              price: controller.notList[index].totalAmountToBePaid,
                              orderId: controller.notList[index].orderId,
                              delivery:  controller.notList[index].paymentType,
                              read:  controller.notList[index].isRead,
                            ),
                          ),
                        );
                      });
                }
            ),
          ),
          Center(child: GetX<NotificationController>(builder: (controller) {
            print("data" + controller.rxEmpty.value.toString());
            return new Center(
                child: Visibility(
                    visible: controller.rxEmpty.value,
                    child: Container(
                        child: Text(
                          "No Data Available.....",
                          style: TextStyle(
                              color: Color.fromRGBO(98, 98, 100, 1),
                              fontSize: 15,
                              fontFamily: "SegoeReg"),
                        ))));
          })),
          Center(child: GetX<NotificationController>(builder: (controller) {
            return Visibility(
                visible: controller.loadIng.value,
                child: Container(
                    child: (CircularProgressIndicator(
                      valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue),
                    ))));
          }))
        ],
      ),
    );
  }
}
