import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/EmergencyControllerPage/EmergencyControllerPage.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/widgets/emergencyContactItem.dart';
import 'package:medimallDelivery/widgets/myAppbar.dart';

class EmergencyContacts extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    EmergencyControllerPage.to.getData();
    return Scaffold(
      backgroundColor: HexColor('#FFFFFF'),
      appBar: myAppbar(context, 'Emergency Contact'),
      body: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 13, right: 13, top: 19),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                GetX<EmergencyControllerPage>(
                    init: EmergencyControllerPage(),
                    builder: (controller) {
                      return ListView.builder(
                          shrinkWrap: true,
                          itemCount: controller.emerGency.length,
                          itemBuilder: (context, index) {
                            return EmergencyContactItem(
                              name: controller.emerGency[index].name,
                              phone: controller.emerGency[index].contactNumber,
                            );
                          });
                    }),
              ],
            ),
          ),
          Center(child: GetX<EmergencyControllerPage>(builder: (controller) {
            return Visibility(
                visible: controller.loadIng.value,
                child: Container(
                    child: (CircularProgressIndicator(
                      valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue),
                    ))));
          }))
        ],
      ),
    );
  }
}
