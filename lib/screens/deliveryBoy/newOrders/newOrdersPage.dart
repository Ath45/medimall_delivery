import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/NewOrderController/newOrderController.dart';
import 'package:medimallDelivery/Routes/routes.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/util/constants.dart';
import 'package:medimallDelivery/widgets/myAppbar.dart';
import 'package:medimallDelivery/widgets/ordersListItem.dart';

import 'newOrderDetail.dart';

class NewOrders extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
        backgroundColor: HexColor('#FAFAFA'),
        appBar: myAppbar(context, 'New Orders'),
        body: Stack(
          children: [
            GetX<NewOrderController>(
                init: NewOrderController(),
                builder: (controller) {
                  return ListView.builder(
                    padding: EdgeInsets.symmetric(
                        vertical: SizeConfig.horizontal * 3,
                        horizontal: SizeConfig.horizontal * 2.5),
                    shrinkWrap: true,
                    itemCount: NewOrderController.to.newOrder.length,
                    itemBuilder: (context, index) {
                      return Container(
                        child: GestureDetector(
                          onTap: () async {
                            Get.toNamed(newOrderDetails, arguments: [
                              NewOrderController
                                  .to.newOrder[index].orderobjectId
                            ]);
                            print("OrderId");
                          },
                          child: orderListItem(
                            name:
                                NewOrderController.to.newOrder[index].userName,
                            price: NewOrderController
                                .to.newOrder[index].totalAmountToBePaid
                                .toString(),
                            orderId:
                                NewOrderController.to.newOrder[index].orderId,
                            delivery: NewOrderController
                                .to.newOrder[index].paymentType,
                            read: "true",
                          ),
                        ),
                      );
                    },
                  );
                }),
            Center(child: GetX<NewOrderController>(builder: (controller) {
              print("data" + controller.rxEmpty.value.toString());
              return new Center(
                  child: Visibility(
                      visible: controller.rxEmpty.value,
                      child: Container(
                          child: Text(
                        "No Data Available.....",
                        style: TextStyle(
                            color: Color.fromRGBO(98, 98, 100, 1),
                            fontSize: 15,
                            fontFamily: "SegoeReg"),
                      ))));
            })),
            Center(child: GetX<NewOrderController>(builder: (controller) {
              return Visibility(
                  visible: controller.loadIng.value,
                  child: Container(
                      child: (CircularProgressIndicator(
                    valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue),
                  ))));
            }))
          ],
        ));
  }
}
