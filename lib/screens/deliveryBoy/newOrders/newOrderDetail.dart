import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/NewInnerOrderController/NewInnerOrderController.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/util/constants.dart';
import 'package:medimallDelivery/widgets/boldSubHeading.dart';
import 'package:medimallDelivery/widgets/customerDetail.dart';
import 'package:medimallDelivery/widgets/deliveryAddress.dart';
import 'package:medimallDelivery/widgets/myAppbar.dart';
import 'package:medimallDelivery/widgets/productDetails.dart';
import 'package:medimallDelivery/widgets/pickedupAddress.dart';
import 'package:medimallDelivery/widgets/subHeading.dart';

class NewOrderDetail extends StatelessWidget {
  final one = Get.arguments;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    NewInnerOrderController.to.getData(one[0]);
    return Scaffold(
      backgroundColor: HexColor('#FAFAFA'),
      appBar: myAppbar(context, 'New Orders'),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.horizontal * 3,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ProductDetails(),
                  SizedBox(
                    height: SizeConfig.horizontal * 3,
                  ),
                  SubHeading(
                    text: 'Customer Details',
                  ),
                  CustomerDetail(),
                  SizedBox(
                    height: SizeConfig.horizontal * 3,
                  ),
                  SubHeading(
                    text: 'Pickup Address',
                  ),
                  PickedUpAddress(),
                  SizedBox(
                    height: SizeConfig.horizontal * 3,
                  ),
                  SubHeading(
                    text: 'Delivery Address',
                  ),
                  DeliveryAddress(),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        vertical: SizeConfig.horizontal * 4,
                        horizontal: SizeConfig.horizontal * 1),
                    child: GetX<NewInnerOrderController>(
                        init: NewInnerOrderController(),
                        builder: (controller) {
                          return Visibility(
                            visible: controller.enableBtn.value,
                            child: Container(
                              width: SizeConfig.horizontal * 70,
                              child: Text(
                                "",
                                style: TextStyle(
                                    color: Colors.black.withOpacity(0.5),
                                    fontFamily: 'SegoeReg',
                                    fontSize: SizeConfig.horizontal * 3.4),
                              ),
                            ),
                          );
                        }),
                  ),
                  GetX<NewInnerOrderController>(
                      init: NewInnerOrderController(),
                      builder: (controller) {
                        return SubHeadingBold(
                          text: 'Amount to pay : ' +
                              ' ₹ ' +
                              controller.amountToPaid.value.toString(),
                        );
                      }),
                  // Padding(
                  //   padding:
                  //   EdgeInsets.symmetric(vertical: SizeConfig.horizontal * 1.5),
                  //   child: GetX<NewInnerOrderController>(
                  //       init: NewInnerOrderController(),
                  //       builder: (controller) {
                  //         return Container(
                  //             height: SizeConfig.horizontal * 20,
                  //             width: SizeConfig.horizontal * 100,
                  //             decoration: BoxDecoration(
                  //                 color: HexColor('#FFFFFF'),
                  //                 borderRadius: BorderRadius.circular(8),
                  //                 boxShadow: myBoxShadow),
                  //             child: Center(
                  //               child: Text(
                  //                 '₹ ' +controller.amountToPaid.value.toString(),
                  //                 style: TextStyle(fontFamily: 'SegoeReg',color: Colors.black.withOpacity(0.7),
                  //                   fontSize: SizeConfig.horizontal*3.4,
                  //                 ),
                  //               ),
                  //             )
                  //         );
                  //       }
                  //   ),
                  // ),
                  // SizedBox(height: SizeConfig.horizontal*2,),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        vertical: SizeConfig.horizontal * 7),
                    child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          GestureDetector(
                            onTap: () async {
                              await showDialog(
                                  context: context,
                                  builder: (_) => AlertDialog(
                                        title: Text('Are you sure ?'),
                                        actions: [
                                          ElevatedButton(
                                              onPressed: () {
                                                NewInnerOrderController.to
                                                    .acceptOrder(
                                                        "decline", one[0]);
                                                Get.back();
                                              },
                                              child: Text('Yes')),
                                          TextButton(
                                              onPressed: () => Get.back(),
                                              child: Text('No'))
                                        ],
                                      ));
                              //
                            },
                            child: Container(
                              height: SizeConfig.horizontal * 10,
                              width: SizeConfig.horizontal * 35,
                              decoration: BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.black.withOpacity(0.07),
                                        offset: Offset(0, 3),
                                        blurRadius: 9)
                                  ],
                                  borderRadius: BorderRadius.circular(5),
                                  color: Colors.white),
                              child: Center(
                                  child: Text(
                                'Decline',
                                style: TextStyle(
                                    fontSize: SizeConfig.horizontal * 3.7,
                                    color: Colors.black,
                                    fontFamily: 'SegoeReg',
                                    fontWeight: FontWeight.w500),
                              )),
                            ),
                          ),
                          Container(
                            width: SizeConfig.horizontal * 6,
                          ),
                          GestureDetector(
                            onTap: () async {
                              await showDialog(
                                  context: context,
                                  builder: (_) => AlertDialog(
                                title: Text('Are you sure ?'),
                                actions: [
                                  ElevatedButton(
                                      onPressed: () {
                                        NewInnerOrderController.to
                                            .acceptOrder("picked up", one[0]);
                                        Get.back();
                                      },
                                      child: Text('Yes')),
                                  TextButton(
                                      onPressed: () => Get.back(),
                                      child: Text('No'))
                                ],
                              ));

                            },
                            child: Container(
                              height: SizeConfig.horizontal * 10,
                              width: SizeConfig.horizontal * 35,
                              decoration: BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.black.withOpacity(0.1),
                                        offset: Offset(0, 3),
                                        blurRadius: 5)
                                  ],
                                  borderRadius: BorderRadius.circular(5),
                                  color: HexColor('#4D9EFF')),
                              child: Center(
                                  child: Text(
                                'Picked Up',
                                style: TextStyle(
                                    fontSize: SizeConfig.horizontal * 3.7,
                                    fontFamily: 'SegoeReg',
                                    fontWeight: FontWeight.w500,
                                    color: Colors.white),
                              )),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          Center(child: GetX<NewInnerOrderController>(builder: (controller) {
            return Visibility(
                visible: controller.loadIng.value,
                child: Container(
                    child: (CircularProgressIndicator(
                  valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue),
                ))));
          }))
        ],
      ),
    );
  }
}
