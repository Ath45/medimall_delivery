import 'dart:io';

import 'package:expansion_tile_card/expansion_tile_card.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:medimallDelivery/Controller/PickUpDeliveryController/PickUpDeliveryController.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/util/constants.dart';
import 'package:medimallDelivery/util/constantsDC.dart';
import 'package:medimallDelivery/util/contantRiyas.dart';
import 'package:medimallDelivery/widgets/myAppbar.dart';
import 'package:medimallDelivery/widgets/signatureField.dart';
import 'package:medimallDelivery/widgets/subHeading.dart';

class PickUpProductDetails extends StatelessWidget {
  final PickUpDeliveryController controller =
      Get.put(PickUpDeliveryController());
  final GlobalKey<ExpansionTileCardState> transactionKey = new GlobalKey();
  String transactionElement = 'Pickup Status';
  final one = Get.arguments;
  List creditType = ["Collected", "Submitted"];
  List creditTypeSyb = ["Submitted"];
  var stylBlackFont13 =
      TextStyle(color: colorblack, fontSize: 13, fontFamily: "SegoeReg");
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    PickUpDeliveryController.to.getData(one[0]);
    PickUpDeliveryController.to.context = context;
    return Scaffold(
      backgroundColor: HexColor('#FAFAFA'),
      appBar: myAppbar(context, 'Return product'),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.horizontal * 3,
              ),
              child: GetX<PickUpDeliveryController>(
                  init: PickUpDeliveryController(),
                  builder: (controller) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: SizeConfig.horizontal * 4),
                          child: GetX<PickUpDeliveryController>(
                              init: PickUpDeliveryController(),
                              builder: (controller) {
                                print("pick Up Updated" +
                                    controller.pickUpDelivery.length
                                        .toString());
                                return ListView.builder(
                                    shrinkWrap: true,
                                    itemCount: controller.pickUpDelivery.length,
                                    itemBuilder: (context, index) {
                                      return Container(
                                        margin: EdgeInsets.only(top: 10),
                                        height: SizeConfig.horizontal * 35,
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(8),
                                            boxShadow: myBoxShadow),
                                        child: Padding(
                                          padding: EdgeInsets.all(
                                              SizeConfig.horizontal * 3),
                                          child: Row(
                                            children: [
                                              Image.network(
                                                controller.pickUpDelivery[index]
                                                    .image,
                                                height:
                                                    SizeConfig.horizontal * 25,
                                                fit: BoxFit.fill,
                                              ),
                                              SizedBox(
                                                width:
                                                    SizeConfig.horizontal * 2,
                                              ),
                                              Container(
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    Center(
                                                      child: SizedBox(
                                                        width: height * 0.110,
                                                        child: Text(
                                                          controller
                                                              .pickUpDelivery[
                                                                  index]
                                                              .productName,
                                                          maxLines: 3,
                                                          style: TextStyle(
                                                              fontFamily:
                                                                  'SegoeSemi',
                                                              fontSize: SizeConfig
                                                                      .horizontal *
                                                                  4.4),
                                                        ),
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      width: height * 0.100,
                                                      child: Text(
                                                        "(" +
                                                            controller
                                                                .pickUpDelivery[
                                                                    index]
                                                                .uomValue +
                                                            ")",
                                                        maxLines: 2,
                                                        style: TextStyle(
                                                            fontFamily:
                                                                'SegoeSemi',
                                                            fontSize: SizeConfig
                                                                    .horizontal *
                                                                4.0),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Spacer(),
                                              Expanded(
                                                child: Column(
                                                  // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    Text(
                                                      'Qty : ' +
                                                          controller
                                                              .pickUpDelivery[
                                                                  index]
                                                              .quantity
                                                              .toString(),
                                                      style: TextStyle(
                                                          fontSize: SizeConfig
                                                                  .horizontal *
                                                              3,
                                                          fontFamily:
                                                              'SegoeReg'),
                                                    ),
                                                    Spacer(),
                                                    Row(
                                                      children: [
                                                        Text(
                                                          '₹ ' +
                                                              functionCalvulate(
                                                                  controller
                                                                      .pickUpDelivery[
                                                                          index]
                                                                      .specialPrice
                                                                      .toString(),
                                                                  controller
                                                                      .pickUpDelivery[
                                                                          index]
                                                                      .quantity
                                                                      .toString()),
                                                          style: TextStyle(
                                                              fontSize: SizeConfig
                                                                      .horizontal *
                                                                  3.4,
                                                              fontFamily:
                                                                  'SegoeBold'),
                                                        ),
                                                        SizedBox(
                                                          width: SizeConfig
                                                                  .horizontal *
                                                              1,
                                                        ),
                                                        Stack(
                                                          children: [
                                                            Padding(
                                                              padding: EdgeInsets
                                                                  .symmetric(
                                                                      horizontal:
                                                                          SizeConfig.horizontal *
                                                                              0.3),
                                                              child: Text(
                                                                functionCalvulate(
                                                                    controller
                                                                        .pickUpDelivery[
                                                                            index]
                                                                        .price
                                                                        .toString(),
                                                                    controller
                                                                        .pickUpDelivery[
                                                                            index]
                                                                        .quantity
                                                                        .toString()),
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .black
                                                                        .withOpacity(
                                                                            0.6),
                                                                    fontSize:
                                                                        SizeConfig.horizontal *
                                                                            2.4,
                                                                    fontFamily:
                                                                        'PoppinsExtraLight'),
                                                              ),
                                                            ),
                                                            Positioned(
                                                              left: 0.0,
                                                              right: 0.0,
                                                              bottom: 0.0,
                                                              top: 0.0,
                                                              child:
                                                                  new RotationTransition(
                                                                turns:
                                                                    new AlwaysStoppedAnimation(
                                                                        15 /
                                                                            360),
                                                                child:
                                                                    Container(
                                                                  child:
                                                                      Divider(
                                                                    color: Colors
                                                                        .black
                                                                        .withOpacity(
                                                                            0.6),
                                                                  ),
                                                                ),
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              )
                                              // Expanded(
                                              //   child: Column(
                                              //     // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                              //     crossAxisAlignment:
                                              //         CrossAxisAlignment.end,
                                              //     mainAxisAlignment:
                                              //         MainAxisAlignment.end,
                                              //     children: [

                                              //     ],
                                              //   ),
                                              // )
                                            ],
                                          ),
                                        ),
                                      );
                                    });
                              }),
                        ),
                        SubHeading(
                          text: 'Customer Details',
                        ),
                        orderDetails(),
                        SizedBox(
                          height: SizeConfig.horizontal * 3,
                        ),
                        SizedBox(
                          height: height * 0.02,
                        ),
                        GetX<PickUpDeliveryController>(
                            init: PickUpDeliveryController(),
                            builder: (controller) {
                              return Visibility(
                                visible: controller.enableData.value,
                                child: Container(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      GestureDetector(
                                        onTap: () async {
                                          await showDialog(
                                              context: context,
                                              builder: (_) => AlertDialog(
                                                    title:
                                                        Text('Are you sure ?'),
                                                    actions: [
                                                      ElevatedButton(
                                                          onPressed: () {
                                                            Get.back();
                                                            PickUpDeliveryController
                                                                .to
                                                                .changeStatus(
                                                                    "rejected");
                                                          },
                                                          child: Text('Yes')),
                                                      TextButton(
                                                          onPressed: () =>
                                                              Get.back(),
                                                          child: Text('No'))
                                                    ],
                                                  ));
                                          //PickUpDeliveryController.to.changeStatus("rejected");
                                        },
                                        child: Container(
                                          height: SizeConfig.horizontal * 10,
                                          width: SizeConfig.horizontal * 35,
                                          decoration: BoxDecoration(
                                              boxShadow: [
                                                BoxShadow(
                                                    color: Colors.black
                                                        .withOpacity(0.07),
                                                    offset: Offset(0, 3),
                                                    blurRadius: 9)
                                              ],
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                              color: Colors.white),
                                          child: Center(
                                              child: Text(
                                            'Decline',
                                            style: TextStyle(
                                                fontSize:
                                                    SizeConfig.horizontal * 3.7,
                                                color: Colors.black,
                                                fontFamily: 'SegoeReg',
                                                fontWeight: FontWeight.w500),
                                          )),
                                        ),
                                      ),
                                      Container(
                                        width: SizeConfig.horizontal * 6,
                                      ),
                                      GestureDetector(
                                        onTap: () async {
                                          await showDialog(
                                              context: context,
                                              builder: (_) => AlertDialog(
                                                    title:
                                                        Text('Are you sure ?'),
                                                    actions: [
                                                      ElevatedButton(
                                                          onPressed: () {
                                                            PickUpDeliveryController
                                                                .to
                                                                .changeStatus(
                                                                    "accepted");
                                                            Get.back();
                                                          },
                                                          child: Text('Yes')),
                                                      TextButton(
                                                          onPressed: () =>
                                                              Get.back(),
                                                          child: Text('No'))
                                                    ],
                                                  ));
                                        },
                                        child: Container(
                                          height: SizeConfig.horizontal * 10,
                                          width: SizeConfig.horizontal * 35,
                                          decoration: BoxDecoration(
                                              boxShadow: [
                                                BoxShadow(
                                                    color: Colors.black
                                                        .withOpacity(0.1),
                                                    offset: Offset(0, 3),
                                                    blurRadius: 5)
                                              ],
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                              color: HexColor('#4D9EFF')),
                                          child: Center(
                                              child: Text(
                                            'Accept',
                                            style: TextStyle(
                                                fontSize:
                                                    SizeConfig.horizontal * 3.7,
                                                fontFamily: 'SegoeReg',
                                                fontWeight: FontWeight.w500,
                                                color: Colors.white),
                                          )),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            }),
                        Visibility(
                            visible: controller.enableData.value,
                            child: SizedBox(
                              height: height * 0.03,
                            )),
                        SubHeading(
                          text: 'Picked Address',
                        ),
                        Padding(
                            padding: EdgeInsets.symmetric(
                                vertical: SizeConfig.horizontal * 1.5),
                            child: Container(
                              alignment: Alignment.centerLeft,
                              width: SizeConfig.horizontal * 100,
                              decoration: BoxDecoration(
                                  color: HexColor('#FFFFFF'),
                                  borderRadius: BorderRadius.circular(5),
                                  boxShadow: myBoxShadow),
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: SizeConfig.horizontal * 5,
                                    vertical: SizeConfig.horizontal * 4),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Pick Up Address',
                                      style: TextStyle(
                                          color: Colors.black.withOpacity(0.7),
                                          fontSize: SizeConfig.horizontal * 3.2,
                                          fontFamily: 'SegoeReg'),
                                    ),
                                    Container(
                                      width: SizeConfig.horizontal * 34,
                                      child: Text(
                                        controller.pickUpAddress.value,
                                        style: TextStyle(
                                            fontSize:
                                                SizeConfig.horizontal * 3.5,
                                            fontFamily: 'SegoeReg'),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )),
                        SizedBox(
                          height: SizeConfig.horizontal * 3,
                        ),
                        SubHeading(
                          text: 'Seller Address',
                        ),
                        Padding(
                            padding: EdgeInsets.symmetric(
                                vertical: SizeConfig.horizontal * 1.5),
                            child: Container(
                              alignment: Alignment.centerLeft,
                              width: SizeConfig.horizontal * 100,
                              decoration: BoxDecoration(
                                  color: HexColor('#FFFFFF'),
                                  borderRadius: BorderRadius.circular(5),
                                  boxShadow: myBoxShadow),
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: SizeConfig.horizontal * 5,
                                    vertical: SizeConfig.horizontal * 4),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Seller Address',
                                      style: TextStyle(
                                          color: Colors.black.withOpacity(0.7),
                                          fontSize: SizeConfig.horizontal * 3.2,
                                          fontFamily: 'SegoeReg'),
                                    ),
                                    Container(
                                      width: SizeConfig.horizontal * 34,
                                      child: Text(
                                        controller.sellerAddress.value,
                                        style: TextStyle(
                                            fontSize:
                                                SizeConfig.horizontal * 3.5,
                                            fontFamily: 'SegoeReg'),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )),
                        SizedBox(
                          height: SizeConfig.horizontal * 3,
                        ),
                        Visibility(
                          visible: !controller.enableData.value,
                          child: SubHeading(
                            text: 'Pickup Status',
                          ),
                        ),
                        GetX<PickUpDeliveryController>(
                            init: PickUpDeliveryController(),
                            builder: (controller) {
                              return Visibility(
                                visible: !controller.enableData.value,
                                child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      vertical: SizeConfig.horizontal * 1.5),
                                  child: Container(
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          boxShadow: myBoxShadow,
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                      child: Container(
                                        margin: EdgeInsets.only(
                                            left: 10, right: 10),
                                        child: DropdownButton(
                                          isExpanded: true,
                                          underline: SizedBox(),
                                          icon: Icon(
                                            Icons.keyboard_arrow_down_rounded,
                                            color: colorblack,
                                          ),
                                          hint: Text(
                                            controller.currentValue.value,
                                            style: stylBlackFont13,
                                          ),
                                          onChanged: (value) {
                                            controller
                                                .changeCn(value.toString());
                                          },
                                          items: !controller.changeBtn.value
                                              ? creditType
                                                  .map((e) => DropdownMenuItem(
                                                      value: e,
                                                      child:
                                                          Text(e.toString())))
                                                  .toList()
                                              : creditTypeSyb
                                                  .map((e) => DropdownMenuItem(
                                                      value: e,
                                                      child:
                                                          Text(e.toString())))
                                                  .toList(),
                                        ),
                                      )
                                      // ListTileTheme(
                                      //   // tileColor:  HexColor('#DFDCDC'),
                                      //   dense: true,
                                      //   child: Container(
                                      //     decoration: BoxDecoration(
                                      //         // color: HexColor('#DFDCDC'),
                                      //         boxShadow: myBoxShadow,
                                      //         borderRadius: BorderRadius.circular(5)),
                                      //     child: ExpansionTileCard(
                                      //       shadowColor: Colors.transparent,
                                      //       baseColor: Colors.white,
                                      //       key: transactionKey,
                                      //       // expandedColor: HexColor('#DFDCDC'),
                                      //       title: Text(
                                      //         transactionElement,
                                      //         style: TextStyle(
                                      //             fontWeight: FontWeight.w500,
                                      //             fontSize: SizeConfig.horizontal * 3.5,
                                      //             fontFamily: 'SegoeReg'),
                                      //       ),
                                      //       //key: stateGlobalKey,
                                      //       children: [
                                      //         // ListTile(
                                      //         //   title: Text(
                                      //         //     'Pickup Status',
                                      //         //     style: TextStyle(
                                      //         //         fontWeight: FontWeight.w500,
                                      //         //         fontSize: SizeConfig.horizontal * 3.5,
                                      //         //         fontFamily: 'SegoeReg'),
                                      //         //   ),
                                      //         //   onTap: () {
                                      //         //     // setState(() {
                                      //         //     //   this.transactionElement = 'Pickup Status';
                                      //         //     //   transactionKey.currentState!.collapse();
                                      //         //     // });
                                      //         //   },
                                      //         // ),
                                      //         DropdownButton(
                                      //           isExpanded: true,
                                      //           underline: SizedBox(),
                                      //           icon: Icon(
                                      //             Icons.keyboard_arrow_down_rounded,
                                      //             color: colorblack,
                                      //           ),
                                      //           hint: Text(
                                      //             controller.currentValue.value,
                                      //             style: stylBlackFont13,
                                      //           ),
                                      //           onChanged: (value) {
                                      //             controller.changeCn(value.toString());
                                      //           },
                                      //           items: creditType
                                      //               .map((e) => DropdownMenuItem(
                                      //               value: e, child: Text(e.toString())))
                                      //               .toList(),
                                      //         )
                                      //       ],
                                      //     ),
                                      //   ),
                                      // ),
                                      ),
                                ),
                              );
                            }),
                        SizedBox(
                          height: SizeConfig.horizontal * 3,
                        ),
                        SubHeading(
                          text: 'Returned Reason',
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: SizeConfig.horizontal * 1.5),
                          child: Container(
                            alignment: Alignment.centerLeft,
                            width: SizeConfig.horizontal * 100,
                            decoration: BoxDecoration(
                                color: HexColor('#FFFFFF'),
                                borderRadius: BorderRadius.circular(5),
                                boxShadow: myBoxShadow),
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: SizeConfig.horizontal * 3,
                                  vertical: SizeConfig.horizontal * 4),
                              child: Text(
                                controller.cancelledReson.value,
                                style: TextStyle(
                                    fontSize: SizeConfig.horizontal * 3.2,
                                    fontFamily: 'SegoeReg'),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: SizeConfig.horizontal * 3,
                        ),
                        SubHeading(
                          text: 'Comments',
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: SizeConfig.horizontal * 1.5),
                          child: Container(
                            alignment: Alignment.centerLeft,
                            width: SizeConfig.horizontal * 100,
                            decoration: BoxDecoration(
                                color: HexColor('#FFFFFF'),
                                borderRadius: BorderRadius.circular(5),
                                boxShadow: myBoxShadow),
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: SizeConfig.horizontal * 3,
                                  vertical: SizeConfig.horizontal * 4),
                              child: Text(
                                controller.comments.value,
                                style: TextStyle(
                                    fontSize: SizeConfig.horizontal * 3.2,
                                    fontFamily: 'SegoeReg'),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: height * 0.01,
                        ),
                        SubHeading(
                          text: 'Status : ' + controller.status.value,
                        ),
                        AbsorbPointer(
                          absorbing: controller.status.value == "Collected" ||
                              controller.status.value == "Submitted",
                          child: Visibility(
                            visible: controller.enableSubmitBtn.value,
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: SizeConfig.horizontal * 5),
                              child: Center(
                                child: Container(
                                  child: Column(
                                    children: [
                                      SubHeading(
                                        text: 'Add images',
                                      ),
                                      SizedBox(
                                        height: SizeConfig.horizontal * 3,
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          addShowDialog(context);
                                          // PickUpDeliveryController.to
                                          //     .getImage(ImageSource.camera);
                                        },
                                        child: Container(
                                          alignment: Alignment.center,
                                          height: SizeConfig.horizontal * 26,
                                          width: SizeConfig.horizontal * 33,
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                              boxShadow: [
                                                BoxShadow(
                                                    color: Colors.black
                                                        .withOpacity(0.15),
                                                    offset: Offset(0, 0),
                                                    blurRadius: 6)
                                              ]),
                                          //  child:controller.pickUpImgBoo.value? Image.file(File(controller.pickUpImg!.path),  height: height*0.09, width: height*0.07,):Image.asset(
                                          //    'assets/images/metro-image.png',
                                          //    height: SizeConfig.horizontal * 10,
                                          // ),
                                          child: (controller.pickUpImgBoo.value)
                                              ? Image.file(
                                                  File(controller
                                                      .pickUpImg!.path),
                                                  height: height * 0.09,
                                                  width: height * 0.07,
                                                )
                                              : (controller.enableProduct.value)
                                                  ? Image.network(
                                                      controller
                                                          .prodctImg.value,
                                                      height: height * 0.09,
                                                      width: height * 0.07,
                                                    )
                                                  : Image.asset(
                                                      'assets/images/metro-image.png',
                                                      height: SizeConfig
                                                              .horizontal *
                                                          10,
                                                    ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: height * 0.01,
                        ),
                        Visibility(
                          visible: controller.enableSubmitBtn.value,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SubHeading(
                                text: 'Add Signature',
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: height * 0.01,
                        ),
                        SizedBox(
                          height: SizeConfig.horizontal * 1,
                        ),
                        Visibility(
                          visible: controller.enableSubmitBtn.value,
                          child: Container(
                              height: (controller.status.value == "Submitted" ||
                                      controller.status.value == "Collected")
                                  ? 190
                                  : 240,
                              child: (controller.enableProduct.value)
                                  ? Container(
                                      width: Get.height,
                                      color: Colors.blue,
                                      child: Center(
                                          child: Image.network(
                                        controller.prodctSignature.value,
                                        height: height * 0.15,
                                        width: height * 0.12,
                                      )))
                                  : AbsorbPointer(
                                      absorbing: (controller.status.value ==
                                              "Submitted" ||
                                          controller.status.value ==
                                              "Collected"),
                                      child: controller.prodctSignature.value ==
                                              "null"
                                          ? Image.network(
                                              controller.prodctSignature.value)
                                          : SignatureField(
                                              flag: controller.status.value,
                                            ),
                                    )),
                        ),
                        SizedBox(
                          height: SizeConfig.horizontal * 3,
                        ),
                        controller.status.value.toLowerCase() ==
                                "Submitted".toLowerCase()
                            ? SizedBox()
                            : Visibility(
                                visible: controller.enableSubmitBtn.value,
                                child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: SizeConfig.horizontal * 3,
                                      vertical: SizeConfig.horizontal * 3),
                                  child: GestureDetector(
                                    onTap: () {
                                      PickUpDeliveryController.to.pickUpData();
                                    },
                                    child: Container(
                                      alignment: Alignment.center,
                                      height: SizeConfig.horizontal * 11,
                                      decoration: BoxDecoration(
                                          color: HexColor('#4D9EFF'),
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          boxShadow: [
                                            BoxShadow(
                                                color: Colors.black
                                                    .withOpacity(0.1),
                                                offset: Offset(0, 3),
                                                blurRadius: 11)
                                          ]),
                                      child: Text(
                                        'Update status ',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize:
                                                SizeConfig.horizontal * 3.8,
                                            fontFamily: 'SegoeReg'),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                      ],
                    );
                  }),
            ),
          ),
          Center(child: GetX<PickUpDeliveryController>(builder: (controller) {
            return Visibility(
                visible: controller.loadIng.value,
                child: Container(
                    child: (CircularProgressIndicator(
                  valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue),
                ))));
          }))
        ],
      ),
    );
  }
  addShowDialog(BuildContext context){
    return showDialog(context: context, builder: (context)=>
        SimpleDialog(
          children: [
            SimpleDialogOption(
              onPressed: (){
                PickUpDeliveryController.to
                    .getImage(ImageSource.gallery);

              },
              child: Row(children: const [
                Icon(Icons.image),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text('Gallery'),
                )
              ],),
            ),
            SimpleDialogOption(
              onPressed: (){
                PickUpDeliveryController.to
                    .getImage(ImageSource.camera);
              },
              child: Row(children: const [
                Icon(Icons.camera),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text('Camera'),
                )
              ],),
            ),
            SimpleDialogOption(
              onPressed: (){
                Get.back();
              },
              child: Row(children: const [
                Icon(Icons.close),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text('Close'),
                )
              ],),
            )
          ],
        ));
  }

  Padding orderDetails() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: SizeConfig.horizontal * 2),
      child: GetX<PickUpDeliveryController>(
          init: PickUpDeliveryController(),
          builder: (controller) {
            return Container(
              alignment: Alignment.center,
              width: SizeConfig.horizontal * 100,
              decoration: BoxDecoration(
                  color: HexColor('#FFFFFF'),
                  borderRadius: BorderRadius.circular(5),
                  boxShadow: myBoxShadow),
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: SizeConfig.horizontal * 4,
                    vertical: SizeConfig.horizontal * 3),
                child: Container(
                  height: SizeConfig.horizontal * 13,
                  child: Row(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Return ID',
                            maxLines: 1,
                            style: TextStyle(
                                fontSize: SizeConfig.horizontal * 3.3,
                                fontFamily: 'SegoeSemi'),
                          ),
                          SizedBox(height: SizeConfig.horizontal * 0.5),
                          Container(
                            width: height * 0.1,
                            child: Text(
                              controller.name.value,
                              maxLines: 1,
                              style: TextStyle(
                                  fontSize: SizeConfig.horizontal * 3.5,
                                  fontFamily: 'SegoeSemi'),
                            ),
                          ),
                          SizedBox(height: SizeConfig.horizontal * 0.5),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: SizeConfig.horizontal * 2),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              ':',
                              style: TextStyle(
                                  fontSize: SizeConfig.horizontal * 3.3,
                                  fontFamily: 'SegoeReg'),
                            ),
                            SizedBox(height: SizeConfig.horizontal * 0.5),
                            Text(
                              ':',
                              style: TextStyle(
                                  fontSize: SizeConfig.horizontal * 3.3,
                                  fontFamily: 'SegoeReg'),
                            ),
                            SizedBox(height: SizeConfig.horizontal * 0.5),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              controller.returnId.value,
                              style: TextStyle(
                                  fontSize: SizeConfig.horizontal * 3.3,
                                  fontFamily: 'SegoeSemi'),
                            ),
                            SizedBox(height: SizeConfig.horizontal * 0.5),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  controller.mob.value,
                                  style: TextStyle(
                                      fontSize: SizeConfig.horizontal * 3.5,
                                      fontFamily: 'SegoeReg'),
                                ),
                                // Padding(
                                //   padding: EdgeInsets.only(
                                //       right: SizeConfig.horizontal * 5),
                                //   child: Text(
                                //     controller.status.value,
                                //     style: TextStyle(
                                //       fontSize: SizeConfig.horizontal * 3.3,
                                //       fontFamily: 'SegoeSemi',
                                //     ),
                                //   ),
                                // )
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          }),
    );
  }
}
