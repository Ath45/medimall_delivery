import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/PickupProductController/PickupProductController.dart';
import 'package:medimallDelivery/Routes/routes.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/util/constants.dart';
import 'package:medimallDelivery/util/constantsDC.dart';
import 'package:medimallDelivery/util/contantRiyas.dart';
import 'package:medimallDelivery/widgets/myAppbar.dart';
import 'package:recase/recase.dart';

class PickedUpProduct extends StatelessWidget {
  final PickupProductController controller = Get.put(PickupProductController());
  final TextStyle styleRegGrey = TextStyle(
      color: Color.fromRGBO(98, 98, 100, 1),
      fontSize: 15,
      fontFamily: "SegoeReg");

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    // PickupProductController.to.getData();
    return Scaffold(
      backgroundColor: HexColor('#FAFAFA'),
      resizeToAvoidBottomInset: false,
      appBar: myAppbar(context, 'Return product'),
      body: Stack(
        children: [
          Column(
            children: [
              SizedBox(
                height: height * .03,
              ),
              SearchPeriod(),
              SizedBox(
                height: height * .02,
              ),
              CreditSession(),
              Container(
                height: height * 0.500,
                child: GetX<PickupProductController>(
                    init: PickupProductController(),
                    builder: (controller) {
                      return ListView.builder(
                        padding: EdgeInsets.symmetric(
                            vertical: SizeConfig.horizontal * 3,
                            horizontal: SizeConfig.horizontal * 2.5),
                        shrinkWrap: true,
                        itemCount: controller.orderData.length,
                        itemBuilder: (context, index) {
                          ReCase sample =
                              new ReCase(controller.orderData[index].status);
                          return Container(
                            child: GestureDetector(
                              onTap: () {
                                Get.toNamed(pickupProduct, arguments: [
                                  controller.orderData[index].orderObjectId
                                ]);
                              },
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    vertical: SizeConfig.horizontal * 2),
                                child: Container(
                                  alignment: Alignment.center,
                                  width: SizeConfig.horizontal * 100,
                                  decoration: BoxDecoration(
                                      color: HexColor('#FFFFFF'),
                                      borderRadius: BorderRadius.circular(5),
                                      boxShadow: myBoxShadow),
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: SizeConfig.horizontal * 4,
                                        vertical: SizeConfig.horizontal * 3),
                                    child: Container(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            controller
                                                .orderData[index].userName,
                                            style: TextStyle(
                                                color: HexColor('#4D9EFF'),
                                                fontSize:
                                                    SizeConfig.horizontal * 4,
                                                fontFamily: 'SegoeSemi'),
                                          ),
                                          SizedBox(
                                              height:
                                                  SizeConfig.horizontal * 1),
                                          Row(
                                            children: [
                                              Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    'Return ID',
                                                    style: TextStyle(
                                                        fontSize: SizeConfig
                                                                .horizontal *
                                                            3.3,
                                                        fontFamily: 'SegoeReg'),
                                                  ),
                                                  SizedBox(
                                                      height: SizeConfig
                                                              .horizontal *
                                                          0.5),
                                                  Container(
                                                    //margin: EdgeInsets.only(left:height*0.1),
                                                    child: Text(
                                                      'Product',
                                                      maxLines: 1,
                                                      style: TextStyle(
                                                          fontSize: SizeConfig
                                                                  .horizontal *
                                                              3.3,
                                                          fontFamily:
                                                              'SegoeSemi'),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal:
                                                        SizeConfig.horizontal *
                                                            2),
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      ':',
                                                      style: TextStyle(
                                                          fontSize: SizeConfig
                                                                  .horizontal *
                                                              3.3,
                                                          fontFamily:
                                                              'SegoeReg'),
                                                    ),
                                                    SizedBox(
                                                        height: SizeConfig
                                                                .horizontal *
                                                            0.5),
                                                    Text(
                                                      '',
                                                      style: TextStyle(
                                                          fontSize: SizeConfig
                                                                  .horizontal *
                                                              3.3,
                                                          fontFamily:
                                                              'SegoeReg'),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Expanded(
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: [
                                                        Text(
                                                          controller
                                                              .orderData[index]
                                                              .returnId,
                                                          style: TextStyle(
                                                              fontSize: SizeConfig
                                                                      .horizontal *
                                                                  3.3,
                                                              fontFamily:
                                                                  'SegoeSemi'),
                                                        ),
                                                        Padding(
                                                          padding: EdgeInsets.only(
                                                              right: SizeConfig
                                                                      .horizontal *
                                                                  5),
                                                          child: Text(
                                                            sample.sentenceCase,
                                                            style: TextStyle(
                                                              fontSize: SizeConfig
                                                                      .horizontal *
                                                                  3.3,
                                                              fontFamily:
                                                                  'SegoeReg',
                                                            ),
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                    SizedBox(
                                                        height: SizeConfig
                                                                .horizontal *
                                                            0.5),
                                                    Row(
                                                      children: [
                                                        Container(
                                                          child: Text(
                                                            controller
                                                                        .orderData[
                                                                            index]
                                                                        .products[
                                                                            0]
                                                                        .productName
                                                                        .length >
                                                                    31
                                                                ? '${controller.orderData[index].products[0].productName.substring(0, 31)}...'
                                                                : controller
                                                                    .orderData[
                                                                        index]
                                                                    .products[0]
                                                                    .productName,
                                                            maxLines: 1,
                                                            style: TextStyle(
                                                                fontSize: SizeConfig
                                                                        .horizontal *
                                                                    3.3,
                                                                fontFamily:
                                                                    'SegoeSemi'),
                                                          ),
                                                        ),
                                                        Text(
                                                          " [" +
                                                              controller
                                                                  .orderData[
                                                                      index]
                                                                  .productCount +
                                                              "] ",
                                                          maxLines: 1,
                                                          style: TextStyle(
                                                              fontSize: SizeConfig
                                                                      .horizontal *
                                                                  3.3,
                                                              fontFamily:
                                                                  'SegoeSemi'),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          );
                        },
                      );
                    }),
              ),
            ],
          ),
          Center(child: GetX<PickupProductController>(builder: (controller) {
            print("data" + controller.rxEmpty.value.toString());
            return new Center(
                child: Visibility(
                    visible: controller.rxEmpty.value,
                    child: Container(
                        child: Text(
                      "No Data Available.....",
                      style: styleRegGrey,
                    ))));
          })),
          Center(child: GetX<PickupProductController>(builder: (controller) {
            return Visibility(
                visible: controller.loadIng.value,
                child: Container(
                    child: (CircularProgressIndicator(
                  valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue),
                ))));
          }))
        ],
      ),
    );
  }
}

class SearchPeriod extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var height = SizeConfig.screenHeight * 1.225;
    var width = SizeConfig.screenWidth * 1.225;
    var fontsize15 = height * .012;
    var styleRegGrey = TextStyle(
        color: Color.fromRGBO(98, 98, 100, 1),
        fontSize: fontsize15,
        fontFamily: "SegoeReg");
    var styleRegGrey70 = TextStyle(
        color: Color.fromRGBO(98, 98, 100, .7),
        fontSize: fontsize15,
        fontFamily: "SegoeReg");
    return GetX<PickupProductController>(
        init: PickupProductController(),
        builder: (controller) {
          print("name" + controller.productName.value);
          return Container(
            margin: EdgeInsets.symmetric(horizontal: width * .04),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: height * 0.01,
                ),
                Container(
                  width: Get.width,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  child: Container(
                    child: Row(
                      children: [
                        Container(
                            margin: EdgeInsets.only(left: 10),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0))),
                            width: SizeConfig.horizontal * 80,
                            child: TextField(
                              decoration: InputDecoration(hintText: "Search"),
                              onChanged: (value) {
                                // if(value.isEmpty)
                                //   {
                                //     PickupProductController.to.stopLoading();
                                //   }else{
                                PickupProductController.to.getSearch(value);
                                // }
                              },
                            )),
                        Center(
                          child: Container(
                            child: ImageIcon(
                              AssetImage('assets/icons/search.png'),
                              size: SizeConfig.horizontal * 5,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: height * 0.02,
                ),
              ],
            ),
          );
        });
  }
}

class CreditSession extends StatelessWidget {
  List creditType = [
    "New orders",
    "Accepted Orders",
    "Collect Orders",
    "All Orders",
  ];
  String cerditValue = "";

  @override
  Widget build(BuildContext context) {
    ///Local variables<<------------------------------------------------------>>
    var height = SizeConfig.screenHeight * 1.225;
    var width = SizeConfig.screenWidth * 1.225;
    var stylBlackFont13 =
        TextStyle(color: colorblack, fontSize: 13, fontFamily: "SegoeReg");

    ///Local variables<<------------------------------------------------------>>
    return Container(
      child: Column(
        children: [
          GetX<PickupProductController>(
              init: PickupProductController(),
              builder: (controller) {
                var currentValue;
                return Container(
                  height: height * .43 * .122,
                  width: width,
                  margin: EdgeInsets.symmetric(horizontal: width * .025),
                  padding: EdgeInsets.symmetric(
                      horizontal: width * .025, vertical: height * .01),
                  decoration: BoxDecoration(
                      color: fillColor,
                      borderRadius: BorderRadius.circular(height * .004),
                      boxShadow: shadow),
                  child: DropdownButton(
                    isExpanded: true,
                    underline: SizedBox(),
                    icon: Icon(
                      Icons.keyboard_arrow_down_rounded,
                      color: colorblack,
                    ),
                    hint: Text(
                      PickupProductController.to.currentValue.value,
                      style: stylBlackFont13,
                    ),
                    onChanged: (value) {
                      PickupProductController.to.changeCn(value.toString());
                    },
                    items: creditType
                        .map((e) => DropdownMenuItem(
                            value: e, child: Text(e.toString())))
                        .toList(),
                  ),
                );
              }),
          SizedBox(
            height: height * .01,
          ),
          // Container(
          //   height: height * .155 * 3,
          //   child: ListView.builder(
          //     physics: NeverScrollableScrollPhysics(),
          //     itemCount: 1,
          //     itemBuilder: (context, index) {
          //       return CreditBox();
          //     },
          //   ),
          // )
        ],
      ),
    );
  }
}
