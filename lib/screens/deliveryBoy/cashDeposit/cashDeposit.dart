import 'package:expansion_tile_card/expansion_tile_card.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/CashDepositController/CashDepositController.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/util/constants.dart';
import 'package:medimallDelivery/util/contantRiyas.dart';
import 'package:medimallDelivery/widgets/myAppbar.dart';
import 'package:medimallDelivery/widgets/myButton.dart';

class CashDeposit extends StatelessWidget {
  final GlobalKey<ExpansionTileCardState> statusKey = new GlobalKey();

  String selectStatus = 'Picked Up';
  String selectStatus1 = 'Picked Up';

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    CashDepositController.to.getData();
    return Scaffold(
      backgroundColor: HexColor('#FAFAFA'),
      appBar: myAppbar(context, 'Cash Deposit'),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Container(
              child: Column(
                children: [
                  banner(),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: SizeConfig.screenWidth * .0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: SizeConfig.horizontal * 3,
                        ),
                        titleText("Pay the collected amount through"),
                        SizedBox(
                          height: SizeConfig.horizontal * 3,
                        ),
                        upiBlock(),
                        SizedBox(
                          height: SizeConfig.horizontal * 3,
                        ),
                        titleText(
                          'Bank Details?',
                        ),
                        SizedBox(
                          height: SizeConfig.horizontal * 3,
                        ),
                        bankDetails(),
                        SizedBox(
                          height: SizeConfig.horizontal * 3,
                        ),
                        SizedBox(
                          height: SizeConfig.horizontal * 3,
                        ),
                        titleText(
                          'How it Works?',
                        ),
                        SizedBox(
                          height: SizeConfig.horizontal * 3,
                        ),
                        Column(
                            children: [0]
                                .map((e) => howItsWork("UPI ID or Bank Account",
                                "Make Payment to the Above UPI ID or Bank Account Share Us The Reference ID of Your Transaction through WhatsappWe’ll Verify the Shared Reference ID We’ll Update Your Wallet"))
                                .toList()),
                        SizedBox(
                          height: SizeConfig.horizontal * 3,
                        ),
                        titleText(
                          'Frequently Asked Questions',
                        ),
                        SizedBox(
                          height: SizeConfig.horizontal * 3,
                        ),
                        Column(
                          children: [0, 1, 2, 3, 4]
                              .map(
                                (e) => faqBlock("what is UPI",
                                "Lorem ipsum dolor sit amet consectetur dolor sit amet conseadipiscing elit present anti"),
                          )
                              .toList(),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Center(child: GetX<CashDepositController>(builder: (controller) {
            return Visibility(
                visible: controller.loadIng.value,
                child: Container(
                    child: (CircularProgressIndicator(
                      valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue),
                    ))));
          }))
        ],
      ),
    );
  }

  Container upiBlock() {
   // SizeConfig().init(context);
    return Container(
      child: GetX<CashDepositController>(
          init: CashDepositController(),
          builder: (controller) {
          return Container(
            height: SizeConfig.vertical * 9,
            child: Stack(
              children: [
                Positioned(
                  child: Center(
                    child: Container(
                      height: SizeConfig.screenHeight * .02,
                      decoration:
                          BoxDecoration(color: HexColor("#00A8FF").withOpacity(.08)),
                    ),
                  ),
                ),
                Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: SizeConfig.screenWidth * .04),
                  child: Container(
                    height: SizeConfig.vertical * 9,
                    width: SizeConfig.horizontal * 100,
                    decoration: BoxDecoration(
                        color: HexColor('#FFFFFF'),
                        borderRadius: BorderRadius.circular(8),
                        boxShadow: myBoxShadow),
                    child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: SizeConfig.horizontal * 3),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              SizedBox(
                                width: SizeConfig.screenWidth * .015,
                              ),
                              Image.asset(
                                "assets/icons/ic_upi_payment.png",
                                height: SizeConfig.screenHeight * .03,
                              ),
                            ],
                          ),
                          SizedBox(
                            width: SizeConfig.blockSizeHorizontal * .05,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                controller.upiId.value,
                                style: TextStyle(
                                  fontFamily: 'SegoeReg',
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: SizeConfig.horizontal * 4,
                                ),
                              ),
                              SizedBox(
                                width: SizeConfig.screenWidth * .04,
                              ),
                              Image.asset(
                                "assets/icons/ic_report.png",
                                height: SizeConfig.screenHeight * .04,
                              ),
                              SizedBox(
                                width: SizeConfig.blockSizeHorizontal * .01,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          );
        }
      ),
    );
  }

  Padding howItsWork(String title, subTitle) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: SizeConfig.screenWidth * .04),
      child: Container(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              CircleAvatar(
                radius: 3.0,
                backgroundColor: Colors.black,
              ),
              SizedBox(
                width: SizeConfig.screenWidth * .02,
              ),
              Text(
                title,
                style: TextStyle(
                  fontFamily: 'SegoeReg',
                  color: HexColor("#000000").withOpacity(.8),
                  fontWeight: FontWeight.bold,
                  fontSize: SizeConfig.screenHeight * .02,
                ),
              ),
            ],
          ),
          SizedBox(
            width: SizeConfig.screenWidth * .02,
            height: SizeConfig.screenHeight * .008,
          ),
          Padding(
            padding: EdgeInsets.only(left: SizeConfig.screenWidth * .04),
            child: Text(
              subTitle,
              style: TextStyle(
                fontFamily: 'SegoeReg',
                color: HexColor("#000000").withOpacity(.7),
                fontSize: SizeConfig.screenHeight * .015,
              ),
            ),
          ),
          SizedBox(
            width: SizeConfig.screenWidth * .02,
            height: SizeConfig.screenHeight * .01,
          ),
        ],
      )),
    );
  }

  Padding faqBlock(String title, subTitle) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: SizeConfig.screenWidth * .04),
      child: Container(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Image.asset(
                "assets/icons/ic_upi_payment.png",
                height: SizeConfig.screenHeight * .01,
              ),
              SizedBox(
                width: SizeConfig.screenWidth * .02,
              ),
              Text(
                title,
                style: TextStyle(
                  fontFamily: 'SegoeReg',
                  color: HexColor("#000000").withOpacity(.8),
                  fontWeight: FontWeight.bold,
                  fontSize: SizeConfig.screenHeight * .02,
                ),
              ),
            ],
          ),
          SizedBox(
            width: SizeConfig.screenWidth * .02,
            height: SizeConfig.screenHeight * .005,
          ),
          Padding(
            padding: EdgeInsets.only(left: SizeConfig.screenWidth * .08),
            child: Text(
              subTitle,
              style: TextStyle(
                fontFamily: 'SegoeReg',
                color: HexColor("#000000").withOpacity(.7),
                fontSize: SizeConfig.screenHeight * .015,
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig.screenHeight * .02,
          ),
        ],
      )),
    );
  }

  Container bankDetails() {
    //SizeConfig().init(context);
    var height = SizeConfig.screenHeight * 1.225;
    var width = SizeConfig.screenWidth * 1.225;
    var colorblacklighter = HexColor("#000000");
    var fontsize12 = height * .013;
    var colorblack = Color.fromRGBO(0, 0, 0, 1);
    var shadow = [
      BoxShadow(color: Color.fromRGBO(0, 0, 0, .07), blurRadius: 10)
    ];

    var style = TextStyle(
        fontSize: fontsize12, fontFamily: "SegoeReg,", color: colorblack);

    var styleLight = TextStyle(
        fontSize: fontsize12,
        fontFamily: "SegoeReg,",
        fontWeight: FontWeight.bold,
        color: colorblacklighter);

    return Container(
      height: height * .16,
      child: Stack(
        children: [
          Positioned(
            child: Center(
              child: Container(
                height: SizeConfig.screenHeight * .02,
                decoration:
                    BoxDecoration(color: HexColor("#00A8FF").withOpacity(.08)),
              ),
            ),
          ),
          Padding(
            padding:
                EdgeInsets.symmetric(horizontal: SizeConfig.screenWidth * .04),
            child: Container(
              height: height * .16,
              width: width * .9,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(3),
                  boxShadow: shadow),
              child:  GetX<CashDepositController>(
                  init: CashDepositController(),
                  builder: (controller) {
                  return Row(
                    children: [
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            bankDetailElements(
                                'Bank', controller.bankNode.value, style, styleLight),
                            SizedBox(
                              height: height * .01,
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Image.asset(
                          "assets/icons/ic_bank.png",
                          height: SizeConfig.screenHeight * .12,
                        ),
                      ),
                    ],
                  );
                }
              ),
            ),
          ),
        ],
      ),
    );
  }

  Container bankDetailElements(
      String title, String text, var style, var styleLight) {
   // SizeConfig().init(context);
    var height = SizeConfig.screenHeight * 1.225;
    var width = SizeConfig.screenWidth * 1.225;
    return Container(
        width: 200,
        child: Row(
          children: [
            SizedBox(
              width: width * .02,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: TextStyle(
                      fontSize: SizeConfig.horizontal * 3.2,
                      fontFamily: 'SegoeSemi'),
                ),
                SizedBox(
                  height: height * .01,
                ),
                Text(
                  "Name",
                  style: TextStyle(
                      fontSize: SizeConfig.horizontal * 3.2,
                      fontFamily: 'SegoeSemi'),
                ),
                SizedBox(
                  height: height * .01,
                ),
                Text(
                  "AC No",
                  style: TextStyle(
                      fontSize: SizeConfig.horizontal * 3.2,
                      fontFamily: 'SegoeSemi'),
                ),
                SizedBox(
                  height: height * .01,
                ),
                Text(
                  "IFSC",
                  style: TextStyle(
                      fontSize: SizeConfig.horizontal * 3.2,
                      fontFamily: 'SegoeSemi'),
                ),
              ],
            ),
            SizedBox(
              width: width * .01,
            ),
            Padding(
              padding:
                  EdgeInsets.symmetric(horizontal: SizeConfig.horizontal * 1),
              child: Column(
                children: [
                  Text(
                    ':',
                    style: TextStyle(
                        fontSize: SizeConfig.horizontal * 3.2,
                        fontFamily: 'SegoeReg'),
                  ),
                  SizedBox(
                    height: height * .01,
                  ),
                  Text(
                    ':',
                    style: TextStyle(
                        fontSize: SizeConfig.horizontal * 3.2,
                        fontFamily: 'SegoeReg'),
                  ),
                  SizedBox(
                    height: height * .01,
                  ),
                  Text(
                    ':',
                    style: TextStyle(
                        fontSize: SizeConfig.horizontal * 3.2,
                        fontFamily: 'SegoeReg'),
                  ),
                  SizedBox(
                    height: height * .01,
                  ),
                  Text(
                    ':',
                    style: TextStyle(
                        fontSize: SizeConfig.horizontal * 3.2,
                        fontFamily: 'SegoeReg'),
                  ),
                ],
              ),
            ),
            SizedBox(
              width: width * .01,
            ),
            GetX<CashDepositController>(
                init: CashDepositController(),
                builder: (controller) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      text,
                      style: TextStyle(
                          fontSize: SizeConfig.horizontal * 3,
                          fontFamily: 'SegoeReg'),
                    ),
                    SizedBox(
                      height: height * .01,
                    ),
                    Container(
                      width: width * .222,
                      child: Text(
                        controller.nameN.value,
                        style: TextStyle(
                            fontSize: SizeConfig.horizontal * 3,
                            fontFamily: 'SegoeReg'
                        ),
                        maxLines: 1,
                      ),
                    ),
                    SizedBox(
                      height: height * .01,
                    ),
                    Text(
                      controller.accountNum.value,
                      style: TextStyle(
                          fontSize: SizeConfig.horizontal * 3,
                          fontFamily: 'SegoeReg'),
                    ),
                    SizedBox(
                      height: height * .01,
                    ),
                    Text(
                      controller.ifscCode.value,
                      style: TextStyle(
                          fontSize: SizeConfig.horizontal * 3,
                          fontFamily: 'SegoeReg'),
                    ),
                  ],
                );
              }
            ),
          ],
        ));
  }

  Padding titleText(String titleText) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: SizeConfig.screenWidth * .04),
      child: Text(
        titleText,
        style: TextStyle(
            fontSize: SizeConfig.horizontal * 3.4,
            color: HexColor('#000000'),
            fontFamily: 'SegoeSemi'),
      ),
    );
  }

  Container banner() {
  //  SizeConfig().init(context);
    var height = SizeConfig.screenHeight * 1.225;
    var width = SizeConfig.screenWidth * 1.225;
    return Container(
      child:  GetX<CashDepositController>(
          init: CashDepositController(),
          builder: (controller) {
          return Container(
            height: SizeConfig.screenHeight * .25,
            width: double.infinity,
            decoration: new BoxDecoration(
              image: new DecorationImage(
                image: new ExactAssetImage('assets/images/bg_deposit_banner.png'),
                fit: BoxFit.fill,
              ),
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: width * .04,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: height * .02),
                  Text(
                    "Due Amount",
                    style: TextStyle(
                        color: HexColor("#FFFFFF").withOpacity(.8),
                        fontSize: SizeConfig.screenHeight * .015),
                  ),
                  SizedBox(
                    height: height * .005,
                  ),
                  Text(
                    "₹ "+controller.totalDueAmt.value,
                    style: TextStyle(
                        color: HexColor("#FFFFFF"),
                        fontSize: SizeConfig.screenHeight * .04,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: height * .005,
                  ),
                  Text(
                    "Pay now and keep riding",
                    style: TextStyle(
                        color: HexColor("#FFFFFF"),
                        fontSize: SizeConfig.screenHeight * .015),
                  ),
                  SizedBox(
                    height: height * .02,
                  ),
                  Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: SizeConfig.screenHeight * .02),
                    child: MyButtonWhite(text: 'Transaction History'),
                  ),
                ],
              ),
            ),
          );
        }
      ),
    );
  }
}
