import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/HelpAndSupportController/HelpAndSupportController.dart';
import 'package:medimallDelivery/util/constantsDC.dart';
import 'package:medimallDelivery/util/constants.dart';
import 'package:medimallDelivery/widgets/myAppbar.dart';
import 'package:medimallDelivery/widgets/myButton.dart';

class HelpAndSupport extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var height = SizeConfig.screenHeight * 1.225;
    var width = SizeConfig.screenWidth * 1.225;
    var fontsize13 = height * .013;
    var stylBlackFont13Semi = TextStyle(
        color: colorblack, fontSize: fontsize13, fontFamily: "SegoeSemi");
    return Scaffold(
      appBar: myAppbar(context, "Help & Support"),
      body: Stack(
        children: [
          ListView(
            children: [
              SizedBox(
                height: height * .02,
              ),
              Container(
                height: height * .42,
                padding: EdgeInsets.all(height * .01),
                margin: EdgeInsets.symmetric(horizontal: width * .025),
                decoration: BoxDecoration(
                  color: fillColor,
                  boxShadow: shadow,
                  borderRadius: BorderRadius.circular(height * .007),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: height * .01,
                    ),
                    Text(
                      "Issue related",
                      style: stylBlackFont13Semi,
                    ),
                    SizedBox(
                      height: height * .01,
                    ),
                    DropDownBlue(),
                    SizedBox(
                      height: height * .03,
                    ),
                    Text(
                      "Text your issues",
                      style: stylBlackFont13Semi,
                    ),
                    SizedBox(
                      height: height * .01,
                    ),
                    myBigTextBox(),
                    SizedBox(
                      height: height * .035,
                    ),
                    GestureDetector(
                      onTap: (){
                        HelpAndSupportController.to.postData();
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          MyButtonDC(
                            text: "Submit",
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
          Center(child: GetX<HelpAndSupportController>(builder: (controller) {
            return Visibility(
                visible: controller.loadIng.value,
                child: Container(
                    child: (CircularProgressIndicator(
                      valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue),
                    ))));
          }))
        ],
      ),
    );
  }
}

// ignore: camel_case_types
class myBigTextBox extends StatelessWidget {
  final HelpAndSupportController controller =
      Get.put(HelpAndSupportController());

  @override
  Widget build(BuildContext context) {
    var height = SizeConfig.screenHeight * 1.225;
    return Container(
      child: TextFormField(
        style: TextStyle(fontSize: height * .013),
        controller: HelpAndSupportController.to.helpTextController,
        decoration: InputDecoration(
            border: OutlineInputBorder(
                borderSide: BorderSide(color: colorblack30),
                borderRadius: BorderRadius.circular(height * .007)),
            hintText: "Text your issues",
            helperStyle: TextStyle(fontSize: height * .013)),
        maxLines: 8,
        onChanged: (value) {
          HelpAndSupportController.to.messages = value.toString();
        },
      ),
    );
  }
}

class DropDownBlue extends StatelessWidget {
  final HelpAndSupportController controller =
      Get.put(HelpAndSupportController());

  List creditType = [
    "Payment Related",
    "Delivery Related",
    "Return Related",
    "Transaction Related",
    "Account Related"
  ];
  String cerditValue = "";

  @override
  Widget build(BuildContext context) {
    var height = SizeConfig.screenHeight * 1.225;
    var width = SizeConfig.screenWidth * 1.225;
    var fontsize15 = height * .015;
    var stylWhiteFont15 = TextStyle(
        color: fillColor, fontSize: fontsize15, fontFamily: "SegoeSemi");
    return GetX<HelpAndSupportController>(
        init: HelpAndSupportController(),
        builder: (controller) {
          return Container(
            padding: EdgeInsets.symmetric(
                horizontal: width * .025, vertical: height * .01),
            margin: EdgeInsets.symmetric(horizontal: width * .0),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(height * .004),
                color: colorMyblue),
            height: height * .051,
            child: DropdownButton(
              isExpanded: true,
              underline: SizedBox(),
              icon: Icon(
                Icons.keyboard_arrow_down_rounded,
                color: fillColor,
              ),
              hint: Text(
                cerditValue == null
                    ? creditType[0]
                    : controller.currentValue.value,
                style: stylWhiteFont15,
              ),
              onChanged: (value) {
                controller.onchangeValue(value.toString());
                // setState(() {
                //   cerditValue=value.toString();
                //   print(cerditValue);
                // });
              },
              items: creditType
                  .map((e) => DropdownMenuItem(
                      value: e,
                      child: Text(
                        e.toString(),
                        style: TextStyle(fontSize: fontsize15),
                      )))
                  .toList(),
            ),
          );
        });
  }
}
