import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/widgets/myAppbar.dart';


import '../../util/constants.dart';


class BankDetailPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var height=SizeConfig.screenHeight*1.225;
    var width=SizeConfig.screenWidth*1.225;

    /// Local variables <<---------------------------------------------------->>
    var fontsize12=height*.012;
    var colorblack= Color.fromRGBO(0, 0, 0, 1);
    var colorblacklighter= Color.fromRGBO(0, 0, 0, .64);
    var style=TextStyle(fontSize: fontsize12,fontFamily: "SegoeReg,",color: colorblack);
    var styleLight=TextStyle(fontSize: fontsize12,fontFamily: "SegoeReg,",color: colorblacklighter);

    /// Local variables <<---------------------------------------------------->>
    var  shadow= [BoxShadow(color: Color.fromRGBO(0, 0, 0, .07),blurRadius: 10)];

    return Scaffold(
      backgroundColor: HexColor('#FFFFFF'),
      appBar: myAppbar(context, 'Bank Details'),
      body:    Container(
        margin: EdgeInsets.fromLTRB(width*.05,height*.02,width*.05,height*.0,),
        padding: EdgeInsets.fromLTRB(width*.025,height*.0,width*.025,height*.0,),
        height: height*.163,
        width: width*.9,
        decoration: BoxDecoration(
            color:Colors.white,
            borderRadius: BorderRadius.circular(3),
            boxShadow: shadow),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            bankDetailElements('Bank Name', 'SBI',style,styleLight),
            SizedBox(
              height: height*.01,
            ),
            bankDetailElements('Account Holder Name', 'Lorem',style,styleLight),
            SizedBox(
              height: height*.01,
            ),
            bankDetailElements('Account Type', 'Lorem',style,styleLight),
            SizedBox(
              height: height*.01,
            ),
            bankDetailElements('Account Number', '87687499328975',style,styleLight),
            SizedBox(
              height: height*.01,
            ),
            bankDetailElements('IFSC Code', 'DDA786',style,styleLight),
          ],
        ),
      )
    );
  }

  Row bankDetailElements(String title, String text,var style,var styleLight) {

    return Row(
      children: [
        SizedBox(
          width: 12,
        ),
        Expanded(
            child: Text(
              title,
              style: styleLight,
            )),
        Text(': ',style: style,),
        SizedBox(width: 10,),
        Expanded(
            child: Text(text,
                style: style))
      ],
    );
  }

  Column editableContent(String title, String text) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Text(
          title,
          style: TextStyle(fontSize: 13),
        ),
        TextFormField(
          initialValue: text,
          style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold),
          onChanged: (text) {},
        ),
        SizedBox(
          height: 15,
        ),
      ],
    );
  }



}
