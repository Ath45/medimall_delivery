import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/MyAccountController/MyAccountController.dart';
import 'package:medimallDelivery/Routes/routes.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../util/constants.dart';
import '../../../widgets/myAppbar.dart';
import 'package:medimallDelivery/widgets/profileCrad.dart';
import 'package:medimallDelivery/widgets/optionList.dart';
import '../../loginPage.dart';
class MyAccounts extends StatelessWidget {
  final AccountController controller = Get.put(AccountController());
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var height=SizeConfig.screenHeight*1.225;
    var width=SizeConfig.screenWidth*1.225;
    /// Local variables <<---------------------------------------------------->>
    var myBlue=Color.fromRGBO(77, 158, 255, 1);
    var colorbackground=Color.fromRGBO(248, 248, 248, 1);
    /// Local variables <<---------------------------------------------------->>
    return Scaffold(backgroundColor: colorbackground,
      appBar: myAppbar(context, "My Account"),
      body: ListView(
        children: [
          SizedBox(height: height*.02,),
          profileCard(),
          SizedBox(height: height*.025,),
          optionList(),
          SizedBox(height: height*.025,),
          GestureDetector(
            onTap: () async {
              await showDialog(
                  context: context,
                  builder: (_) => AlertDialog(
                    title: Text('Are you sure want to leave?'),
                    actions: [
                      ElevatedButton(
                          onPressed: () async{
                            SharedPreferences preferences =
                                await SharedPreferences.getInstance();
                            await preferences.clear();
                            Get.offAllNamed(loginRoute);
                          },
                          child: Text('Yes')),
                      TextButton(onPressed: () => Get.back(), child: Text('No'))
                    ],
                  ));

          },
            child: Container(margin: EdgeInsets.symmetric(horizontal: width*.3),
              child: Row(mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image(image: AssetImage("assets/icons/ic_logout.png"),height: height*.035,),
                  SizedBox(width: width*.025,),
                  Text("Logout",style: TextStyle(fontFamily: "SegoeReg",fontSize: 13,color:myBlue),)
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

