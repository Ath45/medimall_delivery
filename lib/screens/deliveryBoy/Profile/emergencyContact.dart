import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/EmergencyController/EmergencyController.dart';

import '../../../util/constants.dart';
import '../../../widgets/myAppbar.dart';
class EmergencyContact extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var height=SizeConfig.screenHeight*0.1255;
    var width=SizeConfig.screenWidth;
    EmergencyController.to.getData();
    /// Local variables <<---------------------------------------------------->>
    var fontsize14=height*.13;
    var colorblack= Color.fromRGBO(0, 0, 0, 1);
    var colorblacklight= Color.fromRGBO(0, 0, 0, .5);
    var myBlue=Color.fromRGBO(77, 158, 255, 1);
    var colorboxFill=Colors.white;
    var style=TextStyle(color: colorblack,fontSize: fontsize14,fontFamily: "SegoeSemi");
    var styleReg=TextStyle(color: colorblacklight,fontSize: fontsize14,fontFamily: "SegoeReg");
    var shadow= [BoxShadow(color: Color.fromRGBO(0, 0, 0, .07),blurRadius: 10)];
    /// Local variables <<---------------------------------------------------->>
    return Scaffold(
      appBar: myAppbar(context, "Emergency Contact"),
      body: GetX<EmergencyController>(
          init: EmergencyController(),
          builder: (controller) {
          return ListView.builder(padding: EdgeInsets.only(top: height*.17),
            itemCount: controller.emerGency.length,
            itemBuilder:( context,index)
          {
            return Container(
              height: height*.75,
              width: width,
              margin: EdgeInsets.fromLTRB(width*.03, height*.05, width*.03, height*.05,),
                padding: EdgeInsets.fromLTRB(width*.03, height*.14, width*.02, height*.15,),
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(height*.04),
                    color: colorboxFill,boxShadow: shadow),
              child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
               Column(mainAxisAlignment: MainAxisAlignment.spaceBetween,crossAxisAlignment: CrossAxisAlignment.start,
                 children: [
                   Text(controller.emerGency[index].name,style: style,),
                   Text(controller.emerGency[index].contactNumber,style: styleReg,),
                 ],
               ),
                  GestureDetector(
                    onTap: (){
                      EmergencyController.to.onCall(controller.emerGency[index].contactNumber);
                    },
                    child: Container(decoration: BoxDecoration(boxShadow: shadow,color: colorboxFill,shape: BoxShape.circle,),
                        height: height*.4,
                        width: height*.4,
                        padding: EdgeInsets.all(height*.09),
                        child: Image(image: AssetImage("assets/icons/call.png"),color: myBlue,height:height*.015,)),
                  )
                ],
              ),
            );
          },
    );
        }
      ));
  }
}
