import 'package:expansion_tile_card/expansion_tile_card.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/CreditController/CreditController.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/util/constants.dart';
import 'package:medimallDelivery/util/constantsDC.dart';
import 'package:medimallDelivery/widgets/myAppbar.dart';

class CreditPage extends StatelessWidget {
  final GlobalKey<ExpansionTileCardState> creditKey = new GlobalKey();

  String creditItem = 'All Credit';

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var height = SizeConfig.screenHeight * 1.225;
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: HexColor('#FFFFFF'),
        appBar: myAppbar(context, 'Credit'),
        body: Stack(
          children: [
            ListView(
              children: [
                SizedBox(
                  height: height * .02,
                ),
                SizedBox(
                  height: height * .03,
                ),
                SearchPeriod(),
                SizedBox(
                  height: height * .02,
                ),
                CreditSession(),
                SizedBox(
                  height: height * .02,
                ),
              ],
            ),
            Center(child: GetX<CreditController>(builder: (controller) {
              return Visibility(
                  visible: controller.loadIng.value,
                  child: Container(
                      child: (CircularProgressIndicator(
                        valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue),
                      ))));
            })),
            Center(child: GetX<CreditController>(builder: (controller) {
              print("data" + controller.rxEmpty.value.toString());
              return new Center(
                  child: Visibility(
                      visible: controller.rxEmpty.value,
                      child: Container(
                          child: Text(
                            "No Data Available.....",
                            style: TextStyle(
                                color: Color.fromRGBO(98, 98, 100, 1),
                                fontSize: 15,
                                fontFamily: "SegoeReg"),
                          ))));
            })),
          ],
        ));
  }
}

class SearchPeriod extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var height = SizeConfig.screenHeight * 1.225;
    var width = SizeConfig.screenWidth * 1.225;
    var fontsize15 = height * .012;
    var styleRegGrey = TextStyle(
        color: Color.fromRGBO(98, 98, 100, 1),
        fontSize: fontsize15,
        fontFamily: "SegoeReg");
    var styleRegGrey70 = TextStyle(
        color: Color.fromRGBO(98, 98, 100, .7),
        fontSize: fontsize15,
        fontFamily: "SegoeReg");
    return Container(
      margin: EdgeInsets.symmetric(horizontal: width * .04),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: Get.width,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            child: Container(
              child: Row(
                children: [
                  Container(
                      margin: EdgeInsets.only(left: 10),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0))),
                      width: SizeConfig.horizontal * 80,
                      child: TextField(
                        decoration: InputDecoration(
                          hintText: "Search"
                        ),
                        onChanged: (value) {
                          CreditController.to.getSearch(value);
                        },
                      )),
                  Center(
                    child: Container(
                      child: ImageIcon(
                        AssetImage('assets/icons/search.png'),
                        size: SizeConfig.horizontal * 5,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          SizedBox(
            height: height * 0.02,
          ),
          Text(
            "Search Period",
            style: styleRegGrey70,
          ),
          SizedBox(
            height: height * .005,
          ),
          Row(
            children: [
              GestureDetector(
                onTap: () {
                  CreditController.to.fromChooseDate(context);
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "From",
                      style: styleRegGrey,
                    ),
                    Container(
                        height: height * .028,
                        width: width * .27,
                        child: TextFormField(
                            controller: CreditController.to.FromDate,
                            enabled: false,
                            decoration: InputDecoration(
                                border: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color:
                                            Color.fromRGBO(98, 99, 100, 1))))))
                  ],
                ),
              ),
              SizedBox(
                width: width * .06,
              ),
              GestureDetector(
                onTap: () {
                  CreditController.to.toChooseDate(context);
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "To",
                      style: styleRegGrey,
                    ),
                    Container(
                        height: height * .025,
                        width: width * .27,
                        child: TextFormField(
                          enabled: false,
                          controller: CreditController.to.toDate,
                          decoration: InputDecoration(
                              border: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Color.fromRGBO(98, 99, 100, 1)))),
                        ))
                  ],
                ),
              ),
              SizedBox(
                width: width * .08,
              ),
              Column(
                children: [
                  SizedBox(
                    width: width * .05,
                    height: width * .05,
                  ),
                  Image(
                    image: AssetImage("assets/icons/search.png"),
                    height: height * .02,
                  )
                ],
              )
            ],
          ),
          SizedBox(
            height: height * 0.02,
          ),
          GetX<CreditController>(
              init: CreditController(),
              builder: (controller) {
                return Container(
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'Credit : ',
                        style: TextStyle(
                            fontSize: SizeConfig.horizontal * 3.5,
                            fontFamily: 'SegoeSemi'),
                      ),
                      SizedBox(
                        width: SizeConfig.horizontal * 1,
                      ),
                      Text(
                        '₹' + controller.totalIncome.value,
                        style: TextStyle(
                            fontSize: SizeConfig.horizontal * 3.5,
                            fontFamily: 'SegoeSemi',
                            color: HexColor('#00A200')),
                      ),
                    ],
                  ),
                );
              }),
        ],
      ),
    );
  }
}

class CreditSession extends StatelessWidget {
  List creditType = ["Paid to Admin", "Pending To Admin"];
  String cerditValue = "";

  @override
  Widget build(BuildContext context) {
    ///Local variables<<------------------------------------------------------>>
    var height = SizeConfig.screenHeight * 1.225;
    var width = SizeConfig.screenWidth * 1.225;

    var stylBlackFont13 =
        TextStyle(color: colorblack, fontSize: 13, fontFamily: "SegoeReg");

    ///Local variables<<------------------------------------------------------>>
    return GetX<CreditController>(
        init: CreditController(),
        builder: (controller) {
          print("object"+controller.currentValue.value,);
          return Container(
            child: Column(
              children: [
                // Container(
                //   height: height * .43 * .122,
                //   width: width,
                //   margin: EdgeInsets.symmetric(horizontal: width * .025),
                //   padding: EdgeInsets.symmetric(
                //       horizontal: width * .025, vertical: height * .01),
                //   decoration: BoxDecoration(
                //       color: fillColor,
                //       borderRadius: BorderRadius.circular(height * .004),
                //       boxShadow: shadow),
                //   child: DropdownButton(
                //     isExpanded: true,
                //     underline: SizedBox(),
                //     icon: Icon(
                //       Icons.keyboard_arrow_down_rounded,
                //       color: colorblack,
                //     ),
                //     hint: Text(
                //       controller.currentValue.value,
                //       style: stylBlackFont13,
                //     ),
                //     onChanged: (value) {
                //       controller.changeCn(value.toString());
                //     },
                //     items: creditType
                //         .map((e) => DropdownMenuItem(
                //             value: e, child: Text(e.toString())))
                //         .toList(),
                //   ),
                // ),
                SizedBox(
                  height: height * .01,
                ),
                Container(
                 // height: Get.height,
                  child: Column(
                      children: [
                      CreditBox()
                  ]
                   //
                  ),
                )
              ],
            ),
          );
        });
  }
}

class CreditBox extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var height = SizeConfig.screenHeight * 1.225;
    var width = SizeConfig.screenWidth * 1.225;
    var stylBlack50Font10 = TextStyle(
        color: colorblack50, fontSize: height * .010, fontFamily: "SegoeReg");
    var stylBlue50Font12 = TextStyle(
        color: Color.fromRGBO(257, 56, 132, 1),
        fontSize: height * .012,
        fontFamily: "SegoeSemi");
    var stylBlackFont12 = TextStyle(
        color: colorblack, fontSize: height * .012, fontFamily: "SegoeSemi");
    var stylBlackFont12Reg = TextStyle(
        color: colorblack, fontSize: height * .012, fontFamily: "SegoeReg");
    return Container(
      //   height: height * .108 * 1.225,
      width: width,
      margin: EdgeInsets.symmetric(
          horizontal: width * .025, vertical: height * .005),
      padding: EdgeInsets.only(top: height * .01),
      decoration: BoxDecoration(
          color: fillColor,
          boxShadow: shadow,
          borderRadius: BorderRadius.circular(height * .004)),
      child: Column(
        children: [
          Container(
            height: height * .032 * 1.22,
            padding: EdgeInsets.symmetric(horizontal: width * .02),
            child: Row(
              children: [
                Container(
                  width: width * .170,
                  child: Text(
                    "Date",
                    style: stylBlackFont12,
                  ),
                ),
                Container(
                  width: width * .15,
                  child: Text(
                    "Credit",
                    style: stylBlackFont12,
                  ),
                ),
                Container(
                  width: width * .12,
                  child: Text(
                    "Debit",
                    style: stylBlackFont12,
                  ),
                ),
                Container(
                  width: width * .130,
                  child: Text(
                    "Balance",
                    style: stylBlackFont12,
                  ),
                ),
                Container(
                  width: width * .15,
                  child: Center(
                    child: Text(
                      "Order Id",
                      style: stylBlackFont12,
                    ),
                  ),
                )
              ],
            ),
          ),
          GetX<CreditController>(
              init: CreditController(),
              builder: (controller) {
                return ListView.builder(
                    itemCount: controller.transactionData.length,
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return Container(
                        height: height * .032 * 1.22,
                        padding: EdgeInsets.only(left: width * .02),
                        color: getColor(index),                        child: Row(
                          children: [
                            Container(
                              width: width * .190,
                              child: Text(
                                controller.transactionData[index].createdAt,
                                style: stylBlackFont12Reg,
                              ),
                            ),
                            Container(
                              width: width * .15,
                              child: Text(
                                "₹ " +
                                    controller.transactionData[index].credit
                                        .toString(),
                                style: stylBlackFont12,
                              ),
                            ),
                            Container(
                              width: width * .12,
                              child: Text(
                                "₹ " +
                                    controller.transactionData[index].debit
                                        .toString(),
                                style: stylBlackFont12,
                              ),
                            ),
                            Container(
                              width: width * .130,
                              child: Text(
                                "₹ " +
                                    controller.transactionData[index].balance
                                        .toString(),
                                style: stylBlackFont12,
                              ),
                            ),
                            Container(
                              width: width * .14,
                              child: Center(
                                child: Text(
                                  controller.transactionData[index].orderId,
                                  style: stylBlackFont12,
                                ),
                              ),
                            )
                          ],
                        ),
                      );
                    });
              }),
        ],
      ),
    );
  }
}
getColor(int index) {
  if ((index % 2) == 0) {
    return Color.fromRGBO(237, 248, 255, 1);
  } else {
    return Color.fromRGBO(255, 255, 255, 1.0);
  }
}
