import 'package:expansion_tile_card/expansion_tile_card.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/TransactionController/TransactionController.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/util/constants.dart';
import 'package:medimallDelivery/util/constantsDC.dart';
import 'package:medimallDelivery/widgets/myAppbar.dart';

class TransactionsPage extends StatelessWidget {
  final GlobalKey<ExpansionTileCardState> transactionKey = new GlobalKey();
  String transactionElement = 'Paid to Admin';
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var height = SizeConfig.screenHeight * 1.225;
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: HexColor('#FFFFFF'),
        appBar: myAppbar(context, 'Transactions'),
        body: Stack(
          children: [
            ListView(
              children: [
                SizedBox(
                  height: height * .02,
                ),
                SizedBox(
                  height: height * .03,
                ),
                SearchPeriod(),
                SizedBox(
                  height: height * .03,
                ),
                CreditSession(),
                SizedBox(
                  height: height * .02,
                ),
              ],
            ),
            Center(child: GetX<TransactionController>(builder: (controller) {
              return Visibility(
                  visible: controller.loadIng.value,
                  child: Container(
                      child: (CircularProgressIndicator(
                        valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue),
                      ))));
            })),
            Center(child: GetX<TransactionController>(builder: (controller) {
              return new Center(
                  child: Visibility(
                      visible: controller.rxEmpty.value,
                      child: Container(
                          child: Text(
                            "No Data Available.....",
                            style: TextStyle(
                                color: Color.fromRGBO(98, 98, 100, 1),
                                fontSize: 15,
                                fontFamily: "SegoeReg"),
                          )
                      )
                  )
              );
            })),
          ],

        ));
  }
}

class CreditSession extends StatelessWidget {
  List creditType = [
    "Pending To Admin",
    "Paid to Admin",
  ];
  String cerditValue = "";

  @override
  Widget build(BuildContext context) {
    ///Local variables<<------------------------------------------------------>>
    var height = SizeConfig.screenHeight * 1.225;
    var width = SizeConfig.screenWidth * 1.225;

    var stylBlackFont13 =
        TextStyle(color: colorblack, fontSize: 13, fontFamily: "SegoeReg");

    ///Local variables<<------------------------------------------------------>>
    return Container(
      child: Column(
        children: [
          GetX<TransactionController>(
              init: TransactionController(),
              builder: (controller) {
                return Container(
                  height: height * .43 * .122,
                  width: width,
                  margin: EdgeInsets.symmetric(horizontal: width * .025),
                  padding: EdgeInsets.symmetric(
                      horizontal: width * .025, vertical: height * .01),
                  decoration: BoxDecoration(
                      color: fillColor,
                      borderRadius: BorderRadius.circular(height * .004),
                      boxShadow: shadow),
                  child: DropdownButton(
                    isExpanded: true,
                     underline: SizedBox(),
                    icon: Icon(
                      Icons.keyboard_arrow_down_rounded,
                      color: colorblack,
                    ),
                    hint: Text(
                      TransactionController.to.currentValue.value,
                      style: stylBlackFont13,
                    ),
                    onChanged: (value) {
                      TransactionController.to.changeCn(value.toString());
                    },
                    items: creditType
                        .map((e) => DropdownMenuItem(
                            value: e, child: Text(e.toString())))
                        .toList(),
                  ),
                );
              }),
          SizedBox(
            height: height * .01,
          ),
          Container(
            height: height * .155 * 3,
            child: ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              itemCount: 1,
              itemBuilder: (context, index) {
                return CreditBox();
              },
            ),
          )
        ],
      ),
    );
  }
}

class SearchPeriod extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var height = SizeConfig.screenHeight * 1.225;
    var width = SizeConfig.screenWidth * 1.225;
    var fontsize15 = height * .012;
    var styleRegGrey = TextStyle(
        color: Color.fromRGBO(98, 98, 100, 1),
        fontSize: fontsize15,
        fontFamily: "SegoeReg");
    var styleRegGrey70 = TextStyle(
        color: Color.fromRGBO(98, 98, 100, .7),
        fontSize: fontsize15,
        fontFamily: "SegoeReg");
    return GetX<TransactionController>(
        init: TransactionController(),
        builder: (controller) {
          return Container(
            margin: EdgeInsets.symmetric(horizontal: width * .04),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: Get.width,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  child: Container(
                    child: Row(
                      children: [
                        Container(
                            margin: EdgeInsets.only(left: 10),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0))),
                            width: SizeConfig.horizontal * 80,
                            child: TextField(
                              decoration: InputDecoration(
                                hintText: "Search",
                              ),
                              onChanged: (value) {
                                TransactionController.to.getSearch(value);
                              },
                            )),
                        Center(
                          child: Container(
                            child: ImageIcon(
                              AssetImage('assets/icons/search.png'),
                              size: SizeConfig.horizontal * 5,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: height * 0.02,
                ),
                Text(
                  "Search Period",
                  style: styleRegGrey70,
                ),
                SizedBox(
                  height: height * .005,
                ),
                Row(
                  children: [
                    GestureDetector(
                      onTap: () {
                        TransactionController.to.fromChooseDate(context);
                      },
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "From",
                            style: styleRegGrey,
                          ),
                          Container(
                              height: height * .028,
                              width: width * .27,
                              child: TextFormField(
                                  controller: TransactionController.to.FromDate,
                                  enabled: false,
                                  decoration: InputDecoration(
                                      border: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Color.fromRGBO(
                                                  98, 99, 100, 1))))))
                        ],
                      ),
                    ),
                    SizedBox(
                      width: width * .06,
                    ),
                    GestureDetector(
                      onTap: () {
                        TransactionController.to.toChooseDate(context);
                      },
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            controller.tosFUnction.value,
                            style: styleRegGrey,
                          ),
                          Container(
                              height: height * .025,
                              width: width * .27,
                              child: TextFormField(
                                controller: TransactionController.to.toDate,
                                enabled: false,
                                decoration: InputDecoration(
                                    border: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color.fromRGBO(
                                                98, 99, 100, 1)))),
                              ))
                        ],
                      ),
                    ),
                    SizedBox(
                      width: width * .08,
                    ),
                    Column(
                      children: [
                        SizedBox(
                          width: width * .05,
                          height: width * .05,
                        ),
                        Image(
                          image: AssetImage("assets/icons/search.png"),
                          height: height * .02,
                        )
                      ],
                    )
                  ],
                )
              ],
            ),
          );
        });
  }
}

class CreditBox extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var height = SizeConfig.screenHeight * 1.225;
    var width = SizeConfig.screenWidth * 1.225;
    var stylBlack50Font10 = TextStyle(
        color: colorblack50, fontSize: height * .010, fontFamily: "SegoeReg");
    var stylBlue50Font12 = TextStyle(
        color: Color.fromRGBO(257, 56, 132, 1),
        fontSize: height * .012,
        fontFamily: "SegoeSemi");
    var stylBlackFont12 = TextStyle(
        color: colorblack, fontSize: height * .012, fontFamily: "SegoeSemi");
    var stylBlackFont12Reg = TextStyle(
        color: colorblack, fontSize: height * .012, fontFamily: "SegoeReg");
    return Container(
      width: width,
      margin: EdgeInsets.symmetric(
          horizontal: width * .025, vertical: height * .005),
     // padding: EdgeInsets.only(top: height * .01),
      decoration: BoxDecoration(
          color: fillColor,
          boxShadow: shadow,
          borderRadius: BorderRadius.circular(height * .004)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: height * .034 * 1.22,
            padding: EdgeInsets.symmetric(horizontal: width * .02),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: width * .355,
                  child: Text(
                    "Date",
                    style: stylBlackFont12,
                  ),
                ),
                Container(
                  width: width * .15,
                  child: Text(
                    "Order ID",
                    style: stylBlackFont12,
                  ),
                ),
                Container(
                  width: width * .22,
                  child: Text(
                    "Order value",
                    textAlign: TextAlign.center,
                    style: stylBlackFont12,
                  ),
                ),
              ],
            ),
          ),
          GetX<TransactionController>(
              init: TransactionController(),
              builder: (controller) {
                return ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: controller.transactionData.length,
                    itemBuilder: (context, index) {
                      return Container(
                        padding: EdgeInsets.symmetric(horizontal: width * .02),
                        color: getColor(index),
                        height: height * 0.041,
                        child: Row(
                          children: [
                            Container(
                              width: width * .355,
                              child: Text(
                                controller.transactionData[index].deliveredDate
                                    .toString(),
                                style: stylBlackFont12Reg,
                                maxLines: 1,
                              ),
                            ),
                            Container(
                              width: width * .15,
                              child: Text(
                                controller.transactionData[index].orderId,
                                style: stylBlackFont12,
                              ),
                            ),
                            Container(
                              width: width * .22,
                              child: Text(
                                " ₹ " + controller.transactionData[index].totalAmountToBePaid.toString(),
                                style: stylBlackFont12,
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ],
                        ),
                      );
                    });
              }),
        ],
      ),
    );
  }
}

getColor(int index) {
  if ((index % 2) == 0) {
    return Color.fromRGBO(237, 248, 255, 1);
  } else {
    return Color.fromRGBO(255, 255, 255, 1.0);
  }
}
