import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/DeliverdOrdersController/deliverdOrderController.dart';
import 'package:medimallDelivery/Routes/routes.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/util/constants.dart';
import 'package:medimallDelivery/util/constantsDC.dart';
import 'package:medimallDelivery/widgets/myAppbar.dart';
import 'package:medimallDelivery/widgets/paymentDeatails.dart';

class DeliveredOrders extends StatelessWidget {
  final DeliverdOrderController controller = Get.put(DeliverdOrderController());
  String transactionElement = 'All Transactions';
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: HexColor('#FAFAFA'),
      resizeToAvoidBottomInset: false,
      appBar: myAppbar(context, 'Delivered Orders'),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: SizeConfig.horizontal * 3),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: SizeConfig.horizontal * 2,
                        vertical: SizeConfig.horizontal * 4),
                    child: Text(
                      'Search period',
                      style: TextStyle(
                        fontSize: SizeConfig.horizontal * 3,
                        fontFamily: 'SegoeReg',
                        color: HexColor('#626364').withOpacity(0.7),
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                    EdgeInsets.symmetric(horizontal: SizeConfig.horizontal * 2),
                    child: Container(
                      width: Get.width,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(10.0))),
                      child: Container(
                        child: Row(
                          children: [
                            Container(
                                margin: EdgeInsets.only(left: 10),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius:
                                    BorderRadius.all(Radius.circular(10.0))),
                                width: SizeConfig.horizontal * 80,
                                child: TextField(
                                  decoration: InputDecoration(
                                    hintText: 'Search',
                                  ),
                                  onChanged: (value){
                                    DeliverdOrderController.to.getSearch(value);
                                  },
                                )),
                            Center(
                              child: Container(
                                child: ImageIcon(
                                  AssetImage('assets/icons/search.png'),
                                  size: SizeConfig.horizontal * 5,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  // SizedBox(
                  //   height: height * 0.01,
                  // ),
                  Container(
                    padding:
                    EdgeInsets.symmetric(horizontal: SizeConfig.horizontal * 2),
                    margin: EdgeInsets.only(top:height * 0.03,),
                    child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  DeliverdOrderController.to
                                      .fromChooseDate(context);
                                  print("Delivered");
                                },
                                child: Container(
                                  child: Text(
                                    'From',
                                    style: TextStyle(
                                      fontSize: SizeConfig.horizontal * 3,
                                      color: HexColor('#626364'),
                                    ),
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  DeliverdOrderController.to
                                      .fromChooseDate(context);
                                },
                                child: Container(
                                    width: SizeConfig.horizontal * 35,
                                    child: TextField(
                                      controller:
                                      DeliverdOrderController.to.FromDate,
                                      enabled: false,
                                      decoration: InputDecoration(
                                        isDense: true,
                                        enabledBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: HexColor('#707070'),
                                              width: 0.4),
                                        ),
                                        focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: HexColor('#707070'),
                                              width: 0.4),
                                        ),
                                        border: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: HexColor('#707070'),
                                              width: 0.4),
                                        ),
                                      ),
                                    )),
                              )
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  DeliverdOrderController.to.toChooseDate(context);
                                },
                                child: Container(
                                  child: Text(
                                    'To',
                                    style: TextStyle(
                                      fontSize: SizeConfig.horizontal * 3,
                                      color: HexColor('#626364'),
                                    ),
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  DeliverdOrderController.to.toChooseDate(context);
                                },
                                child: Container(
                                    width: SizeConfig.horizontal * 35,
                                    child: TextField(
                                      controller: DeliverdOrderController.to.toDate,
                                      enabled: false,
                                      decoration: InputDecoration(
                                        isDense: true,
                                        enabledBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: HexColor('#707070'),
                                              width: 0.4),
                                        ),
                                        focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: HexColor('#707070'),
                                              width: 0.4),
                                        ),
                                        border: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: HexColor('#707070'),
                                              width: 0.4),
                                        ),
                                      ),
                                    )),
                              )
                            ],
                          ),
                          ImageIcon(
                            AssetImage('assets/icons/search.png'),
                            size: SizeConfig.horizontal * 5,
                          )
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: SizeConfig.horizontal * 2,
                  ),
                  SizedBox(
                    height: 18,
                  ),
                  GetX<DeliverdOrderController>(
                      init: DeliverdOrderController(),
                      builder: (controller) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              child: Row(
                                children: [
                                  Text(
                                    controller.toFormattedDate.value,
                                    style: TextStyle(
                                        fontSize: SizeConfig.horizontal * 3.1,
                                        color: Colors.black.withOpacity(0.5),
                                        fontFamily: 'SegoeReg'),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: SizeConfig.horizontal * 1.5),
                                    child: Text(
                                      '-',
                                      style: TextStyle(
                                          fontSize: SizeConfig.horizontal * 3.1,
                                          color: Colors.black.withOpacity(0.5),
                                          fontFamily: 'SegoeReg'),
                                    ),
                                  ),
                                  Text(
                                    controller.fromFormattedDate.value,
                                    style: TextStyle(
                                        fontSize: SizeConfig.horizontal * 3.1,
                                        color: Colors.black.withOpacity(0.5),
                                        fontFamily: 'SegoeReg'),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              child: Row(
                                children: [
                                  Text(
                                    'Total Income',
                                    style: TextStyle(
                                        fontSize: SizeConfig.horizontal * 3.5,
                                        fontFamily: 'SegoeSemi'),
                                  ),
                                  SizedBox(
                                    width: SizeConfig.horizontal * 1,
                                  ),
                                  Text(
                                    '₹' + controller.totalIncome.value,
                                    style: TextStyle(
                                        fontSize: SizeConfig.horizontal * 3.5,
                                        fontFamily: 'SegoeSemi',
                                        color: HexColor('#00A200')),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        );
                      }),
                  GetX<DeliverdOrderController>(
                      init: DeliverdOrderController(),
                      builder: (controller) {
                        return ListView.builder(
                          padding: EdgeInsets.symmetric(
                              vertical: SizeConfig.horizontal * 3),
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: controller.newOrdersData.length,
                          itemBuilder: (context, index) {
                            return GestureDetector(
                              onTap: () {
                                // Navigator.of(context)
                                //     .push(MaterialPageRoute(builder: (context) {
                                //   return DeliveredOrderDetail();
                                // }));
                                Get.toNamed(deliverdOrderDetails, arguments: [controller.newOrdersData[index].idOr]);
                              },
                              child: PaymentDetails(
                                  name: controller.newOrdersData[index].userName,
                                  price: controller
                                      .newOrdersData[index].totalAmountToBePaid
                                      .toString(),
                                  orderId: controller.newOrdersData[index].orderId,
                                  deleveryCharge: controller
                                      .newOrdersData[index].deliveryCharge
                                      .toString(),
                                  paidStatus: "Delivered",
                                  paymntMethod:
                                  controller.newOrdersData[index].paymentType,
                                  visibility:controller.newOrdersData[index].isThisCartEligibleForFreeDelivery
                              ),
                            );
                          },
                        );
                      }),
                ],
              ),
            ),
          ),
          Center(child: GetX<DeliverdOrderController>(builder: (controller) {
            print("data" + controller.rxEmpty.value.toString());
            return new Center(
                child: Visibility(
                    visible: controller.rxEmpty.value,
                    child: Container(
                        child: Text(
                          "No Data Available.....",
                          style: TextStyle(
                              color: Color.fromRGBO(98, 98, 100, 1),
                              fontSize: 15,
                              fontFamily: "SegoeReg"),
                        ))));
          })),
          Center(child: GetX<DeliverdOrderController>(builder: (controller) {
            return Visibility(
                visible: controller.loadIng.value,
                child: Container(
                    child: (CircularProgressIndicator(
                      valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue),
                    ))));
          })),

        ],
      ),
    );
  }
}
