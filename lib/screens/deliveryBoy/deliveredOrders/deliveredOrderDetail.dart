import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/DeliverdOrderDetailsController/DeliverdOrderDetailsController.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/util/constants.dart';
import 'package:medimallDelivery/util/constantsDC.dart';
import 'package:medimallDelivery/util/contantRiyas.dart';
import 'package:medimallDelivery/widgets/PickedUpDetailsAddress.dart';
import 'package:medimallDelivery/widgets/deliverdDetailsAddress.dart';
import 'package:medimallDelivery/widgets/myAppbar.dart';
import 'package:medimallDelivery/widgets/subHeading.dart';

class DeliveredOrderDetail extends StatelessWidget {
  final one = Get.arguments;
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    DeliverdOrderDetailsController.to.getData(one[0]);
    return Scaffold(
      backgroundColor: HexColor('#FAFAFA'),
      appBar: myAppbar(context, 'Delivered Orders'),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.horizontal * 3,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  GetX<DeliverdOrderDetailsController>(
                      init: DeliverdOrderDetailsController(),
                      builder: (controller) {
                        return ListView.builder(
                            shrinkWrap: true,
                            itemCount: controller.deliOrders.length,
                            physics: const NeverScrollableScrollPhysics(),
                            itemBuilder: (context, index) {
                              return Padding(
                                padding:
                                EdgeInsets.symmetric(vertical: SizeConfig.horizontal *
                                    4),
                                child: Container(
                                  height: SizeConfig.horizontal * 28,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(8),
                                      boxShadow: myBoxShadow),
                                  child: Padding(
                                    padding: EdgeInsets.all(
                                        SizeConfig.horizontal * 3),
                                    child: Row(
                                      children: [
                                        Image.network(
                                          controller.deliOrders[index].image,
                                          height: SizeConfig.horizontal * 25,
                                          fit: BoxFit.fill,
                                        ),
                                        SizedBox(
                                          width: SizeConfig.horizontal * 10,
                                        ),
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment
                                              .start,
                                          children: [
                                            Container(
                                              width:height*0.22,
                                              child: Text(
                                                controller.deliOrders[index].productName,
                                                maxLines: 2,
                                                style: TextStyle(
                                                    fontFamily: 'SegoeSemi',
                                                    fontSize: SizeConfig.horizontal *
                                                        3),
                                              ),
                                            ),
                                            Container(
                                              width:height*0.22,
                                              child: Text(
                                                " (" +controller.deliOrders[index].uomValue+")",
                                                maxLines: 2,
                                                style: TextStyle(
                                                    fontFamily: 'SegoeSemi',
                                                    fontSize: SizeConfig.horizontal *
                                                        3),
                                              ),
                                            ),
                                            SizedBox(
                                                height: SizeConfig.horizontal * 1),
                                            Row(
                                              children: [
                                                Text(
                                                  '₹ ' + functionCalvulate(controller.deliOrders[index].specialPrice.toString(),controller.deliOrders[index].quantity.toString()),
                                                  style: TextStyle(
                                                      fontSize: SizeConfig
                                                          .horizontal * 3,
                                                      fontFamily: 'SegoeBold'),
                                                ),
                                                SizedBox(
                                                  width: SizeConfig.horizontal * 1.5,
                                                ),
                                                Stack(
                                                  children: [
                                                    Padding(
                                                      padding: EdgeInsets.symmetric(
                                                          horizontal:
                                                          SizeConfig.horizontal *
                                                              0.1),
                                                      child: Text(
                                                        '₹ ' + functionCalvulate(controller.deliOrders[index].price.toString(),controller.deliOrders[index].quantity.toString()),
                                                        style: TextStyle(
                                                            color:
                                                            Colors.black.withOpacity(
                                                                0.6),
                                                            fontSize:
                                                            SizeConfig.horizontal *
                                                                1.8,
                                                            fontFamily: 'PoppinsExtraLight'),
                                                      ),
                                                    ),
                                                    Positioned(
                                                      left: 0.0,
                                                      right: 0.0,
                                                      bottom: 0.0,
                                                      top: 0.0,
                                                      child: new RotationTransition(
                                                        turns: new AlwaysStoppedAnimation(
                                                            15 / 360),
                                                        child: Container(
                                                          child: Divider(
                                                            color:
                                                            Colors.black.withOpacity(
                                                                0.6),
                                                          ),
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                                height: SizeConfig.horizontal * 1),
                                            Text(
                                              'Qty : ' + controller.deliOrders[index].quantity.toString(),
                                              style: TextStyle(
                                                  fontSize: SizeConfig.horizontal * 3,
                                                  fontFamily: 'SegoeReg'),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            }
                        );
                      }
                  ),
                  SubHeading(
                    text: 'Customer Details',
                  ),
                  GetX<DeliverdOrderDetailsController>(
                      init: DeliverdOrderDetailsController(),
                      builder: (controller) {
                        return Padding(
                          padding:
                          EdgeInsets.symmetric(vertical: SizeConfig.horizontal * 1.5),
                          child: Container(
                            height: SizeConfig.horizontal * 20,
                            width: SizeConfig.horizontal * 100,
                            decoration: BoxDecoration(
                                color: HexColor('#FFFFFF'),
                                borderRadius: BorderRadius.circular(8),
                                boxShadow: myBoxShadow),
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: SizeConfig.horizontal * 4),
                              child: Row(
                                children: [
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        controller.name.value,
                                        style: TextStyle(
                                            fontSize: SizeConfig.horizontal * 5,
                                            fontFamily: 'SegoeSemi',
                                            fontWeight: FontWeight.bold),
                                      ),
                                      SizedBox(
                                        height: SizeConfig.horizontal * 1,
                                      ),
                                      Text(
                                        'Mobile No :' + controller.idName.value,
                                        style: TextStyle(
                                          fontSize: SizeConfig.horizontal * 3.2,
                                          fontFamily: 'SegoeReg',
                                        ),
                                      ),
                                    ],
                                  ),
                                  Spacer(),
                                  Text(
                                    ( controller.codOrder.value=="cod")?"Cash on Delivery":( controller.codOrder.value=="razorpay")?"Razorpay": controller.codOrder.value,
                                    style: TextStyle(
                                        fontFamily: 'SegoeSemi',
                                        fontSize: SizeConfig.horizontal * 3.5,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      }
                  ),
                  SizedBox(
                    height: SizeConfig.horizontal * 3,
                  ),
                  SubHeading(
                    text: 'Pickup Address',
                  ),
                  PickedUpDetailsAddress(),
                  SizedBox(
                    height: SizeConfig.horizontal * 3,
                  ),
                  SubHeading(
                    text: 'Delivery Address',
                  ),
                  DeliverdDetailsAddress(),
                  SizedBox(
                    height: SizeConfig.horizontal * 3,
                  ),
                  SubHeading(
                    text: 'Payment Details',
                  ),
                  GetX<DeliverdOrderDetailsController>(
                      init: DeliverdOrderDetailsController(),
                      builder: (controller) {
                        return Padding(
                          padding:
                          EdgeInsets.symmetric(vertical: SizeConfig.horizontal * 1.5),
                          child: Container(
                            width: SizeConfig.horizontal * 100,
                            decoration: BoxDecoration(
                                color: HexColor('#FFFFFF'),
                                borderRadius: BorderRadius.circular(5),
                                boxShadow: myBoxShadow),
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: SizeConfig.horizontal * 4,
                                  vertical: SizeConfig.horizontal * 3),
                              child: Row(
                                children: [
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      // paymentDetailHead('Total Amount'),
                                      // SizedBox(height: SizeConfig.horizontal * 1),
                                      // paymentDetailHead('Delivery Charges'),
                                      // SizedBox(height: SizeConfig.horizontal * 1),
                                      // paymentDetailHead('COD Orders'),
                                      // SizedBox(height: SizeConfig.horizontal * 1),
                                      // paymentDetailHead('Total Recievable'),
                                      SizedBox(height: SizeConfig.horizontal * 1),
                                      paymentDetailHead('Total Payable'),
                                    ],
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: SizeConfig.horizontal * 20),
                                    child: Column(
                                      children: [
                                        // paymentDetailDot(),
                                        // SizedBox(height: SizeConfig.horizontal * 1),
                                        // paymentDetailDot(),
                                        // SizedBox(height: SizeConfig.horizontal * 1),
                                        // paymentDetailDot(),
                                        // SizedBox(height: SizeConfig.horizontal * 1),
                                        // paymentDetailDot(),
                                        SizedBox(height: SizeConfig.horizontal * 1),
                                        paymentDetailDot(),
                                      ],
                                    ),
                                  ),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      // paymentDetailsPrice(controller.totalPay.value),
                                      // SizedBox(height: SizeConfig.horizontal * 1),
                                      // paymentDetailsPrice(controller.deliveryCharge.value),
                                      // SizedBox(height: SizeConfig.horizontal * 1),
                                      // paymentDetailsPrice('20'),
                                      // SizedBox(height: SizeConfig.horizontal * 1),
                                      // paymentDetailsPrice('100'),
                                      SizedBox(height: SizeConfig.horizontal * 1),
                                      paymentDetailsPrice(controller.totalPay.value),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      }
                  ),
                  SizedBox(
                    height: SizeConfig.horizontal * 3,
                  ),
                  Row(
                    children: [
                      SubHeading(
                        text: 'Payment Status',
                      ),
                      SizedBox(
                        width: SizeConfig.horizontal * 18,
                      ),
                      Text(
                        'Received',
                        style: TextStyle(
                            fontSize: SizeConfig.horizontal * 3.2,
                            color: HexColor('#28AA46'),
                            fontFamily: 'SegoeSemi'),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: SizeConfig.horizontal * 5,
                  ),
                ],
              ),
            ),
          ),
          Center(child: GetX<DeliverdOrderDetailsController>(builder: (controller) {
            return Visibility(
                visible: controller.loadIng.value,
                child: Container(
                    child: (CircularProgressIndicator(
                      valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue),
                    ))));
          }))
        ],
      ),
    );
  }

  Text paymentDetailsPrice(String price) {
    return Text(
      'Rs ' + price,
      style: TextStyle(
        fontFamily: 'SegoeReg',
        fontSize: SizeConfig.horizontal * 3.3,
      ),
    );
  }

  Text paymentDetailDot() {
    return Text(':',
        style: TextStyle(
            color: HexColor('#858585'),
            fontSize: SizeConfig.horizontal * 3.3,
            fontFamily: 'SegoeReg'));
  }

  Text paymentDetailHead(String text) {
    return Text(
      text,
      style: TextStyle(
          color: HexColor('#858585'),
          fontSize: SizeConfig.horizontal * 3.3,
          fontFamily: 'SegoeReg'),
    );
  }
}
