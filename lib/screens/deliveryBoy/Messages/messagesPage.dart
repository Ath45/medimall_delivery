import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/MessageController/MessageController.dart';
import 'package:medimallDelivery/util/constantsDC.dart';
import 'package:medimallDelivery/util/constants.dart';
import 'package:medimallDelivery/widgets/myAppbar.dart';
class MessagePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MessageController.to.getData();
    return Scaffold(
      appBar: myAppbar(context, "Messages"),
      body: Stack(
        children: [
          GetX<MessageController>(builder: (controller) {
            return ListView.builder(itemCount: controller.messageList.length,
                padding: EdgeInsets.only(top: 10),
                itemBuilder: (context,index){
                  return GestureDetector(
                    onTap: (){
                      MessageController.to.changeStatus(controller.messageList[index].id);
                    },
                    child: (
                        MessageBox(title:controller.messageList[index].issue,
                          body: controller.messageList[index].reply,)
                    ),
                  );
                });
          }
          ),
          Center(child: GetX<MessageController>(builder: (controller) {
            print("data" + controller.rxEmpty.value.toString());
            return new Center(
                child: Visibility(
                    visible: controller.rxEmpty.value,
                    child: Container(
                        child: Text(
                          "No Data Available.....",
                          style: TextStyle(
                              color: Color.fromRGBO(98, 98, 100, 1),
                              fontSize: 15,
                              fontFamily: "SegoeReg"),
                        ))));
          })),
          Center(child: GetX<MessageController>(builder: (controller) {
            return Visibility(
                visible: controller.loadIng.value,
                child: Container(
                    child: (CircularProgressIndicator(
                      valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue),
                    ))));
          }))
        ],
      ),
    );
  }
}

class MessageBox extends StatefulWidget {
  String title;
  String body;
  MessageBox(
      {
        required this.title,required this.body
      }
      );
  @override
  _MessageBoxState createState() => _MessageBoxState();
}

class _MessageBoxState extends State<MessageBox> {
  @override
  Widget build(BuildContext context) {
    ///Local variables<<------------------------------------------------------>>
    var height=SizeConfig.screenHeight*1.225;
    var width=SizeConfig.screenWidth*1.225;
    var fontsize13=height*.013;
    var fontsize12=height*.012;
    var stylBlackFont13Semi=TextStyle(color:colorblack,fontSize: fontsize13,fontFamily: "SegoeSemi");
    var stylBlackFont12Reg=TextStyle(color:colorblack70,fontSize: fontsize12,fontFamily: "SegoeReg");
    ///Local variables<<------------------------------------------------------>>
    return Container(
      margin:EdgeInsets.symmetric(horizontal: width*.025,
          vertical: height*.005),
      padding: EdgeInsets.symmetric(horizontal:height*.01,
          vertical: height*.008),
      height:height*.074,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(height*.007),
        color: fillColor,
        boxShadow: shadow,
      ),child: Column(mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(widget.title,style:stylBlackFont13Semi ,),
        Text(widget.body,style:stylBlackFont12Reg ,)
      ],
    ),

    );
  }
}
