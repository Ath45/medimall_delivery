import 'package:badges/badges.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/HomeController/HomeConroller.dart';
import 'package:medimallDelivery/Routes/routes.dart';
import 'package:medimallDelivery/screens/deliveryBoy/profilePage.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/widgets/drawer.dart';
import 'package:medimallDelivery/widgets/homeAppbar.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'messagesPage.dart';
import 'notificationPage.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    var scaffoldKey = GlobalKey<ScaffoldState>();
    HomeController.to.context = context;
    HomeController.to.onInit();
    return WillPopScope(
      onWillPop: () async {
        bool willLeave = false;
        await showDialog(
            context: context,
            builder: (_) => AlertDialog(
                  title: Text('Are you sure want to leave?'),
                  actions: [
                    ElevatedButton(
                        onPressed: () {
                          willLeave = true;
                          Get.back();
                        },
                        child: Text('Yes')),
                    TextButton(onPressed: () => Get.back(), child: Text('No'))
                  ],
                ));
        return willLeave;
      },
      child: Scaffold(
        key: scaffoldKey,
        drawer: DrawerSession(),
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.transparent,
        extendBody: true,
        body: SafeArea(
          child: Stack(
            children: [
              SmartRefresher(
                enablePullDown: true,
                enablePullUp: false,
                header: ClassicHeader(
                  completeText: "",
                  refreshingText: "",
                  releaseText: "",
                  idleText: "",
                  failedIcon: const Icon(Icons.error, color: Colors.blue),
                  completeIcon: Container(
                    height: 1.0,
                  ),
                  //const Icon(Icons.done, color: Colors.grey),
                  idleIcon: Container(
                    height: 10.0,
                  ),
                  releaseIcon: const Icon(Icons.refresh, color: Colors.blue),
                ),
                onRefresh: _onRefresh,
                onLoading: _onLoading,
                controller: HomeController.to.refreshController,
                child: SingleChildScrollView(
                  physics: ClampingScrollPhysics(),
                  child: Container(
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.shade400,
                            blurRadius: 1, // soften the shadow
                            spreadRadius: .5, //extend the shadow
                            offset: Offset(
                              1, // Move to right 10  horizontally
                              .5, // Move to bottom 10 Vertically
                            ),
                          ),
                        ],
                        gradient: LinearGradient(colors: [
                          HexColor("#FFFFFF"),
                          HexColor("#F9F9F9"),
                          HexColor("#FDFDFD"),
                        ])),
                    child: Column(
                      children: [
                        Container(
                          height: height,
                          width: width,
                          color: Colors.white,
                          child: Column(
                            children: [
                              SizedBox(
                                height: 90,
                              ),
                              GetX<HomeController>(
                                  init: HomeController(),
                                  builder: (controller) {
                                    return Stack(
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.only(
                                              left: width < 390 ? 30 : 40.0),
                                          child: Container(
                                            height: width < 390 ? 120 : 132,
                                            width: width < 390 ? 315 : 325,
                                            decoration: BoxDecoration(
                                                color: HexColor("#FFFFFF"),
                                                //color: Colors.redAccent,
                                                borderRadius:
                                                    BorderRadius.circular(8.0),
                                                boxShadow: [
                                                  BoxShadow(
                                                    // color: HexColor("#0D0D0D1F"),
                                                    color: Colors.grey.shade400,
                                                    blurRadius: 10.0,
                                                    offset: Offset(
                                                      1,
                                                      // Move to right 10  horizontally
                                                      .5, // Move to bottom 10 Vertically
                                                    ),
                                                  )
                                                ]),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      children: [
                                                        Text(
                                                          "Name   ",
                                                          style: TextStyle(
                                                              fontSize: 15),
                                                        ),
                                                        Text(
                                                          HomeController
                                                              .to.name.value,
                                                          style: TextStyle(
                                                              fontSize: 15),
                                                        )
                                                      ],
                                                    ),
                                                    SizedBox(
                                                      height: 10,
                                                    ),
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: [
                                                        Text(
                                                          "ID    ",
                                                          style: TextStyle(
                                                              fontSize: 12),
                                                        ),
                                                        Text(
                                                          ":" +
                                                              HomeController
                                                                  .to.id.value,
                                                          style: TextStyle(
                                                              fontSize: 12),
                                                        ),
                                                        SizedBox(
                                                          width: 26,
                                                        )
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        Positioned(
                                          right: 20.0,
                                          top: 55.0,
                                          bottom: 65.0,
                                          child: Container(
                                            width: 55,
                                            height: 24,
                                            child: GetBuilder<HomeController>(
                                                init: HomeController(),
                                                builder: (controller) {
                                                  return Switch(
                                                    value: HomeController
                                                        .to.docOrClinic.value,
                                                    onChanged: HomeController
                                                        .to.toggleSwitch,
                                                    activeColor:
                                                        HexColor("#005BEC"),
                                                    activeTrackColor:
                                                        HexColor("#00AAFF"),
                                                  );
                                                }),
                                          ),
                                        ),
                                        Positioned(
                                            right: 20.0,
                                            bottom: 12,
                                            child: Text(
                                              "₹ " + controller.credit.value,
                                              style: TextStyle(
                                                  fontSize:
                                                      width < 390 ? 16 : 18,
                                                  color: HexColor("#00AAFF"),
                                                  fontFamily: "Segoe UI"),
                                            )),
                                        Positioned(
                                          left: 0.0,
                                          top: 20.0,
                                          bottom: 20.0,
                                          child: GestureDetector(
                                            onTap: () {
                                              Get.toNamed(accountRoute);
                                            },
                                            child: Container(
                                                height: width < 390 ? 88 : 95,
                                                width: width < 390 ? 88 : 95,
                                                child: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          50.0),
                                                  child: CachedNetworkImage(
                                                    imageUrl:
                                                        controller.url.value,
                                                    placeholder: (context,
                                                            url) =>
                                                        CircularProgressIndicator(),
                                                    errorWidget:
                                                        (context, url, error) =>
                                                            Icon(Icons.error),
                                                    fit: BoxFit.cover,
                                                  ),
                                                )),
                                          ),
                                        ),
                                        Positioned(
                                          left: 0.0,
                                          top: 20.0,
                                          bottom: 24.0,
                                          child: Container(
                                              height: 35.0,
                                              width: 35.0,
                                              child: Image.asset(
                                                "assets/images/path.png",
                                                fit: BoxFit.cover,
                                              )),
                                        ),
                                        Positioned(
                                          left: 70.0,
                                          bottom: 12.0,
                                          child: Text(
                                            controller.status.value,
                                            style: TextStyle(
                                                fontSize: 13,
                                                color: HexColor("#00AAFF"),
                                                fontFamily: "Segoe UI"),
                                          ),
                                        )
                                      ],
                                    );
                                  }),
                              SizedBox(
                                height: 40.0,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 15.0, right: 15.0),
                                child: Container(
                                  child: GridView.builder(
                                      itemCount: 9,
                                      shrinkWrap: true,
                                      physics: ScrollPhysics(),
                                      gridDelegate:
                                          SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount: 3,
                                        crossAxisSpacing: 12,
                                        mainAxisSpacing:
                                            width < 390 ? 16 : 22.0,
                                      ),
                                      itemBuilder: (context, index) {
                                        return Column(
                                          children: [
                                            GestureDetector(
                                              onTap: () {
                                                Get.toNamed(HomeController
                                                    .to.getxRoutes[index]);
                                              },
                                              child: Container(
                                                height: 83,
                                                width: 87,
                                                decoration: BoxDecoration(
                                                    color: HexColor("#008DF8"),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8.0),
                                                    boxShadow: [
                                                      BoxShadow(
                                                          color: HexColor(
                                                              "#3E3E3E30"),
                                                          blurRadius: 9,
                                                          offset:
                                                              Offset(2.0, 2.0))
                                                    ]),
                                                child: Center(
                                                  child: Image.asset(
                                                    HomeController
                                                        .to.gridimage[index],
                                                    height: 44.03,
                                                    width: 34.4,
                                                  ),
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              height: 8,
                                            ),
                                            Text(
                                              HomeController
                                                  .to.gridlabel[index],
                                              style: TextStyle(
                                                fontSize: width < 390 ? 10 : 13,
                                              ),
                                              textAlign: TextAlign.center,
                                            )
                                          ],
                                        );
                                      }),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 0.0,
                child: Container(
                  height: 70,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                          colors: [HexColor("#00AAFF"), HexColor("#005BEC")])),
                  child: HomeAppbar(scaffoldKey),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  // void toggleSwitch(bool value) {
  //
  //   if(switchControl == false)
  //   {
  //     setState(() {
  //       switchControl = true;
  //     });
  //     print('Switch is ON');
  //     // Put your code here which you want to execute on Switch ON event.
  //
  //   }
  //   else
  //   {
  //     setState(() {
  //       switchControl = false;
  //     });
  //     print('Switch is OFF');
  //     // Put your code here which you want to execute on Switch OFF event.
  //   }
  // }

  GestureDetector notificationIcon(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(builder: (context) {
          return NotificationsPage();
        }));
      },
      child: Container(
          height: 10,
          margin: EdgeInsets.only(right: 25.0, top: 15),
          child: Badge(
              alignment: Alignment.topLeft,
              badgeContent: Text("2",
                  style: TextStyle(color: Colors.white, fontSize: 12)),
              child: ImageIcon(AssetImage('assets/icons/notification.png'),
                  color: HexColor('#000000')))),
    );
  }

  GestureDetector messageIcon(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(builder: (context) {
          return MessagesPage();
        }));
      },
      child: Container(
        height: 10,
        margin: EdgeInsets.only(right: 20.0, top: 15),
        child: Badge(
            alignment: Alignment.topLeft,
            badgeContent:
                Text("1", style: TextStyle(color: Colors.white, fontSize: 12)),
            child: ImageIcon(AssetImage('assets/icons/message.png'),
                color: HexColor('#000000'))),
      ),
    );
  }

  RichText titleText() {
    return RichText(
      text: TextSpan(children: [
        TextSpan(
            text: "SPI",
            style: TextStyle(
                color: HexColor(
                  "#ED1B24",
                ),
                fontSize: 20)),
        TextSpan(
            text: "KART",
            style: TextStyle(color: HexColor("#25317B"), fontSize: 20))
      ]),
    );
  }

  IconButton drawerMenu(scaffoldKey) {
    return IconButton(
        icon: Icon(Icons.menu, color: HexColor("#ED1B24")),
        onPressed: () {
          scaffoldKey.currentState.openDrawer();
        });
  }

  GestureDetector profleDetail(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(builder: (context) {
          return ProfilePage();
        }));
      },
      child: Padding(
        padding: const EdgeInsets.only(left: 5.0, right: 20.0, top: 8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: 85,
              width: 85,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(45.0),
                  image: DecorationImage(
                      image: AssetImage('assets/images/profile.png'),
                      fit: BoxFit.cover)),
            ),
            SizedBox(height: 15),
            Text("Amal Rahman",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 17,
                    fontWeight: FontWeight.bold)),
            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('On Duty',
                      style: TextStyle(
                          color: HexColor('#ED1B24'),
                          fontSize: 15,
                          fontWeight: FontWeight.bold)),
                  SizedBox(width: 20),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FlutterSwitch(
                      //activeColor:HexColor('#ED1B24'),
                      inactiveColor: HexColor('#ED1B24'),
                      width: 33.0,
                      height: 17.0,
                      toggleSize: 15.0,
                      padding: 1.0,
                      value: true,
                      onToggle: (val) {
                        // setState(() {
                        //   status = val;
                        // });
                      },
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Padding closeIcon(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 14, right: 14),
      child: Row(
        children: [
          Spacer(),
          GestureDetector(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Icon(
                Icons.close,
                color: HexColor('#707070'),
              ))
        ],
      ),
    );
  }

  GestureDetector drawerItems(String image, String title, Function onClick) {
    return GestureDetector(
      onTap: onClick(),
      child: ListTile(
        leading: Image.asset(image,
            color: HexColor('#ED1B24'), height: 34, width: 36),
        title: Text(title, style: TextStyle(fontSize: 15, color: Colors.black)),
      ),
    );
  }

  void _onRefresh() async {
    try {
      HomeController.to.onInit();
      HomeController.to.refreshController.refreshCompleted();
    } catch (e) {
      HomeController.to.refreshController.refreshFailed();
    }
  }

  void _onLoading() {
    try {
      HomeController.to.onInit();
      HomeController.to.refreshController.loadComplete();
    } catch (e) {
      HomeController.to.refreshController.refreshFailed();
    }
  }
}
