import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/UserInfoController/UserInfoController.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/util/constants.dart';
import 'package:medimallDelivery/widgets/myAppbar.dart';

class UserInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var height = SizeConfig.screenHeight * 1.225;
    var width = SizeConfig.screenWidth * 1.225;

    /// Local variables <<---------------------------------------------------->>
    var fontsize12 = height * .012;
    var colorblack = Color.fromRGBO(0, 0, 0, 1);
    var colorblacklighter = Color.fromRGBO(0, 0, 0, .5);
    var style = TextStyle(
        fontSize: fontsize12, fontFamily: "SegoeSemi,", color: colorblack);
    var styleReg = TextStyle(
        fontSize: fontsize12,
        fontFamily: "SegoeReg,",
        color: colorblacklighter);
    var styleRegView = TextStyle(
        fontSize: fontsize12,
        fontFamily: "SegoeReg,",
        color: Colors.black);
    var contentP = EdgeInsets.only(
        top: height * .015, bottom: height * .0, left: width * .008);
    UserInfoController.to.getData();
    return Scaffold(
      backgroundColor: HexColor('#FFFFFF'),
      appBar: myAppbar(context, 'User info'),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(right: 30, left: 25, top: 20),
              child:Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text(
                        "Name",
                        style: styleReg,
                      ),
                      Container(
                        height: 50,
                        child: TextFormField(
                          enabled: false,
                          style: styleRegView,
                          onChanged: (text) {},
                          controller: UserInfoController.to.nameController,
                          decoration: InputDecoration(contentPadding: contentP),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text(
                        "Email",
                        style: styleReg,
                      ),
                      Container(
                        height: 50,
                        child: TextFormField(
                          enabled: false,
                          style: styleRegView,
                          onChanged: (text) {},
                          controller: UserInfoController.to.emailController,
                          decoration: InputDecoration(contentPadding: contentP),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text(
                        "Mobile Number",
                        style: styleReg,
                      ),
                      Container(
                        height: 50,
                        child: TextFormField(
                          enabled: false,
                          style: styleRegView,
                          onChanged: (text) {},
                          controller: UserInfoController.to.mobileController,
                          decoration: InputDecoration(contentPadding: contentP),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text(
                        "Address",
                        style: styleReg,
                      ),
                      Container(
                        height: 50,
                        child: TextFormField(
                          enabled: false,
                          style: styleRegView,
                          onChanged: (text) {},
                          controller: UserInfoController.to.addressController,
                          decoration: InputDecoration(contentPadding: contentP),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text(
                        "City",
                        style: styleReg,
                      ),
                      Container(
                        height: 50,
                        child: TextFormField(
                          enabled: false,
                          style: styleRegView,
                          onChanged: (text) {},
                          controller: UserInfoController.to.cityController,
                          decoration: InputDecoration(contentPadding: contentP),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                    ],
                  ),
                  // userInfoContent("Address",controller.address.value,
                  //     styleReg, style, contentP),
                  // userInfoContent("City",controller.city.value, styleReg,
                  //     style, contentP),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Column userInfoContent(String title, String initialValue, var styleReg,
      var style, var contentP) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Text(
          title,
          style: styleReg,
        ),
        Container(
          height: 50,
          child: TextFormField(
            initialValue: initialValue,
            style: style,
            onChanged: (text) {},
            decoration: InputDecoration(contentPadding: contentP),
          ),
        ),
        SizedBox(
          height: 15,
        ),
      ],
    );
  }
}
