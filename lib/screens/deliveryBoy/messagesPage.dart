import 'package:flutter/material.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/widgets/myAppbar.dart';


class MessagesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: HexColor('#FFFFFF'),
      appBar: myAppbar(context, 'Messeges'),
      body: Padding(
        padding: const EdgeInsets.only(left: 12, right: 12, top: 14),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            messageContainer('New Delivery',
                'Lorem ipsum dolor sit amet consectetur adipiscing elit prese nt ant'),
            messageContainer('New Delivery',
                'Lorem ipsum dolor sit amet consectetur adipiscing elit prese nt ant'),
            messageContainer('New Delivery',
                'Lorem ipsum dolor sit amet consectetur adipiscing elit prese nt ant'),
          ],
        ),
      ),
    );
  }

  Padding messageContainer(String title, String text) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 7),
      child: Container(
        height: 77,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.shade400,
                blurRadius: 4,
              )
            ]),
        child: Padding(
          padding: const EdgeInsets.only(left: 17),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title,
                style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 3,
              ),
              Text(
                text,
                style: TextStyle(
                  fontSize: 13,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
