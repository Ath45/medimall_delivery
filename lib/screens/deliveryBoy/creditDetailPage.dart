import 'package:expansion_tile_card/expansion_tile_card.dart';
import 'package:flutter/material.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/widgets/customerDetail.dart';
import 'package:medimallDelivery/widgets/deliveryAddress.dart';
import 'package:medimallDelivery/widgets/myAppbar.dart';
import 'package:medimallDelivery/widgets/myButton.dart';
import 'package:medimallDelivery/widgets/productDetails.dart';
import 'package:medimallDelivery/widgets/pickedupAddress.dart';


class CreditDetail extends StatelessWidget {
  final GlobalKey<ExpansionTileCardState> statusKey = new GlobalKey();

  String statusItem = 'Picked Up';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: HexColor('#FFFFFF'),
      appBar: myAppbar(context, 'Credit Status'),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(right: 12, left: 12, top: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
             ProductDetails(),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Text(
                  'Customer Details',
                  style: TextStyle(
                      color: HexColor('#858585'), fontWeight: FontWeight.bold),
                ),
              ),
             CustomerDetail(),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Text(
                  'Pickup Address',
                  style: TextStyle(
                      color: HexColor('#858585'), fontWeight: FontWeight.bold),
                ),
              ),
              PickedUpAddress(),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Text(
                  'Delivery Address',
                  style: TextStyle(
                      color: HexColor('#858585'), fontWeight: FontWeight.bold),
                ),
              ),
              DeliveryAddress(),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Text(
                  'Credit',
                  style: TextStyle(
                      color: HexColor('#858585'), fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Container(
                  height: 122,
                  width: 400,
                  decoration: BoxDecoration(
                      color: HexColor('#FFFFFF'),
                      borderRadius: BorderRadius.circular(3),
                      boxShadow: [
                        BoxShadow(
                         color: Colors.grey.shade400,
                          blurRadius: 4,
                        )
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 13),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        paymentDetailRow('Total Amount', 'Rs 900'),
                        SizedBox(
                          height: 4,
                        ),
                        paymentDetailRow('Credit of Spikart', 'Rs 700'),
                        SizedBox(
                          height: 4,
                        ),
                        paymentDetailRow('Spikart Commision', 'Rs 20'),
                        SizedBox(
                          height: 4,
                        ),
                        paymentDetailRow('Delivery Charges', 'RS 100'),
                        SizedBox(
                          height: 4,
                        ),
                        paymentDetailRow('Total Payable', 'RS 850'),
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Text(
                  'Payment Status',
                  style: TextStyle(
                      color: HexColor('#858585'), fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 12, left: 12, top: 20),
                child: Container(
                  child: ListTileTheme(
                    dense: true,
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(color: Colors.grey.shade400, blurRadius: 4)
                          ],
                          borderRadius: BorderRadius.circular(5)),
                      child: ExpansionTileCard(
                        shadowColor: Colors.transparent,
                        baseColor: Colors.white,
                        key: statusKey,
                        title: Text(
                          statusItem,
                          style: TextStyle(fontSize: 13),
                        ),
                        //key: stateGlobalKey,
                        children: [
                          ListTile(
                            title: const Text('Picked Up'),
                            onTap: () {
                              // setState(() {
                              //   this.statusItem = 'Picked Up';
                              //   statusKey.currentState!.collapse();
                              // });
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              MyButton(
                text: 'Update',
              ),
              SizedBox(
                height: 50,
              )
            ],
          ),
        ),
      ),
    );
  }

  Row paymentDetailRow(String title, String text) {
    return Row(
      children: [
        Expanded(
            child: Text(
          title,
          style: TextStyle(
              fontSize: 13,
              color: HexColor('#858585'),
              fontWeight: FontWeight.bold),
        )),
        Text(': '),
        Expanded(
            child: Text(text,
                style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold)))
      ],
    );
  }
}
