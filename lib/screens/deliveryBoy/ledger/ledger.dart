import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:medimallDelivery/util/constants.dart';
import 'package:medimallDelivery/util/constantsDC.dart';
import 'package:medimallDelivery/widgets/myAppbar.dart';
class Ledger extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var height=SizeConfig.screenHeight*1.225;
    return Scaffold(
      appBar: myAppbar(context, "Ledger"),
      body: ListView(physics: ScrollPhysics(),
        children: [
          SizedBox(height: height*.02,),
          TopBox(title:"Account Balance"),
          SizedBox(height: height*.03,),
          SearchPeriod(),
          SizedBox(height: height*.02,),
          Container(child: CreditSession()),
          SizedBox(height: height*.02,),


        ],
      )
      ,
    );
  }
}

class CreditSession extends StatefulWidget {
  @override
  _CreditSessionState createState() => _CreditSessionState();
}

class _CreditSessionState extends State<CreditSession> {
  List creditType=[
    "All Credit",
    "All Credit1",
    "All Credit2",
    "All Credit3",
  ];
  String cerditValue="";
  @override
  Widget build(BuildContext context) {
    ///Local variables<<------------------------------------------------------>>
    var height=SizeConfig.screenHeight*1.225;
    var width=SizeConfig.screenWidth*1.225;

    var stylBlackFont13=TextStyle(color:colorblack,fontSize:13,fontFamily: "SegoeReg");
    ///Local variables<<------------------------------------------------------>>
    return Container(
      child: Column(
        children: [
          Container(
            height: height*.43*.122,width: width,
            margin: EdgeInsets.symmetric(horizontal: width*.025),
            padding: EdgeInsets.symmetric(horizontal: width*.025,
            vertical: height*.01),
            decoration: BoxDecoration(color:fillColor,
            borderRadius: BorderRadius.circular(height*.004),
            boxShadow: shadow),child:
            DropdownButton(isExpanded: true,
              underline: SizedBox(),
              icon: Icon(Icons.keyboard_arrow_down_rounded,color: colorblack,),
              hint: Text(cerditValue==null?creditType[0]:cerditValue,style: stylBlackFont13,),

              onChanged: (value)
              {
                setState(() {
                  cerditValue=value.toString();
                  print(cerditValue);
                });
              },
              items: creditType.map((e) => DropdownMenuItem(
              value: e,
                child: Text(e.toString()))).toList(),),
          ),
          SizedBox(height: height*.01,),
          Container(height: height*.155*3,
            child: ListView.builder(
              physics: NeverScrollableScrollPhysics(),

              itemCount: 3,
              itemBuilder: (context,index)
              {
                return CreditBox();
              },
            ),
          )

        ],
      ),
    );
  }
}
class CreditBox extends StatefulWidget {

  @override
  _CreditBoxState createState() => _CreditBoxState();
}

class _CreditBoxState extends State<CreditBox> {
  @override
  Widget build(BuildContext context) {
    var height=SizeConfig.screenHeight*1.225;
    var width=SizeConfig.screenWidth*1.225;
    var stylBlack50Font10=TextStyle(color:colorblack50,fontSize:height*.010,fontFamily: "SegoeReg");
    var stylBlue50Font12=TextStyle(color:Color.fromRGBO(257, 56, 132, 1),fontSize:height*.012,fontFamily: "SegoeSemi");
    var stylBlackFont12=TextStyle(color:colorblack,fontSize: height*.012,fontFamily: "SegoeSemi");
    var stylBlackFont12Reg=TextStyle(color:colorblack,fontSize:height*.012,fontFamily: "SegoeReg");
    return  Container(height: height*.108*1.225,width: width,
      margin: EdgeInsets.symmetric(horizontal: width*.025,vertical: height*.005),
      padding:EdgeInsets.only(top: height*.01) ,
      decoration: BoxDecoration(color: fillColor,
          boxShadow: shadow,
          borderRadius: BorderRadius.circular(height*.004)),
      child:
      Column(
        children: [
          Container(height: height*.032*1.22,
            padding:EdgeInsets.symmetric(horizontal: width*.02) ,
            child: Row(
              children: [
                Container(width: width*.355,
                  child: Text("Description",style: stylBlackFont12,),
                ),
                Container(width: width*.15,
                  child: Text("Credit",style: stylBlackFont12,),
                ),
                Container(width: width*.12,
                  child: Text("Debit",style: stylBlackFont12,),
                ),
                Container(width: width*.1,
                  child: Text("Balance",style: stylBlackFont12,),
                )
              ],
            ),
          ),
          Container(height: height*.032*1.22,
            padding: EdgeInsets.only(left: width*.02),
            color:Color.fromRGBO(237, 248, 255, 1),
            child: Row(
              children: [
                Container(width: width*.36,
                  child: Text("21 jan 2021 closing ",style: stylBlackFont12Reg,),
                ),
                Container(width: width*.15,
                  child: Text("₹ 5000",style: stylBlackFont12,),
                ),
                Container(width: width*.12,
                  child: Text(" -",style: stylBlackFont12,),
                ),
                Container(width: width*.1,
                  child: Text("₹ 5000",style: stylBlackFont12,),
                )
              ],
            ),

          ),
          Container(
            padding: EdgeInsets.only(top: height*.01,left:width*.025),
            child: Row(
              children: [
                Container(width: width*.15,
                    child: Text("ID: POK558",style: stylBlack50Font10,)),
                Container(width: width*.15,
                    child: Text("12-dec-2021",style: stylBlack50Font10,)),
                Container(width: width*.31,
                    child: Text("12:20 am",style: stylBlack50Font10,)),
                Container(width: width*.12,
                    child: Text("Not Paid",style: stylBlue50Font12,)),
              ],
            ),
          )
        ],
      ),);
  }
}


// ignore: must_be_immutable
class TopBox extends StatefulWidget {
  String title;
  TopBox({
    required this.title
});
  _TopBoxState createState() => _TopBoxState();
}

class _TopBoxState extends State<TopBox> {
  @override
  Widget build(BuildContext context) {
    var height=SizeConfig.screenHeight*1.225;
    var width=SizeConfig.screenWidth*1.225;

    var fontsize15=height*.012*1.2;
    var fontsize20=height*.02;
    var styleRegGrey=TextStyle(color: Color.fromRGBO(98, 98, 100, .7),fontSize: fontsize15,fontFamily: "SegoeReg");
    var styleBold=TextStyle(color: Color.fromRGBO(98, 98, 100, 1),fontSize: fontsize20,fontFamily: "SegoeBold");
    var  shadow= [BoxShadow(color: Color.fromRGBO(0, 0, 0, .07),blurRadius: 10)];
    var fillColor=Colors.white;
    return  Container(
      height:height*.70*.122,width: width,
      margin: EdgeInsets.symmetric(horizontal: width*.025),
      padding: EdgeInsets.symmetric(horizontal: width*.035),
      decoration: BoxDecoration(boxShadow: shadow,
          borderRadius: BorderRadius.circular(height*.04*.122),
          color: fillColor),child:
    Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(widget.title,style:
        styleRegGrey,),
        Text("₹ 1500",style: styleBold,)
      ],),

    );
  }
}


class SearchPeriod extends StatefulWidget {
  @override
  _SearchPeriodState createState() => _SearchPeriodState();
}

class _SearchPeriodState extends State<SearchPeriod> {
  @override
  Widget build(BuildContext context) {
    var height=SizeConfig.screenHeight*1.225;
    var width=SizeConfig.screenWidth*1.225;
    var fontsize15=height*.012;
    var styleRegGrey=TextStyle(color: Color.fromRGBO(98, 98, 100, 1),fontSize: fontsize15,fontFamily: "SegoeReg");
    var styleRegGrey70=TextStyle(color: Color.fromRGBO(98, 98, 100, .7),fontSize: fontsize15,fontFamily: "SegoeReg");
    return Container(
      margin: EdgeInsets.symmetric(horizontal: width*.04),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Search Period",style:styleRegGrey70,),
          SizedBox(height: height*.005,),
          Row(children: [
            Column(crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("From",style: styleRegGrey,),
                Container(height: height*.028,width: width*.27,
                    child: TextFormField(
                        decoration: InputDecoration(
                            border: UnderlineInputBorder(borderSide: BorderSide(color:Color.fromRGBO(98, 99, 100, 1)))
                        )
                    ))
              ],
            ),
            SizedBox(width: width*.06,),
            Column(crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("To",style: styleRegGrey,),
                Container(height: height*.025,width: width*.27,
                    child: TextFormField(decoration: InputDecoration(
                      border: UnderlineInputBorder(borderSide: BorderSide(color:Color.fromRGBO(98, 99, 100, 1)))
                    ),))
              ],
            ),
            SizedBox(width: width*.08,),
            Column(
              children: [
                SizedBox(width: width*.05,height: width*.05,),
                Image(image: AssetImage("assets/icons/search.png"),height: height*.02,)
              ],
            )
          ],)
        ],
      ),
    );
  }
}
