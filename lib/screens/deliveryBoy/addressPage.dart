
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/widgets/myAppbar.dart';



import '../../util/constants.dart';

class AddressPage extends StatelessWidget {
  ///Local variables  Text Controller <<-------------------------------------->>
TextEditingController buildingNameController =TextEditingController(text:"Loream",);
TextEditingController buildingNumberController =TextEditingController(text:"Loream",);
TextEditingController postController =TextEditingController(text:"Loream",);
TextEditingController districtController  =TextEditingController(text:"Loream",);
TextEditingController cityController  =TextEditingController(text:"Loream",);
TextEditingController stateController  =TextEditingController(text:"Loream",);
TextEditingController pinCodeController  =TextEditingController(text:"Loream",);
///Local variables  Text Controller <<---------------------------------------->>

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var height=SizeConfig.screenHeight*1.225;
    var width=SizeConfig.screenWidth*1.225;

    /// Local variables <<---------------------------------------------------->>
    var fontsize13=height*.013;
    var colorblack= Color.fromRGBO(0, 0, 0, 1);
    var colorblacklighter= Color.fromRGBO(0, 0, 0, .5);
    var style=TextStyle(fontSize: fontsize13,fontFamily: "SegoeSemi,",color: colorblack);
    var styleReg=TextStyle(fontSize: fontsize13,fontFamily: "SegoeReg,",color: colorblacklighter);
    var borderDec=UnderlineInputBorder(borderSide: BorderSide(color: Colors.black,width: 0.5));
    var contentP=EdgeInsets.only(bottom: height*.02,left: width*.008);
    /// Local variables <<---------------------------------------------------->>

    return Scaffold(
      resizeToAvoidBottomInset: true,
        backgroundColor: HexColor('#FFFFFF'),
        appBar: myAppbar(context, 'Address'),
        body: ListView(
          children: [
            Container(margin: EdgeInsets.symmetric(horizontal: width*.05),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: height*.03,),
                  Text("Building/House Number",style: styleReg,),
                  Container(height: height*.035,
                    child:   TextFormField(
                      controller: buildingNumberController,
                      style: style,
                      validator: (value){
                        if(value!.isEmpty){
                          return "This field required";
                        }else{
                          return null;
                        }
                      },
                      autovalidateMode: AutovalidateMode.onUserInteraction,

                      decoration: InputDecoration(
                        contentPadding: contentP,
                        enabledBorder: borderDec,
                        focusedBorder:borderDec,
                      ),
                    ),
                  ),
                  SizedBox(height: height*.03,),
                  Text("Building/House Name",style: styleReg,),
                  Container(height: height*.035,
                    child:   TextFormField(
                      controller: buildingNameController,
                      style: style,
                      validator: (value){
                        if(value!.isEmpty){
                          return "This field required";
                        }else{
                          return null;
                        }
                      },
                      autovalidateMode: AutovalidateMode.onUserInteraction,

                      decoration: InputDecoration(
                        contentPadding: contentP,
                        enabledBorder: borderDec,
                        focusedBorder:borderDec,
                      ),
                    ),
                  ),
                  SizedBox(height: height*.03,),
                  Text("Post office",style: styleReg,),
                  Container(height: height*.04,
                    child:   TextFormField(
                      controller: postController,
                      style: style,
                      validator: (value){
                        if(value!.isEmpty){
                          return "This field required";
                        }else{
                          return null;
                        }
                      },
                      autovalidateMode: AutovalidateMode.onUserInteraction,

                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.only(),
                        enabledBorder: borderDec,
                        focusedBorder:borderDec,
                      ),
                    ),
                  ),
                  SizedBox(height: height*.03,),
                  Text("District",style: styleReg,),
                  Container(height: height*.04,
                    child:   TextFormField(
                      controller: districtController,
                      style: style,
                      validator: (value){
                        if(value!.isEmpty){
                          return "This field required";
                        }else{
                          return null;
                        }
                      },
                      autovalidateMode: AutovalidateMode.onUserInteraction,

                      decoration: InputDecoration(
                        contentPadding: contentP,
                        enabledBorder: borderDec,
                        focusedBorder:borderDec,
                      ),
                    ),
                  ),
                  SizedBox(height: height*.03,),
                  Text("City",style: styleReg,),
                  Container(height: height*.035,
                    child:   TextFormField(
                      controller: cityController,
                      style: style,
                      validator: (value){
                        if(value!.isEmpty){
                          return "This field required";
                        }else{
                          return null;
                        }
                      },
                      autovalidateMode: AutovalidateMode.onUserInteraction,

                      decoration: InputDecoration(
                        contentPadding: contentP,
                        enabledBorder: borderDec,
                        focusedBorder:borderDec,
                      ),
                    ),
                  ),
                  SizedBox(height: height*.03,),
                  Text("State",style: styleReg,),
                  Container(height: height*.035,
                    child:   TextFormField(
                      controller: stateController,
                      style: style,
                      validator: (value){
                        if(value!.isEmpty){
                          return "This field required";
                        }else{
                          return null;
                        }
                      },
                      autovalidateMode: AutovalidateMode.onUserInteraction,

                      decoration: InputDecoration(
                        contentPadding: contentP,
                        enabledBorder: borderDec,
                        focusedBorder:borderDec,
                      ),
                    ),
                  ),
                  SizedBox(height: height*.03,),
                  Text("Pin Code",style: styleReg,),
                  Container(height: height*.035,
                    child:   TextFormField(
                      controller: pinCodeController,
                      style: style,
                      validator: (value){
                        if(value!.isEmpty){
                          return "This field required";
                        }else{
                          return null;
                        }
                      },
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      decoration: InputDecoration(
                        contentPadding: contentP,
                        enabledBorder: borderDec,
                        focusedBorder:borderDec,
                      ),
                    ),
                  ),



                ],
              ),
            ),
            SizedBox(height: height*.03,),

          ],
        ));
  }

}
