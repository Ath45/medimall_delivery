import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Routes/routes.dart';
import 'package:medimallDelivery/screens/deliveryBoy/userInfo.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/widgets/myAppbar.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../loginPage.dart';
import 'addressPage.dart';
import 'bankDetailPage.dart';

class ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: HexColor('#FFFFFF'),
      appBar: myAppbar(context, 'My Account'),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(top: 7),
          child: Column(
            children: [
              Container(
                height: 134,
                color: HexColor('#ED1B24'),
                child: Padding(
                  padding: const EdgeInsets.only(top: 20, bottom: 20),
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Stack(
                          children: [
                            Container(
                              height: 71,
                              width: 69,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(45.0),
                                  image: DecorationImage(
                                      image: AssetImage(
                                          "assets/images/profile.png"),
                                      fit: BoxFit.cover)),
                            ),
                            Align(
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(left: 45, top: 30),
                                child: Container(
                                  height: 31,
                                  width: 30,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(45.0),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(6),
                                    child:
                                        Image.asset('assets/icons/camera.png'),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Row(
                                children: [
                                  Text('Amal Rahman',
                                      style: TextStyle(
                                          fontSize: 18,
                                          color: HexColor('#FFFFFF'))),
                                  SizedBox(width: 90),
                                  Container(
                                      height: 28,
                                      width: 28,
                                      decoration: BoxDecoration(
                                          color: HexColor('#FFFFFF'),
                                          shape: BoxShape.circle),
                                      child:
                                          Icon(Icons.edit_outlined, size: 20))
                                ],
                              ),
                              Text('amalrahman@gmail.com',
                                  style: TextStyle(
                                      fontSize: 18,
                                      color: HexColor('#FFFFFF'))),
                              Text('+91 85412679565',
                                  style: TextStyle(
                                      fontSize: 18,
                                      color: HexColor('#FFFFFF'))),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 40, left: 40, top: 35),
                child: Column(
                  children: [
                    profileContents('User Info', () {
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) {
                        return UserInfo();
                      }));
                    }),
                    profileContents('Address', () {
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) {
                        return AddressPage();
                      }));
                    }),
                    profileContents('Bank Details', () {
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) {
                        return BankDetailPage();
                      }));
                    }),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 60),
                child: GestureDetector(
                  onTap: () async {
                    SharedPreferences preferences =
                        await SharedPreferences.getInstance();
                    await preferences.clear();
                    Get.offAllNamed(loginRoute);
                  },
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ImageIcon(
                          AssetImage("assets/icons/logout.png"),
                          color: HexColor('#ED1B24'),
                          size: 30,
                        ),
                        SizedBox(width: 5),
                        Text('Logout',
                            style: TextStyle(
                                fontSize: 13, color: HexColor('#ED1B24')))
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  GestureDetector profileContents(String title, Function onClick) {
    return GestureDetector(
      onTap: onClick(),
      child: Padding(
        padding: const EdgeInsets.only(top: 5),
        child: Container(
          height: 38,
          width: 331,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(3),
              color: HexColor('#FFFFFF'),
              boxShadow: [
                BoxShadow(color: HexColor('#60606029'), blurRadius: 4)
              ]),
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 16),
                child: Text(title, style: TextStyle(fontSize: 12)),
              ),
              Spacer(),
              Padding(
                  padding: const EdgeInsets.only(right: 25),
                  child: Icon(Icons.navigate_next))
            ],
          ),
        ),
      ),
    );
  }
}
