import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/util/constants.dart';
import 'package:medimallDelivery/util/contantRiyas.dart';
import 'package:medimallDelivery/widgets/customerDetail.dart';
import 'package:medimallDelivery/widgets/deliveryAddress.dart';
import 'package:medimallDelivery/widgets/myAppbar.dart';
import 'package:medimallDelivery/widgets/productDetails.dart';
import 'package:medimallDelivery/widgets/subHeading.dart';

class CancelledOrderDetail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: HexColor('#FAFAFA'),
      appBar: myAppbar(context, 'Cancelled Orders'),
      body: SingleChildScrollView(
        child: Padding(
          padding:  EdgeInsets.symmetric(horizontal: SizeConfig.horizontal*3,),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ProductDetails(),
              SubHeading(text: 'Customer Details',),
              CustomerDetail(),
              SizedBox(height: SizeConfig.horizontal*3,),
              SubHeading(text: 'Delivery Address',),
              DeliveryAddress(),
              SizedBox(height: SizeConfig.horizontal*3,),
              SubHeading(text: 'Delivery Status',),
              Padding(
                padding:  EdgeInsets.symmetric(vertical: SizeConfig.horizontal*1.5),
                child: Container(
                  alignment: Alignment.centerLeft,
                  width: SizeConfig.horizontal*100,
                  decoration: BoxDecoration(
                      color: HexColor('#FFFFFF'),
                      borderRadius: BorderRadius.circular(5),
                      boxShadow: myBoxShadow),
                  child: Padding(
                    padding:  EdgeInsets.symmetric(horizontal: SizeConfig.horizontal*3,vertical: SizeConfig.horizontal*4),
                    child: Text(
                      'Cancelled',
                      style: TextStyle(
                        fontSize: SizeConfig.horizontal*3.2,fontFamily: 'SegoeReg'
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: SizeConfig.horizontal*3,),
              SubHeading(text: 'Reason for Canceling',),
              Padding(
                padding:  EdgeInsets.symmetric(vertical: SizeConfig.horizontal*1.5),
                child: Container(
                  alignment: Alignment.centerLeft,
                  width: SizeConfig.horizontal*100,
                  decoration: BoxDecoration(
                      color: HexColor('#FFFFFF'),
                      borderRadius: BorderRadius.circular(5),
                      boxShadow: myBoxShadow),
                  child: Padding(
                    padding:  EdgeInsets.symmetric(horizontal: SizeConfig.horizontal*3,vertical: SizeConfig.horizontal*4),
                    child: Text(
                      'Other',
                      style: TextStyle(
                          fontSize: SizeConfig.horizontal*3.2,fontFamily: 'SegoeReg'
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: SizeConfig.horizontal*3,),
              SubHeading(text: 'Comments',),
              Padding(
                padding:  EdgeInsets.symmetric(vertical: SizeConfig.horizontal*1.5),
                child: Container(
                  alignment: Alignment.centerLeft,
                  width: SizeConfig.horizontal*100,
                  decoration: BoxDecoration(
                      color: HexColor('#FFFFFF'),
                      borderRadius: BorderRadius.circular(5),
                      boxShadow: myBoxShadow),
                  child: Padding(
                    padding:  EdgeInsets.symmetric(horizontal: SizeConfig.horizontal*3,vertical: SizeConfig.horizontal*4),
                    child: Text(
                      'Lorem ipsum dolor sit amet consectetur adipiscing elit present anti',
                      style: TextStyle(
                          fontSize: SizeConfig.horizontal*3.2,fontFamily: 'SegoeReg'
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding:  EdgeInsets.symmetric(horizontal: SizeConfig.horizontal*5,vertical: SizeConfig.horizontal*5),
                child: Container(
                  child: Column(
                    children: [
                      deliveryAmount('COD Paid amount','522'),
                      SizedBox(height: SizeConfig.horizontal*3.5,),
                      deliveryAmount('COD Paid amount','522'),
                      SizedBox(height: SizeConfig.horizontal*3.5,),
                      deliveryAmount('COD Paid amount','522'),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Container deliveryAmount(String text,String amount) {
    return Container(
                      height: SizeConfig.horizontal*11,
                      decoration: BoxDecoration(
                        color: HexColor('#4D9EFF'),
                        borderRadius: BorderRadius.circular(5),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.15),
                            offset: Offset(0,3),
                            blurRadius: 6
                          )
                        ]
                      ),
                      child: Padding(
                        padding:  EdgeInsets.symmetric(horizontal: SizeConfig.horizontal*3),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(text,style: TextStyle(color: Colors.white,fontSize: SizeConfig.horizontal*3.8,fontFamily: 'SegoeReg'),),
                            Text('₹ '+amount,style: TextStyle(color: Colors.white,fontSize: SizeConfig.horizontal*3.8,fontFamily: 'SegoeBold'),)
                          ],
                        ),
                      ),
                    );
  }
}
