import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Routes/routes.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/util/constants.dart';
import 'package:medimallDelivery/widgets/myAppbar.dart';
import 'package:medimallDelivery/widgets/ordersListItem.dart';
import 'cancelledOrderDetail.dart';

class CancelledOrders extends StatelessWidget {

  List cancelledOrder = [
    {
      'name': 'John',
      'price': '434',
      'orderId': '64645475',
      'delivery': 'Cash on Delivery'
    },
    {
      'name': 'Amal Rahman',
      'price': '299',
      'orderId': '64645475',
      'delivery': 'Cash on Delivery'
    },
    {
      'name': 'Sijo',
      'price': '644',
      'orderId': '64645475',
      'delivery': 'Cash on Delivery'
    },
    {
      'name': 'Rohith',
      'price': '444',
      'orderId': '64645475',
      'delivery': 'Cash on Delivery'
    },
  ];
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: HexColor('#FAFAFA'),
      appBar: myAppbar(context, 'Cancelled Orders'),
        body: ListView.builder(
          padding: EdgeInsets.symmetric(vertical: SizeConfig.horizontal * 3,horizontal: SizeConfig.horizontal*2.5),
          shrinkWrap: true,
          itemCount: cancelledOrder.length,
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () {
                Get.toNamed(cancelledOrderDetails);
              },
              child: orderListItem(
                name: cancelledOrder[index]['name'],
                price:  cancelledOrder[index]['price'],
                orderId: cancelledOrder[index] ['orderId'],
                delivery:  cancelledOrder[index]['delivery'],
                read:  cancelledOrder[index]['delivery'],
              ),
            );
          },
        ));
  }
}
