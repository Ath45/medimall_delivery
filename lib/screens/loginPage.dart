import 'package:flutter/material.dart';
import 'package:medimallDelivery/Controller/LoginController/LoginController.dart';
import 'package:medimallDelivery/Routes/routes.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:get/get.dart';


class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        // image: DecorationImage(
        //   image: AssetImage("assets/images/registerbg.png"),
        // )
        gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomCenter,
            colors: [
              HexColor("#00AAFF"),
              HexColor("#005BEC"),
            ]),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: SingleChildScrollView(
          child: Form(
            key: LoginController.to.LoginformKey,
            child: Stack(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 130.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset(
                            "assets/images/delilogo.png",
                            height: 208,
                            width: 161,
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 40.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width - 45,
                            child: TextFormField(
                              cursorColor: HexColor("#F5CF00"),
                              //controller: emailcontroller,
                              validator: (value){
                                return LoginController.to
                                    .validateEmail(value!);
                              },
                              onChanged: (value) {
                                LoginController.to.username = value;
                              },
                              style: TextStyle(color: Colors.white),
                              decoration: InputDecoration(
                                isDense: true,
                                hintText: "EMAIL",
                                hintStyle: TextStyle(
                                    color: Colors.white54, fontSize: 12),
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.white, width: 0.5),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.white, width: 0.5),
                                ),
                                errorStyle:
                                    TextStyle(color:  Colors.red),
                                errorBorder: new UnderlineInputBorder(
                                  borderSide: new BorderSide(
                                      color: HexColor("#F5CF00"), width: 0.0),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 30.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Stack(
                            children: [
                              Container(
                                width: MediaQuery.of(context).size.width - 45,
                                child: TextFormField(
                                  cursorColor: HexColor("#F5CF00"),
                                  // controller: passwordcontroller,
                                  validator: (value){
                                    return LoginController.to
                                        .validatePass(value!);
                                  },
                                  onChanged: (value) {
                                    LoginController.to.password = value;
                                  },
                                  obscureText: true,
                                  style: TextStyle(color: Colors.white),
                                  autovalidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  decoration: InputDecoration(
                                    isDense: true,
                                      hintText: "PASSWORD",
                                      errorBorder: new UnderlineInputBorder(
                                        borderSide: new BorderSide(
                                            color: HexColor("#F5CF00"),
                                            width: 0.0),
                                      ),
                                      hintStyle: TextStyle(
                                          color: Colors.white54, fontSize: 12),
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.white, width: 0.5),
                                      ),
                                      focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.white, width: 0.5),
                                      ),
                                      errorStyle: TextStyle(
                                          color:  Colors.red)),
                                ),
                              ),
                              // Positioned(
                              //     right: 0.0,
                              //     bottom: 5.0,
                              //     child: Text(
                              //       "forgot?",
                              //       style: TextStyle(
                              //           fontSize: 12, color: Colors.white54),
                              //     ))
                            ],
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 45.0),
                      child: GestureDetector(
                        onTap: () {
                          if (LoginController.to.LoginformKey.currentState!
                              .validate()) {
                            LoginController.to.verifyCallOtp(context);
                          }
                        },
                        child: Container(
                          height: 48,
                          width: 165,
                          decoration: BoxDecoration(
                              color: HexColor("#FFFFFF"),
                              borderRadius: BorderRadius.circular(8.0),
                              boxShadow: [
                                BoxShadow(
                                  color: HexColor("#00000029"),
                                  blurRadius: 6,
                                )
                              ]),
                          child: Center(
                            child: Text(
                              "Sign in",
                              style: TextStyle(
                                  fontSize: 17,
                                  color: HexColor("#005CEC"),
                                  fontFamily: "Segoe UI"),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 60.0),
                      child: Container(
                        width: MediaQuery.of(context).size.width - 45,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              "Don't have an account ?",
                              style: TextStyle(
                                  color: HexColor("#FFFFFF"),
                                  fontSize: 12,
                                  fontFamily: "Segoe UI"),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            GestureDetector(
                              onTap: () {
                                Get.toNamed(signUpRoute);
                              },
                              child: Container(
                                height: 22,
                                width: 55,
                                decoration: BoxDecoration(
                                  color: HexColor("#4D9EFF"),
                                  borderRadius: BorderRadius.circular(5.0),
                                ),
                                child: Center(
                                  child: Text(
                                    "Sign up",
                                    style: TextStyle(
                                        fontSize: 12,
                                        color: HexColor("#FFFFFF")),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                Positioned(
                    right: 0,
                    top: 160.0,
                    child: Image.asset(
                      "assets/images/deliveryboy.png",
                      height:
                          MediaQuery.of(context).size.width < 390 ? 120 : 127,
                      width:
                          MediaQuery.of(context).size.width < 390 ? 140 : 147,
                    ))
              ],
            ),
          ),
        ),
      ),
    ));
  }
}

// class LoginPage extends StatefulWidget {
//   @override
//   _LoginPageState createState() => _LoginPageState();
// }
//
// class _LoginPageState extends State<LoginPage> {
//   final _formKey = GlobalKey<FormState>();
//   TextEditingController emailcontroller = new TextEditingController();
//   TextEditingController passwordcontroller = new TextEditingController();
//   bool _autovalidate = false;
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       height: MediaQuery.of(context).size.height,
//       width: MediaQuery.of(context).size.width,
//       decoration: BoxDecoration(
//         // image: DecorationImage(
//         //   image: AssetImage("assets/images/registerbg.png"),
//         // )
//         gradient: LinearGradient(
//             begin: Alignment.topRight,
//             end: Alignment.bottomCenter,
//             colors: [
//               HexColor("#00AAFF"),
//               HexColor("#005BEC"),
//             ]
//         ),
//       ),
//       child: Scaffold(
//         backgroundColor: Colors.transparent,
//         body: SingleChildScrollView(
//           child: Form(
//             key: _formKey,
//             autovalidate: _autovalidate,
//             child: Stack(
//               children: [
//                 Column(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: [
//                     Padding(
//                       padding: const EdgeInsets.only(top:130.0),
//                       child: Row(
//                         mainAxisAlignment: MainAxisAlignment.center,
//                         children: [
//                           Image.asset("assets/images/delilogo.png",height: 208,width: 161,),
//                         ],
//                       ),
//                     ),
//                     Padding(
//                       padding: const EdgeInsets.only(top:20.0),
//                       child: Row(
//                         mainAxisAlignment: MainAxisAlignment.center,
//                         children: [
//                           Container(
//                             width: MediaQuery.of(context).size.width - 45,
//                             child: TextFormField(
//                               cursorColor: HexColor("#F5CF00"),
//                               controller: emailcontroller,
//                               validator: (value){
//                                 if(value.toString().contains("@") && value.toString().contains(".com")){
//                                   return null;
//                                 }else if(value.toString().isEmpty){
//                                   return "This field required";
//                                 }else {
//                                   return "Invalid Email";
//                                 }
//                               },
//                               style: TextStyle(color: Colors.white),
//                               decoration: InputDecoration(
//                                 hintText: "EMAIL",
//                                 hintStyle: TextStyle(color: Colors.white54,fontSize: 12),
//                                 enabledBorder: UnderlineInputBorder(
//                                   borderSide: BorderSide(color: Colors.white,width: 0.5),
//                                 ),
//                                 focusedBorder: UnderlineInputBorder(
//                                   borderSide: BorderSide(color: Colors.white,width: 0.5),
//                                 ),
//                                   errorStyle: TextStyle(
//                                       color: HexColor("#F5CF00")
//                                   ),
//                                 errorBorder:  new UnderlineInputBorder(
//                                   borderSide: new BorderSide(color: HexColor("#F5CF00"), width: 0.0),
//                                 ),
//                               ),
//                             ),
//                           )
//                         ],
//
//                       ),
//                     ),
//                     Padding(
//                       padding: const EdgeInsets.only(top:20.0),
//                       child: Row(
//                         mainAxisAlignment: MainAxisAlignment.center,
//                         children: [
//                           Stack(
//                             children: [
//                               Container(
//                                 width: MediaQuery.of(context).size.width - 45,
//                                 child: TextFormField(
//                                   cursorColor: HexColor("#F5CF00"),
//                                   controller: passwordcontroller,
//                                   validator: (value){
//                                     if (value.toString().isEmpty) {
//                                       return "This field required";
//                                     } else if (value.toString().length < 6) {
//                                       return "Password must be atleast 6 characters long";
//                                     } else {
//                                       return null;
//                                     }
//                                   },
//                                   obscureText: true,
//                                   style: TextStyle(color: Colors.white),
//                                   autovalidateMode: AutovalidateMode.onUserInteraction,
//                                   decoration: InputDecoration(
//                                     hintText: "PASSWORD",
//                                     errorBorder:  new UnderlineInputBorder(
//                                       borderSide: new BorderSide(color: HexColor("#F5CF00"), width: 0.0),
//                                     ),
//                                     hintStyle: TextStyle(color: Colors.white54,fontSize: 12),
//                                     enabledBorder: UnderlineInputBorder(
//                                       borderSide: BorderSide(color: Colors.white,width: 0.5),
//                                     ),
//                                     focusedBorder: UnderlineInputBorder(
//                                       borderSide: BorderSide(color: Colors.white,width: 0.5),
//                                     ),
//                                     errorStyle: TextStyle(
//                                       color: HexColor("#F5CF00")
//                                     )
//                                   ),
//                                 ),
//                               ),
//                               Positioned(
//                                   right: 0.0,
//                                   bottom: 5.0,
//                                   child: Text("forgot?",style: TextStyle(fontSize: 12,color: Colors.white54),))
//                             ],
//                           )
//                         ],
//                       ),
//                     ),
//
//                     Padding(
//                       padding: const EdgeInsets.only(top:45.0),
//                       child: GestureDetector(
//                         onTap: (){
//                           if (_formKey.currentState!.validate()) {
//                              setState(() {
//                             _autovalidate = true;
//                              });
//                              Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage() ));
//                           }
//                         },
//                         child: Container(
//                           height: 48,
//                           width: 165,
//                           decoration: BoxDecoration(
//                               color: HexColor("#FFFFFF"),
//                               borderRadius: BorderRadius.circular(8.0),
//                               boxShadow: [
//                                 BoxShadow(
//                                   color: HexColor("#00000029"),
//                                   blurRadius: 6,
//                                 )
//                               ]
//                           ),
//                           child: Center(
//                             child: Text("Sign in",style: TextStyle(fontSize: 17,color: HexColor("#005CEC"),fontFamily: "Segoe UI"),),
//                           ),
//                         ),
//                       ),
//                     ),
//                     Padding(
//                       padding: const EdgeInsets.only(top:60.0),
//                       child: Container(
//                         width: MediaQuery.of(context).size.width - 45,
//                         child: Row(
//                           mainAxisAlignment: MainAxisAlignment.end,
//                           children: [
//                             Text("Don't have an account ?",style: TextStyle(color: HexColor("#FFFFFF"),fontSize: 12,fontFamily: "Segoe UI"),),
//                             SizedBox(width: 10,),
//                             GestureDetector(
//                               onTap: (){
//                                Get.toNamed(registerPageRoute);
//                               },
//                               child: Container(
//                                 height: 22,
//                                 width: 55,
//                                 decoration: BoxDecoration(
//                                   color: HexColor("#4D9EFF"),
//                                   borderRadius: BorderRadius.circular(5.0),
//                                 ),
//                                 child: Center(
//                                   child: Text("Sign up",style: TextStyle(fontSize: 12,color: HexColor("#FFFFFF")),),
//                                 ),
//                               ),
//                             )
//                           ],
//                         ),
//                       ),
//                     ),
//                   ],
//                 ),
//                 Positioned(
//                     right: MediaQuery.of(context).size.width < 390 ? 5.0 : 15.0,
//                     top: 160.0,
//                     child: Image.asset("assets/images/deliveryboy.png",height: MediaQuery.of(context).size.width < 390 ? 120 : 127,width: MediaQuery.of(context).size.width < 390 ? 140 : 147,))
//               ],
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }
