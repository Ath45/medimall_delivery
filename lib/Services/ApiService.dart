import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:medimallDelivery/API.dart';
import 'package:medimallDelivery/Model/signUpModel/RegisterModel.dart';

class Api {
  static Future getVerifyData(SignUpRequestModel data) async {
    var responseData;
    Uri url = Uri.parse(baseUrl + registerURL);
    var request = http.MultipartRequest("POST", url);
    print("LAst" + data.rcBookImage.path.split("/").last);
    print("LAst" + data.adhaarImage.path.split("/").last);
    print("LAst" + data.licenseImage.path.split("/").last);
    var rcBook = await http.MultipartFile.fromPath(
        "rcBook", data.rcBookImage.path,
        filename: data.rcBookImage.path.split("/").last,
        contentType: new MediaType('image', 'jpg'));
    var aadhar = await http.MultipartFile.fromPath(
        "aadhar", data.adhaarImage.path,
        filename: data.adhaarImage.path.split("/").last,
        contentType: new MediaType('image', 'jpg'));
    var licence = await http.MultipartFile.fromPath(
        "licence", data.licenseImage.path,
        filename: data.licenseImage.path.split("/").last,
        contentType: new MediaType('image', 'jpg'));
    // request.files.addAll(await [rcBook,aadhar,licence]);
    request.files.add(rcBook);
    request.files.add(aadhar);
    request.files.add(licence);

    request.fields["fullName"] = data.fullName;
    request.fields["mobile"] = data.mobileNumber;
    request.fields["email"] = data.emailId;
    request.fields["password"] = data.passwordRepeat;
    request.fields["drivingLicenseNumber"] = data.licenceNumber;
    request.fields["address"] = data.address;
    request.fields["city"] = data.city;
    var response = await request.send();
    print(response.stream.bytesToString());
    if (response.statusCode == 200) {
      var responseString = await response.stream.bytesToString();
      print("Data" + "Js" + responseString.toString());
      var jsData = json.decode(responseString.toString());
      return jsData;
    } else {
      print(response.stream.transform(utf8.decoder).listen((event) {
        responseData = json.decode(event);
        return responseData;
      }));
    }
    // return responseData;
  }

  static loginService(String use, String pass) async {
    var params = {"email": use, "password": pass};
    var response = await Dio().post(
      baseUrl + 'delivery/delivery_boys_signin',
      data: jsonEncode(params),
    );

    if (response.statusCode == 200) {
      return (response.data);
    } else {
      //do something for error
    }
    print("Function Worked");
  }

  static getProfile(String auth, String use) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "$auth";
    var response = await dio.get(
      baseUrl + 'delivery-user/get_delivery_boy_profile?status=""$use"',
    );
    if (response.statusCode == 200) {
      // print("data true" + response.data.toString());
      return (response.data);
    } else if (response.statusCode == 500) {}
  }

  static getDeliverd(String auth, String use) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "$auth";
    var response = await dio.get(
      baseUrl + 'delivery-user/delivered_orders',
    );
    if (response.statusCode == 200) {
      print("data true" + response.data.toString());
      return (response.data);
    } else if (response.statusCode == 500) {}
  }

  static getReturnOrder(String auth, String use) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "$auth";
    var response = await dio.get(
      baseUrl + 'delivery-user/return_orders',
    );
    if (response.statusCode == 200) {
      print("data true" + response.data.toString());
      return (response.data);
    } else if (response.statusCode == 500) {}
  }

  static changeStatusAvailable(String auth, String offline) async {
    var params = {"status": offline};
    print("Params" + params.toString());
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "$auth";
    var response = await dio.post(
      baseUrl + 'delivery-user/change_status',
      data: jsonEncode(params),
    );
    if (response.statusCode == 200) {
      print("data true" + response.data.toString());
      return (response.data);
    } else if (response.statusCode == 500) {}
  }

  static getOrders(String auth, String use) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "$auth";
    var response = await dio.get(
      baseUrl + 'delivery-user/new_orders',
    );
    if (response.statusCode == 200) {
      print("data true" + response.data.toString());
      return (response.data);
    } else if (response.statusCode == 500) {}
  }

  static getMsgData(String auth) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "$auth";
    var response = await dio.get(
      baseUrl + 'delivery-user/get_deliveryBoy_messages',
    );
    if (response.statusCode == 200) {
      print("data true" + response.data.toString());
      return (response.data);
    } else if (response.statusCode == 500) {}
  }

  static getPendingOrders(String auth, String use) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "$auth";
    var response = await dio.get(
      baseUrl + 'delivery-user/pending_delivery_orders',
    );
    if (response.statusCode == 200) {
      print("data true" + response.data.toString());
      return (response.data);
    } else if (response.statusCode == 500) {}
  }

  static getinnerOrders(String auth, String use, String orderId) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "$auth";
    var params = {
      "orderObjectId": orderId,
    };
    print("Params" + params.toString());
    var response = await dio.post(
      baseUrl + 'delivery-user/new_single_order',
      data: jsonEncode(params),
    );
    if (response.statusCode == 200) {
      print("data true" + response.data.toString());
      return (response.data);
    } else if (response.statusCode == 500) {}
  }

  static getDeliverdOrder(String auth, String use, String orderId) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "$auth";
    var params = {
      "orderId": orderId,
    };
    var response = await dio.post(
      baseUrl + 'delivery-user/single_delivered_order',
      data: jsonEncode(params),
    );
    if (response.statusCode == 200) {
      print("data true" + response.data.toString());
      return (response.data);
    } else if (response.statusCode == 500) {}
  }

  static getDeliveryOrder(String auth, String use, String orderId) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "$auth";
    var params = {
      "orderObjectId": orderId,
    };
    var response = await dio.post(
      baseUrl + 'delivery-user/single_pending_delivery_order',
      data: jsonEncode(params),
    );
    if (response.statusCode == 200) {
      print("data true" + response.data.toString());
      return (response.data);
    } else if (response.statusCode == 500) {}
  }

  static acceptOrders(String auth, String orderId, String status) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "$auth";
    var params = {"orderObjectId": orderId, "status": status};
    var response = await dio.post(
      baseUrl + 'delivery-user/change_order_status',
      data: jsonEncode(params),
    );
    if (response.statusCode == 200) {
      print("data true" + response.data.toString());
      return (response.data);
    } else if (response.statusCode == 500) {}
  }

  static deliverdOrders(String auth, String orderId, String status) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "$auth";
    var params = {"orderObjectId": orderId, "status": status};
    var response = await dio.post(
      baseUrl + 'delivery-user/change_pending_delivery_order_status',
      data: jsonEncode(params),
    );
    if (response.statusCode == 200) {
      print("data true" + response.data.toString());
      return (response.data);
    } else if (response.statusCode == 500) {}
  }

  static deliverDate(
      String auth, String StartingDate, String EndingDate) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "$auth";
    var params = {"StartingDate": StartingDate, "EndingDate": EndingDate};
    print("Date params" + params.toString());
    var response = await dio.post(
      baseUrl + 'delivery-user/date_delivered_orders',
      data: jsonEncode(params),
    );
    if (response.statusCode == 200) {
      print("data true" + response.data.toString());
      return (response.data);
    } else if (response.statusCode == 500) {}
  }

  static transactionDates(String auth, String StartingDate, String EndingDate,
      String typeData) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "$auth";
    var params = {
      "StartingDate": StartingDate,
      "EndingDate": EndingDate,
      "Type": typeData
    };
    print("Date params" + params.toString());
    var response = await dio.post(
      baseUrl + 'delivery-user/get_dated_deliveryboy_transactions',
      data: jsonEncode(params),
    );
    if (response.statusCode == 200) {
      print("data true" + response.data.toString());
      return (response.data);
    } else if (response.statusCode == 500) {}
  }

  static creditDateWiseData(String auth, String StartingDate, String EndingDate,
      String typeData) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "$auth";
    var params = {
      "StartingDate": StartingDate,
      "EndingDate": EndingDate,
      "Type": typeData
    };
    print("Date params" + params.toString());
    var response = await dio.post(
      baseUrl + 'delivery-user/get_dated_deliveryboy_credit',
      data: jsonEncode(params),
    );
    if (response.statusCode == 200) {
      print("data true" + response.data.toString());
      return (response.data);
    } else if (response.statusCode == 500) {}
  }

  static searchData(String auth, String value) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "$auth";
    var params = {"searchBy": value};
    var response = await dio.post(
      baseUrl + 'delivery-user/search_delivered_orders',
      data: jsonEncode(params),
    );
    if (response.statusCode == 200) {
      print("data true" + response.data.toString());
      return (response.data);
    } else if (response.statusCode == 500) {}
  }

  static searchTraData(String auth, String value, String type) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "$auth";
    var params = {"searchBy": value, "Type": type};
    var response = await dio.post(
      baseUrl + 'delivery-user/search_deliveryboy_transactions',
      data: jsonEncode(params),
    );
    if (response.statusCode == 200) {
      print("data true" + response.data.toString());
      return (response.data);
    } else if (response.statusCode == 500) {}
  }

  static searchCredit(String auth, String value, String type) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "$auth";
    var params = {"searchBy": value, "Type": type};
    var response = await dio.post(
      baseUrl + 'delivery-user/search_deliveryboy_credit',
      data: jsonEncode(params),
    );
    if (response.statusCode == 200) {
      print("data true" + response.data.toString());
      return (response.data);
    } else if (response.statusCode == 500) {}
  }

  static getTransaction(String auth, String type) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "$auth";
    var params = {
      "Type": type,
    };
    var response = await dio.post(
      baseUrl + 'delivery-user/get_deliveryboy_transactions',
      data: jsonEncode(params),
    );
    if (response.statusCode == 200) {
      print("data true" + response.data.toString());
      return (response.data);
    } else if (response.statusCode == 500) {}
  }

  static getCreditList(String auth, String type) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "$auth";
    var params = {
      "Type": type,
    };
    var response = await dio.post(
      baseUrl + 'delivery-user/get_deliveryboy_credits',
      data: jsonEncode(params),
    );
    if (response.statusCode == 200) {
      print("data true" + response.data.toString());
      return (response.data);
    } else if (response.statusCode == 500) {}
  }

  static getProfileData(String auth, String use) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "$auth";
    var response = await dio.get(
      baseUrl + 'delivery-user/get_delivery_boy_profile_details',
    );
    if (response.statusCode == 200) {
      print("data true" + response.data.toString());
      return (response.data);
    } else if (response.statusCode == 500) {}
  }

  static Future<String?> pickUpDetails(String auth, String orderId) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "$auth";
    var params = {
      "orderObjectId": orderId,
    };
    print("Params" + params.toString() + " ," + "Auth : " + auth);
    var response = await http.post(
        Uri.parse(baseUrl + 'delivery-user/return_single_order'),
        headers: {
          "Authorization": auth
        },
        body: {
          "orderObjectId": orderId,
        });
    if (response.statusCode == 200) {
      print("data true" + response.body.toString());
      return (response.body);
    } else if (response.statusCode == 500) {}
  }

  static changeOrderStatus(String auth, String orderId, String status) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "$auth";
    var params = {
      "orderObjectId": orderId,
      "status": status,
    };
    print("Data" + params.toString());
    var response = await dio.post(
      baseUrl + 'delivery-user/change_return_order_status',
      data: jsonEncode(params),
    );
    if (response.statusCode == 200) {
      print("data true" + response.data.toString());
      return (response.data);
    } else if (response.statusCode == 500) {}
  }

  static getEmergency(String auth) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "$auth";
    var response = await dio.get(
      baseUrl + 'delivery-user/get_emergency_contact',
    );
    if (response.statusCode == 200) {
      return (response.data);
    } else if (response.statusCode == 500) {}
  }

  static getBankAccept(String auth) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "$auth";
    var response = await dio.get(
      baseUrl + 'delivery-user/get_bank_details',
    );
    if (response.statusCode == 200) {
      print("data true" + response.data.toString());
      return (response.data);
    } else if (response.statusCode == 500) {}
  }

  static getDeliveryNot(String auth) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "$auth";
    var response = await dio.get(
      baseUrl + 'delivery-user/get_deliveryBoy_notifications',
    );
    if (response.statusCode == 200) {
      //  print("data true" + response.data.toString());
      return (response.data);
    } else if (response.statusCode == 500) {}
  }

  static updateHelpAndSupport(
      String auth, String status, String message) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "$auth";
    var params = {
      "IssueRelated": status,
      "Issue": message,
    };
    print("Data" + params.toString());
    var response = await dio.post(
      baseUrl + 'delivery-user/add_query',
      data: jsonEncode(params),
    );
    if (response.statusCode == 200) {
      print("data true" + response.data.toString());
      return (response.data);
    } else if (response.statusCode == 500) {}
  }

  static notUpdate(String auth, String id) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "$auth";
    var params = {"notificationId": id};
    var response = await dio.post(
      baseUrl + 'delivery-user/change_notification_read_status',
      data: jsonEncode(params),
    );
    if (response.statusCode == 200) {
      //   print("data true" + response.data.toString());
      return (response.data);
    } else if (response.statusCode == 500) {}
  }

  static messageUpdate(String auth, String id) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "$auth";
    var params = {"queryId": id};
    var response = await dio.post(
      baseUrl + 'delivery-user/change_read_status',
      data: jsonEncode(params),
    );
    if (response.statusCode == 200) {
      //   print("data true" + response.data.toString());
      return (response.data);
    } else if (response.statusCode == 500) {}
  }

  static returnMyOrdersData(String auth, String status) async {
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "$auth";
    var params = {"type": status};
    print("j" + params.toString());
    var response = await dio.post(
      baseUrl + 'delivery-user/return_orders_by_type',
      data: jsonEncode(params),
    );
    if (response.statusCode == 200) {
      print("data true" + response.data.toString());
      return (response.data);
    } else if (response.statusCode == 500) {}
  }

  static searchReturnMyOrdersData(
      String auth, String serach, String status) async {
    var params = {"searchBy": serach, "Type": status};
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "$auth";
    dio.options.headers['content-Type'] = 'application/json';
    // print("params" +  baseUrl + 'delivery-user/search_return_orders'.toString());
    //
    print("params" + params.toString());
    var response = await dio.post(
      baseUrl + 'delivery-user/search_return_orders',
      data: jsonEncode(params),
    );
    if (response.statusCode == 200) {
      print("data true" + response.data.toString());
      return (response.data);
    } else if (response.statusCode == 500) {}
  }
}
