import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/SplashController/splashController.dart';

import 'Controller/AddressController/AddressController.dart';
import 'Controller/AdharPageControlller/AdharPageControlller.dart';
import 'Controller/BankDetailsController/BankDetailsController.dart';
import 'Controller/CancelledController/CancelledController.dart';
import 'Controller/CashDepositController/CashDepositController.dart';
import 'Controller/CreditController/CreditController.dart';
import 'Controller/DeliverdOrderDetailsController/DeliverdOrderDetailsController.dart';
import 'Controller/DeliverdOrdersController/deliverdOrderController.dart';
import 'Controller/DrawerController/DrawerController.dart';
import 'Controller/EmergencyController/EmergencyController.dart';
import 'Controller/EmergencyControllerPage/EmergencyControllerPage.dart';
import 'Controller/HelpAndSupportController/HelpAndSupportController.dart';
import 'Controller/HelpSupportPageController/HelpSupportPageController.dart';
import 'Controller/HomeController/HomeConroller.dart';
import 'Controller/LedgerController/LedgerController.dart';
import 'Controller/LiceneceViewController/LiceneceViewController.dart';
import 'Controller/LoginController/LoginController.dart';
import 'Controller/MessageController/MessageController.dart';
import 'Controller/MessageControllerPage/MessageControllerPage.dart';
import 'Controller/MyAccountController/MyAccountController.dart';
import 'Controller/NewInnerOrderController/NewInnerOrderController.dart';
import 'Controller/NewOrderController/newOrderController.dart';
import 'Controller/NotificationController/NotificationController.dart';
import 'Controller/NotificationPageController/NotificationPageController.dart';
import 'Controller/OrderDetailsPageController/OrderDetailsPageController.dart';
import 'Controller/PendingDeliveryController/pendingDeliveryController.dart';
import 'Controller/PendingOrderDetailsController/PendingOrderDetailsController.dart';
import 'Controller/PickUpDeliveryController/PickUpDeliveryController.dart';
import 'Controller/PickupController/PickUpController.dart';
import 'Controller/PickupProductController/PickupProductController.dart';
import 'Controller/ProfileControllers/ProfileControllers.dart';
import 'Controller/RcBookController/RcBookController.dart';
import 'Controller/RegisterController/registerController.dart';
import 'Controller/TermsAndCondition/terms_controller.dart';
import 'Controller/TransactionController/TransactionController.dart';
import 'Controller/UserInfoController/UserInfoController.dart';

class SplashBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => SplashController());
  }
}

class RegisterBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => RegisterController());
  }
}

class LoginBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => LoginController());
  }
}

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => HomeController());
    Get.lazyPut(() => DrawerControllers());
  }
}

class MyAccountBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => AccountController());
  }
}

class NewOrderBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => NewOrderController());
  }
}

class PendingOrderBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => PendingDeliveryController());
  }
}

class DeliverdOrderBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => DeliverdOrderController());
  }
}

class CancelledOrderBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => CancelledOrderController());
  }
}

class PickUpOrderBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => PickupProductController());
  }
}

class TransactionOrderBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => TransactionController());
  }
}

class CreditBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => CreditController());
  }
}

class EmergencyBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => EmergencyController());
  }
}

class HelpAndSupportBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => HelpAndSupportController());
  }
}

class NewOrderInnerPagetBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => NewInnerOrderController());
  }
}

class PendingOrderInnerPageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => PendingOrderDetailsController());
  }
}

class DeliverdInnerOrderInnerPageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => DeliverdOrderDetailsController());
  }
}

class CancelledOrdersDetailsPage extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => CancelledOrderController());
  }
}

class CashDepositBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => CashDepositController());
  }
}

class LedgerBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => LedgerController());
  }
}

class MessageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => MessageController());
  }
}

class PickUpController extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => PickupProductController());
  }
}
class PickUpDetailsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => PickUpDeliveryController());
  }
}

class AddressBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => AddressController());
  }
}

class BandOrderBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => BankDetailsController());
  }
}

class CredDetailsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => OrderDetailsPageController());
  }
}

class EmergencyControllerPageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => EmergencyControllerPage());
  }
}

class HelpSupportBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => HelpSupportPageController());
  }
}

class MessageBindingPage extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => MessageControllerPage());
  }
}

class NotificationBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => NotificationController());
  }
}

class NotificationPageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => NotificationPageController());
  }
}

class ProfileBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ProfileControllers());
  }
}

class UserInfoBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => UserInfoController());
  }
}
class LincenceBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => LiceneceViewController());
  }
}

class AdharBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => AdharPageControlller());
  }
}

class RcBookBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => RcBookController());
  }
}
class TermsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => TermsPageController());
  }
}