import 'package:flutter/material.dart';
import 'package:medimallDelivery/themes/HexColor.dart';

class DrawerMenuItem extends StatelessWidget {
  String title;
  Function onClick;
  String image;
  DrawerMenuItem({
    required this.title,
    required this.onClick,
    required this.image
});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onClick(),
      child: ListTile(
        leading: Image.asset(image,
            color: HexColor('#ED1B24'), height: 34, width: 36),
        title:
        Text(title, style: TextStyle(fontSize: 15, color: Colors.black)),
      ),
    );
  }
}
