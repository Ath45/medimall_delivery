import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/util/constantsDC.dart';

import '../util/constants.dart';

class NotificationBox extends StatefulWidget {
  @override
  String orderStaus;
  String name;
  String price;
  String orderId;
  String delivery;
  String read;

  NotificationBox(
      {required this.orderStaus,
      required this.name,
      required this.price,
      required this.orderId,
      required this.delivery,
      required this.read});

  _NotificationBoxState createState() => _NotificationBoxState();
}

class _NotificationBoxState extends State<NotificationBox> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var height = SizeConfig.screenHeight;
    var width = SizeConfig.screenWidth;

    /// Local variables <<---------------------------------------------------->>
    var fontsize14 = height * .013 * 1.225;
    var fontsize15 = height * .015 * 1.225;
    var colorblack = Color.fromRGBO(0, 0, 0, 1);
    var style = TextStyle(
        color: colorblack, fontSize: fontsize14, fontFamily: "SegoeSemi");
    var styleReg = TextStyle(
        color: colorblack, fontSize: fontsize14, fontFamily: "SegoeReg");
    var stylewhiteSemiFont15 = TextStyle(
        color: fillColor, fontSize: fontsize15, fontFamily: "SegoeReg");

    /// Local variables <<---------------------------------------------------->>

    return Container(
      height: height * .14,
      width: width,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(height * .008),
          boxShadow: shadow),
      margin: EdgeInsets.symmetric(
          horizontal: width * .03, vertical: height * .006),
      padding: EdgeInsets.fromLTRB(
        height * .015,
        height * .01,
        height * .0,
        height * .01,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                widget.name,
                style: widget.read == "true"
                    ? TextStyle(
                        fontWeight: FontWeight.w500,
                        fontFamily: 'SegoeSemi',
                        fontSize: SizeConfig.horizontal * 3,
                        color: HexColor('#5D5D5D'))
                    : TextStyle(
                        fontWeight: FontWeight.w900,
                        fontFamily: 'SegoeSemi',
                        fontSize: SizeConfig.horizontal * 4,
                        color: HexColor('#5D5D5D')),
              ),
              Text(
                "₹ " + widget.price,
                style: widget.read == "true"
                    ? TextStyle(
                        fontWeight: FontWeight.w500,
                        fontFamily: 'SegoeSemi',
                        fontSize: SizeConfig.horizontal * 3,
                        color: HexColor('#5D5D5D'))
                    : TextStyle(
                        fontWeight: FontWeight.w900,
                        fontFamily: 'SegoeSemi',
                        fontSize: SizeConfig.horizontal * 4,
                        color: HexColor('#5D5D5D')),
              ),
              Row(
                children: [
                  Text(
                    "Order ID : ",
                    style: widget.read == "true"
                        ? TextStyle(
                            fontWeight: FontWeight.normal,
                            fontFamily: 'SegoeSemi',
                            fontSize: SizeConfig.horizontal * 3,
                            color: HexColor('#5D5D5D'))
                        : TextStyle(
                            fontWeight: FontWeight.w900,
                            fontFamily: 'SegoeSemi',
                            fontSize: SizeConfig.horizontal * 4,
                            color: HexColor('#5D5D5D')),
                  ),
                  Text(widget.orderId,style: widget.read == "true"
                      ? TextStyle(
                      fontWeight: FontWeight.normal,
                      fontFamily: 'SegoeSemi',
                      fontSize: SizeConfig.horizontal * 3,
                      color: HexColor('#5D5D5D'))
                      : TextStyle(
                      fontWeight: FontWeight.w900,
                      fontFamily: 'SegoeSemi',
                      fontSize: SizeConfig.horizontal * 4,
                      color: HexColor('#5D5D5D')),)
                ],
              ),
              Text(
                widget.delivery,
                style: widget.read == "true"
                    ? TextStyle(
                        fontWeight: FontWeight.normal,
                        fontFamily: 'SegoeSemi',
                        fontSize: SizeConfig.horizontal * 3,
                        color: HexColor('#5D5D5D'))
                    : TextStyle(
                        fontWeight: FontWeight.w900,
                        fontFamily: 'SegoeSemi',
                        fontSize: SizeConfig.horizontal * 4,
                        color: HexColor('#5D5D5D')),
              ),
              SizedBox(
                height: height * .002,
              )
            ],
          ),
          Container(
              height: height * .05,
              width: width * .3,
              alignment: Alignment.centerRight,
              padding: EdgeInsets.only(right: width * .0),
              child: Stack(
                children: [
                  Image(
                      image: AssetImage(widget.orderStaus == "NewOrder"
                          ? "assets/images/bluebanner.png"
                          : widget.orderStaus == "Replace"
                              ? "assets/images/redbanner.png"
                              : "assets/images/yellowbanner.png")),
                  Positioned(
                    top: height * .01,
                    right: width * .02,
                    child: Text(
                      widget.orderStaus == "NewOrder"
                          ? "New Order"
                          : widget.orderStaus == "Replace"
                              ? "Replace"
                              : "Return",
                      textAlign: TextAlign.center,
                      style: stylewhiteSemiFont15,
                    ),
                  ),
                ],
              )),
        ],
      ),
    );
  }
}
