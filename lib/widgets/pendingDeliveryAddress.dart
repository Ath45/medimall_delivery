import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/PendingOrderDetailsController/PendingOrderDetailsController.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/util/constants.dart';
import 'package:medimallDelivery/util/contantRiyas.dart';

class PendingDeliveryAddress extends StatelessWidget {
  final PendingOrderDetailsController controller = Get.put(PendingOrderDetailsController());
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Padding(
      padding:  EdgeInsets.symmetric(vertical: SizeConfig.horizontal*1.5),
      child:GetX<PendingOrderDetailsController>(
          init: PendingOrderDetailsController(),
          builder: (controller) {
          return Container(
            height: SizeConfig.horizontal*26,
            width: SizeConfig.horizontal*100,
            decoration: BoxDecoration(
                color: HexColor('#FFFFFF'),
                borderRadius: BorderRadius.circular(8),
                boxShadow: myBoxShadow),
            child: Padding(
              padding:  EdgeInsets.symmetric(horizontal: SizeConfig.horizontal*3),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                  controller.address.value,
                    style: TextStyle(fontFamily: 'SegoeReg',color: Colors.black.withOpacity(0.7),
                      fontSize: SizeConfig.horizontal*3.4,
                    ),
                  ),
                  SizedBox(
                    height: SizeConfig.horizontal*1,
                  ),
                  Text(
                    '',
                    style: TextStyle(fontFamily: 'SegoeReg',color: Colors.black.withOpacity(0.7),
                      fontSize: SizeConfig.horizontal*3.4,),
                  ),
                ],
              ),
            ),
          );
        }
      ),
    );
  }
}
