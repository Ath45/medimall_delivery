import 'package:expansion_tile_card/expansion_tile_card.dart';
import 'package:flutter/material.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/util/constants.dart';

import 'myButton.dart';

class ShowAlertBox extends StatefulWidget {
  @override
  _ShowAlertBoxState createState() => _ShowAlertBoxState();
}

class _ShowAlertBoxState extends State<ShowAlertBox> {
  final GlobalKey<ExpansionTileCardState> cancelKey = new GlobalKey();
  bool _otherClicked = false;
  String cancelReason = 'Not Available';

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Material(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(8))),
        child: Container(
          width: SizeConfig.horizontal*90,
          child: Padding(
            padding:  EdgeInsets.symmetric(horizontal: SizeConfig.horizontal*4,vertical: SizeConfig.horizontal*3),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Icon(
                        Icons.close,
                        color: Colors.black,
                        size: SizeConfig.horizontal*5,
                      ),
                    ),
                  ],
                ),
                Text(
                  "Reason for Canceling",
                  style: TextStyle(
                    fontSize: SizeConfig.horizontal *3.1,
                    color: Colors.black,fontFamily: 'SegoeReg'
                  ),
                ),
                SizedBox(
                  height: SizeConfig.horizontal*2,
                ),
                ListTileTheme(
                  dense: true,
                  child: Container(
                    decoration: BoxDecoration(
                        border: Border.all(color: HexColor('#707070'), width: 0.4),
                        borderRadius: BorderRadius.circular(5)),
                    child: ExpansionTileCard(
                      shadowColor: Colors.transparent,
                      key: cancelKey,
                      title: Text(
                        cancelReason,
                        style: TextStyle(fontSize: SizeConfig.horizontal*3.1,fontFamily: 'SegoeReg',fontWeight: FontWeight.w500,color: Colors.black),
                      ),
                      //key: stateGlobalKey,
                      children: [
                        ListTile(
                          title:  Text('Not Available',style: TextStyle(fontSize: SizeConfig.horizontal*3.1,fontFamily: 'SegoeReg',fontWeight: FontWeight.w500,color: Colors.black),),
                          onTap: () {
                            setState(() {
                              cancelReason = 'Not Available';
                              _otherClicked = false;
                              cancelKey.currentState!.collapse();
                            });
                          },
                        ),
                        ListTile(
                          title:  Text('Other',
                          style: TextStyle(fontSize: SizeConfig.horizontal*3.1,fontFamily: 'SegoeReg',fontWeight: FontWeight.w500,color: Colors.black),),
                          onTap: () {
                            setState(() {
                              cancelReason = 'Other';
                              _otherClicked = true;
                              cancelKey.currentState!.collapse();
                            });
                          },
                        ),
                      ],
                    ),
                  ),
                ),
                _otherClicked == false ? Container() : cancelCommentContainer(),
                SizedBox(
                  height: SizeConfig.horizontal*5,
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Center(
                    child: MyButton(
                      text: 'Confirm',
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Padding cancelCommentContainer() {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Comments",
              style: TextStyle(
                  fontSize: SizeConfig.horizontal *3.1,
                  color: Colors.black,fontFamily: 'SegoeReg'
              ),
            ),
            SizedBox(
              height: SizeConfig.horizontal*2,
            ),
            Container(
              height: SizeConfig.horizontal*25,
              decoration: BoxDecoration(
                  border: Border.all(color: HexColor('#707070'), width: 0.4),
                  borderRadius: BorderRadius.circular(5)),
              child: Padding(
                padding:  EdgeInsets.symmetric(horizontal: SizeConfig.horizontal*2),
                child: TextFormField(
                  style: TextStyle(  fontSize: SizeConfig.horizontal *3.2,
                      color: Colors.black,fontFamily: 'SegoeReg'),
                  decoration: InputDecoration(

                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none),
                  keyboardType: TextInputType.multiline,
                  maxLines: null,
                  initialValue:
                      'Lorem ipsum dolor sit amet consectetur adipiscing elit present anti',
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
