import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:medimallDelivery/Controller/DrawerController/DrawerController.dart';
import 'package:medimallDelivery/Controller/HomeController/HomeConroller.dart';
import 'package:medimallDelivery/Routes/routes.dart';
import 'package:medimallDelivery/util/constantsDC.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../util/constants.dart';
import 'package:get/get.dart';

class DrawerSession extends StatelessWidget {
  final myDrawerController = Get.put(DrawerControllers());
  final myHomeController = Get.put(HomeController());
  bool val = false;
  @override
  Widget build(BuildContext context) {
    myDrawerController.getData();
    myDrawerController.checkOnline();
    DrawerControllers.to.context=context;
    List data = [
      {
        "text": "New Order",
        "image": "assets/icons/NewOrders2.png",
        "route": newOrders
      },
      {
        "text": "Pending Delivery",
        "image": "assets/icons/Pendingorder.png",
        "route": pendingDelivery
      },
      {
        "text": "Delivered Orders",
        "image": "assets/icons/Deliveredorders.png",
        "route": deliverdOrder
      },
      // {
      //   "text": "Cancelled Orders",
      //   "image": "assets/icons/cancelledorders.png",
      //   "route": cancelledOrder
      // },
      {
        "text": "Transactions",
        "image": "assets/icons/Trasactions.png",
        "route": transactionOrder
      },
      {
        "text": "Cash Deposit",
        "image": "assets/icons/Trasactions.png",
        "route": cashDeposit
      },
      {
        "text": "Credits",
        "image": "assets/icons/Credits.png",
        "route": creditRoute
      },
      //{
      // "text": "Ledger",
      // "image": "assets/icons/ledger.png",
      // "route": Ledger()
      // },
      {
        "text": "Emergency Contact",
        "image": "assets/icons/emergencycontact.png",
        "route": emergencyRoute
      },
      {
        "text": "Help & Support",
        "image": "assets/icons/Helpandsupport.png",
        "route": helpAndSupportRoute
      },
      {
        "text": "Terms & Conditions",
        "image": "assets/icons/Outline.png",
        "route": termsandcondition
      },
      {
        "text": "Logout",
        "image": "assets/icons/ic_logout.png",
        "route": loginRoute
      },
    ];
    SizeConfig().init(context);
    var height = SizeConfig.screenHeight * 1.225;
    var width = SizeConfig.screenWidth * 1.225;

    /// Local variables <<---------------------------------------------------->>
    var fontsize15 = height * .013;
    var fontsize17 = height * .018;
    var styleRegblue = TextStyle(
        color: colorMyblue, fontSize: fontsize15, fontFamily: "SegoeReg");
    var styleRegGrey = TextStyle(
        color: Color.fromRGBO(200, 200, 200, 1),
        fontSize: fontsize15,
        fontFamily: "SegoeReg");
    var style = TextStyle(
        color: colorblack, fontSize: fontsize17, fontFamily: "SegoeSemi");

    /// Local variables <<---------------------------------------------------->>
    return  GetX<DrawerControllers>(
        init: DrawerControllers(),
        builder: (controller) {
        return Container(
          width: width * .62,
          color: Colors.white,
          child: ListView(
            physics: ScrollPhysics(),
            children: [
              SizedBox(
                height: height * .00,
              ),
              GestureDetector(
                  onTap: () {
                    Get.toNamed(accountRoute);
                  },
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(80.0),
                    child: CachedNetworkImage(
                      imageUrl:controller.url.value,
                      placeholder:(context, url) => CircularProgressIndicator(),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                      height: height * .11,
                      width: height*.11,
                    ),
                  )),
              Container(
                  alignment: Alignment.center,
                  child: Text(
                    controller.name.value,
                    style: style,
                  )),
              SizedBox(
                height: height * .01,
              ),
              GetX<DrawerControllers>(
                  init: DrawerControllers(),
                  builder: (controller) {
                  return Container(
                    margin: EdgeInsets.only(left: width * .18),
                    child: Row(
                      children: [
                        Text(
                          controller.docOrClinic.value ? "On Duty" : "Off Duty",
                          style: controller.docOrClinic.value ? styleRegblue : styleRegGrey,
                        ),
                        SizedBox(
                          width: width * .03,
                        ),
                        FlutterSwitch(
                            width: width * .1,
                            height: height * .025,
                            value:  controller.docOrClinic.value,
                            onToggle: (value) {
                              myHomeController.checkOnline();
                              controller.toggleSwitch(value);
                            })
                      ],
                    ),
                  );
                }
              ),
              Container(
                height: height * .057 * data.length,
                child: ListView.builder(
                    itemCount: data.length,
                    physics: ScrollPhysics(),
                    itemBuilder: (context, index) {
                      return DrawerMenuItem(
                          title: data[index]["text"],
                          image: data[index]["image"],
                          route: data[index]["route"]);
                    }),
              )
            ],
          ),
        );
      }
    );
  }
}

// ignore: must_be_immutable
class DrawerMenuItem extends StatelessWidget {
  String title;
  String route;
  String image;
  DrawerMenuItem({
    required this.title,
    required this.image,
    required this.route,
  });
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var height = SizeConfig.screenHeight * 1.225;
    var fontsize15 = height * .014 * 1.225;
    var colorblack = Color.fromRGBO(0, 0, 0, 1);
    var colorMyblue = Color.fromRGBO(77, 158, 255, 1);
    var styleReg = TextStyle(
        color: colorblack, fontSize: fontsize15, fontFamily: "SegoeReg");
    return GestureDetector(
      onTap: () async {
        if(title=="Terms & Condition")
          {

          }
        else if(title=="Logout")
          {
            await showDialog(
                context: context,
                builder: (_) => AlertDialog(
              title: Text('Are you sure want to Logout?'),
              actions: [
                ElevatedButton(
                    onPressed: () async {
                      SharedPreferences preferences =
                          await SharedPreferences.getInstance();
                      await preferences.clear();
                      Get.offAllNamed(loginRoute);                    },
                    child: Text('Yes')),
                TextButton(
                    onPressed: () =>   Get.back(),
                    child: Text('No'))
              ],
            ));
          }
        else
          {
           // Navigator.push(context, MaterialPageRoute(builder: (context) => route));
            Get.toNamed(route);
          }

      },
      child: ListTile(
        leading: Image.asset(image, color: colorMyblue, height: 34, width: 36),
        title: Text(title, style: styleReg),
      ),
    );
  }
}
