import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';


Future pickImage()async
{
  XFile? image = (await ImagePicker().pickImage(source: ImageSource.gallery));
 final croppedImage=(await ImageCropper().cropImage(
      sourcePath: image!.path,
   //    androidUiSettings: AndroidUiSettings(
   //        toolbarTitle: 'Cropper',
   //        toolbarColor: Colors.blue.shade400,
   //        toolbarWidgetColor: Colors.white,
   //        initAspectRatio: CropAspectRatioPreset.original,
   //        lockAspectRatio: false),
   //    iosUiSettings: IOSUiSettings(
   //      title: 'Cropper',
   //    )
   // ,)
 ));
 return croppedImage;
}
