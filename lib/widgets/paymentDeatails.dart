import 'package:flutter/material.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/util/constants.dart';
import 'package:medimallDelivery/util/contantRiyas.dart';

class PaymentDetails extends StatelessWidget {
  String name;
  String price;
  String orderId;
  String deleveryCharge;
  String paidStatus;
  String paymntMethod;
  bool visibility;

  PaymentDetails(
      {
      required this.name,
      required this.price,
      required this.orderId,
      required this.deleveryCharge,
      required this.paidStatus,
      required this.paymntMethod,
      required this.visibility,
      });
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: SizeConfig.horizontal * 2),
      child: Container(
        alignment: Alignment.center,
        height: SizeConfig.horizontal * 25,
        width: SizeConfig.horizontal * 100,
        decoration: BoxDecoration(
            color: HexColor('#FFFFFF'),
            borderRadius: BorderRadius.circular(5),
            boxShadow: myBoxShadow),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: SizeConfig.horizontal * 4),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                name,
                style: TextStyle(
                    fontSize: SizeConfig.horizontal * 3.2,
                    fontFamily: 'SegoeSemi'),
              ),
              SizedBox(height: SizeConfig.horizontal * 0.5),
              Row(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Order ID',
                        style: TextStyle(
                            fontSize: SizeConfig.horizontal * 3.2,
                            fontFamily: 'SegoeReg'),
                      ),
                      SizedBox(height: SizeConfig.horizontal * 0.5),
                      Text(
                        'Total Amount',
                        style: TextStyle(
                            fontSize: SizeConfig.horizontal * 3.2,
                            fontFamily: 'SegoeReg'),
                      ),
                      SizedBox(height: SizeConfig.horizontal * 0.5),
                      Visibility(
                        visible: visibility,
                        child: Text(
                          'Delivery Charge',
                          style: TextStyle(
                              fontSize: SizeConfig.horizontal * 3.2,
                              fontFamily: 'SegoeReg'),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: SizeConfig.horizontal * 1),
                    child: Column(
                      children: [
                        Text(
                          ':',
                          style: TextStyle(
                              fontSize: SizeConfig.horizontal * 3.2,
                              fontFamily: 'SegoeReg'),
                        ),
                        SizedBox(height: SizeConfig.horizontal * 0.5),
                        Text(
                          ':',
                          style: TextStyle(
                              fontSize: SizeConfig.horizontal * 3.2,
                              fontFamily: 'SegoeReg'),
                        ),
                        SizedBox(height: SizeConfig.horizontal * 0.5),
                        Visibility(
                          visible: visibility,
                          child: Text(
                            ':',
                            style: TextStyle(
                                fontSize: SizeConfig.horizontal * 3.2,
                                fontFamily: 'SegoeReg'),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        orderId,
                        style: TextStyle(
                            fontSize: SizeConfig.horizontal * 3.2,
                            fontFamily: 'SegoeSemi'),
                      ),
                      SizedBox(height: SizeConfig.horizontal * 0.5),
                      Text(
                        '₹' +price,
                        style: TextStyle(
                            fontSize: SizeConfig.horizontal * 3.2,
                            fontFamily: 'SegoeSemi'),
                      ),
                      SizedBox(height: SizeConfig.horizontal * 0.5),
                      Visibility(
                        visible: visibility,
                        child: Text(
                          '₹' +deleveryCharge,
                          style: TextStyle(
                              fontSize: SizeConfig.horizontal * 3.2,
                              fontFamily: 'SegoeSemi'),
                        ),
                      ),
                    ],
                  ),
                  Spacer(),
                  Padding(
                    padding: EdgeInsets.only(right: SizeConfig.horizontal * 3),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          paidStatus,
                          textAlign: TextAlign.end,
                          style: TextStyle(
                              fontSize: SizeConfig.horizontal * 3.2,
                              fontFamily: 'SegoeSemi',
                              color: HexColor(paidStatus == 'Not Paid'
                                  ? '#1B3884'
                                  : '#28AA46')),
                        ),
                        Text(
                          (paymntMethod=="cod")?"Cash on Delivery":(paymntMethod=="razorpay")?"Razorpay":
                          paymntMethod,
                          textAlign: TextAlign.end,
                          style: TextStyle(
                              fontSize: SizeConfig.horizontal * 3.2,
                              fontFamily: 'SegoeSemi',
                              color: HexColor('#28AA46')),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
