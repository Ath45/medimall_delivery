import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/util/constants.dart';


AppBar myAppbar(BuildContext context, String title) {
  SizeConfig().init(context);
  return AppBar(
    centerTitle: true,
    backgroundColor: Colors.white,
    title: Text(
      title,
      style: TextStyle(fontSize: SizeConfig.horizontal*3.5, color:Colors.black,fontWeight: FontWeight.bold,fontFamily: 'SegoeSemi'),
    ),
    leading: GestureDetector(
        onTap: () {
          Get.back();
        },
        child: Icon(
          Icons.arrow_back_ios,
          color: Colors.black,
          size: 15,
        )),
  );
}
