import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/NewInnerOrderController/NewInnerOrderController.dart';
import 'package:medimallDelivery/util/constants.dart';
import 'package:medimallDelivery/util/constantsDC.dart';
import 'package:medimallDelivery/util/contantRiyas.dart';

class ProductDetails extends StatelessWidget {
  final NewInnerOrderController controller = Get.put(NewInnerOrderController());
  @override
  Widget build(BuildContext context) {
    return Container(
      child: GetX<NewInnerOrderController>(
          init: NewInnerOrderController(),
          builder: (controller) {
            return ListView.builder(
                shrinkWrap: true,
                itemCount: NewInnerOrderController.to.orddersList.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  return Container(
                    margin: EdgeInsets.only(top: 10),
                    height: SizeConfig.horizontal * 32,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(8),
                        boxShadow: myBoxShadow),
                    child: Padding(
                      padding: EdgeInsets.all(SizeConfig.horizontal * 3),
                      child: Row(
                        children: [
                          Image.network(
                            controller.orddersList[index].image,
                            height: SizeConfig.horizontal * 25,
                            fit: BoxFit.fill,
                          ),
                          SizedBox(
                            width: SizeConfig.horizontal * 5,
                          ),
                          Column(
                            children: [
                              Container(
                                width: height*0.14,
                                child: Text(
                                  controller.orddersList[index].productName,
                                  maxLines: 3,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      fontFamily: 'SegoeSemi',
                                      fontSize: SizeConfig.horizontal * 4.5),
                                ),
                              ),
                              Container(
                                width: height*0.14,
                                child: Text(
                                 " (" +controller.orddersList[index].uomValue+")",
                                  maxLines: 3,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      fontFamily: 'SegoeSemi',
                                      fontSize: SizeConfig.horizontal * 4.0),
                                ),
                              ),
                            ],
                          ),
                         // Spacer(),
                          Expanded(
                            child: Column(
                              // mainAxisAlignment: MainAxisAlignment.spaceAround,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  'Qty : ' + controller.orddersList[index].quantity.toString(),
                                  style: TextStyle(
                                      fontSize: SizeConfig.horizontal * 3,
                                      fontFamily: 'SegoeReg'),
                                ),
                                Spacer(),
                                Row(
                                  children: [
                                    Row(
                                      children: [
                                        Text(
                                          '₹ ',
                                          style: TextStyle(
                                              fontSize: SizeConfig.horizontal * 4.9,
                                              fontFamily: 'SegoeBold'),
                                        ), Text(
                                          functionCalvulate(controller.orddersList[index].specialPrice.toString(),controller.orddersList[index].quantity.toString()),
                                          style: TextStyle(
                                              fontSize: SizeConfig.horizontal * 4.0,
                                              fontFamily: 'SegoeBold'),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      width: SizeConfig.horizontal * 1.5,
                                    ),
                                    Stack(
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal:
                                                  SizeConfig.horizontal * 0.7),
                                          child: Text(
                                            '₹ ' +functionCalvulate(controller.orddersList[index].price.toString(),controller.orddersList[index].quantity.toString()),
                                            style: TextStyle(
                                                color: Colors.black
                                                    .withOpacity(0.6),
                                                fontSize:
                                                    SizeConfig.horizontal * 2.0,
                                                fontFamily:
                                                    'PoppinsExtraLight'),
                                          ),
                                        ),
                                        Positioned(
                                          left: 0.0,
                                          right: 0.0,
                                          bottom: 0.0,
                                          top: 0.0,
                                          child: new RotationTransition(
                                            turns: new AlwaysStoppedAnimation(
                                                15 / 360),
                                            child: Container(
                                              child: Divider(
                                                color: Colors.black
                                                    .withOpacity(0.6),
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  );
                });
          }),
    );
  }
}
