import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/DeliverdOrderDetailsController/DeliverdOrderDetailsController.dart';
import 'package:medimallDelivery/Controller/NewInnerOrderController/NewInnerOrderController.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/util/constants.dart';
import 'package:medimallDelivery/util/contantRiyas.dart';

class DeliverdDetailsAddress extends StatelessWidget {
  final DeliverdOrderDetailsController controller = Get.put(DeliverdOrderDetailsController());
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Padding(
      padding:  EdgeInsets.symmetric(vertical: SizeConfig.horizontal*1.5),
      child: GetX<DeliverdOrderDetailsController>(
          init: DeliverdOrderDetailsController(),
          builder: (controller) {
            return Container(
              height: SizeConfig.horizontal*26,
              width: SizeConfig.horizontal*100,
              decoration: BoxDecoration(
                  color: HexColor('#FFFFFF'),
                  borderRadius: BorderRadius.circular(8),
                  boxShadow: myBoxShadow),
              child: Padding(
                padding:  EdgeInsets.symmetric(horizontal: SizeConfig.horizontal*3),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      controller.addressDta.value,
                      style: TextStyle(fontFamily: 'SegoeReg',color: Colors.black.withOpacity(0.7),
                        fontSize: SizeConfig.horizontal*3.4,
                      ),
                    ),
                    SizedBox(
                      height: SizeConfig.horizontal*1,
                    ),
                    Text(
                      '',
                      style: TextStyle(fontFamily: 'SegoeReg',color: Colors.black.withOpacity(0.7),
                        fontSize: SizeConfig.horizontal*3.4,),
                    ),
                  ],
                ),
              ),
            );
          }
      ),
    );
  }
}