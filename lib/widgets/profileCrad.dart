import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:medimallDelivery/Controller/MyAccountController/MyAccountController.dart';

import '../util/constants.dart';

class profileCard extends StatelessWidget {
  final AccountController controller = Get.put(AccountController());
  var colorMyBlue = Color.fromRGBO(77, 158, 255, 1);

  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var height = SizeConfig.screenHeight * 1.225;
    var width = SizeConfig.screenWidth * 1.225;

    /// Local variables <<---------------------------------------------------->>
    var fontsize15 = height * .013;
    var colorblack = Color.fromRGBO(0, 0, 0, 1);
    var stylefontwhite = TextStyle(
        color: Colors.white, fontSize: fontsize15, fontFamily: "SegoeSemi");

    var stylefontwhitesReg = TextStyle(
        color: Colors.white, fontSize: fontsize15, fontFamily: "SegoeReg");
    var shadow = [
      BoxShadow(color: Color.fromRGBO(0, 0, 0, .02), blurRadius: 3)
    ];

    /// Local variables <<---------------------------------------------------->>
    return Container(
      height: height * .140,
      decoration: BoxDecoration(),
      child: Stack(
        clipBehavior: Clip.none, children: [
          GetX<AccountController>(
              init: AccountController(),
              builder: (controller) {
              return Container(
                margin: EdgeInsets.only(left: width * .19),
                width: width * .61,
                decoration: BoxDecoration(
                    color: colorMyBlue,
                    boxShadow: shadow,
                    borderRadius: BorderRadius.circular(height * .01)),
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        controller.name.value,
                        style: stylefontwhite,
                      ),
                      SizedBox(
                        height: height * .002,
                      ),
                      Text(
                        controller.email.value,
                        style: stylefontwhite,
                      ),
                      SizedBox(
                        height: height * .002,
                      ),
                      Text(
                        controller.mob.value,
                        style: stylefontwhitesReg,
                      )
                    ],
                  ),
                ),
              );
            }
          ),
          Positioned(
              left: width * .01,
              top: height * .01,
              child: GestureDetector(
                onTap: () {
                  AccountController.to.getImage(ImageSource.gallery);
                },
                child: GetX<AccountController>(
                    init: AccountController(),
                    builder: (controller) {
                    return Container(
                      height: height * .12,
                      width: height * .12,
                      decoration: BoxDecoration(
                        image: controller.recBookImage.value ? DecorationImage(
                          image: new FileImage(File(controller.imageShow.value)
                          ) ,
                        ):DecorationImage(
                          image: NetworkImage(controller.url.value),
                        ),
                        boxShadow: shadow,
                        shape: BoxShape.circle,
                      ),
                    );
                  }
                ),
              )),
          Positioned(
              left: width * .1,
              top: height * .10,
              child: Container(
                  height: height * .028,
                  padding: EdgeInsets.all(height * .006),
                  margin: EdgeInsets.only(top:12),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      boxShadow: shadow,
                      color: Colors.white),
                  child: Image(
                    image: AssetImage("assets/icons/cam.png"),
                    color: colorblack,
                  )))
        ],
      ),
    );
  }
}
