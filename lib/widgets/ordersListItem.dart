import 'package:flutter/material.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/util/constants.dart';

class orderListItem extends StatelessWidget {
  String name;
  String price;
  String orderId;
  String delivery;
  String read;

  orderListItem(
      {required this.name,
      required this.price,
      required this.orderId,
      required this.delivery,
      required this.read});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Padding(
      padding: EdgeInsets.symmetric(
        vertical: SizeConfig.horizontal * 1.5,
      ),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
            boxShadow: [
              BoxShadow(
                offset: Offset(0, 0),
                color: Colors.black.withOpacity(0.07),
                blurRadius: 6,
              )
            ]),
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.horizontal * 3,
              vertical: SizeConfig.horizontal * 5),
          child: Row(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    name,
                    style: read == "true"
                        ? TextStyle(
                            fontWeight: FontWeight.w500,
                            fontFamily: 'SegoeSemi',
                            fontSize: SizeConfig.horizontal * 4,
                            color: HexColor('#5D5D5D'))
                        : TextStyle(
                            fontWeight: FontWeight.w900,
                            fontFamily: 'SegoeSemi',
                            fontSize: SizeConfig.horizontal * 4,
                            color: HexColor('#5D5D5D')),
                  ),
                  SizedBox(
                    height: SizeConfig.horizontal * 1,
                  ),
                  Row(
                    children: [
                      Text(
                        'Order ID',
                        style: read == "true"
                            ? TextStyle(
                                fontWeight: FontWeight.w500,
                                fontFamily: 'SegoeReg',
                                fontSize: SizeConfig.horizontal * 3,
                                color: Colors.black.withOpacity(0.5))
                            : TextStyle(
                                fontWeight: FontWeight.w900,
                                fontFamily: 'SegoeSemi',
                                fontSize: SizeConfig.horizontal * 4,
                                color: HexColor('#5D5D5D')),
                      ),
                      SizedBox(
                        width: SizeConfig.horizontal * 2,
                      ),
                      Text(
                        ':',
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontFamily: 'SegoeReg',
                            fontSize: SizeConfig.horizontal * 3,
                            color: Colors.black.withOpacity(0.5)),
                      ),
                      SizedBox(
                        width: SizeConfig.horizontal * 2,
                      ),
                      Text(
                        orderId,
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontFamily: 'SegoeReg',
                            fontSize: SizeConfig.horizontal * 3,
                            color: Colors.black.withOpacity(0.5)),
                      ),
                    ],
                  ),
                ],
              ),
              Spacer(),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    '₹' + price,
                    style: TextStyle(
                        fontFamily: 'SegoeSemi',
                        fontWeight: FontWeight.w500,
                        fontSize: SizeConfig.horizontal * 3.5,
                        color: HexColor('#4D9EFF')),
                  ),
                  SizedBox(
                    height: SizeConfig.horizontal * 1,
                  ),
                  Text(
                    (delivery=="cod")?"Cash on Delivery":(delivery=="razorpay")?"Razorpay":delivery,
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontFamily: 'SegoeReg',
                        fontSize: SizeConfig.horizontal * 3),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
