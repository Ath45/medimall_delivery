import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/HomeController/HomeConroller.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:medimallDelivery/Routes/routes.dart';


class HomeAppbar extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey1;

  HomeAppbar(this.scaffoldKey1);

  @override
  _HomeAppbarState createState() => _HomeAppbarState();
}

class _HomeAppbarState extends State<HomeAppbar> {
  final myDrawerController = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    double notification = MediaQuery.of(context).padding.top;
    return Container(
      padding: EdgeInsets.only(left: 10, right: 10),
      width: MediaQuery.of(context).size.width,
      height: 67,
      child: Row(
        children: [
          Row(
            children: [
              GestureDetector(
                onTap: () {
                  widget.scaffoldKey1.currentState!
                      .openDrawer(); // This opens drawer.
                },
                child: Container(
                  height: 70,
                  width: 30,
                  child: Icon(
                    Icons.menu,
                    color: Colors.white,
                    size: 25,
                  ),
                ),
              )
            ],
          ),
          SizedBox(
            width: 20,
          ),
          GestureDetector(
            onTap: () {},
            child: Container(
              height: 42,
              width: 87,
              child: Image.asset("assets/images/appbarimg.png"),
            ),
          ),
          SizedBox(
            width: width < 380 ? width / 4.0 : width / 3.7,
          ),
          GestureDetector(
            onTap: () {
              Get.toNamed(messagePage);
            },
            child: GetX<HomeController>(builder: (controller) {
              return Stack(
                children: [
                  // SizedBox(height: height*0.2000,),
                  Container(
                    height: 22,
                    width: 27,
                    child: Image.asset("assets/images/msg.png"),
                  ),
                  Positioned(
                    right: 0,
                    child: new Container(
                      padding: EdgeInsets.all(1),
                      decoration: new BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(6),
                      ),
                      constraints: BoxConstraints(
                        minWidth: 12,
                        minHeight: 12,
                      ),
                      child: new Text(
                        controller.megsCount.value.toString(),
                        style: new TextStyle(
                          color: Colors.black,
                          fontSize: 8,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  )
                ],
              );
            }),
          ),
          SizedBox(
            width: width / 18.0,
          ),
          GestureDetector(
            onTap: () {
              Get.toNamed(notificationRoute);
            },
            child: GetX<HomeController>(builder: (controller) {
              return Stack(
                children: [
                  // SizedBox(height: height*0.2000,),
                  Container(
                    height: 22,
                    width: 27,
                    child: Image.asset("assets/images/notbell.png"),
                  ),
                  Positioned(
                    right: 0,
                    child: new Container(
                      padding: EdgeInsets.all(1),
                      decoration: new BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(6),
                      ),
                      constraints: BoxConstraints(
                        minWidth: 12,
                        minHeight: 12,
                      ),
                      child: new Text(
                        controller.notCount.value.toString(),
                        style: new TextStyle(
                          color: Colors.black,
                          fontSize: 8,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  )
                ],
              );
            }),
          ),
          SizedBox(
            width: width / 18.0,
          ),
          GestureDetector(
            onTap: () async {
              await showDialog(
                  context: context,
                  builder: (_) => AlertDialog(
                        title: Text('Are you sure want to Logout?'),
                        actions: [
                          ElevatedButton(
                              onPressed: () async {
                                SharedPreferences preferences =
                                    await SharedPreferences.getInstance();
                                await preferences.clear();
                                Get.offAllNamed(signupPage);
                              },
                              child: Text('Yes')),
                          TextButton(
                              onPressed: () => Get.back(), child: Text('No'))
                        ],
                      ));

              // Navigator.push(context,MaterialPageRoute(builder: (BuildContext) => LoginPage()));
            },
            child: Container(
              height: 22,
              width: 27,
              child: Image.asset("assets/images/goout.png"),
            ),
          )
        ],
      ),
    );
  }
}
