import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/NewInnerOrderController/NewInnerOrderController.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/util/constants.dart';
import 'package:medimallDelivery/util/contantRiyas.dart';

class CustomerDetail extends StatelessWidget {
  final NewInnerOrderController controller = Get.put(NewInnerOrderController());

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Padding(
      padding: EdgeInsets.symmetric(vertical: SizeConfig.horizontal * 1.5),
      child: GetX<NewInnerOrderController>(
          init: NewInnerOrderController(),
          builder: (controller) {
            return Container(
              height: SizeConfig.horizontal * 20,
              width: SizeConfig.horizontal * 100,
              decoration: BoxDecoration(
                  color: HexColor('#FFFFFF'),
                  borderRadius: BorderRadius.circular(8),
                  boxShadow: myBoxShadow),
              child: Padding(
                padding:
                    EdgeInsets.symmetric(horizontal: SizeConfig.horizontal * 4),
                child: Row(
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          controller.nameCustomer.value,
                          style: TextStyle(
                              fontSize: SizeConfig.horizontal * 5,
                              fontFamily: 'SegoeSemi',
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: SizeConfig.horizontal * 1,
                        ),
                        Text(
                          controller.mobileCustomer.value,
                          style: TextStyle(
                              fontSize: SizeConfig.horizontal * 3,
                              fontFamily: 'SegoeReg',
                              color: Colors.black.withOpacity(0.7)),
                        ),
                      ],
                    ),
                    Spacer(),
                    Text(
                      (controller.codTypeCustomer.value=="cod")?"Cash on Delivery":(controller.codTypeCustomer.value=="razorpay")?"Razorpay":controller.codTypeCustomer.value,
                      style: TextStyle(
                          fontFamily: 'SegoeSemi',
                          fontSize: SizeConfig.horizontal * 3.5,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            );
          }),
    );
  }
}
