import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/MyAccountController/MyAccountController.dart';
import 'package:medimallDelivery/Routes/routes.dart';

import '../screens/deliveryBoy/addressPage.dart';
import '../screens/deliveryBoy/bankDetailPage.dart';
import '../screens/deliveryBoy/userInfo.dart';
import '../util/constants.dart';

class optionList extends StatelessWidget {
  final AccountController controller = Get.put(AccountController());
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var height = SizeConfig.screenHeight * 1.225;
    var width = SizeConfig.screenWidth * 1.225;

    /// Local variables <<---------------------------------------------------->>
    var fontsize13 = height * .013;
    var colorblack = Color.fromRGBO(0, 0, 0, 1);
    var style = TextStyle(
        fontSize: fontsize13, fontFamily: "SegoeReg,", color: colorblack);
    var shadow = [
      BoxShadow(color: Color.fromRGBO(0, 0, 0, .07), blurRadius: 10)
    ];

    /// Local variables <<---------------------------------------------------->>

    List data = [
      {"text": "User Info", "route": useInfoRoute},
      {"text": "Licence", "route": lincenceRoute},
      {"text": "Aadhaar ", "route": adharRoute},
      {"text": "RC Book", "route": rcbookPage},
    ];
    return Container(
      height: height * .075 * data.length,
      width: width,
      padding: EdgeInsets.symmetric(horizontal: width * .008),
      margin: EdgeInsets.symmetric(horizontal: width * .045),
      child: ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              Get.toNamed(data[index]["route"]);
            },
            child: Container(
              height: height * .055,
              padding: EdgeInsets.all(height * .01),
              margin: EdgeInsets.symmetric(vertical: height * .01),
              decoration: BoxDecoration(
                  boxShadow: shadow,
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(height * .005)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    data[index]["text"],
                    style: style,
                  ),
                  Icon(
                    Icons.arrow_forward_ios_rounded,
                    size: fontsize13,
                    color: colorblack,
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
