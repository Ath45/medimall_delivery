import 'package:flutter/material.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/util/constants.dart';

class SubHeading extends StatelessWidget {
  final String text;

  SubHeading({required this.text});

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
          fontSize: SizeConfig.horizontal * 3.3,
          color: HexColor('#858585'),
          fontFamily: 'SegoeSemi'),
    );
  }
}
