import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/NewInnerOrderController/NewInnerOrderController.dart';
import 'package:medimallDelivery/Controller/PendingOrderDetailsController/PendingOrderDetailsController.dart';
import 'package:medimallDelivery/util/constants.dart';
import 'package:medimallDelivery/util/constantsDC.dart';
import 'package:medimallDelivery/util/contantRiyas.dart';

class WidgetPendingList extends StatelessWidget {
  final PendingOrderDetailsController controller = Get.put(PendingOrderDetailsController());
  @override
  Widget build(BuildContext context) {
    return Container(
      child: GetX<PendingOrderDetailsController>(
          init: PendingOrderDetailsController(),
          builder: (controller) {
            return ListView.builder(
                shrinkWrap: true,
                itemCount: PendingOrderDetailsController.to.pendOrdersList.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  return Container(
                    margin: EdgeInsets.only(top: 10),
                    height: SizeConfig.horizontal * 32,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(8),
                        boxShadow: myBoxShadow),
                    child: Padding(
                      padding: EdgeInsets.all(SizeConfig.horizontal * 3),
                      child: Row(
                        children: [
                          Image.network(
                            controller.pendOrdersList[index].image,
                            height: SizeConfig.horizontal * 25,
                            fit: BoxFit.fill,
                          ),
                          SizedBox(
                            width: SizeConfig.horizontal * 5,
                          ),
                          Column(
                            children: [
                              Container(
                                width: height*0.14,
                                child: Text(
                                  controller.pendOrdersList[index].productName,
                                  maxLines: 3,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      fontFamily: 'SegoeSemi',
                                      fontSize: SizeConfig.horizontal * 4.5),
                                ),
                              ),
                              Container(
                                width: height*0.14,
                                child: Text(
                                    " (" +controller.pendOrdersList[index].uomValue+")",
                                  maxLines: 3,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      fontFamily: 'SegoeSemi',
                                      fontSize: SizeConfig.horizontal * 4.0),
                                ),
                              ),
                            ],
                          ),
                         // Spacer(),
                          SizedBox(width: height*0.008,),
                          Expanded(
                            child: Column(
                              // mainAxisAlignment: MainAxisAlignment.spaceAround,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  'Qty : ' + controller.pendOrdersList[index].quantity.toString(),
                                  style: TextStyle(
                                      fontSize: SizeConfig.horizontal * 3,
                                      fontFamily: 'SegoeReg'),
                                ),
                                Spacer(),
                                Row(
                                  children: [
                                    Text(
                                      '₹ ' +functionCalvulate(controller.pendOrdersList[index].specialPrice.toString(),controller.pendOrdersList[index].quantity.toString()),
                                      style: TextStyle(
                                          fontSize: SizeConfig.horizontal * 4.0,
                                          fontFamily: 'SegoeBold'),
                                    ),
                                    SizedBox(
                                      width: SizeConfig.horizontal * 1,
                                    ),
                                    Stack(
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal:
                                              SizeConfig.horizontal * 0.7),
                                          child: Text(
                                            '₹ ' +functionCalvulate(controller.pendOrdersList[index].price.toString(),controller.pendOrdersList[index].quantity.toString()),
                                            style: TextStyle(
                                                color: Colors.black
                                                    .withOpacity(0.6),
                                                fontSize:
                                                SizeConfig.horizontal * 2.2,
                                                fontFamily:
                                                'PoppinsExtraLight'),
                                          ),
                                        ),
                                        Positioned(
                                          left: 0.0,
                                          right: 0.0,
                                          bottom: 0.0,
                                          top: 0.0,
                                          child: new RotationTransition(
                                            turns: new AlwaysStoppedAnimation(
                                                15 / 360),
                                            child: Container(
                                              child: Divider(
                                                color: Colors.black
                                                    .withOpacity(0.6),
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  );
                });
          }),
    );
  }
}