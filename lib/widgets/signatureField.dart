import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/PickUpDeliveryController/PickUpDeliveryController.dart';
import 'package:path_provider/path_provider.dart';
import 'package:signature/signature.dart';

class SignatureField extends StatelessWidget {
  String flag;
  SignatureField({required this.flag});
  final PickUpDeliveryController controller =
      Get.put(PickUpDeliveryController());
  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (BuildContext context) => Scaffold(
        body: Column(
          children: <Widget>[
            //SIGNATURE CANVAS
            Signature(
              controller: PickUpDeliveryController.to.controller,
              height: 160,
              backgroundColor: Colors.lightBlueAccent,
            ),
            //OK AND CLEAR BUTTONS
            flag == "Submitted" || flag == "Collected"
                ? SizedBox()
                : Container(
                    decoration: const BoxDecoration(color: Colors.white),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        //SHOW EXPORTED IMAGE IN NEW ROUTE
                        IconButton(
                          icon: const Icon(Icons.check),
                          color: Colors.blue,
                          onPressed: () async {
                            if (PickUpDeliveryController
                                .to.controller.isNotEmpty) {
                              final Uint8List data =
                                  (await PickUpDeliveryController.to.controller
                                      .toPngBytes())!;
                              if (data != null) {
                                // await Navigator.of(context).push(
                                //   MaterialPageRoute<void>(
                                //     builder: (BuildContext context) {
                                //       return Scaffold(
                                //         appBar: AppBar(),
                                //         body: Center(
                                //           child: Container(
                                //             color: Colors.grey[300],
                                //             child: Image.memory(data),
                                //           ),
                                //         ),
                                //       );
                                //     },
                                //   ),
                                // );
                                Directory tempDir =
                                    await getTemporaryDirectory();
                                // String tempPath = tempDir.path;
                                File file =
                                    await File('${tempDir.path}/image.png')
                                        .create();
                                file.writeAsBytesSync(data);
                                PickUpDeliveryController.to.poatImage(file);
                              }
                            }
                          },
                        ),
                        //CLEAR CANVAS
                        IconButton(
                          icon: const Icon(Icons.clear),
                          color: Colors.blue,
                          onPressed: () {
                            //setState(() => _controller.clear());
                            PickUpDeliveryController.to.clear();
                          },
                        ),
                      ],
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}
