import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:medimallDelivery/Controller/RegisterController/registerController.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/util/constantsDC.dart';
import 'package:medimallDelivery/util/style.dart';

class LoginTextBox extends StatelessWidget {
  final String placeHolder;
  /// pass "" ie empty string if no placeholder is needed
  final String header;
  final int maxLength;
  /// pass "" ie empty string if no header is needed
  final bool enablePadding;
  ///use in case when padding is need for place holder
  final TextInputType textInputType;
  final TextEditingController controller;
  final  validator;
  LoginTextBox({required this.controller,
    required this.maxLength,
  required this.header,
  this.validator,
  required this.placeHolder,
  required this.enablePadding,required this.textInputType});
  @override
  Widget build(BuildContext context) {
    return   Container(margin: EdgeInsets.only(bottom: height*.03),
      child: TextFormField(
        maxLength: maxLength,
        controller: controller,
        validator: (value){
          if(value!.isEmpty){
            return "This field required";
          }else{
            return null;
          }
        },
        style: TextStyle(color: Colors.white),
        autovalidateMode: AutovalidateMode.onUserInteraction,
        cursorColor: HexColor("#F5CF00"),
        keyboardType: textInputType,
        decoration: InputDecoration(
          isDense: true,
          contentPadding: EdgeInsets.only(bottom: height*.009),
          counterText: "",
          hintText: placeHolder,
          hintStyle: f12CWhiteO50,

          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white,width: 0.5),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white,width: 0.5),
          ),
          errorStyle: TextStyle(
            fontWeight: FontWeight.bold,
              color: Colors.red
          ),
          errorBorder:  new UnderlineInputBorder(
            borderSide: new BorderSide(color: HexColor("#F5CF00"), width: 0.0),
          ),
        ),
      ),
    );
  }
}
class AadharTextBox extends StatelessWidget {
  final String placeHolder;
  /// pass "" ie empty string if no placeholder is needed
  final String header;
  /// pass "" ie empty string if no header is needed
  final bool enablePadding;
  ///use in case when padding is need for place holder
  final TextInputType textInputType;
  final TextEditingController controller;
  final  validator;
  AadharTextBox({required this.controller,
  required this.header,
  this.validator,
  required this.placeHolder,
  required this.enablePadding,required this.textInputType});
  @override
  Widget build(BuildContext context) {
    return   Container(margin: EdgeInsets.only(bottom: height*.03),
      child: TextFormField(
        maxLength: textInputType==TextInputType.number?12:50,
        controller: controller,
        validator: (value){
          if(value!.length!=12)
            {
              return "Please enter a valid aadhar card number";
            }
          else if(value.isEmpty){
            return "This field required";
          }else{
            return null;
          }
        },
        style: TextStyle(color: Colors.white),
        autovalidateMode: AutovalidateMode.onUserInteraction,
        cursorColor: HexColor("#F5CF00"),
        keyboardType: textInputType,
        decoration: InputDecoration(
          isDense: true,
          contentPadding: EdgeInsets.only(bottom: height*.009),
          counterText: "",
          hintText: placeHolder,
          hintStyle: f12CWhiteO50,
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white,width: 0.5),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white,width: 0.5),
          ),
          errorStyle: TextStyle(
              color:  Colors.red,
            fontWeight: FontWeight.bold,
          ),
          errorBorder:  new UnderlineInputBorder(
            borderSide: new BorderSide(color: HexColor("#F5CF00"), width: 0.0),
          ),
        ),
      ),
    );
  }
}
class EmailTextBox extends StatelessWidget {
  final String placeHolder;
  /// pass "" ie empty string if no placeholder is needed
  final String header;
  /// pass "" ie empty string if no header is needed
  final bool enablePadding;
  ///use in case when padding is need for place holder
  final TextInputType textInputType;
  final TextEditingController controller;
  final  validator;
  EmailTextBox({required this.controller,
  required this.header,
  this.validator,
  required this.placeHolder,
  required this.enablePadding,required this.textInputType});
  @override
  Widget build(BuildContext context) {
    return   Container(margin: EdgeInsets.only(bottom: height*.03),
      child: TextFormField(
        maxLength: textInputType==TextInputType.number?10:50,
        controller: controller,
        validator: (value){
          return RegisterController.to
              .validateEmail(value!.trim());
        },
        style: TextStyle(color: Colors.white),
        autovalidateMode: AutovalidateMode.onUserInteraction,
        cursorColor: HexColor("#F5CF00"),
        keyboardType: textInputType,
        decoration: InputDecoration(
          isDense: true,
          contentPadding: EdgeInsets.only(bottom: height*.009),
          counterText: "",
          hintText: placeHolder,
          hintStyle: f12CWhiteO50,
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white,width: 0.5),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white,width: 0.5),
          ),
          errorStyle: TextStyle(
              color: Colors.red,
            fontWeight: FontWeight.bold,
          ),
          errorBorder:  new UnderlineInputBorder(
            borderSide: new BorderSide(color: HexColor("#F5CF00"), width: 0.0),
          ),
        ),
      ),
    );
  }
}
class PasswordTextBox extends StatelessWidget {
  final String placeHolder;
  /// pass "" ie empty string if no placeholder is needed
  final String header;
  /// pass "" ie empty string if no header is needed
  final bool enablePadding;
  ///use in case when padding is need for place holder
  final TextInputType textInputType;
  final TextEditingController controller;
  final  validator;
  PasswordTextBox({required this.controller,
  required this.header,
  this.validator,
  required this.placeHolder,
  required this.enablePadding,required this.textInputType});
  @override
  Widget build(BuildContext context) {
    RxBool isObscure=true.obs;
    return   Container(margin: EdgeInsets.only(bottom: height*.03),
      child: GetX<RegisterController>(
        builder: (controllerRx) {
          return TextFormField(
            obscureText: isObscure.value,
            maxLength: textInputType==TextInputType.number?10:50,
            controller: controller,
            validator: (value){
              return RegisterController.to
                  .validatePass(value!);
            },
            style: TextStyle(color: Colors.white),
            autovalidateMode: AutovalidateMode.onUserInteraction,
            cursorColor: HexColor("#F5CF00"),
            keyboardType: textInputType,
            decoration: InputDecoration(
              suffix: GetX<RegisterController>(
                builder: (controller) {
                  return GestureDetector(onTap: ()
                      {
                         isObscure.value=controller.changeObscure(isObscure.value);
                      },
                      child: isObscure.value?Icon(Icons.remove_red_eye_outlined,color: Colors.blue,):Icon(Icons.remove_red_eye,color: Colors.blue,));
                }
              ),
              isDense: true,
              contentPadding: EdgeInsets.only(bottom: height*.009),
              counterText: "",
              hintText: placeHolder,
              hintStyle: f12CWhiteO50,
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white,width: 0.5),
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white,width: 0.5),
              ),
              errorStyle: TextStyle(
                  color: Colors.red,
                fontWeight: FontWeight.bold,
              ),
              errorBorder:  new UnderlineInputBorder(
                borderSide: new BorderSide(color: HexColor("#F5CF00"), width: 0.0),
              ),
            ),
          );
        }
      ),
    );
  }
}
class PasswordConfirmTextBox extends StatelessWidget {
  final String placeHolder;
  /// pass "" ie empty string if no placeholder is needed
  final String header;
  /// pass "" ie empty string if no header is needed
  final bool enablePadding;
  ///use in case when padding is need for place holder
  final TextInputType textInputType;
  final TextEditingController controller;
  final  validator;
  PasswordConfirmTextBox({required this.controller,
  required this.header,
  this.validator,
  required this.placeHolder,
  required this.enablePadding,required this.textInputType});
  @override
  Widget build(BuildContext context) {
    RxBool isObscure=true.obs;
    return   Container(margin: EdgeInsets.only(bottom: height*.03),
      child: GetX<RegisterController>(
        builder: (controllerR) {
          return TextFormField(
            obscureText: isObscure.value,
            maxLength: textInputType==TextInputType.number?10:50,
            controller: controller,
            validator: (value){
              return RegisterController.to
                  .validateConfPass(value!);
            },
            style: TextStyle(color: Colors.white),
            autovalidateMode: AutovalidateMode.onUserInteraction,
            cursorColor: HexColor("#F5CF00"),
            keyboardType: textInputType,
            decoration: InputDecoration(
              isDense: true,
              suffix: GetX<RegisterController>(
                  builder: (controller) {
                    return GestureDetector(onTap: ()
                    {
                      isObscure.value=controller.changeObscure(isObscure.value);
                    },
                        child: isObscure.value?Icon(Icons.remove_red_eye_outlined,color: Colors.blue,):Icon(Icons.remove_red_eye,color: Colors.blue,));
                  }
              ),
              contentPadding: EdgeInsets.only(bottom: height*.009),
              counterText: "",
              hintText: placeHolder,
              hintStyle: f12CWhiteO50,
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white,width: 0.5),
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white,width: 0.5),
              ),
              errorStyle: TextStyle(
                  color:  Colors.red,
                fontWeight: FontWeight.bold,
              ),
              errorBorder:  new UnderlineInputBorder(
                borderSide: new BorderSide(color: HexColor("#F5CF00"), width: 0.0),
              ),
            ),
          );
        }
      ),
    );
  }
}
class PhoneNumberTextBox extends StatelessWidget {
  final String placeHolder;
  /// pass "" ie empty string if no placeholder is needed
  final String header;
  /// pass "" ie empty string if no header is needed
  final bool enablePadding;
  ///use in case when padding is need for place holder
  final TextInputType textInputType;
  final TextEditingController controller;
  final  validator;
  PhoneNumberTextBox({required this.controller,
  required this.header,
  this.validator,
  required this.placeHolder,
  required this.enablePadding,required this.textInputType});
  @override
  Widget build(BuildContext context) {
    return   Container(margin: EdgeInsets.only(bottom: height*.03),
      child: TextFormField(
        maxLength: textInputType==TextInputType.number?10:50,
        controller: controller,
        validator: (value){
            return RegisterController.to
                .validatePhone(value!);
        },
        style: TextStyle(color: Colors.white),
        autovalidateMode: AutovalidateMode.onUserInteraction,
        cursorColor: HexColor("#F5CF00"),
        keyboardType: textInputType,
        decoration: InputDecoration(
          isDense: true,
          contentPadding: EdgeInsets.only(bottom: height*.009),
          counterText: "",
          hintText: placeHolder,
          hintStyle: f12CWhiteO50,
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white,width: 0.5),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white,width: 0.5),
          ),
          errorStyle: TextStyle(
              color:  Colors.red,
            fontWeight: FontWeight.bold,
          ),
          errorBorder:  new UnderlineInputBorder(
            borderSide: new BorderSide(color: HexColor("#F5CF00"), width: 0.0),
          ),
        ),
      ),
    );
  }
}




