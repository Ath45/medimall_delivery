import 'package:flutter/material.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/util/constants.dart';

class MyButton extends StatelessWidget {
  final String text;

  MyButton({required this.text});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: SizeConfig.horizontal * 10,
      width: SizeConfig.horizontal * 40,
      decoration: BoxDecoration(
          color: HexColor('#4D9EFF'),
          boxShadow: [
            BoxShadow(
                color: Colors.black.withOpacity(0.1),
                offset: Offset(0, 3),
                blurRadius: 10)
          ],
          borderRadius: BorderRadius.circular(5)),
      child: Center(
          child: Text(
        text,
        style: TextStyle(
            fontSize: SizeConfig.horizontal * 3.7,
            fontFamily: 'SegoeReg',
            fontWeight: FontWeight.w500,
            color: HexColor('#FFFFFF')),
      )),
    );
  }
}

class MyButtonWhite extends StatelessWidget {
  final String text;

  MyButtonWhite({required this.text});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: SizeConfig.horizontal * 06,
      width: SizeConfig.horizontal * 28,
      decoration: BoxDecoration(
          color: HexColor('#FFFFFF'),
          boxShadow: [
            BoxShadow(
                color: Colors.black.withOpacity(0.1),
                offset: Offset(0, 3),
                blurRadius: 6)
          ],
          borderRadius: BorderRadius.circular(2)),
      child: Center(
          child: Text(
        text,
        style: TextStyle(
            fontSize: SizeConfig.horizontal * 3,
            fontFamily: 'SegoeReg',
            fontWeight: FontWeight.w500,
            color: HexColor('#1D1D1D')),
      )),
    );
  }
}

class MyButtonDC extends StatefulWidget {
  @override
  final String text;
  MyButtonDC({required this.text});
  _MyButtonDCState createState() => _MyButtonDCState();
}

class _MyButtonDCState extends State<MyButtonDC> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var height = SizeConfig.screenHeight * 0.1255;
    var width = SizeConfig.screenWidth;
    return Container(
      height: height * .40,
      width: width * .306,
      decoration: BoxDecoration(
          color: HexColor('#4D9EFF'),
          boxShadow: [
            BoxShadow(
                color: Colors.black.withOpacity(0.2),
                offset: Offset(0, 3),
                blurRadius: 10)
          ],
          borderRadius: BorderRadius.circular(5)),
      child: Center(
          child: Text(
        widget.text,
        style: TextStyle(
            fontSize: SizeConfig.horizontal * 3.7,
            fontFamily: 'SegoeReg',
            fontWeight: FontWeight.w500,
            color: HexColor('#FFFFFF')),
      )),
    );
  }
}
