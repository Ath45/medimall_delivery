import 'package:flutter/material.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/util/constants.dart';

class SubHeadingBold extends StatelessWidget {
  final String text;

  SubHeadingBold({required this.text});

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
          fontSize: SizeConfig.horizontal * 4.0,
          color: HexColor('#858585'),
          fontFamily: 'SegoeSemi',
          fontWeight: FontWeight.w900),
    );
  }
}
