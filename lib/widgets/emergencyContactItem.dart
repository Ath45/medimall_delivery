import 'package:flutter/material.dart';
import 'package:medimallDelivery/themes/HexColor.dart';

class EmergencyContactItem extends StatelessWidget {
  String name; String phone;

  EmergencyContactItem({
    required this.name,required this.phone
});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Container(
        height: 70,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: Colors.white,
            boxShadow: [BoxShadow(color: Colors.grey.shade400, blurRadius: 4)]),
        child: Row(
          children: [
            SizedBox(
              width: 19,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  name,
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  phone,
                  style: TextStyle(
                    fontSize: 15,
                  ),
                ),
              ],
            ),
            Spacer(),
            Padding(
              padding: const EdgeInsets.only(right: 27),
              child: Container(
                  height: 35,
                  width: 35,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.circle,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.shade300,
                          blurRadius: 4,
                        )
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ImageIcon(
                      AssetImage('assets/icons/call.png'),
                      color: HexColor('#FF0000'),
                    ),
                  )),
            )
          ],
        ),
      ),
    );
  }
}
