import 'package:get/get.dart';
import 'package:medimallDelivery/Routes/routes.dart';
import 'package:medimallDelivery/screens/LIcencePage/LicencePage.dart';
import 'package:medimallDelivery/screens/RegisterPage.dart';
import 'package:medimallDelivery/screens/Splashpage.dart';
import 'package:medimallDelivery/screens/adharPage/adharPage.dart';
import 'package:medimallDelivery/screens/deliveryBoy/Credits/creditPage.dart';
import 'package:medimallDelivery/screens/deliveryBoy/Messages/messagesPage.dart';
import 'package:medimallDelivery/screens/deliveryBoy/Profile/emergencyContact.dart';
import 'package:medimallDelivery/screens/deliveryBoy/Profile/myAccounts.dart';
import 'package:medimallDelivery/screens/deliveryBoy/addressPage.dart';
import 'package:medimallDelivery/screens/deliveryBoy/bankDetailPage.dart';
import 'package:medimallDelivery/screens/deliveryBoy/cancelledOrders/cancelledOrderDetail.dart';
import 'package:medimallDelivery/screens/deliveryBoy/cancelledOrders/cancelledOrders.dart';
import 'package:medimallDelivery/screens/deliveryBoy/cashDeposit/cashDeposit.dart';
import 'package:medimallDelivery/screens/deliveryBoy/creditDetailPage.dart';
import 'package:medimallDelivery/screens/deliveryBoy/deliveredOrders/deliveredOrderDetail.dart';
import 'package:medimallDelivery/screens/deliveryBoy/deliveredOrders/deliveredOrders.dart';
import 'package:medimallDelivery/screens/deliveryBoy/emergencyContactPage.dart';
import 'package:medimallDelivery/screens/deliveryBoy/helpAndSupport.dart';
import 'package:medimallDelivery/screens/deliveryBoy/helpSupportPage.dart';
import 'package:medimallDelivery/screens/deliveryBoy/homePage.dart';
import 'package:medimallDelivery/screens/deliveryBoy/ledger/ledger.dart';
import 'package:medimallDelivery/screens/deliveryBoy/messagesPage.dart';
import 'package:medimallDelivery/screens/deliveryBoy/newOrders/newOrderDetail.dart';
import 'package:medimallDelivery/screens/deliveryBoy/newOrders/newOrdersPage.dart';
import 'package:medimallDelivery/screens/deliveryBoy/noticication.dart';
import 'package:medimallDelivery/screens/deliveryBoy/notificationPage.dart';
import 'package:medimallDelivery/screens/deliveryBoy/pendingDelivery/pendingDeliveryPage.dart';
import 'package:medimallDelivery/screens/deliveryBoy/pendingDelivery/pendingOrderDetail.dart';
import 'package:medimallDelivery/screens/deliveryBoy/profilePage.dart';
import 'package:medimallDelivery/screens/deliveryBoy/returnProduct/pickupProduct.dart';
import 'package:medimallDelivery/screens/deliveryBoy/returnProduct/pickupProductDetails.dart';
import 'package:medimallDelivery/screens/deliveryBoy/terms_and_condition.dart';
import 'package:medimallDelivery/screens/deliveryBoy/transactions/transactionsPage.dart';
import 'package:medimallDelivery/screens/deliveryBoy/userInfo.dart';
import 'package:medimallDelivery/screens/rcbookPage/rcbookPage.dart';
import 'package:medimallDelivery/screens/signupPage.dart';
import 'package:medimallDelivery/screens/loginPage.dart';

import '../binding.dart';

abstract class AppPages {
  static final List<GetPage> pages = [
    GetPage(name: alpha, page: () => SplashScreen(), binding: SplashBinding()),
    GetPage(name: loginRoute, page: () => LoginPage(), binding: LoginBinding()),
    GetPage(name: signupPage, page: () => SignupPage()),
    GetPage(
        name: signUpRoute,
        page: () => RegisterPage(),
        binding: RegisterBinding()),
    GetPage(name: homeRoute, page: () => HomePage(), binding: HomeBinding()),

    GetPage(
        name: accountRoute,
        page: () => MyAccounts(),
        binding: MyAccountBinding()),

    GetPage(name: newOrders, page: () => NewOrders(), binding: NewOrderBinding()),

    GetPage(name: pendingDelivery, page: () => PendingDelevery(), binding: PendingOrderBinding()),

    GetPage(name: deliverdOrder, page: () => DeliveredOrders(), binding: DeliverdOrderBinding()),

    GetPage(name: cancelledOrder, page: () => CancelledOrders(), binding: CancelledOrderBinding()),

    GetPage(name: pickupOrder, page: () => PickedUpProduct(), binding: PickUpOrderBinding()),

    GetPage(name: transactionOrder, page: () => TransactionsPage(), binding: TransactionOrderBinding()),

    GetPage(name: creditRoute, page: () => CreditPage(), binding: CreditBinding()),

    GetPage(name: emergencyRoute, page: () => EmergencyContact(), binding: EmergencyBinding()),

    GetPage(name: helpAndSupportRoute, page: () => HelpAndSupport(), binding: HelpAndSupportBinding()),

    GetPage(name: newOrderDetails, page: () => NewOrderDetail(), binding: NewOrderInnerPagetBinding()),

    GetPage(name: pendingOrderDetail, page: () => PendingOrderDetail(), binding: PendingOrderInnerPageBinding()),

    GetPage(name: deliverdOrderDetails, page: () => DeliveredOrderDetail(), binding: DeliverdInnerOrderInnerPageBinding()),

    GetPage(name: cancelledOrderDetails, page: () => CancelledOrderDetail(), binding: CancelledOrdersDetailsPage()),

    GetPage(name: cashDeposit, page: () => CashDeposit(), binding: CashDepositBinding()),

    GetPage(name: ledGerDetails, page: () => Ledger(), binding: LedgerBinding()),

    GetPage(name: messagePage, page: () => MessagePage(), binding: MessageBinding()),

    GetPage(name: pickupProduct, page: () => PickUpProductDetails(), binding: PickUpDetailsBinding()),

    GetPage(name: addressPage, page: () => AddressPage(), binding: AddressBinding()),

    GetPage(name: bankDetailsPage, page: () => BankDetailPage(), binding: BandOrderBinding()),

    GetPage(name: creditialdetailsPage, page: () => CreditDetail(), binding: CredDetailsBinding()),

    GetPage(name: emergencyCont, page: () => EmergencyContacts(), binding: EmergencyControllerPageBinding()),

    GetPage(name: emergencyCont, page: () => HelpSupport(), binding: HelpSupportBinding()),

    GetPage(name: messageRoutue, page: () => MessagesPage(), binding: MessageBindingPage()),

    GetPage(name: notificationRoute, page: () => NotificationPage(), binding: NotificationBinding()),

    GetPage(name: notificationPage, page: () => NotificationsPage(), binding: NotificationPageBinding()),

    GetPage(name: profilePageRoute, page: () => ProfilePage(), binding: ProfileBinding()),

    GetPage(name: useInfoRoute, page: () => UserInfo(), binding: UserInfoBinding()),

    GetPage(name: lincenceRoute, page: () => LicencePage(), binding: LincenceBinding()),

    GetPage(name: adharRoute, page: () => AdharPage(), binding: AdharBinding()),

    GetPage(name: rcbookPage, page: () => RcbookPage(), binding: RcBookBinding()),
    GetPage(name: termsandcondition, page: () => TermsAndCondition(),binding: TermsBinding()),

  ];
}
