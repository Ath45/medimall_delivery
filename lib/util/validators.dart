import 'package:get/get.dart';

String? isEmpty(String value)
{
  if(value.isEmpty)
  {
    return "This Field Is Required";
  }
  else if(value.length<=6)
  {
    return "Password should be 6 characters";

  }else{
    return null;
  }
}
String? isAadhar(String value)
{
  if(value.isEmpty)
  {
    return "This Field Is Required";
  }
  else if(value.length<=13)
  {
    return "Password should be 6 characters";

  }else{
    return null;
  }
}
String? isLicense(String value)
{
  if(value.isEmpty)
  {
    return "This Field Is Required";
  }
  else if(value.length<=12)
  {
    return "Password should be 6 characters";

  }else{
    return null;
  }
}
String? repeatPassword(String value,String value2) {
  if (value.isEmpty) {
    return "This Field Is Required";
  }
  else if (value != value2) {
    return " The Passwords should be same";
  }
  else if(value.length<=6)
  {
    return "Password should be 6 characters";

  }
  else {
    return null;
  }
}

    String? isEmail(String value)
{
  if(value.isNotEmpty){
    if (!GetUtils.isEmail(value)) {
      return "This Is Not a Valid Email Format";
    } else {
      return null;
    }
  }
  else
  {
    return "This Field Is Required";
  }
}
String? isPassword(String value)
{
  if(value.isNotEmpty){
    if (value.length<=8) {
      return "This Is Not a Valid  Password";
    } else {
      return null;
    }
  }
  else
  {
    return "This Field Is Required";
  }
}

String? isMobile(String value)
{
  if(value.isNotEmpty){
    if (GetUtils.isNumericOnly(value.trim()) && value.length!=10) {
      return "This Is Not a Valid mobile number";
    } else {
      return null;
    }
  }
  else
  {
    return "This Field Is Required";
  }
}