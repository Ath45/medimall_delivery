import 'package:flutter/material.dart';

var myBoxShadow = [
  BoxShadow(
      color: Colors.black.withOpacity(0.07),
      offset: Offset(0, 0),
      blurRadius: 10)
];