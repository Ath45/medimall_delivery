

import 'package:flutter/material.dart';

class SizeConfig {
  // static MediaQueryData _mediaQueryData=MediaQuery.of(context);
  static double screenWidth=0;
  static double screenHeight=0;
  static double blockSizeHorizontal=0;
  static double blockSizeVertical=0;

  static double _safeAreaHorizontal=0;
  static double _safeAreaVertical=0;
  static double horizontal=0;
  static double vertical=0;

  void init(BuildContext context) {
    MediaQueryData _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    blockSizeHorizontal = screenWidth / 100;
    blockSizeVertical = screenHeight / 100;

    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;
    horizontal = (screenWidth - _safeAreaHorizontal) / 100;
    vertical = (screenHeight - _safeAreaVertical) / 100;
  }
}
functionCalvulate(String orderValue,String qty) {
  try{
    var amt = double.parse(orderValue);
    var qtyd = double.parse(qty);
    var total=amt*qtyd;
    return total.toString();
  }catch(ex)
  {
    return "";
  }
}
