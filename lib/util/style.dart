import 'package:flutter/material.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:medimallDelivery/util/fonts.dart';

TextStyle f12CWhiteO50 =TextStyle(color: Colors.white54,fontSize: 12);
TextStyle f12CWhite = TextStyle(fontSize: 12,color: Colors.white);
TextStyle sRegF12CWhite = TextStyle(fontSize: 12,color: Colors.white,fontFamily: segoeReg);
TextStyle sRegF9CWhite = TextStyle(fontSize: 9,color: Colors.white54,fontFamily: segoeReg);
TextStyle sSBoldF12 = TextStyle(fontSize: 17,color: HexColor("#005CEC"),fontFamily: segoeSemi);