import 'package:flutter/material.dart';
import 'package:medimallDelivery/themes/HexColor.dart';
import 'package:get/get.dart';

///Global variables Color <<-------------------------------------------------->>
var colorblack = Color.fromRGBO(0, 0, 0, 1);
var colorblack70 = Color.fromRGBO(0, 0, 0, .7);
var colorblack50 = Color.fromRGBO(0, 0, 0, .5);
var colorblack30 = Color.fromRGBO(0, 0, 0, .3);
var colorMyblue = Color.fromRGBO(77, 158, 255, 1);
var colorGrey = Color.fromRGBO(98, 98, 100, 1);
var fillColor = Colors.white;
var colorShadow = Color.fromRGBO(0, 0, 0, 0.07058823529411765);
var purple = HexColor("#1B3884");

///Global variables Color <<-------------------------------------------------->>

///Global variables Shadow <<------------------------------------------------->>
var shadow = [BoxShadow(color: colorShadow, blurRadius: 10)];
///Global variables Shadow <<------------------------------------------------->>

double sp= (Get.width*(414*1.05));
double width= Get.width;
double height= Get.height*1.222;