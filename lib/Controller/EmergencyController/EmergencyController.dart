import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Model/EmergenceyModelClass/EmergenceyModelClass.dart';
import 'package:medimallDelivery/Services/ApiService.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class EmergencyController extends GetxController {
  static EmergencyController get to => Get.find();
  RxList<dynamic> emerGency = [].obs;
  var mobNUmber="";
  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  }

  void getData() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    var data = await Api.getEmergency(auth);
    var it = EmergenceyModelClass.fromJson(data);
    if (it.error) {
      Get.snackbar(
        "Info",
        "Some thing went wrong",
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    }else{
      emerGency.value=it.data.emergencyContact;
      update();
    }
  }

  void onCall(String mob) async{
    mobNUmber=mob;
    if (await canLaunch('tel://$mob')) {
    await launch('tel://$mob');
    update();
    } else {
      onCall(mobNUmber);
    throw 'Could not launch $mob';
    }

  }
}