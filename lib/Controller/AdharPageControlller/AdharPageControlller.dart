//AdharPageControlller
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Model/UserInfoModelClass/UserInfoModelClass.dart';
import 'package:medimallDelivery/Services/ApiService.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AdharPageControlller extends GetxController {
  static AdharPageControlller get to => Get.find();
  var url = "".obs;
  var loadIng=false.obs;
  late TextEditingController aadharController;

  @override
  void onInit() {
    aadharController = TextEditingController();
    //getData();
    super.onInit();
  }

  void getData() async {
    loadIng.value=true;
    update();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    var status = prefs.getString('Status') ?? '';
    var data = await Api.getProfileData(auth, status);
    var it = UserInfoModelClass.fromJson(data);
    if (!it.status) {
      loadIng.value=false;
      update();
      Get.snackbar(
        "Info",
        "Some thing went wrong",
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    } else {
      loadIng.value=false;
      update();
      url.value = it.data[0].adhaar.aadhar;
      if (it.data[0].adhaar.aadharCardNumber != "null") {
        aadharController.text = it.data[0].adhaar.aadharCardNumber;
      } else {
        aadharController.text = "";
      }
      update();
    }
  }
}
