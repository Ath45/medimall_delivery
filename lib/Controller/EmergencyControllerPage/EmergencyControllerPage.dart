import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Model/EmergenceyModelClass/EmergenceyModelClass.dart';
import 'package:medimallDelivery/Services/ApiService.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EmergencyControllerPage extends GetxController {
  static EmergencyControllerPage get to => Get.find();
  RxList<dynamic> emerGency = [].obs;
  var loadIng=false.obs;
  @override
  void onInit() {
    super.onInit();
  }

  void getData() async{
    loadIng.value=true;
    update();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    var data = await Api.getEmergency(auth);
    var it = EmergenceyModelClass.fromJson(data);
    if (it.error) {
      loadIng.value=false;
      update();
      Get.snackbar(
        "Info",
        "Some thing went wrong",
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    }else{
      loadIng.value=false;
      update();
      emerGency.value=it.data.emergencyContact;
      update();
    }
  }
}