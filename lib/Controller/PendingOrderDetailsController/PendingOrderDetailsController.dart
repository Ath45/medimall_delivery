import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/PendingDeliveryController/pendingDeliveryController.dart';
import 'package:medimallDelivery/Model/BaseApiResponse/BaseApiResponse.dart';
import 'package:medimallDelivery/Model/PendingDeliveryInnerModelClass/PendingDeliveryInnerModelClass.dart';
import 'package:medimallDelivery/Services/ApiService.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PendingOrderDetailsController extends GetxController {
  static PendingOrderDetailsController get to => Get.find();
  RxList<dynamic> pendOrdersList = [].obs;
  var nameCustomer = "".obs;
  var mobileCustomer = "".obs;
  var codTypeCustomer = "".obs;
  var amountToPaid = "".obs;
  var address = "".obs;
  var picUpAddress = "".obs;
  var orderId = "";
  var loadIng = false.obs;
  void pendGetData(String id) async {
    loadIng.value = true;
    update();
    orderId = id;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    var status = prefs.getString('Status') ?? '';
    var data = await Api.getDeliveryOrder(auth, status, id);
    var it = PendingDeliveryInnerModelClass.fromJson(data);
    if (it.error) {
      loadIng.value = false;
      update();
      Get.snackbar(
        "Info",
        "Some thing went wrong",
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    } else {
      loadIng.value = false;
      update();
      pendOrdersList.value = it.data.deliveryPendingOrder[0].products;
      nameCustomer.value = it.data.deliveryPendingOrder[0].address.name;
      mobileCustomer.value = it.data.deliveryPendingOrder[0].address.mobile;
      codTypeCustomer.value = it.data.deliveryPendingOrder[0].paymentType;
      amountToPaid.value =
          it.data.deliveryPendingOrder[0].totalAmountToBePaid.toString();
      address.value = it.data.deliveryPendingOrder[0].address.wholeAddress;
      if (it.data.deliveryPendingOrder[0].storeAddress.name != "null") {
        picUpAddress.value =
            it.data.deliveryPendingOrder[0].storeAddress.name + " ,";
      }
      if (it.data.deliveryPendingOrder[0].storeAddress.address != "null") {
        picUpAddress.value = picUpAddress.value +
            it.data.deliveryPendingOrder[0].storeAddress.address +
            " ,";
      }
      if (it.data.deliveryPendingOrder[0].storeAddress.country != "null") {
        picUpAddress.value = picUpAddress.value +
            it.data.deliveryPendingOrder[0].storeAddress.country +
            " ,";
      }
      if (it.data.deliveryPendingOrder[0].storeAddress.state != "null") {
        picUpAddress.value = picUpAddress.value +
            it.data.deliveryPendingOrder[0].storeAddress.state +
            " ,";
      }
      if (it.data.deliveryPendingOrder[0].storeAddress.pin != "null") {
        picUpAddress.value = picUpAddress.value +
            it.data.deliveryPendingOrder[0].storeAddress.pin +
            ", ";
      }
      if (it.data.deliveryPendingOrder[0].storeAddress.phone != "null") {
        picUpAddress.value = picUpAddress.value +
            it.data.deliveryPendingOrder[0].storeAddress.phone;
      }
      update();
    }
  }

  void changeStatus() async {
    loadIng.value = true;
    update();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    var data = await Api.deliverdOrders(auth, orderId, "delivered");
    var it = BaseApiResponse.fromJson(data);
    if (it.error) {
      loadIng.value = false;
      update();
      Get.snackbar(
        "Info",
        "Some thing went wrong",
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    } else {
      loadIng.value = false;
      update();
      PendingDeliveryController.to.onInit();
      Get.back();
    }
  }
}
