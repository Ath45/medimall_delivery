import 'dart:convert';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:medimallDelivery/Model/signUpModel/BaseApiResponse.dart';
import 'package:medimallDelivery/Model/signUpModel/RegisterModel.dart';
import 'package:medimallDelivery/Model/signUpModel/SignUpModelApi.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:medimallDelivery/util/constantsDC.dart';
import 'package:medimallDelivery/widgets/CustomAlertDialog.dart';
import '../../API.dart';

class RegisterController extends GetxController {
  static RegisterController get to => Get.find();
  GlobalKey<FormState> formKeyFirst =
  new GlobalKey<FormState>(debugLabel: '_formKeyFirst');
  var imgaeSelcted = 0;
  changeObscure(value)=>!value;
  late BuildContext context;
  FocusNode myFocusNode = new FocusNode();
  TextEditingController nameController = new TextEditingController();
  TextEditingController phoneController = new TextEditingController();
  TextEditingController mailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  TextEditingController passwordRepeatController = new TextEditingController();
  TextEditingController licenseController = new TextEditingController();
  TextEditingController addressController = new TextEditingController();
  TextEditingController aadarController = new TextEditingController();
  TextEditingController cityController = new TextEditingController();

  // Rx<File> rcBookImage = File("").obs;
  var recBookImage = false;
  var loading = false.obs;
  var lincenceImageLoad = false;
  var adharImageLoad = false;
  bool validate = false;
  //Rx<File> adhaarImage = File("").obs;
  // Rx<File> licenseImage = File("").obs;
  late XFile? rcBookImage = XFile("");
  late XFile? licenseImage = XFile("");
  late XFile? adhaarImage = XFile("");

  String? validatePhone(String value) {
    String pattern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
    RegExp regExp = new RegExp(pattern);
    if (value.length != 10) {
      validate = false;
      return 'Please enter mobile number';
    } else if (!regExp.hasMatch(value)) {
      validate = false;
      return 'Please enter valid mobile number';
    }
    return null;
  }
  String? validatePass(String value) {
    if (value.length < 6) {
      validate = false;
      return 'Please enter password more than 6 characters';
    } else if(value.isEmpty){
      validate = false;
      return 'This field required';

    }
    return null;
  }
  String? validateConfPass(String value) {
    if (value != passwordController.text) {
      print("value = "+value+" controller= "+passwordController.text);
      validate = false;
      return 'Passwords not match..';
    }
    else if(value.isEmpty){
      validate = false;
      return 'This field required';

    }
    return null;
  }
  String? validateEmail(String value) {
    if (!GetUtils.isEmail(value)) {
      return "Please provide valid Email";
    }
    return null;
  }



  fetchUserData() async {
    print("Data Called");
    if (imgaeSelcted >= 3) {
      loading.value=true;
      SignUpRequestModel userData = SignUpRequestModel(
          fullName: nameController.text,
          mobileNumber: phoneController.text,
          emailId: mailController.text,
          password: passwordController.text,
          passwordRepeat: passwordRepeatController.text,
          licenceNumber: licenseController.text,
          address: addressController.text,
          aadhar: aadarController.text,
          city: cityController.text,
          rcBookImage: File(rcBookImage!.path),
          adhaarImage: File(rcBookImage!.path),
          licenseImage: File(adhaarImage!.path));


      Uri url = Uri.parse(baseUrl + registerURL);
      var request = http.MultipartRequest("POST", url);
      var rcBook = await http.MultipartFile.fromPath(
          "rcBook", File(rcBookImage!.path).path,
          filename: File(rcBookImage!.path).path.split("/").last,
        contentType: new MediaType('image', 'jpg'),
          );
      var aadhar = await http.MultipartFile.fromPath(
          "aadhar", File(adhaarImage!.path).path,
          filename: File(adhaarImage!.path).path.split("/").last,
        contentType: new MediaType('image', 'jpg'),
     );
      var licence = await http.MultipartFile.fromPath(
          "licence", File(licenseImage!.path).path,
          filename: File(licenseImage!.path).path.split("/").last,
        contentType: new MediaType('image', 'jpg'),
         );
      // request.files.addAll(await [rcBook,aadhar,licence]);
      request.files.add(rcBook);
      request.files.add(aadhar);
      request.files.add(licence);

      request.fields["fullName"] = userData.fullName;
      request.fields["mobile"] = userData.mobileNumber;
      request.fields["email"] = userData.emailId;
      request.fields["password"] = userData.passwordRepeat;
      request.fields["drivingLicenseNumber"] = userData.licenceNumber;
      request.fields["aadharCardNumber"] = userData.aadhar;
      request.fields["address"] = userData.address;
      request.fields["city"] = userData.city;
      var response = await request.send();
      if (true) {
        // print(response.stream.bytesToString());
        loading.value = false;
        var responseString = await response.stream.bytesToString();
        print(responseString);
        var jsData = json.decode(responseString.toString());
        var it = BaseApiResponse.fromJson(jsData);
        if (it.error) {
          loading.value = false;
          var dialog = CustomAlertDialog(
              title: "Info",
              message: it.message,
              onPostivePressed: () {
                Navigator.pop(context);
              },
              positiveBtnText: 'Ok',
              negativeBtnText: '');
          showDialog(
              context: context,
              builder: (BuildContext context) => dialog);
          // Get.snackbar(
          //   "Info",
          //   it.message,
          //   icon: Icon(Icons.person, color: Colors.white),
          //   snackPosition: SnackPosition.BOTTOM,
          //   backgroundColor: Colors.white,
          //   borderRadius: 20,
          //   margin: EdgeInsets.all(15),
          //   colorText: Colors.black,
          //   duration: Duration(seconds: 4),
          //   isDismissible: true,
          //   dismissDirection: SnackDismissDirection.HORIZONTAL,
          //   forwardAnimationCurve: Curves.easeOutBack,
          // );
        } else {
          loading.value = false;
          var it = SignUpModelApi.fromJson(jsData);
          if (!it.error) {
           Get.back();
           Get.defaultDialog(title: "Message",middleText:
               it.message,
               contentPadding: EdgeInsets.symmetric(horizontal: width*.05,vertical: 5),
               confirm: Container(decoration: BoxDecoration(
                 borderRadius: BorderRadius.circular(20),
                 border: Border.all(color: Colors.blue,width: 3)
               ),
                   padding:EdgeInsets.symmetric(horizontal:20,vertical: 5),child: GestureDetector(onTap: ()
                       {
                         Get.back();
                       },
                       child: Text("OK"))),
               onConfirm:()=> Get.back(),
           );

            // Get.offAll(loginRoute);

          }
        }
      }
      else {
        loading.value = false;
        print(response.stream.transform(utf8.decoder).listen((event) {
          Get.snackbar(
            "Info",
            event,
            icon: Icon(Icons.person, color: Colors.white),
            snackPosition: SnackPosition.BOTTOM,
            backgroundColor: Colors.white,
            borderRadius: 20,
            margin: EdgeInsets.all(15),
            colorText: Colors.black,
            duration: Duration(seconds: 4),
            isDismissible: true,
            dismissDirection: DismissDirection.horizontal,
            forwardAnimationCurve: Curves.easeOutBack,
          );
        }));
        // return responseData;
      }
    } else {
      loading.value = false;
      // Get.snackbar(
      //   "Info",
      //   "Please Select All Images",
      //   icon: Icon(Icons.person, color: Colors.white),
      //   snackPosition: SnackPosition.BOTTOM,
      //   backgroundColor: Colors.white,
      //   borderRadius: 20,
      //   margin: EdgeInsets.all(15),
      //   colorText: Colors.black,
      //   duration: Duration(seconds: 4),
      //   isDismissible: true,
      //   dismissDirection: SnackDismissDirection.HORIZONTAL,
      //   forwardAnimationCurve: Curves.easeOutBack,
      // );
      var dialog = CustomAlertDialog(
          title: "Info",
          message: "Please upload all the required document images",
          onPostivePressed: () {
            Navigator.pop(context);
          },
          positiveBtnText: 'Ok',
          negativeBtnText: '');
      showDialog(
          context: context,
          builder: (BuildContext context) => dialog);
    }
  }

  void getImage(ImageSource gallery) async {
    rcBookImage = (await ImagePicker().pickImage(source: ImageSource.gallery));
    if (File(rcBookImage!.path).path.isNotEmpty) {
      recBookImage = true;
    }
    imgaeSelcted = imgaeSelcted + 1;
    update();
  }

  void getImageLincence(ImageSource gallery) async {
    licenseImage = (await ImagePicker().pickImage(source: ImageSource.gallery));
    if (File(licenseImage!.path).path.isNotEmpty) {
      lincenceImageLoad = true;
    }
    imgaeSelcted = imgaeSelcted + 1;
    update();
  }

  void getImageAdhar(ImageSource gallery) async {
    adhaarImage = (await ImagePicker().pickImage(source: ImageSource.gallery));
    if (File(adhaarImage!.path).path.isNotEmpty) {
      adharImageLoad = true;
    }
    imgaeSelcted = imgaeSelcted + 1;
    update();
  }
  @override
  void dispose() {
    super.dispose();
    RegisterController().dispose();
  }
}
