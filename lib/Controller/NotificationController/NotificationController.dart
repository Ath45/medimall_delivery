import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/HomeController/HomeConroller.dart';
import 'package:medimallDelivery/Model/BaseApiResponse/BaseApiResponse.dart';
import 'package:medimallDelivery/Model/NotificationModel/NotificationModel.dart';
import 'package:medimallDelivery/Routes/routes.dart';
import 'package:medimallDelivery/Services/ApiService.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NotificationController extends GetxController {
  static NotificationController get to => Get.find();
  var notList = [].obs;
  var rxEmpty = false.obs;
  var loadIng = false.obs;

  @override
  void onInit() {
    super.onInit();
  }

  void getData() async {
    loadIng.value = true;
    update();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    print("Print" + auth);
    var data = await Api.getDeliveryNot(auth);
    var it = NotificationModel.fromJson(data);
    if (it.error) {
      loadIng.value = false;
      update();
      Get.snackbar(
        "Info",
        "Some thing went wrong",
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    } else {
      loadIng.value = false;
      update();
      HomeController.to.onInit();
      if (it.data.deliveryBoyNotification.isNotEmpty) {
        notList.value = it.data.deliveryBoyNotification;
        rxEmpty.value = false;
        update();
      } else {
        rxEmpty.value = true;
        update();
        notList.clear();
      }
    }
  }

  void changeStatus(String id,String odrId) async {
    loadIng.value = true;
    update();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    var data = await Api.notUpdate(auth, id);
    var it = BaseApiResponse.fromJson(data);
    if (it.error) {
      loadIng.value = false;
      update();
      Get.snackbar(
        "Info",
        "Some thing went wrong",
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    } else {
      loadIng.value = false;
      update();
      getData();
      Get.toNamed(newOrderDetails, arguments: [odrId]);

      HomeController.to.onInit();
    }
  }
}
