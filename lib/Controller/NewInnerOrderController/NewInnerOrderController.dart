import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/NewOrderController/newOrderController.dart';
import 'package:medimallDelivery/Model/BaseApiResponse/BaseApiResponse.dart';
import 'package:medimallDelivery/Model/NewInnerOrderModal/NewInnerOrdersModal.dart';
import 'package:medimallDelivery/Services/ApiService.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NewInnerOrderController extends GetxController {
  static NewInnerOrderController get to => Get.find();
  RxList<dynamic> orddersList = [].obs;
  var loadIng = false.obs;
  var nameCustomer = "".obs;
  var mobileCustomer = "".obs;
  var codTypeCustomer = "".obs;
  var amountToPaid = "".obs;
  var addressDta = "".obs;
  var enableBtn = false.obs;
  var picUpAddress="".obs;
  void getData(String orderId) async {
    loadIng.value=true;
    update();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    var status = prefs.getString('Status') ?? '';
    var data = await Api.getinnerOrders(auth, status, orderId);
    var it = NewInnerOrdersModal.fromJson(data);
    if (it.error) {
      loadIng.value=false;
      update();
      Get.snackbar(
        "Info",
        "Some thing went wrong",
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    } else {
      loadIng.value=false;
      update();
      orddersList.value = it.data.pickupPendingOrders[0].products;
      nameCustomer.value = it.data.pickupPendingOrders[0].address.name;
      mobileCustomer.value = it.data.pickupPendingOrders[0].address.mobile;
      codTypeCustomer.value = it.data.pickupPendingOrders[0].paymentType;
      amountToPaid.value =
          it.data.pickupPendingOrders[0].totalAmountToBePaid.toString();
      addressDta.value = it.data.pickupPendingOrders[0].address.wholeAddress;
      if (it.data.pickupPendingOrders[0].pickAddress.name != "null") {
        picUpAddress.value =
            it.data.pickupPendingOrders[0].pickAddress.name + " ,";
      }  if (it.data.pickupPendingOrders[0].pickAddress.address !=
          "null") {
        picUpAddress.value = picUpAddress.value +
            it.data.pickupPendingOrders[0].pickAddress.address +
            " ,";
      }  if (it.data.pickupPendingOrders[0].pickAddress.country !=
          "null") {
        picUpAddress.value = picUpAddress.value +
            it.data.pickupPendingOrders[0].pickAddress.country +
            " ,";
      }  if (it.data.pickupPendingOrders[0].pickAddress.state != "null") {
        picUpAddress.value = picUpAddress.value +
            it.data.pickupPendingOrders[0].pickAddress.state +
            " ,";
      }  if (it.data.pickupPendingOrders[0].pickAddress.pin != "null") {
        picUpAddress.value = picUpAddress.value +
            it.data.pickupPendingOrders[0].pickAddress.pin;
      }
    //  picUpAddress.value=it.data.pickupPendingOrders[0].pickAddress.name+" ,"+it.data.pickupPendingOrders[0].pickAddress.address+" ,"+it.data.pickupPendingOrders[0].pickAddress.country+" ,"+it.data.pickupPendingOrders[0].pickAddress.state+" ,"+it.data.pickupPendingOrders[0].pickAddress.pin;

      if (it.data.pickupPendingOrders[0].paymentType == "cod") {
        enableBtn.value = true;
      } else {
        enableBtn.value = false;
      }
      update();
    }
  }

  void acceptOrder(String s, String order) async {
    loadIng.value=true;
    update();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    var data = await Api.acceptOrders(auth, order, s);
    var it = BaseApiResponse.fromJson(data);
    if (it.error) {
      loadIng.value=false;
      update();
      Get.snackbar(
        "Info",
        "Some thing went wrong",
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    } else {
      loadIng.value=false;
      update();
      NewOrderController.to.onInit();
      Get.back();
    }
  }
}
