import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/HomeController/HomeConroller.dart';
import 'package:medimallDelivery/Model/BaseApiResponse/BaseApiResponse.dart';
import 'package:medimallDelivery/Model/MessageModalClass/MessageModalClass.dart';
import 'package:medimallDelivery/Services/ApiService.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MessageController extends GetxController {
  static MessageController get to => Get.find();
  RxList<dynamic> messageList = [].obs;
  // RxList<dynamic> loadIng = [].obs;
  var rxEmpty = false.obs;
  var loadIng = false.obs;
  @override
  void onInit() {
    super.onInit();
  }

  void getData() async {
    loadIng.value=true;
    update();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    var status = prefs.getString('Status') ?? '';
    var data = await Api.getMsgData(auth);
    var it = MessageModalClass.fromJson(data);
    if (it.error) {
      loadIng.value=false;
      update();
      Get.snackbar(
        "Info",
        "Some thing went wrong",
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    }else{
      loadIng.value=false;
      update();
      if(it.data.deliveryBoyMessage.isNotEmpty)
        {
          messageList.value=it.data.deliveryBoyMessage;
          print("Data"+it.data.deliveryBoyMessage.toString());
          rxEmpty.value=false;
          update();
        }else{
        rxEmpty.value=true;
        update();
      }

    }
  }

  void changeStatus(String id) async{
    loadIng.value = true;
    update();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    var data = await Api.messageUpdate(auth, id);
    var it = BaseApiResponse.fromJson(data);
    if (it.error) {
      loadIng.value = false;
      update();
      Get.snackbar(
        "Info",
        "Some thing went wrong",
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    } else {
      loadIng.value = false;
      update();
      getData();
      HomeController.to.onInit();
    }
  }
}
