import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Model/BaseApiResponse/BaseApiResponse.dart';
import 'package:medimallDelivery/Model/MessageModalClass/MessageModalClass.dart';
import 'package:medimallDelivery/Model/NotificationModel/NotificationModel.dart';
import 'package:medimallDelivery/Model/ProfileModal/ProfileModal.dart';
import 'package:medimallDelivery/Routes/routes.dart';
import 'package:medimallDelivery/Services/ApiService.dart';
import 'package:medimallDelivery/screens/deliveryBoy/Credits/creditPage.dart';
import 'package:medimallDelivery/screens/deliveryBoy/Profile/emergencyContact.dart';
import 'package:medimallDelivery/screens/deliveryBoy/cancelledOrders/cancelledOrders.dart';
import 'package:medimallDelivery/screens/deliveryBoy/deliveredOrders/deliveredOrders.dart';
import 'package:medimallDelivery/screens/deliveryBoy/helpAndSupport.dart';
import 'package:medimallDelivery/screens/deliveryBoy/newOrders/newOrdersPage.dart';
import 'package:medimallDelivery/screens/deliveryBoy/pendingDelivery/pendingDeliveryPage.dart';
import 'package:medimallDelivery/screens/deliveryBoy/returnProduct/pickupProduct.dart';
import 'package:medimallDelivery/screens/deliveryBoy/transactions/transactionsPage.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeController extends GetxController {
  static HomeController get to => Get.find();
  RxBool docOrClinic = false.obs;
  var name = "".obs;
  var id = "".obs;
  var credit = "".obs;
  var status = "".obs;
  var url = "".obs;
  var rxEmpty = false.obs;
  var loadIng = false.obs;
  var notCount="".obs;
  var megsCount="".obs;
  late BuildContext context;
  RefreshController refreshController = RefreshController(initialRefresh: false);

  RxList<String> gridlabel = [
    "New Orders",
    "Pending Delivery",
    "Delivered Orders",
    "Return Product",
    "Transactions",
    "Credits",
    "Cash Deposit",
    "Emergency Contact",
    "Help & Support",

  ].obs;
  RxList getxRoutes = [
    newOrders,
    pendingDelivery,
    deliverdOrder,
    pickupOrder,
    transactionOrder,
    creditRoute,
    cashDeposit,
    emergencyRoute,
    helpAndSupportRoute,

  ].obs;

  RxList<String> gridimage = [
    "assets/images/gridimages/neworders.png",
    "assets/images/gridimages/pendingdel.png",
    "assets/images/gridimages/deliorder.png",
    "assets/images/gridimages/rtproduct.png",
    "assets/images/gridimages/transactions.png",
    "assets/images/gridimages/credits.png",
    "assets/images/gridimages/cnclorder.png",
    "assets/images/gridimages/emcontact.png",
    "assets/images/gridimages/helpsupport.png",
  ].obs;

  @override
  void onInit() {
    getData();
    getNotification();
    checkOnline();
    getMessageCount();
    super.onInit();
  }

  @override
  void dispose() {
    HomeController.to.dispose();
    refreshController.dispose();
    super.dispose();
  }

  Future<void> toggleSwitch(bool value) async {
    await showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: Text('Are you sure want to change?'),
          actions: [
            ElevatedButton(
                onPressed: () async {
                  if (docOrClinic.value == false) {
                    docOrClinic.value = true;
                    status.value = "Online";
                    update();
                    SharedPreferences prefs = await SharedPreferences.getInstance();
                    prefs.setString("Status", "Online");
                    updateOnline();
                  } else {
                    docOrClinic.value = false;
                    status.value = "Offline";
                    update();
                    SharedPreferences prefs = await SharedPreferences.getInstance();
                    prefs.setString("Status", "Offline");
                    updateOnline();
                  }
                  Get.back();
                },
                child: Text('Yes')),
            TextButton(onPressed: () => Get.back(), child: Text('No'))
          ],
        ));

  }

  void getData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    var statuss = prefs.getString('Status') ?? '';
    var data = await Api.getProfile(auth, statuss);
    var it = ProfileModal.fromJson(data);
    if (!it.status) {
      Get.snackbar(
        "Info",
        "Some thing went wrong",
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    } else {
      name.value = it.data.fullName;
      id.value = it.data.deliveryBoyId;
      credit.value = it.data.credit;
      url.value = it.data.profilePic;
      if(it.data.status=="Offline")
        {
          docOrClinic.value = false;
          status.value = "Offline";
          update();
        }else{
        docOrClinic.value = true;
        status.value = "Online";
        update();
      }
      update();
    }
  }

  updateOnline() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    var abc = prefs.getString('Status') ?? '';
    var data = await Api.changeStatusAvailable(auth,abc);
    var it = BaseApiResponse.fromJson(data);
    if(!it.error)
      {
        getData();
      }
    //if(data.)
  }

  Future<void> checkOnline() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    var abc = prefs.getString('Status') ?? '';
    if (abc == "Online") {
      docOrClinic.value = true;
      status.value = "Online";
      update();
    } else if (abc == "Offline") {
      docOrClinic.value = false;
      status.value = "Offline";
      update();
    } else if (abc.isEmpty) {
      prefs.setString("Status", "Offline");
      status.value = "Offline";
      update();
     // updateOnline();
    }
  }

  void getNotification() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    var data = await Api.getDeliveryNot(auth);
    var it = NotificationModel.fromJson(data);
    if (it.error) {
      Get.snackbar(
        "Info",
        "Some thing went wrong",
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    } else {
      if (it.data.deliveryBoyNotification.isNotEmpty) {
        // notList.value = it.data.deliveryBoyNotification;
        notCount.value=it.data.notificationCount;
        update();
      }
    }
  }

  void getMessageCount() async{
    loadIng.value=true;
    update();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    var status = prefs.getString('Status') ?? '';
    var data = await Api.getMsgData(auth);
    var it = MessageModalClass.fromJson(data);
    if (it.error) {
      loadIng.value=false;
      update();
      Get.snackbar(
        "Info",
        "Some thing went wrong",
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    }else{
      loadIng.value=false;
      update();
      megsCount.value=it.data.messageCount;
      update();
    }
  }
}
