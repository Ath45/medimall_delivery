import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Model/NewOrderListingModel/NewOrderListingModel.dart';
import 'package:medimallDelivery/Model/PendingOrderModalClass/PendingOrderLimit.dart';
import 'package:medimallDelivery/Services/ApiService.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PendingDeliveryController extends GetxController {
  static PendingDeliveryController get to => Get.find();
  RxList<dynamic> pendingOrder = [].obs;
  var rxEmpty = false.obs;
  var loadIng = false.obs;

  @override
  void onInit() {
    getData();
    super.onInit();
  }

  void getData() async {
    loadIng.value=true;
    update();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    var status = prefs.getString('Status') ?? '';
    var data = await Api.getPendingOrders(auth, status);
    var it = PendingOrderLimit.fromJson(data);
    if (it.error) {
      loadIng.value=false;
      update();
      Get.snackbar(
        "Info",
        "Some thing went wrong",
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    } else {
      loadIng.value=false;
      update();
      if (it.data.deliveryPendingOrders.isNotEmpty) {
        pendingOrder.value = it.data.deliveryPendingOrders;
        rxEmpty.value = false;
        update();
      } else {
        pendingOrder.clear();
        rxEmpty.value = true;
        update();
      }
    }
  }
}
