import 'dart:ui';
import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:medimallDelivery/Routes/routes.dart';
import 'package:medimallDelivery/screens/deliveryBoy/homePage.dart';

class SplashController extends GetxController
    with SingleGetTickerProviderMixin {
  static SplashController get to => Get.find();
  late AnimationController controller;
  late Animation<Offset> animation;
  late AnimationController controller2;
  late Animation<Offset> animation2;
  late Animation<double> scaleAnimatoin;
  var opacity = 0.0.obs;

  late BuildContext context;

  @override
  void onInit() {
    super.onInit();
    controller =
        AnimationController(duration: const Duration(seconds: 3), vsync: this);
    animation = Tween<Offset>(
      begin: Offset.zero,
      end: const Offset(.1, 0.0),
    ).animate(CurvedAnimation(
      parent: controller,
      curve: Curves.slowMiddle,
    ));
    changeOpacity(opacity);
    controller2 = AnimationController(
      duration: const Duration(seconds: 3),
      vsync: this,
    )..forward();
    animation2 = Tween<Offset>(
      begin: const Offset(-0.5, 0.0),
      end: const Offset(1, 0.0),
    ).animate(CurvedAnimation(
      parent: controller2,
      curve: Curves.easeInCubic,
    ));
    _launchPage();
  }



  initController() async {}

  changeOpacity(opacity) {
    Future.delayed(Duration(seconds: 2), () {
      opacity.value = opacity.value == 0.0 ? 1.0 : 0.0;
    });
  }

  void contextData(context) {
    this.context = context;
  }

  _launchPage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final myString = prefs.getString('Auth') ?? '';

    await Future.delayed(const Duration(seconds:3 ), () {
      if (myString.isEmpty) {
        Get.offNamed(signupPage);
      } else {
        Get.offNamed(homeRoute);
      }
      });
  }

  @override
  void dispose() {
    super.dispose();
  }
}
