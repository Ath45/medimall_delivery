import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:medimallDelivery/Controller/HomeController/HomeConroller.dart';
import 'package:medimallDelivery/Model/BaseApiResponse/BaseApiResponse.dart';
import 'package:medimallDelivery/Model/ProfileModal/ProfileModal.dart';
import 'package:medimallDelivery/Services/ApiService.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../API.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';

class AccountController extends GetxController {
  static AccountController get to => Get.find();
  late XFile? rcBookImage = XFile("");
  late XFile? rcBookImageLst = XFile("");
  RxBool recBookImage = false.obs;
  var imageShow = "".obs;
  var name = "".obs;
  var email = "".obs;
  var mob = "".obs;
  var url="".obs;
  @override
  void onInit() {
    getData();
    super.onInit();
  }

  void getData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    var status = prefs.getString('Status') ?? '';
    print("data" + auth);
    var data = await Api.getProfile(auth, status);
    var it = ProfileModal.fromJson(data);
    if (!it.status) {
      Get.snackbar(
        "Info",
        "Some thing went wrong",
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    } else {
      name.value = it.data.fullName;
      email.value = it.data.email;
      mob.value = it.data.mobile;
      HomeController.to.onInit();
      if(it.data.profilePic.isNotEmpty)
        {
          url.value=it.data.profilePic;
          recBookImage.value = false;

        }
      update();
    }
  }

  void getImage(ImageSource gallery) async {
    rcBookImage = (await ImagePicker().pickImage(source: ImageSource.gallery));
    if (File(rcBookImage!.path).path.isNotEmpty) {
      uploadImage(rcBookImage!);
    }
    update();
  }

  Future<Null> uploadImage(XFile imageC) async {
    XFile? imagetoList = (imageC);
    if (imagetoList.path != "") {
      final mage = await (ImageCropper().cropImage(sourcePath: imagetoList.path));
      if (mage.toString() == "null") {
      } else {
        imagetoList = XFile(mage!.path);
        if (imagetoList.toString() == "null") {
          recBookImage.value = false;
        } else {
          rcBookImageLst = imagetoList;
          print("data user");
          recBookImage.value = true;
          imageShow.value = imagetoList.path.toString();
          postImage();
        }
        update();
      }
    }
  }

  void postImage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    Uri url = Uri.parse(baseUrl + account);
    var request = http.MultipartRequest("POST", url);
    var rcBook = await http.MultipartFile.fromPath(
      "image",
      File(rcBookImageLst!.path).path,
      filename: File(rcBookImageLst!.path).path.split("/").last,
      contentType: new MediaType('image', 'jpg'),
    );
    request.headers['Authorization'] = "$auth";
    request.files.add(rcBook);
    var response = await request.send();
    var responseString = await response.stream.bytesToString();
    print(responseString);
    var jsData = json.decode(responseString.toString());
    var it = BaseApiResponse.fromJson(jsData);
    if (it.error) {
      Get.snackbar(
        "Info",
        it.message,
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    } else {
      HomeController.to.onInit();
      getData();
    }
  }
}
