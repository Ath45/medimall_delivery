import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:medimallDelivery/Routes/routes.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:medimallDelivery/Model/LoginModel/LoginModel.dart';
import 'package:medimallDelivery/Services/ApiService.dart';
import 'package:medimallDelivery/screens/deliveryBoy/homePage.dart';
import 'package:medimallDelivery/widgets/CustomAlertDialog.dart';
class LoginController extends GetxController {
  static LoginController get to => Get.find();
  var username = "";
  var password = "";
  bool validate = false;
  GlobalKey<FormState> LoginformKey = GlobalKey<FormState>();
  void verifyCallOtp(BuildContext context) async {
    var data = await Api.loginService(username, password);
    print("DATA" + data.toString());
    var it = LoginModel.fromJson(data);
    if (it.error) {
      var dialog = CustomAlertDialog(
          title: "Info",
          message: it.message,
          onPostivePressed: () {
            Navigator.pop(context);
          },
          positiveBtnText: 'Ok',
          negativeBtnText: '');
      showDialog(
          context: context,
          builder: (BuildContext context) => dialog);
    } else {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString("Auth", "Bearer " +it.data.token);
      Get.offNamed(homeRoute);

      Get.snackbar(
        "Info",
        it.message,
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    }
  }
  String? validateEmail(String value) {
    if (!GetUtils.isEmail(value)) {
      return "Please provide valid Email";
    }
    return null;
  }
  String? validatePass(String value) {
    if (value.length < 6) {
      validate = false;
      return 'Please enter password more than 6 characters';
    } else if(value.isEmpty){
      validate = false;
      return 'This field required';

    }
    return null;
  }
  @override
  void dispose() {
    super.dispose();
    LoginController().dispose();
  }
}
