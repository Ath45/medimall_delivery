import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Controller/HomeController/HomeConroller.dart';
import 'package:medimallDelivery/Model/BaseApiResponse/BaseApiResponse.dart';
import 'package:medimallDelivery/Model/ProfileModal/ProfileModal.dart';
import 'package:medimallDelivery/Services/ApiService.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DrawerControllers extends GetxController {
  static DrawerControllers get to => Get.find();
  RxBool docOrClinic = false.obs;
  var name="".obs;
  var id="".obs;
  var status="".obs;
  var url="".obs;
  late BuildContext context;

  @override
  void onInit() {
    checkOnline();
    getData();
    super.onInit();
  }

  void checkOnline() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var abc = prefs.getString('Status') ?? '';
    if (abc == "Online") {
      docOrClinic.value = true;
      update();
    } else if (abc == "Offline") {
      docOrClinic.value = false;
      update();
    } else if (abc.isEmpty) {
      prefs.setString("Status", "Offline");
      update();
    }
  }

  void getData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    var statuss = prefs.getString('Status') ?? '';
    var data = await Api.getProfile(auth, statuss);
    var it = ProfileModal.fromJson(data);
    if(!it.status)
    {
      Get.snackbar(
        "Info",
        "Some thing went wrong",
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    }else{
      name.value=it.data.fullName;
      id.value=it.data.deliveryBoyId;
      url.value=it.data.profilePic;
      if(it.data.status=="Offline")
      {
        docOrClinic.value = false;
        status.value = "Offline";
        update();
      }else{
        docOrClinic.value = true;
        status.value = "Online";
        update();
      }
      update();
    }
  }
  Future<void> toggleSwitch(bool value) async {
    await showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: Text('Are you sure want to change?'),
          actions: [
            ElevatedButton(
                onPressed: () async {
                  if (docOrClinic.value == false) {
                    docOrClinic.value = true;
                    status.value= "Online";
                    update();
                    SharedPreferences prefs = await SharedPreferences.getInstance();
                    prefs.setString("Status", "Online");
                    updateOnline();
                    HomeController.to.checkOnline();
                  } else {
                    docOrClinic.value = false;
                    status.value= "Offline";
                    update();
                    SharedPreferences prefs = await SharedPreferences.getInstance();
                    prefs.setString("Status", "Offline");
                    // updateOnline();
                    HomeController.to.checkOnline();
                  }
                  Get.back();
                },
                child: Text('Yes')),
            TextButton(onPressed: () => Get.back(), child: Text('No'))
          ],
        ));

  }
  Future<void> updateOnline() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    var abc = prefs.getString('Status') ?? '';
    var data = await Api.changeStatusAvailable(auth,abc);
    var it = BaseApiResponse.fromJson(data);
    if(!it.error)
    {
      getData();
    }
  }

}