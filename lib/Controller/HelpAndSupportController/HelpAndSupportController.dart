import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Model/BaseApiResponse/BaseApiResponse.dart';
import 'package:medimallDelivery/Services/ApiService.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HelpAndSupportController extends GetxController {
  static HelpAndSupportController get to => Get.find();
  var currentValue = "Payment Related".obs;
  var messages = "";
  var loadIng=false.obs;
  late TextEditingController helpTextController;

  @override
  void onInit() {
    helpTextController = TextEditingController();
    super.onInit();
  }

  void onchangeValue(String value) {
    currentValue.value = value;
    update();
  }

  void postData() async {
    if(helpTextController.text.isNotEmpty)
      {
        loadIng.value=true;
        update();
        SharedPreferences prefs = await SharedPreferences.getInstance();
        var auth = prefs.getString('Auth') ?? '';
        var data = await Api.updateHelpAndSupport(
            auth, currentValue.value, helpTextController.text);
        var it = BaseApiResponse.fromJson(data);
        if (it.error) {
          loadIng.value=false;
          update();
          Get.snackbar(
            "Info",
            "Some thing went wrong",
            icon: Icon(Icons.person, color: Colors.white),
            snackPosition: SnackPosition.BOTTOM,
            backgroundColor: Colors.black,
            borderRadius: 20,
            margin: EdgeInsets.all(15),
            colorText: Colors.white,
            duration: Duration(seconds: 4),
            isDismissible: true,
            dismissDirection: DismissDirection.horizontal,
            forwardAnimationCurve: Curves.easeOutBack,
          );
        } else {
          loadIng.value=false;
          update();
          Get.back();
          Get.snackbar(
            "Info",
            "Delivery boy query added successfully",
            icon: Icon(Icons.person, color: Colors.white),
            snackPosition: SnackPosition.BOTTOM,
            backgroundColor: Colors.black,
            borderRadius: 20,
            margin: EdgeInsets.all(15),
            colorText: Colors.white,
            duration: Duration(seconds: 4),
            isDismissible: true,
            dismissDirection: DismissDirection.horizontal,
            forwardAnimationCurve: Curves.easeOutBack,
          );
        }
      }else{
      Get.snackbar(
        "Info",
        "Please Fill all data's.",
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    }

  }
}
