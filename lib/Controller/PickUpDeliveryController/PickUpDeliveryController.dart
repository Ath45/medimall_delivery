import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:http_parser/http_parser.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_picker_platform_interface/src/types/image_source.dart';
import 'package:medimallDelivery/Controller/PickupProductController/PickupProductController.dart';
import 'package:medimallDelivery/Model/BaseApiResponse/BaseApiResponse.dart';
import 'package:medimallDelivery/Model/PickUpDetailsModel/PickUpDetailsModel.dart';
import 'package:medimallDelivery/Services/ApiService.dart';
import 'package:medimallDelivery/util/constantsDC.dart';
import 'package:recase/recase.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:signature/signature.dart';

import '../../API.dart';

class PickUpDeliveryController extends GetxController {
  static PickUpDeliveryController get to => Get.find();
  RxList<dynamic> pickUpDelivery = [].obs;
  var orderId = "".obs;
  var returnId = "".obs;
  late BuildContext context;
  var name = "".obs;
  var productName = "".obs;
  var enableData = false.obs;
  var changeBtn = false.obs;
  var enableSubmitBtn = false.obs;
  var pickUpAddress = "".obs;
  var cancelledReson = "".obs;
  var prodctImg = "".obs;
  var enableProduct = false.obs;
  var prodctSignature = "".obs;
  var enableSignature = false.obs;
  var mob = "".obs;
  var status = "".obs;
  var sellerAddress = "".obs;
  var rtReson = "".obs;
  var comments = "".obs;
//  var productName="".obs;
  var currentValue = "Collected".obs;
  var abc = "Collected".obs;
  var cbc = "Submitted".obs;
  var ordderId = "".obs;
  late XFile? pickUpImg = XFile("");
  late XFile? signature = XFile("");
  var signatureFile;
  ByteData _img = ByteData(0);
  var pickUpImgBoo = false.obs;
  var loadIng = false.obs;
  final SignatureController controller = SignatureController(
    penStrokeWidth: 1,
    penColor: Colors.red,
    exportBackgroundColor: Colors.blue,
  );

  @override
  void onInit() {
    controller.addListener(() => print('Value changed'));
    super.onInit();
  }

  getData(String one) async {
    loadIng.value = true;
    update();
    ordderId.value = one;
    print("Datas" + one.toString());
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';

    var data1 = await Api.pickUpDetails(auth, one);
    print("data1" + data1!);
    var it = PickUpDetailsModel.fromJson(json.decode(data1));
    if (it.error) {
      loadIng.value = false;
      update();
      Get.snackbar(
        "Info",
        it.message,
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    } else {
      loadIng.value = false;
      update();
      pickUpDelivery.value = it.data.singleReturnOrders[0].products;
      orderId.value = it.data.singleReturnOrders[0].orderId;
      returnId.value = it.data.singleReturnOrders[0].returnId;
      name.value = it.data.singleReturnOrders[0].address.name;
      mob.value = it.data.singleReturnOrders[0].address.mobile;
      if (it.data.singleReturnOrders[0].status == "pickup pending") {
        enableData.value = true;
        update();
      } else if (it.data.singleReturnOrders[0].status == "accepted") {
        enableSubmitBtn.value = true;
        enableData.value = false;
        update();
      } else if (it.data.singleReturnOrders[0].status == "submitted") {
        enableSubmitBtn.value = true;
        update();
      } else if (it.data.singleReturnOrders[0].status == "collected") {
        changeBtn.value = true;
        currentValue.value = "Submitted";
        enableSubmitBtn.value = true;
        update();
      } else if (it.data.singleReturnOrders[0].status == "rejected") {
        Get.back();
        PickupProductController.to.getData();
      }
      if (it.data.singleReturnOrders[0].storeAddress.name != "null") {
        sellerAddress.value = it.data.singleReturnOrders[0].storeAddress.name;
      }
      if (it.data.singleReturnOrders[0].storeAddress.address != "null") {
        sellerAddress.value = sellerAddress.value +
            " ," +
            it.data.singleReturnOrders[0].storeAddress.address;
      }
      if (it.data.singleReturnOrders[0].storeAddress.pin != "null") {
        sellerAddress.value = sellerAddress.value +
            " ," +
            it.data.singleReturnOrders[0].storeAddress.pin;
      }
      if (it.data.singleReturnOrders[0].storeAddress.state != "null") {
        sellerAddress.value = sellerAddress.value +
            " ," +
            it.data.singleReturnOrders[0].storeAddress.state;
      }
      if (it.data.singleReturnOrders[0].storeAddress.country != "null") {
        sellerAddress.value = sellerAddress.value +
            " ," +
            it.data.singleReturnOrders[0].storeAddress.country;
      }
      if (it.data.singleReturnOrders[0].storeAddress.phone != "null") {
        sellerAddress.value = sellerAddress.value +
            " ," +
            it.data.singleReturnOrders[0].storeAddress.phone;
      }
      if (it.data.singleReturnOrders[0].address.house != "null") {
        pickUpAddress.value = it.data.singleReturnOrders[0].address.house;
      }
      if (it.data.singleReturnOrders[0].address.landmark != "null") {
        pickUpAddress.value = pickUpAddress.value +
            ", " +
            it.data.singleReturnOrders[0].address.landmark;
      }
      if (it.data.singleReturnOrders[0].address.street != "null") {
        pickUpAddress.value = pickUpAddress.value +
            ", " +
            it.data.singleReturnOrders[0].address.street;
      }
      if (it.data.singleReturnOrders[0].address.state != "null") {
        pickUpAddress.value = pickUpAddress.value +
            ", " +
            it.data.singleReturnOrders[0].address.state;
      }
      if (it.data.singleReturnOrders[0].address.type != "null") {
        pickUpAddress.value = pickUpAddress.value +
            ", " +
            it.data.singleReturnOrders[0].address.type;
      }
      cancelledReson.value = "";
      cancelledReson.value = it.data.singleReturnOrders[0].returnOrderReason;
      if (it.data.singleReturnOrders[0].Comments == "null") {
        comments.value = " ";
      } else {
        comments.value = it.data.singleReturnOrders[0].Comments;
      }
      if (it.data.singleReturnOrders[0].status == "null") {
        status.value = " ";
      } else {
        ReCase sample = new ReCase(it.data.singleReturnOrders[0].status);
        status.value = sample.sentenceCase;
      }
      if (it.data.singleReturnOrders[0].product_img.isEmpty) {
        enableProduct.value = false;
        update();
      } else {
        prodctImg.value = it.data.singleReturnOrders[0].product_img;
        enableProduct.value = true;
        update();
      }
      if (it.data.singleReturnOrders[0].signature == "null") {
        enableSignature.value = false;
        update();
      } else {
        prodctSignature.value = it.data.singleReturnOrders[0].signature;
        enableSignature.value = true;
        update();
      }
      update();
    }
  }

  void changeCn(String string) {
    currentValue.value = string;
    // changeStatus(string);
    update();
  }

  void changeStatus(String value) async {
    loadIng.value = true;
    update();
    var selected = "";
    if (value == "Submitted") {
      selected = "submitted";
    } else if (value == "Decline") {
      selected = "decline";
    } else {
      selected = value;
    }
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    var data = await Api.changeOrderStatus(auth, ordderId.value, selected);
    var it = BaseApiResponse.fromJson(data);
    if (it.error) {
      loadIng.value = false;
      update();
      Get.snackbar(
        "Info",
        it.message,
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    } else {
      loadIng.value = false;
      update();
      print("Working" + it.message);
      Get.back();
      PickupProductController.to.onInit();
      Get.snackbar(
        "Info",
        it.message,
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    }
  }

  Future<void> getImage(ImageSource imageSource) async {
    pickUpImg = (await ImagePicker().pickImage(source: imageSource));
    if (File(pickUpImg!.path).path.isNotEmpty) {
      pickUpImgBoo.value = true;
      update();
      Get.back();
    }
  }

  void clear() {
    controller.clear();
    update();
  }

  void poatImage(File data) {
    signatureFile = data;
    update();
    Get.snackbar(
      "Info",
      "Signature added successfully..",
      icon: Icon(Icons.person, color: Colors.white),
      snackPosition: SnackPosition.BOTTOM,
      backgroundColor: Colors.black,
      borderRadius: 20,
      margin: EdgeInsets.all(15),
      colorText: Colors.white,
      duration: Duration(seconds: 4),
      isDismissible: true,
      dismissDirection: DismissDirection.horizontal,
      forwardAnimationCurve: Curves.easeOutBack,
    );
  }

  void postData() async {
    loadIng.value = true;
    update();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    Uri url = Uri.parse(baseUrl + pickupController);
    var request = http.MultipartRequest("POST", url);
    http.MultipartFile prodImg = await http.MultipartFile.fromPath(
      "product_img",
      pickUpImg!.path,
      filename: pickUpImg!.path.split("/").last,
      contentType: new MediaType('image', 'jpg'),
    );
    http.MultipartFile signImg = await http.MultipartFile.fromPath(
      "signature",
      File(signatureFile!.path).path,
      filename: File(signatureFile!.path).path.split("/").last,
      contentType: new MediaType('image', 'jpg'),
    );
    request.headers['Authorization'] = "$auth";
    request.fields["orderObjectId"] = ordderId.value;
    request.fields["status"] = "collected";
    request.files.add(prodImg);
    request.files.add(signImg);
    StreamedResponse response = await request.send();
    String responseString = await response.stream.bytesToString();
    print(responseString);
    var jsData = json.decode(responseString.toString());
    var it = BaseApiResponse.fromJson(jsData);
    if (it.error) {
      loadIng.value = false;
      update();
      Get.snackbar(
        "Info",
        it.message,
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    } else {
      loadIng.value = false;
      update();
      // var dialog = CustomAlertDialog(
      //     title: "Info",
      //     message: it.message,
      //     onPostivePressed: () {
      //       Navigator.pop(context);
      //     },
      //     positiveBtnText: 'Ok',
      //     negativeBtnText: '');
      // showDialog(context: context, builder: (BuildContext context) => dialog);
      Get.defaultDialog(
          contentPadding: EdgeInsets.symmetric(horizontal: width * .06),
          title: "Info",
          content: Column(
            children: [
              Text(it.message),
              SizedBox(
                height: height * .07,
              ),
              GestureDetector(
                  onTap: () {
                    Get.back();
                    Get.back();
                    PickupProductController.to.getData();
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text("ok"),
                    ],
                  )),
            ],
          ),
          onWillPop: () async {
            Get.back();
            Get.back();
            return true;
          });
      getData(ordderId.value);
    }
  }

  void pickUpData() {
    if (currentValue.value == "Collected") {
      if (File(pickUpImg!.path).path.isEmpty) {
        Get.snackbar(
          "Info",
          "Product image is required.",
          icon: Icon(Icons.person, color: Colors.white),
          snackPosition: SnackPosition.BOTTOM,
          backgroundColor: Colors.black,
          borderRadius: 20,
          margin: EdgeInsets.all(15),
          colorText: Colors.white,
          duration: Duration(seconds: 4),
          isDismissible: true,
          dismissDirection: DismissDirection.horizontal,
          forwardAnimationCurve: Curves.easeOutBack,
        );
      } else if (signatureFile.toString() == "null") {
        Get.snackbar(
          "Info",
          "Signature field is required.",
          icon: Icon(Icons.person, color: Colors.white),
          snackPosition: SnackPosition.BOTTOM,
          backgroundColor: Colors.black,
          borderRadius: 20,
          margin: EdgeInsets.all(15),
          colorText: Colors.white,
          duration: Duration(seconds: 4),
          isDismissible: true,
          dismissDirection: DismissDirection.horizontal,
          forwardAnimationCurve: Curves.easeOutBack,
        );
      } else {
        postData();
      }
    } else {
      changeStatus(currentValue.value);
    }
  }
}
