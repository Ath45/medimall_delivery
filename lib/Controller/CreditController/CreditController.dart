import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Model/CreditDatedCredits/CreditDatedCreditsModel.dart';
import 'package:medimallDelivery/Model/CreditModelClass/CreditModelClass.dart';
import 'package:medimallDelivery/Model/CreditSearchModel/CreditSearchModel.dart';
import 'package:medimallDelivery/Services/ApiService.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CreditController extends GetxController {
  DateTime selectedDate = DateTime.now();
  var toFormattedDate = "".obs;
  var tos = "";
  var loadIng =false.obs;
  var rxEmpty =false.obs;
  var tosFUnction = "To".obs;
  var from = "";
  var fromFormattedDate = "".obs;
  var total="".obs;
  var totalIncome = "".obs;
  var forMated = "hfytfy".obs;
  var currentValue = "Paid to Admin".obs;
  late TextEditingController toDate;
  late TextEditingController FromDate;
  var valuePass = "".obs;
  var transactionData = [].obs;
  static CreditController get to => Get.find();
  var nXYear=2005;
  var nXMonth=12;
  var nXDay=12;
  @override
  void onInit() {
    toDate = TextEditingController();
    FromDate = TextEditingController();
    tos = selectedDate.toString();
    from = selectedDate.toString();
    fromFormattedDate.value =
    "${selectedDate.day}-${selectedDate.month}-${selectedDate.year}";
    toFormattedDate.value =
    "${selectedDate.day}-${selectedDate.month}-${selectedDate.year}";
    toDate.text =
    "${selectedDate.day}-${selectedDate.month}-${selectedDate.year}";
    FromDate.text =
    "${selectedDate.day}-${selectedDate.month}-${selectedDate.year}";
    forMated.value = fromFormattedDate.value;
    //getData();
    nXMonth=selectedDate.month;
    nXDay=selectedDate.day;
    forMated.value = fromFormattedDate.value;
    getDatanot();
    super.onInit();
  }
  void changeCn(String string) {
    currentValue.value = string;
    getData();
    update();
  }
  void fromChooseDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2005),
      lastDate: DateTime.now(),
    );
    if (picked != null && picked != selectedDate) selectedDate = picked;
    from = selectedDate.toString();
    fromFormattedDate.value =
    "${selectedDate.day}-${selectedDate.month}-${selectedDate.year}";
    nXYear=selectedDate.year;
    nXDay=selectedDate.day;
    nXMonth=selectedDate.month;
    FromDate.text = fromFormattedDate.value.toString();
    forMated.value = fromFormattedDate.value.toString();
    update();
    getDatanot();
  }

  void toChooseDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(nXYear,nXMonth,nXDay),
      lastDate: DateTime.now(),
    );
    if (picked != null && picked != selectedDate) selectedDate = picked;
    tos = selectedDate.toString();
    fromFormattedDate.value =
    "${selectedDate.day}-${selectedDate.month}-${selectedDate.year}";
    toDate.text = fromFormattedDate.value.toString();
    forMated.value = fromFormattedDate.value.toString();
    update();
    getDatanot();
  }
  void getData() async{
    loadIng.value=true;
    update();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    if (currentValue.value == "Paid to Admin") {
      valuePass.value = "paidToAdmin";
    } else {
      valuePass.value = "pendingToAdmin";
    }
    var data = await Api.getCreditList(auth, valuePass.value);
    var it = CreditModelClass.fromJson(data);
    if (it.error) {
      loadIng.value=false;
      update();
      Get.snackbar(
        "Info",
        "Some thing went wrong",
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    } else {
      loadIng.value=false;
      update();
      transactionData.value = it.data.deliveryBoyCredits;
      totalIncome.value=it.data.creditBalance.toString();
      update();
    }
  }

  void getSearch(String value) async{
    loadIng.value=true;
    update();
    if (currentValue.value == "Paid to Admin") {
      valuePass.value = "paidToAdmin";
    } else {
      valuePass.value = "pendingToAdmin";
    }
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    var data = await Api.searchCredit(auth, value, valuePass.value);
    var it = CreditSearchModel.fromJson(data);
    if (it.error) {
      loadIng.value=false;
      update();
      Get.snackbar(
        "Info",
        "Some thing went wrong",
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    } else {
      loadIng.value=false;
      update();
      if (it.data.deliveryBoyCredits.isEmpty) {
        rxEmpty.value=true;
        totalIncome.value="0";
        transactionData.clear();
        update();
      } else {
        transactionData.value = it.data.deliveryBoyCredits;
        totalIncome.value=it.data.creditBalance.toString();
        update();
      }
    }
  }

  Future<void> getDatanot() async {
    loadIng.value=true;
    update();
    if (currentValue.value == "Paid to Admin") {
      valuePass.value = "paidToAdmin";
    } else {
      valuePass.value = "pendingToAdmin";
    }
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    var data = await Api.creditDateWiseData(auth, from,tos, valuePass.value);
    var it = CreditDatedCreditsModel.fromJson(data);
    if (it.error) {
      loadIng.value=false;
      update();
      Get.snackbar(
        "Info",
        "Some thing went wrong",
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    } else {
      loadIng.value=false;
      update();
      if (it.data.deliveryBoyCredits.isEmpty) {
        rxEmpty.value=true;
        totalIncome.value="0";
        transactionData.clear();
      } else {
        rxEmpty.value=false;
        transactionData.value = it.data.deliveryBoyCredits;
        totalIncome.value=it.data.creditBalance.toString();
        update();
      }
    }
  }
}