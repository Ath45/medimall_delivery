import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Model/UserInfoModelClass/UserInfoModelClass.dart';
import 'package:medimallDelivery/Services/ApiService.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserInfoController extends GetxController {
  static UserInfoController get to => Get.find();
  var fullName = "".obs;
  var email = "".obs;
  var mobile = "".obs;
  var address = "".obs;
  var city = "".obs;
  late TextEditingController nameController;
  late TextEditingController emailController;
  late TextEditingController mobileController;
  late TextEditingController addressController;
  late TextEditingController cityController;
  @override
  void onInit() {
    nameController = TextEditingController();
    emailController = TextEditingController();
    mobileController = TextEditingController();
    addressController = TextEditingController();
    cityController = TextEditingController();
    super.onInit();
  }

  void getData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    var status = prefs.getString('Status') ?? '';
    var data = await Api.getProfileData(auth, status);
    var it = UserInfoModelClass.fromJson(data);
    if (!it.status) {
      Get.snackbar(
        "Info",
        "Some thing went wrong",
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    } else {
      print("lkll");
      fullName.value = it.data[0].userInfo.fullName;
      nameController.text = it.data[0].userInfo.fullName;
      email.value = it.data[0].userInfo.email;
      emailController.text = it.data[0].userInfo.email;
      mobile.value = it.data[0].userInfo.mobile;
      mobileController.text = it.data[0].userInfo.mobile;
      address.value = it.data[0].userInfo.address;
      addressController.text = it.data[0].userInfo.address;
      city.value = it.data[0].userInfo.city;
      cityController.text = it.data[0].userInfo.city;
      update();
    }
  }
}
