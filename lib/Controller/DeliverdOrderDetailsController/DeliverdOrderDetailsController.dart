import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Model/DeliverdOrderModel/DeliverdOrderModel.dart';
import 'package:medimallDelivery/Services/ApiService.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DeliverdOrderDetailsController extends GetxController {
  static DeliverdOrderDetailsController get to => Get.find();
  RxList<dynamic> deliOrders = [].obs;
  var name = "".obs;
  var idName = "".obs;
  var codOrder = "".obs;
  var addressDta = "".obs;
  var totalPay = "".obs;
  var deliveryCharge = "".obs;
  var picUpAddress = "".obs;
  var loadIng = false.obs;

  @override
  void onInit() {
    super.onInit();
  }

  void getData(String orderId) async {
    loadIng.value = true;
    update();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    print("Auth" + auth + " ," + orderId + " ");
    var status = prefs.getString('Status') ?? '';
    var data = await Api.getinnerOrders(auth, status, orderId);
    var it = DeliverdOrderModel.fromJson(data);
    if (it.error) {
      loadIng.value = false;
      update();
      Get.snackbar(
        "Info",
        "Some thing went wrong",
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    } else {
      loadIng.value = false;
      update();
      deliOrders.value = it.data.delivered[0].products;
      name.value = it.data.delivered[0].address.name;
      idName.value = it.data.delivered[0].address.mobile;
      codOrder.value = it.data.delivered[0].paymentType;
      addressDta.value = it.data.delivered[0].address.wholeAddress;
      totalPay.value = it.data.delivered[0].totalAmountToBePaid.toString();
      deliveryCharge.value = it.data.delivered[0].deliveryCharge.toString();
      if (it.data.delivered[0].storeAddress.name != "null") {
        picUpAddress.value = it.data.delivered[0].storeAddress.name + " ,";
      }
      if (it.data.delivered[0].storeAddress.address != "null") {
        picUpAddress.value = picUpAddress.value +
            it.data.delivered[0].storeAddress.address +
            " ,";
      }
      if (it.data.delivered[0].storeAddress.country != "null") {
        picUpAddress.value = picUpAddress.value +
            it.data.delivered[0].storeAddress.country +
            " ,";
      }
      if (it.data.delivered[0].storeAddress.state != "null") {
        picUpAddress.value =
            picUpAddress.value + it.data.delivered[0].storeAddress.state + " ,";
      }
      if (it.data.delivered[0].storeAddress.pin != "null") {
        picUpAddress.value =
            picUpAddress.value + it.data.delivered[0].storeAddress.pin;
      }
      update();
    }
  }
}
