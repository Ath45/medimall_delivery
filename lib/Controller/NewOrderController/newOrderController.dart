import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Model/NewOrderListingModel/NewOrderListingModel.dart';
import 'package:medimallDelivery/Services/ApiService.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NewOrderController extends GetxController {
  static NewOrderController get to => Get.find();
  RxList<dynamic> newOrder = [].obs;
 // RxList<dynamic> loadIng = [].obs;
  var rxEmpty = false.obs;
  var loadIng = false.obs;

  RxList newOrders = [
    {
      'name': 'John',
      'price': '434',
      'orderId': '64645475',
      'delivery': 'Cash on Delivery'
    },
    {
      'name': 'Amal Rahman',
      'price': '299',
      'orderId': '64645475',
      'delivery': 'Cash on Delivery'
    },
    {
      'name': 'Sijo',
      'price': '644',
      'orderId': '64645475',
      'delivery': 'Cash on Delivery'
    },
    {
      'name': 'Rohith',
      'price': '444',
      'orderId': '64645475',
      'delivery': 'Cash on Delivery'
    },
  ].obs;

  @override
  void onInit() {
    getData();
    super.onInit();
  }

  void getData() async {
    loadIng.value=true;
    update();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    var status = prefs.getString('Status') ?? '';
    var data = await Api.getOrders(auth, status);
    var it = NewOrderListingModel.fromJson(data);
    if (it.error) {
      loadIng.value=false;
      update();
      Get.snackbar(
        "Info",
        "Some thing went wrong",
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    }else{
      loadIng.value=false;
      update();
      if(it.data.pickupPendingOrders.isNotEmpty)
        {
          newOrder.value=it.data.pickupPendingOrders;
          rxEmpty.value = false;

          update();
        }else{
        newOrder.clear();
         rxEmpty.value = true;
        update();
      }

    }
  }
}
