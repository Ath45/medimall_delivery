import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Model/PickUpModelClass/PickUpModelClass.dart';
import 'package:medimallDelivery/Services/ApiService.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PickupProductController extends GetxController {
  static PickupProductController get to => Get.find();
  RxList<ReturnOrder> orderData = <ReturnOrder>[].obs;
  var productName = "".obs;
  var rxEmpty = false.obs;
  var loadIng = false.obs;
  var currentValue = "New orders".obs;

  @override
  void onInit() {
    super.onInit();
    getData();
    currentValue.value = "New orders";
    orderData.clear();
  }

  void getData() async {
    loadIng.value = true;
    update();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    var status = prefs.getString('Status') ?? '';
    var data = await Api.getReturnOrder(auth, status);
    var it = PickUpModelClass.fromJson(data);
    if (it.error) {
      loadIng.value = false;
      update();
      Get.snackbar(
        "Info",
        it.message,
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    } else {
      loadIng.value = false;
      update();
      if (it.data.returnOrders.isNotEmpty) {
        orderData.value = it.data.returnOrders;
        // orderData[0].products[0].uomValue;
        productName.value = it.data.returnOrders[0].products[0].productName;
        rxEmpty.value = false;
        update();
      } else {
        orderData.clear();
        rxEmpty.value = true;
        update();
      }
    }
  }

  Future<void> getSearch(String value) async {
    if (value.isNotEmpty) {
      loadIng.value = true;
      update();
      var statusCrt;
      if (currentValue.value == "New orders") {
        statusCrt = "NewOrders";
      }
      if (currentValue.value == "Collect Orders") {
        statusCrt = "collectedOrders";
      }
      if (currentValue.value == "Accepted Orders") {
        statusCrt = "AcceptedOrders";
      }
      if (currentValue.value == "All Orders") {
        statusCrt = "AllOrders";
      }
      SharedPreferences prefs = await SharedPreferences.getInstance();
      var auth = prefs.getString('Auth') ?? '';
      var data = await Api.searchReturnMyOrdersData(auth, value, statusCrt);
      var it = PickUpModelClass.fromJson(data);
      if (it.error) {
        loadIng.value = false;
        update();
        Get.snackbar(
          "Info",
          it.message,
          icon: Icon(Icons.person, color: Colors.white),
          snackPosition: SnackPosition.BOTTOM,
          backgroundColor: Colors.black,
          borderRadius: 20,
          margin: EdgeInsets.all(15),
          colorText: Colors.white,
          duration: Duration(seconds: 4),
          isDismissible: true,
          dismissDirection: DismissDirection.horizontal,
          forwardAnimationCurve: Curves.easeOutBack,
        );
      } else {
        loadIng.value = false;
        update();
        if (it.data.returnOrders.isNotEmpty) {
          orderData.value = it.data.returnOrders;
          productName.value = it.data.returnOrders[0].products[0].productName;
          rxEmpty.value = false;
          update();
        } else {
          rxEmpty.value = true;
          orderData.clear();
          update();
        }
      }
    } else {
      getData();
    }
  }

  void changeCn(String string) {
    currentValue.value = string;
    update();
    getDatasByInfo();
  }

  void getDatasByInfo() async {
    loadIng.value = true;
    update();
    var statusCrt;
    if (currentValue.value == "New orders") {
      statusCrt = "NewOrders";
    } else if (currentValue.value == "Collect Orders") {
      statusCrt = "collectedOrders";
    } else if (currentValue.value == "Accepted Orders") {
      statusCrt = "AcceptedOrders";
    } else if (currentValue.value == "All Orders") {
      statusCrt = "AllOrders";
    }
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    var data = await Api.returnMyOrdersData(auth, statusCrt);
    var it = PickUpModelClass.fromJson(data);

    if (it.error) {
      loadIng.value = false;
      update();
      Get.snackbar(
        "Info",
        it.message,
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    } else {
      loadIng.value = false;
      update();
      if (it.data.returnOrders.isNotEmpty) {
        orderData.value = it.data.returnOrders;
        productName.value = it.data.returnOrders[0].products[0].productName;
        rxEmpty.value = false;
        update();
      } else {
        rxEmpty.value = true;
        orderData.clear();
        update();
      }
    }
  }

  void stopLoading() {
    loadIng.value = false;
    update();
  }
}
