import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medimallDelivery/Model/CashModelClass/CashModelClass.dart';
import 'package:medimallDelivery/Services/ApiService.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CashDepositController extends GetxController {
  static CashDepositController get to => Get.find();
  var cashDeposit = [].obs;
  var dueAmt="".obs;
  var upiId="".obs;
  var bankNode="".obs;
  var nameN="".obs;
  var accountNum="".obs;
  var ifscCode="".obs;
  var loadIng=false.obs;
  var totalDueAmt="".obs;
  @override
  void onInit() {
    //getData();
    super.onInit();
  }

  void getData() async {
    loadIng.value=true;
    update();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    var data = await Api.getBankAccept(auth);
    var it = CashModelClass.fromJson(data);
    if (it.error) {
      loadIng.value=false;
      update();
      Get.snackbar(
        "Info",
        "Some thing went wrong",
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    } else {
      loadIng.value=false;
      update();
      if (it.data.bankDetails.isNotEmpty) {
        cashDeposit.value = it.data.bankDetails;
        dueAmt.value=it.data.bankDetails[0].v;
        upiId.value=it.data.bankDetails[0].upid;
        bankNode.value=it.data.bankDetails[0].bankName;
        nameN.value=it.data.bankDetails[0].accountHolderName;
        accountNum.value=it.data.bankDetails[0].accountNumber;
        ifscCode.value=it.data.bankDetails[0].ifscCode;
        totalDueAmt.value=it.data.totalDueAmt;
        update();
      }
    }
  }
}
