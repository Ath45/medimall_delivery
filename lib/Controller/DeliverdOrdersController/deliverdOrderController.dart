import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:medimallDelivery/Model/DeliveredModelClass/DeliveredModelClass.dart';
import 'package:medimallDelivery/Services/ApiService.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DeliverdOrderController extends GetxController {
  static DeliverdOrderController get to => Get.find();
  DateTime selectedDate = DateTime.now();
  late TextEditingController toDate;
  late TextEditingController FromDate;
  var toFormattedDate = "".obs;
  var tos = "";
  var from = "";
  var fromFormattedDate = "".obs;
  var abc=0;
  var totalIncome = "".obs;
  var forMated = "hfytfy".obs;
  RxList<dynamic> newOrdersData = [].obs;
  var loadIng=false.obs;
  var rxEmpty=false.obs;
  var nXYear=2005;
  var nXMonth=12;
  var nXDay=12;
  @override
  void onInit() {
    toDate = TextEditingController();
    FromDate = TextEditingController();
    tos = selectedDate.toString();
    from = selectedDate.toString();
    fromFormattedDate.value =
        "${selectedDate.day}-${selectedDate.month}-${selectedDate.year}";
    toFormattedDate.value =
        "${selectedDate.day}-${selectedDate.month}-${selectedDate.year}";
    toDate.text =
        "${selectedDate.day}-${selectedDate.month}-${selectedDate.year}";
    FromDate.text =
        "${selectedDate.day}-${selectedDate.month}-${selectedDate.year}";
    forMated.value = fromFormattedDate.value;
    abc=selectedDate.year;
    nXMonth=selectedDate.month;
    nXDay=selectedDate.day;
    forMated.value = fromFormattedDate.value;
    getDatanot();
    super.onInit();
  }

  void fromChooseDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2005),
      lastDate: DateTime.now(),
    );
    if (picked != null && picked != selectedDate) selectedDate = picked;
    from = selectedDate.toString();
    fromFormattedDate.value =
    "${selectedDate.day}-${selectedDate.month}-${selectedDate.year}";
    nXYear=selectedDate.year;
    nXDay=selectedDate.day;
    nXMonth=selectedDate.month;
    FromDate.text = fromFormattedDate.value.toString();
    forMated.value = fromFormattedDate.value.toString();
    update();
    getDatanot();
  }

  void toChooseDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(nXYear,nXMonth,nXDay),
      lastDate: DateTime.now(),
    );
    if (picked != null && picked != selectedDate) selectedDate = picked;
    tos = selectedDate.toString();
    fromFormattedDate.value =
    "${selectedDate.day}-${selectedDate.month}-${selectedDate.year}";
    toDate.text = fromFormattedDate.value.toString();
    forMated.value = fromFormattedDate.value.toString();
    update();
    getDatanot();
  }

  void getData() async {
    loadIng.value=true;
    update();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    var status = prefs.getString('Status') ?? '';
    var data = await Api.getDeliverd(auth, status);
    var it = DeliveredModelClass.fromJson(data);
    if (it.error) {
      loadIng.value=false;
      update();
      Get.snackbar(
        "Info",
        "Some thing went wrong",
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    } else {
      loadIng.value=false;
      update();
      newOrdersData.value = it.data.deliveredOrders;
      totalIncome.value=it.data.totalAmount.toString();
      update();
    }
  }

  void getDatanot() async {
    loadIng.value=true;
    update();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    var data = await Api.deliverDate(auth, from, tos);
    var it = DeliveredModelClass.fromJson(data);
    if (it.error) {
      loadIng.value=false;
      update();
      Get.snackbar(
        "Info",
        "Some thing went wrong",
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    } else {
      loadIng.value=false;
      update();
      if (it.data.deliveredOrders.isEmpty) {
        newOrdersData.clear();
        totalIncome.value="0";
        rxEmpty.value=true;
        update();
      } else {
        loadIng.value=false;
        update();
        newOrdersData.value = it.data.deliveredOrders;
        totalIncome.value=it.data.totalAmount.toString();
        rxEmpty.value=false;
        update();
      }
    }
  }

  Future<void> getSearch(String value) async {
    loadIng.value=true;
    update();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var auth = prefs.getString('Auth') ?? '';
    var data = await Api.searchData(auth, value);
    var it = DeliveredModelClass.fromJson(data);
    if (it.error) {
      loadIng.value=false;
      update();
      Get.snackbar(
        "Info",
        "Some thing went wrong",
        icon: Icon(Icons.person, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        dismissDirection: DismissDirection.horizontal,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    } else {
      loadIng.value=false;
      update();
      if (it.data.deliveredOrders.isEmpty) {
        newOrdersData.clear();
        totalIncome.value="0";
        rxEmpty.value=true;
        update();
      } else {
        newOrdersData.value = it.data.deliveredOrders;
        totalIncome.value=it.data.totalAmount.toString();
        rxEmpty.value=false;
        update();
      }
    }
  }
}
